var en = {
  landing_page : {
    home : "Home",
    features : "Features",
    overview : "Overview",
    about_us : "About Us"
  },
  code : "6-Digit Code",
  submit_answers : "Submit Answers",
  required_text : "Answer is required",
  reset_password_response : {
    success : "Your password has been reset successfully",
    unexistent_user : "The Username you entered does not exist",
    unexistent_code : "The 6-Digit Code you entered does not exist"
  },
  forgot_nickname : "Forgot Nickname",
  unexistent : "Sorry! No account matched the E-Mail or Phone Number you provided",
  find_username : "Find Username",
  find_nickname : "Find Nickname",
  no_stacks : "Log in to receive a reward for answering this questionnaire",
  unav_s : "SS - Unavailable Survey",
  unav_p : "SS - Unavailable Poll",
  not_target : "Note: Unfortunately, you do not meet the targeted preference and will therefore not receive any stacks/coins for answering this survey/poll",
  not_allowed : "Note: You have already answered this survey/poll and will not receive any stacks/coins for answering again",
  not_available : "Sorry, the survey/poll you are looking for is no longer avilable",
  forgot_password_error : {
    spam : function(time) {
      return "Please wait " + time + " seconds before you send another request";
    },
    prefered_method_needed : "A checkbox must be checked",
    success : "A 6-Digit Code has been sent to you"
  },
  unexistentProfile : function(username) {
    return "A profile with the username: "+username+" does not exist";
  },
  total_stacks : "Stacks Earned!",
  total_coins : "Coins Earned!",
  surveys_answered_ex : "Surveys Answered!",
  questions_answered : "Questions Answered!",
  rewards_claimed : "Rewards Claimed!",
  member_since : "Member Since",
  reset_password : "Reset Password",
  firstname : "First name",
  lastname : "Last name",
  fullname : "Full name",
  email_phone : "E-mail or Phone Number",
  email : "E-mail",
  phone_number : "Phone number",
  username : "Username",
  dob : "Date of Birth MM-DD-YYYY",
  register : "Register",
  registerError : {
    invalid_phone_format : "Phone Number MUST contain a Country Code and NO Letters",
    invalid_dob_format : "Date of Birth must follow the following format: Month-Day-Year i.e 01-01-2000",
    email_phone : "The email or phone number you entered is already in use",
    username : "Sorry! The username you entered is taken",
    nickname : "Sorry! The nickname you entered is taken",
    dob : "You must be 13 or older to register",
    bad_email : "The E-Mail you entered is not allowed"
  },
  agreement : {
    first : "By clicking 'REGISTER' you agree to our",
    terms : "Terms",
    and : "and our",
    cookies_policy : "Cookies Policy"
  },
  login : "Login",
  forgot_username : "Forgot Username",
  forgot_password : "Forgot Password",
  blank_field : "Do not leave any empty fields!",
  error : "There was an error processing the request!",
  loginError : {
    username : "Username not recognized",
    nickname : "Nickname not recognized",
    password : "Password does not match our records",
    locked : "This account is locked due to numerous failed login attempts"
  },
  submit : "Submit",
  new_password : "New Password",
  have_reset : "Have a reset code?",
  date_birth : "Date of Birth",
  reset_password : "Reset Password",
  need_account : "Need an account?",
  sign_up : "Sign Up",
  team_fullname : "Full name of Leader",
  team_email_phone : "Email or Phone # of Team or Leader",
  team_name : "Team Name",
  team_nickname : "Team Nickname",
  password : "Password",
  register_team : "Register Team",
  have_team : "Own a team?",
  have_account : "Have an account?",
  blank_field : "Please fill out all fields",
  log_in : "Log In",
  login : "Login",
  forgot : "Forgot",
  or : "or",
  wtrt : "Need a team?",
  con : "Create one now!",
  emoji_survey : "🔥 😎 Emojis Only for this Survey 🤪 🤣",
  key_needed_reply : "🔑 Enter the Key Needed to Reply 🔒",
  quiz_answer : function(points) {
    return "📝 This is a "+points+" point quiz! Good Luck! 📝"
  },
  times_up : "Time's Up!",
  points : function(points) {
    if (points === 1) return "("+points+" point)";
    return "("+points+" points)";
  },
  error_snackbar : "Trouble Loading! Reload!",
  error_verify_snackbar : "Server Cannot Verify Credentials!",
  success_snackbar : "Success!",
  wrong_key : "The key you entered is invalid or has been used"
};
