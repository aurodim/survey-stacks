//Bubble background Canvas

var canvas = document.getElementById('animateCanvas');

canvas.width = window.innerWidth;
canvas.height = (window.innerHeight * 0.93);

var c = canvas.getContext('2d');
window.addEventListener('resize', function() {
  c.clearRect(0, 0, innerWidth, innerHeight);

  canvas.width = window.innerWidth;
  canvas.height = (window.innerHeight * 0.93);

});

function Bubble(x, y, dx, dy, radius, fillColor) {
  this.x = x;
  this.y = y;
  this.dx = dx;
  this.dy = dy;
  this.radius = radius;
  this.minRadius = radius;
  this.fillColor = fillColor;
  this.draw = function() {

    c.beginPath();
    c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
    c.fillStyle = this.fillColor;
    c.fill();
  }

  this.update = function() {
    if (this.x + this.radius > innerWidth || this.x - this.radius < 0) {
      this.dx = -this.dx;
    }
    if (this.y + this.radius > innerHeight || this.y - this.radius < 0) {
      this.dy = -this.dy;
    }
    this.x += this.dx;
    this.y += this.dy;

    // interactivity

    this.draw();
  }
}

function Sparkle(x, y, radius, index) {
  this.x = x;
  this.y = y;
  this.radius = radius;
  this.index = index;
  this.alpha = 0;

  this.draw = function() {
    c.beginPath();
    c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
    c.fillStyle = "rgba(46,204,113,"+this.alpha+")";
    c.fill();
  }

  this.update = function() {
    if (this.radius < 8) {
      this.radius += 0.05;
      this.alpha += 0.002;

      this.draw();
    } else {
      this.radius = Math.random() * 8;
      this.x = Math.random() * (innerWidth - radius * 2) + radius;
      this.y = Math.random() * (innerHeight - radius * 2) + radius;
      this.alpha = 0;

      this.draw();
    }
  }
}

var circleArray = [];

function bubbleAnim(fillColor) {

  circleArray = [];

  for (var i = 0; i < 20; i++) {
    var radius = (Math.random() * 5) + 50;
    var x = Math.random() * (innerWidth - radius * 2) + radius;
    var y = Math.random() * (innerHeight - radius * 2) + radius;
    var dx = (Math.random() - 0.8) * 2.5;
    var dy = (Math.random() - 0.8) * 2.5;

    circleArray.push(new Bubble(x, y, dx, dy, radius, fillColor));
  }
}

function sparkleAnim(amount) {
  for (var i = 0; i < amount; i++) {
    this.radius = Math.random() * 8;
    var x = Math.random() * (innerWidth - radius * 2) + radius;
    var y = Math.random() * (innerHeight - radius * 2) + radius;

    circleArray.push(new Sparkle(x, y, radius, circleArray.length));
  }
}

function animate() {
  requestAnimationFrame(animate);
  c.clearRect(0, 0, innerWidth, innerHeight);

  for (var i = 0; i < circleArray.length; i++) {
    circleArray[i].update();
  }
}

window.onload = function() {
  var i = Math.floor(Math.random() * 10);
  if (i < 5) {
    bubbleAnim("#2ecc71");
  } else {
    sparkleAnim(60);
  }
}

function changeColor(fillColor) {
  circleArray = [];

  bubbleAnim(fillColor);
}
animate();
