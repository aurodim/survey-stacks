var alphabet=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y",
"Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",",","!",
"?",".","#","@","$","%","^","&","*","(",")","-","_","+","=","{","}","[","]","\\","/","|","`","~","<",">",":",";","'",
'"',"1","2","3","4","5","6","7","8","9","0"];function shift(r){for(var e=[],t=r;t<alphabet.length;t++)e.push(alphabet[t]);
for(t=0;t<r;t++)e.push(alphabet[t]);return e}function loopD(r,e){var t={};Array.isArray(e)&&(t=[]);for(var a in e){var o=decrypt(r,a);
if(Array.isArray(e)&&(o=a),"object"!=typeof e[a]){var n=decrypt(r,e[a]);t[o]=n}else t[o]=loopD(r,e[a])}return t}
function decrypt(r,e){if("number"==typeof e||"boolean"==typeof e)return e;var t=shift(r),a="";for(var o in e){var n=e[o],p=alphabet.indexOf(n);a+=-1==p?" ":t[p]}return a}
function decryptResponse(r){var e=r.__s__,t={};for(var a in r)"resultCode"!=a&&"resultDate"!=a?"__s__"!=a&&(ke=decrypt(e,a),"object"!=typeof r[a]?(ve=decrypt(e,r[a]),t[ke]=ve):t[ke]=loopD(e,r[a])):t[a]=r[a];return t}

app.service('language', function($route, $cookies, $window) {
  var self = this;
  self.langFile = $cookies.get("lang");
  self.langFull = "";
  self.langDir = "ltr";
  var userLang = $cookies.get("lang");
  $.ajaxSetup({
    cache: true
  });

  switch (userLang) {
    case "en":
      self.langFull = "English";
      $.getScript('/javascript/en_translations.js', function() {
         self.dictionary = en;
      });
      break;
    case "es":
      self.langFull = "Spanish";
      $.getScript('/javascript/es_translations.js', function() {
         self.dictionary = es;
      });
      break;
    default:
      $.getScript('/javascript/en_translations.js', function() {
         self.dictionary = en;
      });
      self.langFile = "en";
      self.langFull = "English";
  };
  $.ajaxSetup({
    cache: false
  });

  self.dictionary = "";
  self.globals = globals;
});

app.service('metadata', function($route, $cookies, $window, language) {
  var self = this;
  self.title = "SurveyStacks";
  self.description = language.dictionary.description_metadata;
});

app.service('socketAPI', function ($cookies, $timeout, $http, alertsAPI, language) {
  var self = this;
  self.socket;
  self.desktop_notifications = $cookies.get('dnotif');
  self.notifications = {
    "unread" : 0,
    "list" : []
  };
  self.$scope;
  self.digestCycle = function($scope) {
    self.$scope = $scope;
  };
  self.desktopNotification = function(notification) {

    if (self.desktop_notifications == "true" && Notification.permission === "granted") {
        var notification = new Notification(notification.title, {
          lang : language.langFile,
          dir : language.langDir,
          tag : notification.token,
          icon : "https://res.cloudinary.com/survey-stacks/image/upload/c_scale,w_512/v1510847720/icon_r.png",
          sound : "notificationSound",
          data : {
            type : notification.type,
            link : notification.url
          },
          body : notification.content

        });
        $(document).prop('title', language.dictionary.new_notification);

        $timeout(function() {
          notification.close();
        }, 5000);

        notification.onclick = function() {
          if (notification.data.link != "") {
            $window.location = notification.data.link;
          }
        };

        notification.onclose = function() {
          $(document).prop('title', 'SurveyStacks');
        };
    }
  };
  self.init = function(socket, notifications) {
    self.socket = io();
    self.notifications = notifications;

    self.socket.on(socket, function(todo) {
      if (todo.type === "cn") {
        $http({
            method : "GET",
            url : "/api/notifications/e1760a7b88184d0517f11c900342e1c4fba47fdd",
            headers: {'Content-Type': 'application/json'},
            params : {
              reference_code : "TuesdayEveningPizza",
              key : "992018",
              lang : language.langFile,
              webApp : true
            }
        }).then(function success(res) {
            var response = decryptResponse(res.data);
            self.notifications.list.push(response.notification);
            self.notifications.unread += 1;
            self.desktopNotification(response.notification);
        }, function error(res) {
          if (res.status == 500) {
            alertsAPI.show(language.dictionary.error_snackbar);
          } else {
            alertsAPI.show(language.dictionary.error_verify_snackbar);
          }
        });
      } else if (todo.type === "mar") {
        self.notifications.unread = 0;
        self.$scope.$apply();
      } else if (todo.type === "es") {
        window.location = "/logout";
      }
    });
  };
});

app.service('userAPI', function($http, $cookies, $rootScope, $window, $timeout, $interval, language, alertsAPI, socketAPI) {
  var self = this;
  var date = new Date();
  var dateInt = new Date(date).getTime();
  self.username = $cookies.get("un");
  self.setup_completed = $cookies.get("sd");
  self.profile_picture_url = "";
  self.stacks_amount = 0;
  self.coins_amount = 0;
  self.payment_completed = false;
  self.socket = "";
  self.listener = socketAPI;
  self.csrf = "";
  self.request_pending = false;
  self.wishlist_info = [];
  self.sessionData = function() {
    $http({
        method : "GET",
        url : "/api/user/b0058c9ee07d3b5a49",
        headers: {'Content-Type': 'application/json'},
        params : {
          reference_code : "WednesdayNightClearSky",
          key : "990178",
          lang : language.langFile,
          webApp : true
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      var account_data = response.accountInfo;
      var wishlistInfo = response.wishlistInfo;
      self.profile_picture_url = response.accountInfo.profile_picture_url;
      self.stacks_amount = account_data.stacks_amount;
      self.coins_amount = account_data.coins_amount;
      self.socket = account_data.sat;
      self.payment_completed = account_data.payment_saved;
      self.wishlist_info = wishlistInfo;
      self.csrf = response.csrf;
      self.listener.init(self.socket, response.notificationsInfo);
    }, function error(res) {
      $window.location = "/500"
    });
  };
  self.sessionData();
});

app.service("searchAPI", function($http, $cookies, $timeout, $q, $window, $location, $rootScope, language, userAPI, alertsAPI) {
  var self = this;
  self.usersSearchData = [];
  self.companiesSearchData = [];
  self.surveysSearchData = [];
  self.rewardsSearchData = [];
  self.searchQuery = "";
  self.searchConstraint = "";
  self.loading = false;

  self.validateSearch = function() {
    self.searchConstraint = self.searchQuery[0];

    if (self.searchQuery == "") {
      self.usersSearchData = [];
      self.companiesSearchData = [];
      self.surveysSearchData = [];
      self.rewardsSearchData = [];
      self.loading = false;
      return;
    }

    if (self.searchConstraint != "$" && self.searchConstraint != "*" && self.searchConstraint != "@") {
      self.searchConstraint = "";
    }

    self.performSearch();
  };

  self.performSearch = function() {
    self.loading = true;

    $http({
      method : "GET",
      url : "/api/user/f02d69d2b65a3a17f4",
      headers : {'Content-Type' : "application/json"},
      params : {
         reference_code : "WednesdayNightEmptySky",
         key : "990128",
         webApp : true,
         lang : language.langFile,
         searchQuery : self.searchQuery,
         searchConstraint : self.searchConstraint
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      var searchData = response.searchData;
      self.loading = false;
      self.usersSearchData = searchData.users;
      self.companiesSearchData = searchData.companies;
      self.surveysSearchData = searchData.surveys;
      self.rewardsSearchData = searchData.rewards;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("alertsAPI", function($timeout, language) {
  var self = this;
  self.alert = "";
  self.show = function(text) {
    self.alert = text;
    $("#snackbar").fadeIn(400);

    $timeout(function() {
      $("#snackbar").fadeOut(400);
      self.alert = "";
    }, 2000);
  };
});

app.service("surveyAPI", function($timeout, $window, $http, language, userAPI, alertsAPI) {
  var self = this;

  self.surveysColumnOne = [
    {

    }
  ];
  self.surveysColumnTwo = [];
  self.surveysColumnThree = [];
  self.indexes = {
    target : 0,
    non : 0
  };
  self.loading = true;
  self.request_pending = false;
  self.getSurveyData = function() {
    self.surveysColumnOne = [];
    self.surveysColumnTwo = [];
    self.surveysColumnThree = [];
    self.loading = true;
    $http({
      method : "GET",
      url : "/api/user/9e476d177153b44685",
      headers : {'Content-Type' : "application/json"},
      params : {
        reference_code : "MondayEveningBeautifulSky",
        key : "991212",
        lang : language.langFile,
        webApp : true,
        "indexes[target]" : self.indexes.target,
        "indexes[non_target]" : self.indexes.non
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      var currentColumn = self.surveysColumnOne;
      for (var i = 0; i < response.surveysData.length; i++) {
        currentColumn.push(response.surveysData[i]);
        if (currentColumn == self.surveysColumnOne) {
          currentColumn = self.surveysColumnTwo;
        } else if (currentColumn == self.surveysColumnTwo) {
          currentColumn = self.surveysColumnThree;
        } else if (currentColumn == self.surveysColumnThree) {
          currentColumn = self.surveysColumnOne;
        }
      }
      self.loading = false;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.bookmark = function($event, $iop, $index) {
    $event.stopPropagation();
    var array;
    switch ($iop) {
      case 0:
        array = self.surveysColumnOne;
        break;
      case 1:
        array = self.surveysColumnTwo;
        break;
      case 2:
        array = self.surveysColumnThree;
      default:
        return false;
    }
    $http({
        method : "POST",
        url : "/api/user/bookmark",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "c0905eeacea2815d5a7877f4fe35a38c94c81e46",
          token : array[$index].token,
          type : "survey",
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      self.request_pending = false;
      array[$index].bookmarked = response.bookmarked;
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("analyticsAPI", function($timeout, $http, language, userAPI, alertsAPI) {
  var self = this;
  self.loading = true;
  self.analyticsData = {
    surveys : [],
    polls : []
  };
  self.getAnalyticsData = function() {
    self.loading = true;
    $http({
      method : "GET",
      url : "/api/user/41562427f26404d237",
      headers : {'Content-Type' : "application/json"},
      params : {
        reference_code : "TuesdayEveningWindy",
        key : "991205",
        lang : language.langFile,
        webApp : true
      }
    }).then(function success(res) {
      var response = res.data;
      self.analyticsData = response.analytics;
      self.loading = false;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service('bookmarksAPI', function($window, $location, $http, $timeout, userAPI, alertsAPI, language) {
  var self = this;
  self.surveyBookmarkData = [];
  self.pollBookmarkData = [];
  self.loading = true;
  self.request_pending = false;
  self.gatherBookmarks = function(){
    self.loading = true;
    $http({
      method : "GET",
      url : "/api/user/d634cb4a34e7746fdc",
      headers : {'Content-Type' : "application/json"},
      params : {
        reference_code : "SaturdayAfternoonCloudy",
        key : "021299",
        lang : language.langFile,
        webApp : true,
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      console.log(response);
      self.surveyBookmarkData = response.surveys_bookmarks;
      self.pollBookmarkData = response.polls_bookmarks;
      self.loading = false;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.unbookmark = function($event, type, $index) {
    $event.stopPropagation();
    var array;
    switch (type) {
      case "survey":
        array = self.surveyBookmarkData;
        break;
      case "poll":
        array = self.pollBookmarkData;
        break;
      default:
        return false;
    }
    $http({
        method : "POST",
        url : "/api/user/bookmark",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "c0905eeacea2815d5a7877f4fe35a38c94c81e46",
          token : array[$index].token,
          type : type,
          webApp : true,
          lang : language.langFile,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      self.request_pending = false;
      if (!response.bookmarked) {
        array[$index].bookmarked = false;
        $timeout(function() {
          array.splice($index, 1);
        }, 200);
      }
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("surveyManagerAPI", function($timeout, $http, language, userAPI, alertsAPI) {
  var self = this;

  self.surveyData = [];
  self.pollData = [];
  self.loading = true;
  self.gatherData = function() {
    self.loading = true;
    $http({
          method : "GET",
          url : "/api/user/4bc78765e700fc4c2b",
          headers: {'Content-Type': 'application/json'},
          params : {
            reference_code : "WednesdayNightThanksgivingEve",
            key : "954262",
            lang : language.langFile,
            webApp : true
          }
      }).then(function success(res) {
          var response = decryptResponse(res.data);
          self.surveyData = response.surveys;
          self.pollData = response.polls;
          self.loading = false;
      }, function error(res) {
          if (res.status == 500) {
            alertsAPI.show(language.dictionary.error_snackbar);
          } else {
            alertsAPI.show(language.dictionary.error_verify_snackbar);
          }
      });
  };
});

app.service("pollsAPI", function($timeout, $window, $http, language, userAPI, alertsAPI) {
  var self = this;

  self.pollColumnOne = [];
  self.pollColumnTwo = [];
  self.pollColumnThree = [];
  self.loading = true;
  self.getPollData = function() {
    self.loading = true;
    self.pollColumnOne = [];
    self.pollColumnTwo = [];
    self.pollColumnThree = [];
    $http({
      method : "GET",
      url : "/api/user/1c18d221496174a6aa",
      headers : {'Content-Type' : "application/json"},
      params : {
        reference_code : "SundayEveningBTS",
        key : "991126",
        lang : language.langFile,
        webApp : true,
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      var currentColumn = self.pollColumnOne;
      for (var i = 0; i < response.polls.length; i++) {
        currentColumn.push(response.polls[i]);
        if (currentColumn == self.pollColumnOne) {
          currentColumn = self.pollColumnTwo;
        } else if (currentColumn == self.pollColumnTwo) {
          currentColumn =  self.pollColumnThree;
        } else if (currentColumn == self.pollColumnThree) {
          currentColumn = self.pollColumnOne;
        }
      }
      self.loading = false;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.bookmark = function($event, $iop, $index) {
    if (self.request_pending) return false;
    $event.stopPropagation();
    var column;
    if ($iop == 0) {
      column = self.pollColumnOne;
    } else if ($iop == 1) {
      column = self.pollColumnTwo;
    } else if ($iop == 2) {
      column = self.pollColumnThree;
    }
    $http({
        method : "POST",
        url : "/api/user/bookmark",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "c0905eeacea2815d5a7877f4fe35a38c94c81e46",
          token : column[$index].token,
          type : "poll",
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      self.request_pending = false;
      column[$index].bookmarked = response.bookmarked;
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("profileAPI", function($http, $rootScope, $window, $timeout, $interval, language, userAPI, alertsAPI) {
  var self = this;
  self.user = userAPI;
  self.profile_picture_url = "";
  self.profile_theme_url = "";
  self.motto = "";
  self.since = "";
  self.achievements = {
    total_stacks : 0,
    total_coins : 0,
    total_surveys_answered : 0,
    total_questions_answered : 0,
    total_rewards_claimed : 0,
  };

  self.contactInfo = {
    email : "",
    phone_number : "",
    website : "",
    facebook : "",
    instagram : ""
  };

  self.newProfilePictureData = {};
  self.request_pending = false;
  self.loading = true;

  self.profileData = function() {
    self.loading = true;
    $http({
        method : "GET",
        url : "/api/user/d61beb4fc45900f94d",
        headers: {'Content-Type': 'application/json'},
        params : {
          reference_code : "ThursdayNightBrightMoon",
          key : "991756",
          lang : language.langFile,
          webApp : true
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      var accountInfo = response.accountInfo;
      var achievementsInfo = response.achievementsInfo;
      var contactInfo = response.contactInfo;

      self.profile_theme_url = accountInfo.profile_theme_url;
      self.profile_picture_url = accountInfo.profile_picture_url;
      self.motto = accountInfo.motto;
      self.since = accountInfo.since;
      self.profile_theme_url = accountInfo.profile_theme_url;
      self.achievements.total_stacks = achievementsInfo.stacks_earned;
      self.achievements.total_coins = achievementsInfo.coins_earned;
      self.achievements.total_surveys_answered = achievementsInfo.surveys_answered;
      self.achievements.total_questions_answered = achievementsInfo.questions_answered;
      self.achievements.total_rewards_claimed = achievementsInfo.rewards_claimed;
      self.contactInfo.email = contactInfo.email;
      self.contactInfo.phone_number = contactInfo.phone_number;
      self.contactInfo.website = contactInfo.website;
      self.contactInfo.facebook = contactInfo.facebook;
      self.contactInfo.instagram = contactInfo.instagram;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };

  self.updateProfilePicture = function() {
    var fd = new FormData();
    fd.append('AUTH_TOKEN', '29cf0a5f4e05b9cbcb7e3b08da0109d576b8d33f');
    fd.append('profilePicture', self.newProfilePictureData);
    fd.append('webApp', true);
    fd.append('_csrf', userAPI.csrf);
    $http({
      method : "POST",
      url : "/api/user/settings/profile_picture",
      headers : {'Content-Type' : undefined},
      transformRequest: angular.identity,
      data : fd
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      var date = new Date();
      var dateInt = new Date(date).getTime();
      self.profile_picture_url = response.imageURL;
      self.user.profile_picture_url = response.imageURL +"?"+dateInt;
      self.newProfilePictureData = {};
      self.request_pending = false;
      $("#ppa").html(language.dictionary.change_profile_picture);
      $("#ppa").fadeOut(200);
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.updateMotto = function() {
    if (!$("#p_motto").hasClass('ng-dirty')) {
      return false;
    }
    self.request_pending = true;
    $http({
        method : "POST",
        url : "/api/user/settings/motto",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "n0102esa2el2l1ed9a78a7m4fe35a38c94c81e46",
          motto : self.motto,
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      $("#p_motto").removeClass('ng-dirty');
      self.request_pending = false;
    }, function error(res) {
      self.motto = "";
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("redeemAPI", function($http, $cookies, $timeout, userAPI, alertsAPI, language) {
  var self = this;
  self.rewardsAfford = [];
  self.rewardsOther = [];
  self.loading = true;
  self.request_pending = false;
  self.getRewardsData = function() {
    self.rewardsAfford = [];
    self.rewardsOther = [];
    self.loading = true;
    $http({
          method : "GET",
          url : "/api/user/0357e6e320980d2d8a",
          headers: {'Content-Type': 'application/json'},
          params : {
            reference_code : "FridayEvening1124",
            key : "991124",
            lang : language.langFile,
            webApp : true
          }
      }).then(function success(res) {
          var response = decryptResponse(res.data);
          self.rewardsAfford = response.rewards_afford;
          self.rewardsOther = response.rewards_other;

          self.loading = false;
      }, function error(res) {
          if (res.status == 500) {
            alertsAPI.show(language.dictionary.error_snackbar);
          } else {
            alertsAPI.show(language.dictionary.error_verify_snackbar);
          }
      });
  };
  self.claimReward = function(index) {
    self.request_pending = true;
    $http({
        method : "POST",
        url : "/api/user/redeem",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "318f00d5aaa397e312d4e88773128b25",
          reward_token : self.rewardsAfford[index].token,
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == "1") {
        self.request_pending = false;
        self.loading = true;
        self.getRewardsData();
        userAPI.stacks_amount = response.stacks;
        self.rewardsAfford[index].progress = 0;
      }
    }, function error(res) {
      self.request_pending = false;
      self.rewardsAfford[index].progress = 0;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.wishlist = function(i, index) {
    if(self.request_pending) return;
    self.request_pending = true;
    var reward;
    if (i == 0) {
      reward = self.rewardsAfford[index];
    } else {
      reward = self.rewardsOther[index];
    }
    $http({
        method : "POST",
        url : "/api/user/wishlist",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "6da2c39b7d41fd5bb5d545173db09988df3e9364",
          reward_token : reward.token,
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == "1") {
        self.request_pending = false;
        reward.wishlisted = !reward.wishlisted;
        if (reward.wishlisted) {
          userAPI.wishlist_info.push(response.item);
        } else {
          userAPI.wishlist_info.forEach(function(wish, i) {
            if (wish.token == reward.token) {
              userAPI.wishlist_info.splice(i, 1);
              return;
            }
          });
        }
      }
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service('settingsAPI', function($timeout, $cookies, $rootScope, $window, $http, language, userAPI, alertsAPI) {
  var self = this;

  self.ep = "";
  self.nu = userAPI.username;
  self.phone_number_ci = "";
  self.email_ci = "";
  self.website = "";
  self.facebook = "";
  self.instagram = "";
  self.email_message_notifications = false;
  self.match_preferences_notifications = false;
  self.about_blank = false;
  self.sex = "";
  self.ethnicity = [];
  self.lang = [];
  self.education = "";
  self.relationship = "";
  self.employment = "";
  self.religion = "";
  self.street_address = "";
  self.apt_number = "";
  self.city = "";
  self.state = "";
  self.zipcode = "";
  self.country = "";
  self.payment_blank = false;
  self.labs_membership = false;
  self.lp = 0;
  self.labs_expire = "";
  self.card_type = "";
  self.card_name = "";
  self.card_ending_digits = "";
  self.card_expiration_date = "";
  self.preferences_blank = false;
  self.sex_preference = false;
  self.translated_preference = false;
  self.category_preferences = [];
  self.company_preferences = [];
  self.request_pending = false;
  self.current_password = "";
  self.new_password = "";
  self.loading = true;

  self.getSettingsData = function() {
    self.loading = true;
    $http({
          method : "GET",
          url : "/api/user/75076262024E2A793D",
          headers: {'Content-Type': 'application/json'},
          params : {
            reference_code : "TuesdayEveningSunnyClouds",
            key : "991223",
            lang : language.langFile,
            webApp : true
          }
      }).then(function success(res) {
          var response = decryptResponse(res.data);
          var accountInfo = response.accountInfo,
              contactInfo = response.contactInfo,
              notificationsInfo = response.notificationsInfo,
              aboutInfo = response.aboutInfo,
              paymentInfo = response.paymentInfo,
              surveyInfo = response.surveyInfo,
              preferencesInfo = response.preferencesInfo;

          self.ep = accountInfo.ep;
          self.phone_number_ci = contactInfo.phone_number;
          self.email_ci = contactInfo.email;
          self.website = contactInfo.website;
          self.facebook = contactInfo.facebook;
          self.instagram = contactInfo.instagram;
          self.email_message_notifications = notificationsInfo.email_message_notifications;
          self.match_preferences_notifications = notificationsInfo.match_preferences_notifications;
          self.about_blank = aboutInfo.blank;
          self.sex = aboutInfo.sex;
          self.ethnicity = aboutInfo.ethnicity;
          self.lang = aboutInfo.lang;
          self.education = aboutInfo.education;
          self.relationship = aboutInfo.relationship;
          self.employment = aboutInfo.employment;
          self.religion = aboutInfo.religion;
          self.payment_blank = paymentInfo.blank;
          self.labs_membership = paymentInfo.labs_membership;
          self.labs_expire = paymentInfo.labs_expire;
          self.lp = paymentInfo.labs_price;
          self.card_type = paymentInfo.card_type;
          self.card_name = paymentInfo.card_name;
          self.card_ending_digits = paymentInfo.card_ending_digits;
          self.card_expiration_date = paymentInfo.card_expiration_date;
          self.street_address = paymentInfo.street_address;
          self.apt_number = paymentInfo.apt_number;
          self.city = paymentInfo.city;
          self.state = paymentInfo.state;
          self.zipcode = paymentInfo.zip_code;
          self.country = paymentInfo.country;
          self.preferences_blank = preferencesInfo.blank;
          self.sex_preference = preferencesInfo.sex;
          self.translated_preference = preferencesInfo.translated;
          self.category_preferences = preferencesInfo.categories;
          self.company_preferences = preferencesInfo.companies;
          self.loading = false;
      }, function error(res) {
          if (res.status == 500) {
            alertsAPI.show(language.dictionary.error_snackbar);
          } else {
            alertsAPI.show(language.dictionary.error_verify_snackbar);
          }
      });
  };
  self.updateAccountInfo = function() {
    $http({
        method : "POST",
        url : "/api/user/settings/account",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "684f09b83c13cfc2249d637db86e0bea0e007af1",
          ep : self.ep,
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == "1") {
        self.request_pending = false;
      }
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.updateContactInfo = function() {
    $http({
        method : "POST",
        url : "/api/user/settings/contact",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "7da38a622ab17c090389d806efa1c785a2243977",
          email : self.email_ci,
          phone_number : self.phone_number_ci,
          website : self.website,
          facebook : self.facebook,
          instagram : self.instagram,
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == "1") {
        self.request_pending = false;
      }
    }, function error(response, status) {
      if (status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.updateLabsMem = function() {
    $http({
        method : "POST",
        url : "/api/user/settings/labs",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "39b95a7aea064635f4f6fec22d45bddfe05be85d",
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == "1") {
        self.request_pending = false;
        self.labs_membership = response.labs_membership;
        self.labs_expire = response.labs_expire;
        self.lp = response.labs_price;
      }
    }, function error(response, status) {
      if (status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.updatePassword = function() {
    $http({
        method : "POST",
        url : "/api/user/settings/password",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "a6e86db25db01eb61f9c8b4950b3914ee2f33e87",
          current_password : self.current_password,
          new_password : self.new_password,
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == "1") {
        self.request_pending = false;
      } else if (response.resultCode == "-1") {
        self.request_pending = false;
        $("#current_password_pi").css({border: "1px solid red"});
      }
    }, function error(response, status) {
      if (status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.updateNotifications = function() {
    $http({
        method : "POST",
        url : "/api/user/settings/notifications",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "d4b015c242d095eb62b0acabfac0a8f88d7c9cef",
          email_message_notifications : self.email_message_notifications,
          match_preferences_notifications : self.match_preferences_notifications,
          lang : language.langFile,
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == "1") {
        self.request_pending = false;
      }
    }, function error(response, status) {
      if (status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.delete = function() {
    self.request_pending = true;
    $http({
      method : "POST",
      url : "/api/user/settings/delete",
      headers : {'Content-Type' : "application/json"},
      data : {
        AUTH_TOKEN : "fb11b53abcc1b8f696ae7103337e466b5cc28f2a",
        lang : language.langFile,
        webApp : true,
        _csrf : userAPI.csrf
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == "1") {
        window.location = "/landing_page";
      }
      self.request_pending = false;
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("teamsAPI", function($http, $rootScope, $location, $window, $timeout, $interval, language, userAPI, alertsAPI) {
  var self = this;
  self.loading = false;
  self.teams = [];
  self.getTeams = function() {
    self.loading = true;
    self.teams = [];
    $http({
        method : "GET",
        url : "/api/user/00b169f453b6d34063",
        headers: {'Content-Type': 'application/json'},
        params : {
          reference_code : "SaturdayEveningLate",
          key : "991318",
          lang : language.langFile,
          webApp : true
        }
    }).then(function success(res) {
        var response = decryptResponse(res.data);
        self.teams = response.teams;
        self.loading = false;
    }, function error(res) {
        if (res.status == 500) {
          alertsAPI.show(language.dictionary.error_snackbar);
        } else {
          alertsAPI.show(language.dictionary.error_verify_snackbar);
        }
      });
  };
  self.join = function($scope, code) {
    $http({
        method : "POST",
        url : "/api/user/teams/join",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "e956030f6144dc88e583646a24cdfba30d4aebd2",
          code : code,
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == "1") {
        self.teams.push(response.team);
      } else {
        $scope.teamCodeForm.team_code.$setValidity("minlength",false);
      }
    }, function error(response, status) {
      if (status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.leave = function(i) {
    $http({
        method : "POST",
        url : "/api/user/teams/leave",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "5faaa9f469a85c282b6ec117cc3a8bae32a4fcec",
          token : self.teams[i].token,
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode != "1") {
        self.teams.splice(i, 1);
      }
    }, function error(response, status) {
      if (status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("profilePublicAPI", function($http, $rootScope, $location, $window, $timeout, $interval, language, userAPI, alertsAPI) {
  var self = this;
  self.user = userAPI;
  self.profile_set = false;
  self.profile = {
    pvk : $location.path().split("/")[2],
    username : $location.path().split("/")[3],
    profile_theme_url : "",
    profile_picture_url : "https://res.cloudinary.com/survey-stacks/image/upload/v1499041967/30af3b2db890ab2d3f96a34fa309c0ce/"+$location.path().split("/")[2]+"/profile_picture.png",
    motto : "",
    since : "",
  };
  self.achievements = {
    total_stacks : 0,
    total_coins : 0,
    total_surveys_answered : 0,
    total_questions_answered : 0,
    total_rewards_claimed : 0,
  };
  self.contactInfo = {
    email : "",
    phone_number : "",
    website : "",
    facebook : "",
    instagram : ""
  };
  self.loading = true;
  self.profile_exists = false;
  self.profileData = function() {
    self.loading = true;
    $http({
        method : "GET",
        url : "/api/users/"+self.profile.pvk+"/712b10c4f7ce4432af",
        headers: {'Content-Type': 'application/json'},
        params : {
          reference_code : "FridayEveningCloudySky",
          key : "995215",
          pvk : self.profile.pvk,
          username : self.profile.username,
          webApp : true,
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      var profileInfo = response.profileInfo;
      var achievementsInfo = response.achievementsInfo;
      var contactInfo = response.contactInfo;
      self.profile_exists = (response.resultCode == 1);
      if (self.profile_exists) {
        $("#toolbar_header").html(self.profile.username);
      }
      self.profile.profile_theme_url = profileInfo.profile_theme_url;
      self.profile.profile_picture_url = profileInfo.profile_picture_url;
      self.profile.motto = profileInfo.motto;
      self.profile.since = profileInfo.since;
      self.achievements.total_stacks = achievementsInfo.stacks_earned;
      self.achievements.total_coins = achievementsInfo.coins_earned;
      self.achievements.total_surveys_answered = achievementsInfo.surveys_answered;
      self.achievements.total_questions_answered = achievementsInfo.questions_answered;
      self.achievements.total_rewards_claimed = achievementsInfo.rewards_claimed;
      self.contactInfo.email = contactInfo.email;
      self.contactInfo.phone_number = contactInfo.phone_number;
      self.contactInfo.website = contactInfo.website;
      self.contactInfo.facebook = contactInfo.facebook;
      self.contactInfo.instagram = contactInfo.instagram;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        self.profile_exists = false;
      }
    });
  };
});

app.service("labsAPI", function($timeout, $http, $window, language, userAPI, alertsAPI) {
  var self = this;
  self.loading = true;
  self.response = "";
  self.categories = {};
  self.membership_active = false;
  self.createdQuestionnaire = {
    "lab_option" : null,
    "type" : "",
    "details" : {
      "name" : "",
      "description" : "",
      "public" : false,
      "category_token" : "",
      "sex" : "",
      "ethnicity" : "",
      "age_group" : "",
      "image_url" : "",
      "image_file" : "",
      "data_collected" : {
        "fullname" : false,
        "email_phone" : false,
        "sex" : false,
        "ethinicity" : false,
        "languages" : false,
        "education" : false,
        "relationship" : false,
        "employment" : false,
        "religion" : false,
        "dob" : false
      }
    },
    "method_options" : {},
    "survey" : {
      "freeResponseQuestions" : [],
      "checkboxResponseQuestions" : [],
      "booleanResponseQuestions" : [],
      "radioResponseQuestions" : [],
      "menuResponseQuestions" : [],
      "order" : []
    },
    "poll" : {
      "question" : "",
      "options" : []
    }
  };
  self.retrieveData = function() {
    self.loading = true;
    $http({
      method : "GET",
      url : "/api/user/3ebf12a3d9917ba93b",
      headers : {'Content-Type' : "application/json"},
      params : {
        reference_code : "SundaySnowingSlow",
        key : "990211",
        lang : language.langFile,
        webApp : true
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      self.membership_active = response.membership;
      self.gatherCategories();
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.gatherCategories = function() {
    if (!self.membership_active) {
      self.loading = false;
      return;
    }
    $http({
      method : "GET",
      url : "/api/4868b0b4105908817f",
      headers : {'Content-Type' : "application/json"},
      params : {
        reference_code : "FridayMorningPreMeet",
        key : "993421",
        lang : language.langFile,
        webApp : true
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      self.categories = response.categories;
      self.loading = false;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.createQuestionnaire = function() {
    var file;

    if ($("#imageFile").length) {
      file = $("#imageFile")[0].files[0];
    }

    if (typeof file === "undefined") {
      file = "";
    }

    if (!self.membership_active) {
      self.loading = false;
      return;
    }

    var fd = new FormData();
    fd.append('AUTH_TOKEN', '1ff46b49c380c942733b4d89ac2c86f9602affa5');
    fd.append('qImage', file);
    fd.append('createdQuestionnaire', angular.toJson(self.createdQuestionnaire));
    fd.append('lang', language.langFile);
    fd.append('webApp', true);
    fd.append('_csrf', userAPI.csrf);
    $http({
      method : "POST",
      url : "/api/user/labs/create",
      headers : {'Content-Type' : undefined},
      transformRequest: angular.identity,
      data : fd
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      var resultCode = response.resultCode;
      self.uploading = false;
      switch (resultCode) {
        case 1:
          $window.location.href = "/manage";
          break;
        case -1:
          self.response = language.dictionary.problem_image;
          break;
      }
    }, function error(res) {
      self.uploading = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("surveyCreatorAPI", function($timeout, $window, $http, userAPI, alertsAPI, language) {
  var self = this;

  self.data = {
    "one_survey" : ""
  };
  self.categories = {};
  self.loading = true;
  self.uploading = false;
  self.response = "";

  self.createdSurvey = {
    surveyDetails : {
      "name" : "",
      "description" : "",
      "public" : false,
      "category_token" : "",
      "sex" : "",
      "ethnicity" : "",
      "age_group" : "",
      "image_url" : "",
      "image_file" : "",
      "data_collected" : {
        "fullname" : false,
        "email_phone" : false,
        "sex" : false,
        "ethinicity" : false,
        "languages" : false,
        "education" : false,
        "relationship" : false,
        "employment" : false,
        "religion" : false,
        "dob" : false
      }
    },
    freeResponseQuestions : [],
    checkboxResponseQuestions : [],
    booleanResponseQuestions : [],
    radioResponseQuestions : [],
    menuResponseQuestions : [],
    order : [],
    paymentDetails : {
      "freeSurvey" : false,
      "order_total" : "",
      "cardSaved" : false,
      "cardDetails" : {
        "cardNumber" : "",
        "cardName" : "",
        "cardExpDate" : "",
        "cardSecurityCode" : "",
        "billingStreetAddress" : "",
        "billingFloorAptNumber" : "",
        "billingZipcode" : "",
        "billingCity" : "",
        "billingState" : "",
        "billingCountry" : ""
      }
    }
  };

  self.retrieveData = function() {
    self.loading = true;
    $http({
      method : "GET",
      url : "/api/b574be1ff5d0b12f2f",
      headers : {'Content-Type' : "application/json"},
      params : {
        reference_code : "ThursdayEveningLightBlueSky",
        key : "990304",
        lang : language.langFile,
        webApp : true
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      self.data.one_survey = response.one_survey;
      self.createdSurvey.paymentDetails.order_total = response.one_survey;
      self.categories = response.categories;
      self.loading = false;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.createSurvey = function() {
    var file = $("#imageFile")[0].files[0];

    if (typeof file === "undefined") {
      file = "";
    }

    var fd = new FormData();
    fd.append('AUTH_TOKEN', '5d83912d2037f1c843caa61125c59a3b2be4f7d0');
    fd.append('surveyImage', file);
    fd.append('createdSurvey', angular.toJson(self.createdSurvey));
    fd.append('lang', language.langFile);
    fd.append('webApp', true);
    fd.append('_csrf', userAPI.csrf);
    $http({
      method : "POST",
      url : "/api/user/surveys/create",
      headers : {'Content-Type' : undefined},
      transformRequest: angular.identity,
      data : fd
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      var resultCode = response.resultCode;
      self.uploading = false;
      switch (resultCode) {
        case 1:
          $window.location.href = "/manage";
          break;
        case -1:
          self.response = language.dictionary.no_payment_saved;
          break;
        case -2:
          self.response = language.dictionary.problem_image;
          break;
      }
    }, function error(res) {
      self.uploading = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("pollCreatorAPI", function($timeout, $window, $http, userAPI, alertsAPI, language) {
  var self = this;

  self.categories = {};
  self.loading = true;
  self.uploading = false;
  self.response = "";

  self.createdPoll = {
    pollDetails : {
      "name" : "",
      "public" : false,
      "description" : "",
      "category_token" : "",
      "sex" : "",
      "ethnicity" : "",
      "age_group" : "",
      "image_url" : "",
      "image_file" : "",
      "data_collected" : {
        "fullname" : false,
        "email_phone" : false,
        "sex" : false,
        "ethinicity" : false,
        "languages" : false,
        "education" : false,
        "relationship" : false,
        "employment" : false,
        "religion" : false,
        "dob" : false
      }
    },
    pollQuestion : {
      question : "",
      options : []
    }
  };

  self.retrieveCategories = function() {
    self.loading = true;
    $http({
      method : "GET",
      url : "/api/4868b0b4105908817f",
      headers : {'Content-Type' : "application/json"},
      params : {
        reference_code : "FridayMorningPreMeet",
        key : "993421",
        lang : language.langFile,
        webApp : true
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);

      self.categories = response.categories;
      self.loading = false;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.createPoll = function() {
    var file = $("#imageFile")[0].files[0];

    if (typeof file === "undefined") {
      file = "";
    }

    var fd = new FormData();
    fd.append('AUTH_TOKEN', '0c60247b9e8d9e2d67bf8f1ea07894c63af6ddca');
    fd.append('pollImage', file);
    fd.append('poll', angular.toJson(self.createdPoll));
    fd.append('lang', language.langFile);
    fd.append('webApp', true);
    fd.append('_csrf', userAPI.csrf);
    $http({
      method : "POST",
      url : "/api/user/polls/create",
      headers : {'Content-Type' : undefined},
      transformRequest: angular.identity,
      data : fd
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      var resultCode = response.resultCode;
      self.uploading = false;

      switch (resultCode) {
        case 1:
          $window.location.href = "/manage";
          break;
        case -1:
          self.response = language.dictionary.problem_image;
          break;
      }
    }, function error(res) {
      self.uploading = false;

      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("editorAPI", function($timeout, $window, $http, $location, userAPI, alertsAPI, language) {
  var self = this;

  self.questionnaireData = {
    "editable" : true,
    "type" : $location.path().split("/")[1],
    "q_token" : $location.path().split("/")[2],
    "ppa" : 0,
    "details" : {
      "name" : "",
      "description" : "",
      "image_url" : "",
      "answer_url" : "",
      "free" : true,
      "sc_amount" : 0,
      "sc_update" : 0,
      "labs_created" : false,
      "labs_type" : null
    },
    "labs" : {}
  };
  self.loading = true;
  self.request_pending = false;
  self.response = "";
  self.edit = {
    "delete" : false,
    "name" : "",
    "description" : "",
    "image_url" : "",
    "asm" : 0
  };
  self.getEditorData = function() {
    self.loading = true;
    self.questionnaireData.type = $location.path().split("/")[1];
    self.questionnaireData.q_token = $location.path().split("/")[2];
    $http({
      method : "GET",
      url : "/api/user/afdc2625c2facd6326",
      headers : {'Content-Type' : "application/json"},
      params : {
        reference_code : "Thanksgiving3am",
        key : "991123",
        lang : language.langFile,
        type : self.questionnaireData.type,
        token : self.questionnaireData.q_token,
        webApp : true
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      self.questionnaireData.editable = response.editable;
      self.questionnaireData.ppa = response.ppa;
      self.questionnaireData.details.name = response.details.name;
      self.edit.name = response.details.name;
      self.questionnaireData.details.description = response.details.description;
      self.edit.description = response.details.description;
      self.questionnaireData.details.image_url = response.details.image_url;
      self.questionnaireData.details.answer_url = response.details.answer_url;
      self.edit.image_url = response.details.image_url;
      self.questionnaireData.details.free = response.details.free;
      self.questionnaireData.details.sc_amount = response.details.sc_amount;
      self.questionnaireData.details.sc_update = response.details.sc_amount;
      self.questionnaireData.details.labs_created = response.details.labs_created;
      self.questionnaireData.details.labs_type = response.details.labs_type;

      self.questionnaireData.labs = response.labs;

      self.loading = false;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.editSurvey = function() {
    $http({
        method : "POST",
        url : "/api/user/surveys/edit",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "b922425ee2940e6af1564f105a6911d1c757a3a6",
          type : self.questionnaireData.type,
          token : self.questionnaireData.q_token,
          edit : self.edit,
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      self.request_pending = false;
      if (response.resultCode == "1") {
        self.edit = {
          "delete" : false,
          "name" : "",
          "description" : "",
          "image_url" : "",
          "asm" : 0
        };
        self.getEditorData();
      } else if (response.resultCode == "-1") {
        self.response = language.dictionary.problem_image;
      }
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("receiptAPI", function($timeout, $window, $http, $location, userAPI, alertsAPI, language) {
  var self = this;

  self.receiptData = {
    "token" : $location.path().split("/")[2],
    "type" : $location.path().split("/")[1],
    "viewable" : false,
    "details" : {
      "lang" : "",
      "name" : "",
      "description" : "",
      "category" : "",
      "image" : "",
      "questions_amount" : 0,
      "sc_amount" : 0,
      "edit_url" : "",
      "answer_url" : "",
      "answer_code_url" : ""
    },
    "paymentDetails" : {
      "receipt_token" : "",
      "price" : 0.00,
      "extra" : 0.00,
      "total" : 0.00
    }
  };
  self.loading = true;
  self.getReceiptData = function() {
    self.loading = true;
    $http({
      method : "GET",
      url : "/api/user/0695d378ae2144f4d4",
      headers : {'Content-Type' : "application/json"},
      params : {
        reference_code : "Thanksgiving1pm",
        key : "991213",
        lang : language.langFile,
        type : self.receiptData.type,
        token : self.receiptData.token,
        webApp : true
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
        self.receiptData.viewable = response.viewable;

        self.receiptData.details.lang = response.details.lang;
        self.receiptData.details.name = response.details.name;
        self.receiptData.details.description = response.details.description;
        self.receiptData.details.category = response.details.category;
        self.receiptData.details.image = response.details.image;
        self.receiptData.details.questions_amount = response.details.questions;
        self.receiptData.details.sc_amount = response.details.sc_amount;
        self.receiptData.details.edit_url = response.details.edit_url;
        self.receiptData.details.answer_url = response.details.answer_url;
        self.receiptData.details.answer_code_url = response.details.answer_code_url;

        self.receiptData.paymentDetails.receipt_token = response.payment.receipt;
        self.receiptData.paymentDetails.price = response.payment.price;
        self.receiptData.paymentDetails.extra = response.payment.extra;
        self.receiptData.paymentDetails.total = response.payment.total;

      self.loading = false;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("preferencesSurveyAPI", function($timeout, $window, $http, $location, userAPI, alertsAPI, language, metadata) {
  var self = this;
  self.data = {
    "preferences_completed" : false,
    "categories" : {},
    "companies" : {},
    "stacks" : 0
  };
  self.userAnswers = {
    lang : language.langFile,
    monosex_filter : false,
    translated_filter : false,
    categories : [],
    companies : []
  };
  self.loading = true;
  self.request_pending = false;
  self.getPrefData = function() {
    self.loading = true;
    $http({
        method : "GET",
        url : "/api/user/6c25ce5369fc7c0a5b",
        headers: {'Content-Type': 'application/json'},
        params : {
          reference_code : "FridayNightLunaCat",
          key : "990675",
          lang : language.langFile,
          webApp : true
        }
    }).then(function success(res) {
        var response = decryptResponse(res.data);
        self.data.preferences_completed = response.preferences_completed;
        self.data.categories = response.categories;
        self.data.companies = response.companies;
        self.data.stacks = response.stacks;
        self.userAnswers.monosex_filter = response.preferences.monosex_filter;
        self.userAnswers.translated_filter = response.preferences.translated_filter;
        self.userAnswers.categories = response.preferences.categories;
        self.userAnswers.companies = response.preferences.companies;

        self.loading = false;
    }, function error(res) {
        if (res.status == 500) {
          alertsAPI.show(language.dictionary.error_snackbar);
        } else {
          alertsAPI.show(language.dictionary.error_verify_snackbar);
        }
    });
  };
  self.submitSurvey = function() {
    $http({
        method : "POST",
        url : "/api/user/settings/preferences_survey",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "6de84acd7ebbc319dbd884ce6ddb816172d9b5fc",
          preferences_completed : self.data.preferences_completed,
          preferences : self.userAnswers,
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == "1") {
        self.request_pending = false;
      }
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("aboutSurveyAPI", function($timeout, $window, $http, $location, userAPI, alertsAPI, language, metadata) {
  var self = this;
  self.data = {
    "about_completed" : false,
    "stacks" : 0
  };
  self.loading = true;
  self.request_pending = false;
  self.userAnswers = {
    sex : "",
    ethnicity : [],
    lang : [],
    education : "",
    relationship : "",
    employment : "",
    religion : ""
  };
  self.getAboutData = function() {
    self.loading = true;
    $http({
          method : "GET",
          url : "/api/user/1acef401f0ac771d72",
          headers: {'Content-Type': 'application/json'},
          params : {
            reference_code : "MondayNightDark",
            key : "999666",
            lang : language.langFile,
            webApp : true
          }
      }).then(function success(res) {
          var response = decryptResponse(res.data);
          self.data.about_completed = response.about_completed;
          self.data.stacks = response.stacks;
          self.userAnswers = response.about;

          self.loading = false;
      }, function error(res) {
          if (res.status == 500) {
            alertsAPI.show(language.dictionary.error_snackbar);
          } else {
            alertsAPI.show(language.dictionary.error_verify_snackbar);
          }
      });
  };
  self.submitSurvey = function() {
    $http({
        method : "POST",
        url : "/api/user/settings/about_survey",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "f877990c0256d27d0ab9c28f05eaf6d3b7748502",
          about_completed : self.data.about_completed,
          userAnswers : self.userAnswers,
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == "1") {
        self.request_pending = false;
        $timeout(function() {
          $window.location = "/settings";
        }, 500);
      }
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("paymentSurveyAPI", function($timeout, $window, $http, $location, userAPI, alertsAPI, language, metadata) {
  var self = this;
  self.data = {
    "payment_completed" : false,
    "stacks" : 0
  };
  self.loading = true;
  self.request_pending = false;
  self.response = "";
  self.userAnswers = {
    digits : "",
    name : "",
    exp : "",
    street_address : "",
    apt : "",
    city : "",
    zipcode : "",
    state : "",
    country : "US"
  };
  self.getPaymentData = function() {
    self.loading = true;
    $http({
          method : "GET",
          url : "/api/user/a4fbedfaf5414d9795",
          headers: {'Content-Type': 'application/json'},
          params : {
            reference_code : "TuesdayAfternoonQuarterToEvening",
            key : "929647",
            lang : language.langFile,
            webApp : true
          }
      }).then(function success(res) {
          var response = decryptResponse(res.data);
          self.data.payment_completed = response.payment_completed;
          self.data.stacks = response.stacks;
          self.loading = false;
      }, function error(res) {
          if (res.status == 500) {
            alertsAPI.show(language.dictionary.error_snackbar);
          } else {
            alertsAPI.show(language.dictionary.error_verify_snackbar);
          }
      });
  };
  self.submitSurvey = function() {
    $http({
        method : "POST",
        url : "/api/user/settings/payment_survey",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "dd1fe2a3422d0b1e8b767377e428d12c9098b917",
          payment_completed : self.data.payment_completed,
          userAnswers : self.userAnswers,
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == "1") {
        self.request_pending = false;
        $timeout(function() {
          window.location = "/settings";
        }, 500);
      }
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("pollAnswerAPI", function($timeout, $window, $http, $location, userAPI, alertsAPI, language, metadata) {
  var self = this;
  self.pollDetails = {};
  self.labsAnswers = {};
  self.request_pending = false;
  self.loading = true;
  self.getPollData = function(cb) {
    self.pollDetails = {
      available : false,
      public : false,
      targeted : false,
      answered : false,
      token : $location.path().split("/")[2],
      title : "",
      lang : "",
      description : "",
      collectables : [],
      coins : 0,
      image_url : "",
      question : "",
      options : [],
      analytics : {},
      next_poll : "",
      labs_created : false,
      labs_option : null,
      labs : {}
    };
    self.loading = true;
    $http({
          method : "GET",
          url : "/api/user/c3789b05570894bcfd",
          headers: {'Content-Type': 'application/json'},
          params : {
            reference_code : "SundayNightAC",
            key : "990220",
            lang : language.langFile,
            token : self.pollDetails.token,
            webApp : true
          }
      }).then(function success(res) {
          var response = decryptResponse(res.data);
          var data = {};
          self.pollDetails = response.poll_details;
          if (self.pollDetails.available) {
            metadata.title = self.pollDetails.title;
            $("#toolbar_header").html(self.pollDetails.title);
            metadata.description = self.pollDetails.description;
          } else {
            metadata.title = language.dictionary.unav_p;
          }

          if (self.pollDetails.labs_option === 3) {
            self.labsAnswers.elapsed = 0;
            self.labsAnswers.elapsed_end = 0;
            data.timed = self.pollDetails.labs.timed;
            data.countdown = (self.pollDetails.labs.time > 0);

            for (var i = 0; i < self.pollDetails.labs.questions.length; i++) {
              if (!self.labsAnswers.responses) {
                self.labsAnswers.responses = [];
              }
              self.labsAnswers.responses.push(null);
            }
          }

          cb(data);
      }, function error(res) {
          if (res.status == 500) {
            alertsAPI.show(language.dictionary.error_snackbar);
          } else {
            alertsAPI.show(language.dictionary.error_verify_snackbar);
          }
      });
  };
  self.submitPoll = function($index) {
    $http({
        method : "POST",
        url : "/api/user/polls/answer",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "f6642b19507252dbd126a770e9ea0265c3754bd5",
          lang : language.langFile,
          token : self.pollDetails.token,
          response : $index,
          labs : self.labsAnswers,
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      self.request_pending = false;
      if (response.resultCode == "1") {
        if (typeof response.score !== "undefined") {
          $("#description_section").hide();
          $("#quiz_span").hide();
          $(".time_span1").hide();
          $("#timer_span").removeClass("ng-hide");
          $(".begin-timer").removeClass('ng-hide');
          $(".begin-timer").html((response.score * 100)+"%");
          if (response.score == 0) {
            $(".begin-timer").css("background-color", "red");
            $(".begin-timer").css("border", "1px solid red");
          }
        }

        self.pollDetails.answered = true;
        $timeout(function() {
          self.pollDetails.analytics = response.analytics;
        }, 100);
        if (self.pollDetails.next_poll != "") {
          $('html, body').animate({
            scrollTop: ($('#nextPoll').offset().top)
          },1000);
        }
      }
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("surveyAnswerAPI", function($timeout, $window, $http, $location, userAPI, alertsAPI, language, metadata) {
  var self = this;
  self.surveyDetails = {};
  self.userAnswers = [];
  self.labsAnswers = {};
  self.request_pending = false;
  self.response = "";
  self.loading = true;
  self.getSurveyData = function(cb) {
    self.surveyDetails = {
      available : false,
      public : false,
      targeted : false,
      answered : false,
      token : $location.path().split("/")[2],
      lang : "",
      title : "",
      description : "",
      collectables : [],
      stacks : 0,
      questions : [],
      labs_created : false,
      labs_option : null,
      labs : {}
    };
    self.loading = true;
    $http({
          method : "GET",
          url : "/api/user/5a239f4c0ec530c497",
          headers: {'Content-Type': 'application/json'},
          params : {
            reference_code : "FridayNightMMS",
            key : "991201",
            lang : language.langFile,
            token : self.surveyDetails.token,
            webApp : true
          }
      }).then(function success(res) {
          var response = decryptResponse(res.data);
          var data = {};
          for (var q in response.survey_details.questions) {
            if (response.survey_details.questions[q].type != "fr") continue;
            response.survey_details.questions[q].pattern = new RegExp(response.survey_details.questions[q].pattern[0], response.survey_details.questions[q].pattern[1]);
          }
          self.surveyDetails = response.survey_details;
          self.userAnswers = response.answers_blank;
          if (self.surveyDetails.available) {
            metadata.title = self.surveyDetails.title;
            $("#toolbar_header").html(self.surveyDetails.title);
            metadata.description = self.surveyDetails.description;
          } else {
            metadata.title = language.dictionary.unav_s;
          }

          if (self.surveyDetails.labs_option === 3) {
            self.labsAnswers.elapsed = 0;
            self.labsAnswers.elapsed_end = 0;
            data.timed = self.surveyDetails.labs.timed;
            data.countdown = (self.surveyDetails.labs.time > 0);
          } else if (self.surveyDetails.labs_option === 1) {
            self.labsAnswers.key = "";
          }

          cb(data);
      }, function error(res) {
          if (res.status == 500) {
            alertsAPI.show(language.dictionary.error_snackbar);
          } else {
            alertsAPI.show(language.dictionary.error_verify_snackbar);
          }
      });
  };
  self.submitSurvey = function() {
    if (self.surveyDetails.labs.timed) {
      self.labsAnswers.elapsed_end = performance.now();
    }
    $http({
        method : "POST",
        url : "/api/user/surveys/answer",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "7ccea46ffe440f91a95c84272c919fbe9057c80e",
          lang : language.langFile,
          token : self.surveyDetails.token,
          response : self.userAnswers,
          labs : self.labsAnswers,
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == "1") {
        if (typeof response.score !== "undefined") {
          document.getElementById("submitResponses").remove();
          $("#answer_section").hide();
          $(".begin-timer").removeClass('ng-hide');
          $(".begin-timer").html((response.score * self.surveyDetails.labs.out_of)+"%") ;

          $timeout(function() {
            $window.location = "/main";
            self.request_pending = false;
          }, 3000);
        } else {
          $window.location = "/main";
          self.request_pending = false;
        }
      } else if (response.resultCode == "-1") {
        self.response = language.dictionary.wrong_key
      }
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("reportAPI", function($timeout, $window, $http, $location, userAPI, alertsAPI, language, metadata) {
  var self = this;
  self.questionnaireDetails = {};
  self.loading = true;
  self.request_pending = false;
  self.response = "";
  self.userReport = {
    reason : [],
    additional : ""
  };
  self.getReportData = function() {
    self.questionnaireDetails = {
      reported : false,
      exist : true,
      type : $location.path().split("/")[1],
      token : $location.path().split("/")[2],
      title : "",
    };
    self.loading = true;
    $http({
      method : "GET",
      url : "/api/user/8f5f974a5fda1c42fb",
      headers : {'Content-Type' : "application/json"},
      params : {
        reference_code : "SaturdayMidnight2",
        key : "991202",
        lang : language.langFile,
        type : self.questionnaireDetails.type,
        token : self.questionnaireDetails.token,
        webApp : true
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.available) {
        $("#toolbar_header").html(language.dictionary.report+" '"+response.title+"'");
      }
      self.questionnaireDetails.reported = response.reported;
      self.questionnaireDetails.title = response.title;
      self.questionnaireDetails.exist = response.available;
      self.loading = false;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.submitReport = function() {
    self.request_pending = true;
    $http({
        method : "POST",
        url : "/api/user/report",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "d1c8c9bd26d6083413e43b80948525ce05cab0ba",
          lang : language.langFile,
          token : self.questionnaireDetails.token,
          type : self.questionnaireDetails.type,
          report : self.userReport,
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      self.request_pending = false;
      self.questionnaireDetails.reported = true;
      if (response.resultCode == 1) {
        self.response = language.dictionary.t_report;
      } else {
        self.response = language.dictionary.error;
      }
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("teamPublicAPI", function($http, $location, $window, $timeout, $interval, alertsAPI, language) {
  var self = this;
  self.profile = {
    tvk : $location.path().split("/")[2],
    name : "",
    nickname : $location.path().split("/")[3],
    team_theme_url : "",
    team_logo_url : "",
    description : "",
    since : "",
  };
  self.contactInfo = {
    email : "",
    phone_number : "",
    website : "",
    facebook : "",
    instagram : ""
  };
  self.additional = {
    verified : false,
    members : 0,
  };
  self.loading = true;
  self.team_exists = false;
  self.profileData = function() {
    self.loading = true;
    $http({
        method : "GET",
        url : "/api/teams/"+self.profile.tvk+"/a50cf95ced779706b6",
        headers: {'Content-Type': 'application/json'},
        params : {
          reference_code : "SundayNightTenSeven",
          key : "991821",
          tvk : self.profile.tvk,
          nickname : self.profile.nickname,
          webApp : true,
          lang : language.langFile
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == -1) {
        self.team_exists = false;
        self.loading = false;
        return;
      }
      self.team_exists = true;
      self.profile.name = response.profileData.name;
      self.profile.description = response.profileData.description;
      self.profile.team_theme_url = response.profileData.theme_url;
      self.profile.team_logo_url = response.profileData.logo_url;
      self.profile.since = response.profileData.since.toString();
      self.contactInfo = response.contactInfo;
      self.additional = response.additional;
      self.loading = false;
      $("#toolbar_header").html(self.profile.name);
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        self.team_exists = false;
      }
    });
  };
});
