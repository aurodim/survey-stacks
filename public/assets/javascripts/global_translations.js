var globals = {
  supportedLanguages : {
    "en" : "English",
    "es" : "Espa\u00F1ol"
  },
  supportedCountries : {
    "US" : "United States",
    "CA" : "Canada",
    "MX" : "Mexico"
  },
  US_states : {
    'AL' : 'Alabama',
    'AK' : 'Alaska',
    'AZ' : 'Arizona',
    'AR' : 'Arkansas',
    'CA' : 'California',
    'CO' : 'Colorado',
    'CT' : 'Connecticut',
    'DE' : 'Delaware',
    'DC' : 'District of Columbia',
    'FL' : 'Florida',
    'GA' : 'Georgia',
    'HI' : 'Hawaii',
    'ID' : 'Idaho',
    'IL' : 'Illinois',
    'IN' : 'Indiana',
    'IA' : 'Iowa',
    'KS' : 'Kansas',
    'KY' : 'Kentucky',
    'LA' : 'Louisiana',
    'ME' : 'Maine',
    'MD' : 'Maryland',
    'MA' : 'Massachusetts',
    'MI' : 'Michigan',
    'MN' : 'Minnesota',
    'MS' : 'Mississtackpi',
    'MO' : 'Missouri',
    'MT' : 'Montana',
    'NE' : 'Nebraska',
    'NV' : 'Nevada',
    'NH' : 'New Hampshire',
    'NJ' : 'New Jersey',
    'NM' : 'New Mexico',
    'NY' : 'New York',
    'NC' : 'North Carolina',
    'ND' : 'North Dakota',
    'OH' : 'Ohio',
    'OK' : 'Oklahoma',
    'OR' : 'Oregon',
    'PA' : 'Pennsylvania',
    'RI' : 'Rhode Island',
    'SC' : 'South Carolina',
    'SD' : 'South Dakota',
    'TN' : 'Tennessee',
    'TX' : 'Texas',
    'UT' : 'Utah',
    'VT' : 'Vermont',
    'VA' : 'Virginia',
    'WA' : 'Washington',
    'WV' : 'West Virginia',
    'WI' : 'Wisconsin',
    'WY' : 'Wyoming',
  },
  MX_states : {
    'AG' : 'Aguascalientes',
    'BN' : 'Baja California',
    'BS' : 'Baja California Sur',
    'CH' : 'Coahuila',
    'CI' : 'Chihuahua',
    'CL' : 'Colima',
    'CP' : 'Campeche',
    'CS' : 'Chiapas',
    'DF' : 'Distrito Federal',
    'DG' : 'Durango',
    'GE' : 'Guerrero',
    'GJ' : 'Guanajuato',
    'HD' : 'Hidalgo',
    'JA' : 'Jalisco',
    'MC' : 'Michoacan',
    'MR' : 'Morelos',
    'MX' : 'Mexico',
    'NA' : 'Nayarit',
    'NL' : 'Nuevo Leon',
    'OA' : 'Oaxaca',
    'PU' : 'Puebla',
    'QE' : 'Queretaro',
    'QI' : 'Quintana Roo',
    'SI' : 'Sinaloa',
    'SL' : 'San Luis Potosi',
    'SO' : 'Sonora',
    'TA' : 'Tamaulipas',
    'TB' : 'Tabasco',
    'TL' : 'Tlaxcala',
    'VC' : 'Veracruz',
    'YU' : 'Yucatan',
    'ZA' : 'Zacatecas',
  },
  CA_states : {
    "BC" : "British Columbia",
    "ON" : "Ontario",
    "NL" : "Newfoundland and Labrador",
    "NS" : "Nova Scotia",
    "PE" : "Prince Edward Island",
    "NB" : "New Brunswick",
    "QC" : "Quebec",
    "MB" : "Manitoba",
    "SK" : "Saskatchewan",
    "AB" : "Alberta",
    "NT" : "Northwest Territories",
    "NU" : "Nunavut",
    "YT" : "Yukon Territory"
  },
  cardTypes : {
    "discover" : [
      ["60110"],
      ["60112", "60114"],
      ["60119"]
    ],
    "visa" : [
      ["4"]
    ],
    "mastercard" : [
      ["51", "55"]
    ],
    "american_express" : [
      ["34"],
      ["37"]
    ]
  },
  surveyStacks : function(amount) {
    if (amount == 1) {
      return "Stack";
    } else {
      return "Stacks";
    }
  },
  rewardStacks : function(amount) {
    if (amount == 1) {
      return "Stack";
    } else {
      return "Stacks";
    }
  }
};
