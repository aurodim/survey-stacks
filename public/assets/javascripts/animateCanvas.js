//Bubble background Canvas

var c;
var canvas;
var frame;
var circleArray = [];
var curr = "";

function startAnim() {
  if (typeof canvas == undefined || canvas == undefined) {
    circleArray = [];

    canvas = document.createElement('canvas');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    document.body.appendChild(canvas);

    c = canvas.getContext('2d');

    init();
    animate();
  }
}

function startSparkle() {
  if (typeof canvas == undefined || canvas == undefined) {
    circleArray = [];

    canvas = document.createElement('canvas');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    document.body.appendChild(canvas);

    c = canvas.getContext('2d');

    sparkleAnim(5);
    animateSparkle();
  }
}

function stopAnim() {
  if (typeof canvas == undefined || canvas == undefined) return;
  cancelAnimationFrame(frame);
  document.body.removeChild(canvas);
  canvas = undefined;
  c = undefined;
  frame = undefined;
  circleArray = [];
}

window.addEventListener('resize', function() {
  if (typeof canvas == undefined || canvas == undefined) return;

  c.clearRect(0, 0, innerWidth, innerHeight);


  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;

  if (window.innerWidth < 750) {
    if (curr == "s") return;
    circleArray = [];
    stopAnim();
    startSparkle();
  } else {
    if (curr == "b") return;
    circleArray = [];
    stopAnim();
    startAnim();
  }
});

var maxRadius = 40;

var colorArray = [
  '#2ECC71',
  "#333"
];

function Circle(x, y, dx, dy, radius) {
  this.x = x;
  this.y = y;
  this.dx = dx;
  this.dy = dy;
  this.radius = radius;
  this.minRadius = radius;

  this.draw = function() {

    if (this.x < innerWidth / 2) {
      if (this.color != colorArray[1]) {
        this.color = colorArray[1];
      }
    } else {
      if (this.color != colorArray[0]) {
        this.color = colorArray[0];
      }
    }

    c.beginPath();
    c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
    c.fillStyle = this.color;
    c.fill();
  }

  this.update = function() {
    if (this.x + this.radius > innerWidth || this.x - this.radius < 0) {
      this.dx = -this.dx;
    }
    if (this.y + this.radius > innerHeight || this.y - this.radius < 0) {
      this.dy = -this.dy;
    }
    this.x += this.dx;
    this.y += this.dy;

    // interactivity

    this.draw();
  }
}

function Sparkle(x, y, radius, index) {
  this.x = x;
  this.y = y;
  this.radius = radius;
  this.index = index;
  this.alpha = 0;

  this.draw = function() {
    c.beginPath();
    c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
    c.fillStyle = "rgba(46,204,113,"+this.alpha+")";
    c.fill();
  }

  this.update = function() {
    if (this.radius < 8) {
      this.radius += 0.05;
      this.alpha += 0.002;

      this.draw();
    } else {
      this.radius = Math.random() * 8;
      this.x = Math.random() * (innerWidth - radius * 2) + radius;
      this.y = Math.random() * (innerHeight - radius * 2) + radius;
      this.alpha = 0;

      this.draw();
    }
  }
}

function sparkleAnim(amount) {
  circleArray = [];

  for (var i = 0; i < amount; i++) {
    this.radius = Math.random() * 8;
    var x = Math.random() * (innerWidth - radius * 2) + radius;
    var y = Math.random() * (innerHeight - radius * 2) + radius;

    circleArray.push(new Sparkle(x, y, radius, circleArray.length));
  }
}

function init() {

  circleArray = [];

  for (var i = 0; i < 20; i++) {
    var radius = (Math.random() * 5) + 50;
    var x = Math.random() * (innerWidth - radius * 2) + radius;
    var y = Math.random() * (innerHeight - radius * 2) + radius;
    var dx = (Math.random() - 0.8) * 2.5;
    var dy = (Math.random() - 0.8) * 2.5;

    circleArray.push(new Circle(x, y, dx, dy, radius));
  }
}

function animate() {
  frame = requestAnimationFrame(animate);

  c.clearRect(0, 0, innerWidth, innerHeight);

  c.beginPath();
  c.lineWidth="1";
  c.fillStyle="#2ECC71";
  c.rect(0, 0, innerWidth / 2, innerHeight);
  c.fill();

  curr = "b";

  for (var i = 0; i < circleArray.length; i++) {
    circleArray[i].update();
  }
}

function animateSparkle() {
  frame = requestAnimationFrame(animateSparkle);

  c.clearRect(0, 0, innerWidth, innerHeight);

  curr = "s";

  for (var i = 0; i < circleArray.length; i++) {
    circleArray[i].update();
  }
}
