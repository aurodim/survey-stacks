var en = {
  app_name : "SurveyStacks",
  description_metadata : "Answer Surveys and Recieve 'Stacks'! Ask Surveys and Recieve Stats Instantly",
  motto : "Take Surveys, Make Money",
  log_back_in : "Error! Please log back in",
  new_password : "New password",
  profile_motto : "Profile Motto",
  member_since : "Member Since",
  first_response : "First Response",
  total_responses : "Total Responses",
  responses : "Responses",
  graphics_questions : "Graphics By Question",
  average_responses_per_day : "Average Responses per Day",
  average_responses_per_user : "Average Answers per User",
  average_stacks_per_user : "Average Stacks per Response",
  last_response : "Last Response",
  respondent_analytics : "Respondent Analytics",
  already_reported : "You have already reported this questionnaire",
  unav_s : "SS - Unavailable Survey",
  unav_p : "SS - Unavailable Poll",
  login_to_receive : "Log in to recieve rewards for answering",
  submit : "Submit",
  false : "False",
  answer_url : "Answer URL",
  true : "True",
  select_image : "Select Image From Computer",
  no_surveys : "There are no surveys available at the moment",
  no_polls : "There are no polls available at the moment",
  no_notif : "You have no notifications",
  cookies_policy : "Cookies Policy",
  responses_by_user : "Responses by User",
  affordable_rewards : "Affordable Rewards",
  unaffordable_rewards : "Rewards to set as a Goal",
  surveys_interest : "Surveys for you!",
  surveys_uninterest : "Other Surveys",
  required : "Required",
  language : "Language",
  email_phone : "E-mail or Phone Number",
  email : "E-mail",
  phone_number : "Phone number",
  username : "Username",
  blank_field : "Do not leave any empty fields!",
  error : "There was an error processing the request!",
  street_address : "Street Address",
  apt_number : "Apt/Floor #",
  city : "City",
  zipcode : "Zip Code or Postal Code",
  state : "State",
  cts : "Coins to Stacks",
  logging_out : "Logging Out...",
  country : "Country",
  continue : "Continue",
  skip : "Skip",
  setup_incomplete : "Attention! You have not finished setting up your account. Visit 'Settings' to fully set up your account",
  questionsAmount : function(amount) {
    if (amount == 1) {
      return "question";
    } else {
      return "questions";
    }
  },
  overall_coins : "Total Coins",
  overall_stacks : "Total Stacks",
  total_stacks : "Stacks Earned!",
  total_coins : "Coins Earned!",
  surveys_answered_ex : "Surveys Answered!",
  questions_answered : "Questions Answered!",
  rewards_claimed : "Rewards Claimed!",
  stacks_used : "Stacks Used!",
  achivements : "Achivements",
  contact_info : "Contact Info",
  extra : "Extra",
  surveys : "Surveys",
  users : "Users",
  search_tip : "A Few Tips to Narrow and Quicken Your Search:\n\n\tUse \"@\" to search Users or Companies\n\n\tUse \"$\" to search Surveys\n\n\tUse \"*\" to search Rewards",
  profile : "Profile",
  home : "Home",
  bookmarks : "Bookmarks",
  redeem : "Redeem",
  settings : "Settings",
  teams : "Teams",
  info : "Info",
  no_bookmarks : "Oops! You have not bookmarked any Surveys or Rewards",
  rewards : "Rewards",
  rewardStacksNeeded : function(amount) {
    if (amount == 1) {
      return "Stack Needed";
    } else {
      return "Stacks Needed";
    }
  },
  rewardStacksRemaining : function(amount) {
    if (amount == 1) {
      return "Stack Remaining";
    } else {
      return "Stacks Remaining";
    }
  },
  help : "Help",
  delete_ps : "Enter Questionnaire Name to Delete",
  t_and_c : "Terms and Conditions",
  forums : "Forums",
  contact_us : "Contact Us",
  preferences : "Preferences",
  payment : "Payment",
  address : "Address",
  notifications : "Notifications",
  change_password : "Change Password",
  website : "Website",
  facebook : "Facebook",
  instagram : "Instagram",
  update_contact_info : "Update Contact Info",
  current_password : "Current password",
  email_notifications : "E-mail Notifications",
  text_message_notifications : "Text message notifications",
  allow_desktop_notifications : "Allow Desktop notifications",
  notify_match_preference : "Notify me about surveys that match my preferences",
  update_notifications : "Update Notifications Settings",
  billing_address : "Billing Address",
  type : "Type",
  card_name : "Name on Card",
  card_ending_digits : "Card 4 Ending Digits",
  card_digits : "Card Number",
  expiration_date : "Expiration Date (MM/YY)",
  card_name_error : "Card Name cannot be left empty!",
  update_payment : "Update Payment",
  sex : "Sex",
  birth_date : "Birth Date",

  languages : "Languages",
  languages_spoken : "Languages Spoken",
  categories : "Categories",
  companies : "Companies",
  update_preferences : "Update Preferences",
  save : "Save",
  yet_to_come : "More Data Coming in Our Updates! Hint: Check The Comments ;)",
  account : "Account",
  more_info : "More Info",
  update_account_info : "Update Account Info",
  manage : "Manage",
  zero_zero : "0.00",
  analytics : "Analytics",
  empty_preferences : "You have not set up your preferences yet",
  empty_about : "You have not answered the About Me survey yet",
  empty_payment : "You do not have a saved payment method",
  surveyNotification : function(stacks, questions) {
    var att_seek = ["New Survey!!!", "You found me!", "Attention!", "Ladies and Gentlemen!"];
    const random_int = Math.floor(Math.random() * 4);
    var text = "";

    if (stacks == 1 && questions == 1) {
      text = "A survey with 1 Stack and 1 question has been found";
    } else if (stacks == 1 && questions != 1) {
      text = "A survey with "+questions+" questions has been found, but it only has 1 stack";
    } else if (stacks != 1 && questions == 1) {
      text = "A survey with "+stacks+" Stacks has been found, and it ONLY has 1 question";
    } else if (stacks != 1 && questions != 1) {
      text = "A survey with "+stacks+" Stacks and "+questions+" questions has been found";
    }

    return att_seek[random_int] + " " + text;
  },
  rewardNotification : function(stacks_needed, company, description) {
    var att_seek = ["New Reward!!!", "Hooray!", "Wow!", "Check This Out!"];
    var random_int = Math.floor(Math.random() * 4);
    var one = "A "+description+" from "+company+" just arrived! You need "+stacks_needed+" Stacks more to claim it!";
    var two = "You need "+stacks_needed+" Stacks more to claim this amazing "+description+" from "+company;
    var desc_seek = [one, two] ;
    var random_int_d = Math.floor(Math.random() * 2);

    var text = att_seek[random_int] + " " + desc_seek[random_int_d];

    return text;
  },
  billing_invalid : "Please fill out ALL credit card and billing information",
  new_notification : "New Notification!",
  change_profile_picture : "Change Profile Picture",
  not_image : "This is not an image...",
  updating_progress : "Updating ...",
  education_level : "Highest or Current Level of Education",
  education : "Education",
  education_levels : {
    "n_a" : "No schooling completed",
    "e" : "8th grade",
    "n_d" : "Some high school, no diploma",
    "ged" : "High school graduate, diploma or the equivalent",
    "cc_nd" : "Some college credit, no degree",
    "ttv_t" : "Trade/technical/vocational training",
    "a_d" : "Associate degree",
    "b_d" : "Bachelor’s degree",
    "m_d" : "Master’s degree",
    "p_d" : "Professional degree",
    "d_d" : "Doctorate degree"
  },
  relationship_status : "Relationship Status",
  relationship_statuses : {
    "s" : "Single",
    "r" : "In a relationship",
    "e" : "Engaged",
    "m" : "Married",
    "c" : "It's complicated",
    "o_r" : "In an open relationship",
    "w" : "Widowed",
    "d_p" : "In a domestic partnership",
    "c_u" : "In a civil union"
  },
  about_me : "About Me",
  about_description : "A quick survey that can help us get to know you (all questions are optional)",
  update_about : "Update About Me",
  employment_status : "Employment Status",
  employment_statuses : {
    "s" : "Student",
    "n_l" : "Not employed & Not interested",
    "u" : "Unemployed",
    "e" : "Employed for wages",
    "se" : "Self-employed",
    "h" : "A homemaker",
    "m" : "Military",
    "r" : "Retired",
    "uw" : "Unable to work"
  },
  religion : "Religion",
  religions : {
    "c" : "Christian",
    "m" : "Muslim",
    "ct" : "Catholic",
    "b" : "Buddhist",
    "a" : "Atheist",
    "ag" : "Agnostic",
    "nr" : "Non-religious",
    "o" : "Other"
  },
  preferences_description : "Preferences survey in order for us to provide you with surveys that might interest you",
  payment_description : "Payment survey to store payment & billing information to later use for in-app purchases",
  unexistentProfile : function(username) {
    return "A profile with the username: "+username+" does not exist";
  },
  sexes : {
    "male" : "Male",
    "female" : "Female",
    "both" : "Both"
  },
  fullname : "Full name",
  sex_meta : "Sex will be used to filter surveys that target the sex you choose",
  language_meta : "Language will be used to translate app into the language you choose",
  categories_meta : "Categories of interest (default is any)",
  companies_meta : "Companies of interest (default is any)",
  blank_sex : "Please enter a prefered sex",
  blank_language : "Please enter a prefered language",
  blank_categories : "Please select at least one category",
  blank_companies : "Please select at least one company",
  survey_creator : "Survey Creator",
  details : "Details",
  survey_name : "Survey Name",
  name : "Name",
  description : "Description",
  category : "Category",
  q_name : "Questionnaire Name",
  q_description : "Questionnaire Description (80 character max)",
  public_q : "Public Questionnaire (SurveyStacks account not needed to respond)",
  q_category : "Questionnaire Category",
  ud_group : "Respondent Data to Automatically Collect",
  create_q : "Create Questionnaire",
  q_image : "Questionnaire Image (only use images you are licensed to use)",
  survey_description : "Survey Description (80 character max)",
  survey_image : "Survey Image (only use images you are licensed to use)",
  cover_image : "Questionnaire Image",
  free_response : "Free Response",
  question_here : "Enter Question",
  add_question : "Add Question",
  checkbox_response : "Checkbox Response",
  respondent_answer : "Respondent answer here",
  add_response : "Add Response",
  boolean_response : "\"No\" or \"Yes\" Response",
  radio_response : "Radio Response",
  single_item_response : "Single Item Response",
  toggles_buttons_testing : "Toggles and Buttons are just for testing",
  purchase_details : "Purchase Details",
  use_avail_survey : "Use an available survey",
  order_total : "Order total",
  no_charge : "You will not be charged at all",
  create_survey : "Create Survey",
  credit_card_details : "Credit or Debit Card Details",
  use_stored_payment : "Used stored payment information",
  security_code : "Security Code or CVV",
  billing_address_details : "Billing Address Details",
  survey_category : "Survey Category",
  free_survey : "1-Stack Survey",
  order : "Question Arrangement",
  blank_question_survey : "Blank Question",
  getQuestionText : function(question) {
    var split = question.split("_");
    var type = split[0];
    var index = split[1];
    var question_type;

    switch (type) {
      case "fr":
        question_type = "(Free Response Question)";
        break;
      case "cbr":
        question_type = "(Checkbox Question)";
        break;
      case "br":
        question_type = "(Boolean Question)";
        break;
      case "rr":
        question_type = "(Radio Question)";
        break;
      case "mr":
        question_type = "(Single Item Question)";
        break;
    }

    return question_type;
  },
  blank : "Blank",
  nameless_survey : "Name field cannot be left blank",
  descriptionless_survey : "Please fill out description",
  categoryless_survey : "Please select a category",
  sexless_survey : "Please select a target sex",
  ethnicityless_survey : "Please select a target ethnicity",
  ageless_survey : "Please select a target age group",
  problem_image : "The image URL is invalid",
  no_questions : "Survey must have at least one question",
  empty_questions : function(type) {
    var response = "";

    switch (type) {
      case 'fr':
        response = "Free";
        break;
      case 'cbr':
        response = "Checkbox";
        break;
      case 'br':
        response = "Boolean";
        break;
      case 'rr':
        response = "Radio";
        break;
      case 'mir':
        response = "Single Item";
        break;
    }

    return "There is a blank " + response + " Response question";
  },
  empty_responses : function(type) {
    var response = "";

    switch (type) {
      case 'cbr':
        response = "Checkbox";
        break;
      case 'rr':
        response = "Radio";
        break;
      case 'mir':
        response = "Single Item";
        break;
    }

    return "There is a " + response + " Response question with a blank response";
  },
  empty_security_code : "Card Security Code or CVV cannot be left blank",
  no_payment_saved : "Sorry! You do not have a saved Payment method",
  description_toolong : "Description cannot exceed 80 characters",
  target_sex : "Target Sex",
  error_snackbar : "Trouble Loading! Reload!",
  error_verify_snackbar : "Server Cannot Verify Credentials!",
  success_snackbar : "Success!",
  stacks : "Stacks",
  shfsi : "Scratch here for stacks info",
  rewardLeft : function(quantity) {
    if (quantity < 6) {
      return "Only "+quantity+" left!";
    } else {
      return quantity+" left";
    }
  },
  logout : "Log Out",
  wishlist : "Wishlist",
  terminal_cnf : ": command not registered",
  target_ethnicity : "Target Ethnicity",
  target_age_group : "Target Age Group",
  any : "Any",
  ethnicities : {
    "a" : "Any",
    "w" : "White",
    "h_l" : "Hispanic or Latino",
    "b_aa" : "Black or African American",
    "na_an" : "Native American or American Indian",
    "a_pi" : "Asian / Pacific Islander",
    "o" : "Other"
  },
  ethnicity : "Ethnicity",
  public_survey : "Public Survey (SurveyStacks account not needed to respond)",
  poll_creator : "Poll Creator",
  poll_name : "Poll Name",
  poll_description : "Poll Description",
  public_poll : "Public Poll (SurveyStacks account not needed to respond)",
  poll_category : "Poll Category",
  poll_image : "Poll Image (only use images you are licensed to use)",
  create_poll : "Create Poll",
  poll_question : "Poll Question",
  add_poll_option : "Add Poll Option",
  poll_option : "Poll Option",
  receipt : "Receipt",
  survey : "Survey",
  poll : "Poll",
  answer : "Answer",
  survey_price : "Survey Price",
  extra_stacks : "Extra Stacks",
  questions : "Questions",
  edit : "Edit",
  coins : "Coins",
  report : "Report",
  not_target : "Note: Unfortunately, you do not meet the targeted preference and will therefore not receive any stacks/coins for answering this survey/poll",
  not_allowed : "Note: You have already answered this survey/poll and will not receive any stacks/coins for answering again",
  submit_answers : "Submit Answers",
  required_text : "Answer is required",
  not_available : "Sorry, the survey/poll you are looking for is no longer avilable",
  reported_why : "Reason for report? (check all that apply)",
  report_reasons : {
    "sh" : "Sexual Harrasment",
    "ts" : "Attempts to trick the Stacks/Coins System",
    "h" : "Hateful",
    "d" : "Discriminating",
    "v" : "Vulgar",
    "vpc" : "Violates our User Privacy Code"
  },
  t_report : "Thank you for submitting this report",
  report_additional : "Additional information?",
  vote : "Vote",
  next_poll : "Next Poll",
  polls : "Polls",
  editor : "Editor",
  update : "Update",
  title : "Title",
  updated_price : "Price",
  not_editable : "Sorry! The survey/poll you are looking for does not exist!",
  monosex_filter : "Questionnaires that target only my sex",
  translated_filter : "No translated questionnaires",
  delete : "Delete",
  blank_required : "You must answer all required questions",
  age_group : "Age Group",
  age_groups : {
    "a" : "Any",
    "t" : "13-17",
    "lt" : "18-24",
    "ea" : "25-34",
    "ad" : "35-44",
    "la" : "45-54",
    "es" : "55-64",
    "s" : "65-74",
    "ls" : "75+"
  },
  patterns : {
    "a" : "All Text",
    "t" : "Text Only",
    "n" : "Numbers Only",
    "tn" : "Text and Numbers Only",
    "d" : "Date",
    "ts" : "Time",
    "h" : "Link"
  },
  tabs : [
    {
      "value" : "analytics",
      "icon" : "&#xE6E1;",
      "translation" : "Analytics"
    },
    {
      "value" : "bookmarks",
      "icon" : "&#xE866;",
      "translation" : "Bookmarks"
    },
    {
      "value" : "labs",
      "icon" : "&#xE80E;",
      "translation" : "Labs"
    },
    {
      "value" : "manage",
      "icon" : "&#xE22B;",
      "translation" : "Manage"
    },
    {
      "value" : "main",
      "icon" : "&#xE85D;",
      "translation" : "Home"
    },

    {
      "value" : "polls",
      "icon" : "&#xE801;",
      "translation" : "Polls"
    },
    {
      "value" : "profile",
      "icon" : "&#xE7FD;",
      "translation" : "Profile"
    },
    {
      "value" : "redeem",
      "icon" : "&#xE8F6;",
      "translation" : "Redeem"
    },
    {
      "value" : "settings",
      "icon" : "&#xE8B8;",
      "translation" : "Settings"
    },
    {
      "value" : "teams",
      "icon" : "&#xE7FB;",
      "translation" : "Teams"
    }
  ],
  daily_r : "Daily Responses",
  monthly_r : "Monthly Responses",
  yearly_r : "Yearly Responses",
  days : [
    "Mon",
    "Tue",
    "Wed",
    "Thu",
    "Fri",
    "Sat",
    "Sun"
  ],
  months : [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sept",
    "Oct",
    "Nov",
    "Dec"
  ],
  labs_membership : "Labs Membership",
  no_analytics : "You have not created any questionnaires that have analytics included",
  no_qs_created : "You have not created any questionnaires",
  click_p_to : "Click the green \"+\" button below to create a survey or poll!",
  blank_pq : "Poll question cannot be left blank",
  blank_po : "Poll option cannot be left blank",
  app_lang : "Language (Our services will be given to you in this language)",
  download_as : [
    "As Shown",
    "Survey Analytics",
    "Graphics by Question",
    "Respondent Analytics",
    "Responses w/ User Data",
    "Responses",
    "Question Responses"
  ],
  download_as_p : [
    "As Shown",
    "Poll Analytics",
    "Respondent Analytics",
    "Responses w/ User Data",
  ],
  download_formats : [
    "JSON",
    "CSV",
    "XML"
  ],
  n_r : "New Response",
  n_q_r : function(name) {
    return "A user responsed to " + name;
  },
  delete_account : "Delete Account",
  delete_account_pre : "Enter Username to Delete",
  no_mem : "You do not have an active membership",
  active_mem : function(date) {
    if (date === "" || date == 0) {
      return "";
    }
    var d = new Date(date).toLocaleString().split(",")[0];
    return "Your labs membership will expire " + d;
  },
  labs : "Labs",
  cancel_labs_mem : "Cancel Labs Membership",
  purchase_labs_mem : "Purchase Labs Membership",
  recommended_poll : "Recommended Poll",
  no_labs : function(price) {
    return "Purchase a labs membership for $"+price.toFixed(2)+" USD monthly. Labs membership prices may vary monthly. It is recommended that you verify the current price for the Labs membership. The payment method stored will be charged the current Labs membership price.";
  },
  labs_types : ["📝\xa0\xa0\xa0Quiz\xa0\xa0📝", "🔑\xa0\xa0\xa0Key Needed to Reply\xa0\xa0🔑", "⭐️\xa0\xa0\xa0Emoji Based\xa0\xa0❤️", "😎\xa0\xa0\xa0Game\xa0\xa0😎"],
  grading_algorithm : "Grading Algorithm",
  grading_algorithms : {
    "c" : "Contains",
    "bw" : "Begins With",
    "ew" : "Ends With",
    "o" : "Is Only",
    "pm" : "Percentage Match"
  },
  show_grade : "Show Grade to Respondent",
  possible_quiz_points : "Possible Quiz Points",
  correct_answer : "Correct Answer",
  no : "No",
  yes : "Yes",
  question_worth : "Question Worth",
  unique_keys : "Unique Keys",
  global_key : "Global Key",
  timed : "Timed",
  max_time : "Seconds to Answer",
  quiz_options : "Quiz Options",
  quiz_bad_total : "The total quiz score is invalid",
  quiz_bad_worth : "Questions cannot have an empty \"Question Worth\"",
  quiz_bad_answer : "Questions must have a valid answer",
  game_bad_time : "Game timer must be at least 5 seconds",
  quiz_bad_amounted : "The amounted questions' worth does not equal the total quiz score",
  collectables_answer : "Data automatically collected from profile",
  emoji_survey : "🔥 😎 Emojis Only for this Survey 🤪 🤣",
  key_needed_reply : "🔑 Enter the Key Needed to Reply 🔒",
  quiz_answer : function(points) {
    return "📝 This is a "+points+" point quiz! Good Luck! 📝"
  },
  times_up : "Time's Up!",
  points : function(points) {
    if (points === 1) return "("+points+" point)";
    return "("+points+" points)";
  },
  error_snackbar : "Trouble Loading! Reload!",
  error_verify_snackbar : "Server Cannot Verify Credentials!",
  success_snackbar : "Success!",
  wrong_key : "The key you entered is invalid or has been used"
};
