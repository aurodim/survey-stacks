"use strict";

var app = angular.module("survey-stacks", ['ngResource', 'ngRoute', 'ngCookies', 'ngAnimate']);
app.config(function($locationProvider) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false,
        rewriteLinks: false
    });
});

app.config(['$compileProvider', function ($compileProvider) {
  $compileProvider.debugInfoEnabled(false);
}]);

app.config(['$sceProvider', function ($sceProvider) {
  $sceProvider.enabled(true);
}]);

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $locationProvider.hashPrefix('');
  $routeProvider

    .when('/', {
      templateUrl : '/views/landing_page.html',
      controller  : 'LandingController'
    })

    .when('/landing_page', {
      templateUrl : '/views/landing_page.html',
      controller  : 'LandingController'
    })

    .when('/login', {
       templateUrl : '/views/login.html',
       controller  : 'LoginController',
    })

    .when('/register', {
       templateUrl : '/views/register.html',
       controller  : 'RegisterController',
    })

    .when('/forgot_username', {
       templateUrl : '/views/forgot_username.html',
       controller  : 'ForgotUsernameController',
    })

    .when('/forgot_password', {
       templateUrl : '/views/forgot_password.html',
       controller  : 'ForgotPasswordController',
    })

    .when('/reset_password', {
       templateUrl : '/views/reset_password.html',
       controller  : 'ResetPasswordController',
    })

    .when('/users/:user_validation_key/:username/profile', {
      templateUrl : '/views/profile_public_outline.html',
      controller  : 'ProfilePublicController',
    })

    .when('/teams/:team_validation_key/:nickname/profile', {
      templateUrl : '/views/team_view_outline.html',
      controller  : 'TeamPublicController',
    })

    .when('/teams/login', {
       templateUrl : '/teams/views/login.html',
       controller  : 'TeamsLoginController'
    })

    .when('/teams/register', {
       templateUrl : '/teams/views/register.html',
       controller  : 'TeamsRegisterController'
    })

    .when('/teams/forgot_nickname', {
       templateUrl : '/teams/views/forgot_nickname.html',
       controller  : 'TeamsForgotNicknameController'
    })

    .when('/teams/forgot_password', {
       templateUrl : '/teams/views/forgot_password.html',
       controller  : 'TeamsForgotPasswordController'
    })

    .when('/teams/reset_password', {
       templateUrl : '/teams/views/reset_password.html',
       controller  : 'TeamsResetPasswordController'
    })

    .when('/surveys/:survey_token/answer', {
      templateUrl : '/views/survey_answer_public.html',
      controller : 'SurveyAnswerController'
    })

    .when('/polls/:poll_token/answer', {
      templateUrl : '/views/poll_answer_public.html',
      controller : 'PollAnswerController'
    })

    .otherwise({
     redirectTo : "/"
    });

   $locationProvider.html5Mode({
       enabled: true,
       requireBase: true,
       rewriteLinks: true
   });
 }]);

app.directive("onErrorSrc", function() {
  return {
    link: function(scope, element, attrs) {
      element.bind('error', function() {
        if (attrs.src != attrs.onErrorSrc || attrs.src == "") {
          attrs.$set('src', attrs.onErrorSrc);
        }
      });
    }
  };
});

app.controller("sessionController", function($scope, $rootScope, $timeout, language, metadata) {

  $scope.language = language;
  $scope.metadata = metadata;
  $scope.mobilecheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
  };

  $scope.$on('$routeChangeStart', function($event, next, current) {
    if (next["$$route"] == null || next["$$route"] == undefined || typeof next["$$route"] == undefined) return;

    let route = next["$$route"].originalPath.split("/")[1];
    $scope.metadata.title = "SurveyStacks";
    $scope.metadata.description = "Answer Surveys and Recieve 'Stacks'! Ask Surveys and Recieve Stats Instantly";
    if (route === "landing_page" || route === "") {
      stopAnim();
    } else if ($scope.mobilecheck() || window.innerWidth < 750) {
      $timeout(startSparkle(), 500);
    } else {
      $timeout(startAnim(), 500);
    }
  });
});

app.controller("AlertsController", function($scope, language, alertsAPI) {
  $scope.api = alertsAPI;
});

app.controller("mobile", function($scope) {
  $scope.mobileCheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);

    return check;
  };
});

app.controller('LandingController', function($scope, $location, language) {
  $scope.language = language;
  $scope.openNav = function() {
    $("#navDrawer").removeClass("fadeOut");
    $("#navDrawer").removeClass("hidden");
    $("#navDrawer").addClass("fadeIn");
  };
  $scope.closeNav = function() {
    $("#navDrawer").removeClass("fadeIn");
    $("#navDrawer").addClass("fadeOut");
    $("#navDrawer").addClass("hidden");
  };
  $scope.route_hash = "lorem1";
  $scope.tabItem = function(route) {
    $location.hash(route);
    $scope.route_hash = route;
  };
  if ($location.hash() == "" || $location.hash() != "lorem2" && $location.hash() != "lorem3") {
    $location.hash("lorem1");
  } else {
    $scope.route_hash = $location.hash();
  }
});

app.controller('LoginController', function($scope, $routeParams, language) {
  $scope.language = language;
  $scope.openNav = function() {
    $("#navDrawer").fadeIn(500);
    $("#navList").animate({width : '30%'}, 500);
  };
  $scope.closeNav = function() {
    $("#navDrawer").fadeOut(500);
    $("#navList").animate({width : '0%'}, 500);
  };
  $scope.userCredentials = {
    AUTH_TOKEN : "86d6b440f80df257d08375ac9719c24a7828e075",
    lang : language.langFile,
    username : "",
    password : "",
    webApp : true
  };
  $scope.loginResponse = "";
  $scope.login = function() {
    if ($scope.userCredentials.username == "" || $scope.userCredentials.password == "") {
      $scope.loginResponse = language.dictionary.blank_field;
    } else {
      $scope.loginResponse = "";
      $scope.loginRequest();
    }
  };
  $scope.resetResponse = function() {
    $scope.loginResponse = "";
  };
  $scope.loginRequest = function() {

    $.ajax({
      url: "/api/login",
      type: 'POST',
      data: $scope.userCredentials,
      dataType: 'JSON',
      error: function() {
        $scope.loginResponse = language.dictionary.error;
        $scope.$apply();
      },
      success: function(res) {
        var resultCode = res.resultCode;

        var response;

        switch(resultCode) {
          case 1:
            response = "";
            var redirect_to = $routeParams.redirect_url;
            if (redirect_to != null && redirect_to !== "" && redirect_to != undefined) {
              window.location.href = redirect_to;
            } else {
              location.reload();
            }
            break;
          case -1:
            response = language.dictionary.loginError.username;
            break;
          case -2:
            response = language.dictionary.loginError.password;
            break;
          case -3:
            response = language.dictionary.loginError.locked;
            break;
        }
        $scope.loginResponse = response;
        $scope.$apply();
      }
    });
  }
});

app.controller('RegisterController', function($scope, $routeParams, language) {
  $scope.language = language;
  $scope.openNav = function() {
    $("#navDrawer").fadeIn(500);
    $("#navList").animate({width : '30%'}, 500);
  };
  $scope.closeNav = function() {
    $("#navDrawer").fadeOut(500);
    $("#navList").animate({width : '0%'}, 500);
  };

  $scope.userCredentials = {
    AUTH_TOKEN : "ff6e14a65abe095a37bd7e203da37183553a0ae4",
    lang : language.langFile,
    fullname : "",
    email_phone : "",
    username : "",
    password : "",
    dob : "",
    webApp : true
  };
  $scope.register = function() {

    if ($scope.userCredentials.username == "" || $scope.userCredentials.password == "" || $scope.userCredentials.dob == "") {
      $scope.registerResponse = language.dictionary.blank_field;
    } else {
      $scope.registerResponse = "";
      $scope.registerRequest();
    }
  };
  $scope.resetResponse = function() {
    $scope.registerResponse = "";
  };
  $scope.registerResponse = "";
  $scope.registerRequest = function() {

    $.ajax({
      url: "/api/register",
      type: 'POST',
      data: $scope.userCredentials,
      dataType: 'JSON',
      error: function() {
        var response = language.dictionary.error;
        $scope.registerResponse = response;
        $scope.$apply();
      },
      success: function(res) {
        var resultCode = res.resultCode;

        var response;

        switch(resultCode) {
          case 1:
            response = "";
            var redirect_to = $routeParams.redirect_url;
            if (redirect_to != null && redirect_to !== "" && redirect_to != undefined) {
              window.location.href = redirect_to;
            } else {
              window.location.href = "/about_me";
            }
            break;
          case -1:
            response = language.dictionary.registerError.email_phone;
            break;
          case -2:
            response = language.dictionary.registerError.username;
            break;
          case -3:
            response = language.dictionary.registerError.dob;
            break;
        }
        $scope.registerResponse = response;
        $scope.$apply();
      }
    });
  }
});

app.controller('ForgotUsernameController', function($scope, language) {
  $scope.language = language;
  $scope.openNav = function() {
    $("#navDrawer").removeClass("fadeOut");
    $("#navDrawer").removeClass("hidden");
    $("#navDrawer").addClass("fadeIn");
  };
  $scope.closeNav = function() {
    $("#navDrawer").removeClass("fadeIn");
    $("#navDrawer").addClass("fadeOut");
    $("#navDrawer").addClass("hidden");
  };

  $scope.userCredentials = {
    AUTH_TOKEN : "b7a1992ab58f4923369f3a53f0b73d02f71368f1",
    lang : language.langFile,
    email_phone : "",
    dob : "",
    webApp : true
  };
  $scope.funame = function() {
    if ($scope.userCredentials.email_phone == "" || $scope.userCredentials.dob == "") {
      $scope.FUnameResponse = language.dictionary.blank_field;
    } else {
      $scope.FUnameResponse = "";
      $scope.funameRequest();
    }
  };
  $scope.resetResponse = function() {
    $scope.FUnameResponse = "";
  };
  $scope.FUnameResponse = "";
  $scope.funameRequest = function() {

    $.ajax({
      url: "/api/forgot_username",
      type: 'POST',
      data: $scope.userCredentials,
      dataType: 'JSON',
      error: function() {
        $scope.FUnameResponse = language.dictionary.error;
        $scope.$apply();
      },
      success: function(res) {
        var resObj = decryptResponse(res);
        var resultCode = resObj.resultCode;

        var response;

        switch(resultCode) {
          case 1:
            response = resObj.username;
            $scope.forgotForm.$setPristine();
            $scope.userCredentials = {
              AUTH_TOKEN : "b7a1992ab58f4923369f3a53f0b73d02f71368f1",
              lang : language.langFile,
              email_phone : "",
              dob : "",
              webApp : true
            };
            break;
          case -1:
            response = language.dictionary.unexistent;
            break;
        }
        $scope.FUnameResponse = response;
        $scope.$apply();
      }
    });
   }
 });

app.controller('ForgotPasswordController', function($scope, language) {
  $scope.language = language;
  $scope.openNav = function() {
    $("#navDrawer").fadeIn(500);
    $("#navList").animate({width : '30%'}, 500);
  };
  $scope.closeNav = function() {
    $("#navDrawer").fadeOut(500);
    $("#navList").animate({width : '0%'}, 500);
  };

  $scope.userCredentials = {
    AUTH_TOKEN : "106b965c50810171f7ad65198ebeb5c1aa153e7f",
    lang : language.langFile,
    email_phone : "",
    username : "",
    dob : "",
    webApp : true
  };
  $scope.fpass = function() {

    if ($scope.userCredentials.email_phone == "" || $scope.userCredentials.dob == "") {
      $scope.FPassResponse = language.dictionary.blank_field;
    } else {
      $scope.FPassResponse = "";
      $scope.fpassRequest();
    }
  };
  $scope.resetResponse = function() {
    $scope.FpassResponse = "";
  }
  $scope.FpassResponse = "";

  $scope.fpassRequest = function() {

    $.ajax({
      url: "/api/forgot_password",
      type: 'POST',
      data: $scope.userCredentials,
      dataType: 'JSON',
      error: function() {
        $scope.FpassResponse = language.dictionary.error;
        $scope.$apply();
      },
      success: function(res) {
        var resObj = decryptResponse(res);
        var resultCode = resObj.resultCode;

        var response;

        switch(resultCode) {
          case 1:
            response = language.dictionary.forgot_password_error.success;
            $scope.forgotForm.$setPristine();
            $scope.userCredentials = {
              AUTH_TOKEN : "106b965c50810171f7ad65198ebeb5c1aa153e7f",
              lang : language.langFile,
              email_phone : "",
              username : "",
              dob : "",
              webApp : true
            };
            break;
          case -1:
            response = language.dictionary.unexistent;
            break;
          case -2:
            response = language.dictionary.forgot_password_error.spam(resObj.time_to_wait);
        }
        $scope.FpassResponse = response;
        $scope.$apply();
      }
    });
  }
});

app.controller('ResetPasswordController', function($scope, language) {
  $scope.language = language;
  $scope.openNav = function() {
    $("#navDrawer").fadeIn(500);
    $("#navList").animate({width : '30%'}, 500);
  };
  $scope.closeNav = function() {
    $("#navDrawer").fadeOut(500);
    $("#navList").animate({width : '0%'}, 500);
  };
  $scope.userCredentials = {
    AUTH_TOKEN : "1598e8e31a66516d8fd27f15a6190904a31382c9",
    lang : language.langFile,
    username : "",
    password : "",
    code : "",
    webApp : true
  };
  $scope.passToText = function() {
    $("#password").prop("type", "text");
  };
  $scope.textToPass = function() {
    $("#password").prop("type", "password");
  };
  $scope.rpass = function() {

    if ($scope.userCredentials.username == "" || $scope.userCredentials.password == "") {
      $scope.RPassResponse = language.blank_field;
    } else {
      $scope.RPassResponse = "";
      $scope.rpassRequest();
    }
  };
  $scope.resetResponse = function() {
    $scope.RpassResponse = "";
  };
  $scope.RpassResponse = "";

  $scope.rpassRequest = function() {

    $.ajax({
      url: "/api/reset_password",
      type: 'POST',
      data: $scope.userCredentials,
      dataType: 'JSON',
      error: function() {
        $scope.RpassResponse = language.dictionary.error;
        $scope.$apply();
      },
      success: function(res) {
        var resObj = decryptResponse(res);
        var resultCode = resObj.resultCode;

        var response;

        switch(resultCode) {
          case 1:
            response = language.dictionary.reset_password_response.success;
            $scope.resetForm.$setPristine();
            $scope.userCredentials = {
              AUTH_TOKEN : "1598e8e31a66516d8fd27f15a6190904a31382c9",
              lang : language.langFile,
              username : "",
              password : "",
              code : "",
              webApp : true
            };
            break;
          case -1:
            response = language.dictionary.reset_password_response.unexistent_user;
            break;
          case -2:
            response = language.dictionary.reset_password_response.unexistent_code;
        }
        $scope.RpassResponse = response;
        $scope.$apply();
      }
    });
  }
});

app.controller('ProfilePublicController', function($scope, language, profilePublicAPI) {
  $scope.api = profilePublicAPI;
  $scope.api.profileData();
  $scope.language = language;
  $scope.openNav = function() {
    $("#navDrawer").fadeIn(500);
    $("#navList").animate({width : '30%'}, 500);
  };
  $scope.closeNav = function() {
    $("#navDrawer").fadeOut(500);
    $("#navList").animate({width : '0%'}, 500);
  };
});

app.controller('TeamsLoginController', function($scope, language) {
  $scope.language = language;

  $scope.openNav = function() {
    $("#navDrawer").fadeIn(500);
    $("#navList").animate({width : '30%'}, 500);
  };
  $scope.closeNav = function() {
    $("#navDrawer").fadeOut(500);
    $("#navList").animate({width : '0%'}, 500);
  };

  $scope.teamCredentials = {
    AUTH_TOKEN : "949eb67256cd10eef1b152c73e5e67aaedecf3ac",
    lang : language.langFile,
    nickname : "",
    password : "",
    webApp : true
  };
  $scope.loginResponse = "";
  $scope.login = function() {
    if ($scope.teamCredentials.nickname == "" || $scope.teamCredentials.password == "") {
      $scope.loginResponse = language.dictionary.blank_field;
    } else {
      $scope.loginResponse = "";
      $scope.loginRequest();
    }
  };
  $scope.resetResponse = function() {
    $scope.loginResponse = "";
  };
  $scope.loginRequest = function() {

    $.ajax({
      url: "/api/teams/login",
      type: 'POST',
      data: $scope.teamCredentials,
      dataType: 'JSON',
      error: function() {
        $scope.loginResponse = language.dictionary.error;
        $scope.$apply();
      },
      success: function(res) {
        var resultCode = res.resultCode;

        var response;

        switch(resultCode) {
          case 1:
            response = "";
            location.reload();
            break;
          case -1:
            response = language.dictionary.loginError.nickname;
            break;
          case -2:
            response = language.dictionary.loginError.password;
            break;
          case -3:
            response = language.dictionary.loginError.locked;
            break;
        }
        $scope.loginResponse = response;
        $scope.$apply();
      }
    });
  }
});

app.controller('TeamsRegisterController', function($scope, language) {
  $scope.language = language;
  $scope.openNav = function() {
    $("#navDrawer").fadeIn(500);
    $("#navList").animate({width : '30%'}, 500);
  };
  $scope.closeNav = function() {
    $("#navDrawer").fadeOut(500);
    $("#navList").animate({width : '0%'}, 500);
  };

  $scope.language = language;
  $scope.teamCredentials = {
    AUTH_TOKEN : "ed3d5417c418918e311ccb9e4c3e95ba660a30b7",
    lang : language.langFile,
    fullname : "",
    email_phone : "",
    name : "",
    nickname : "",
    password : "",
    webApp : true
  };
  $scope.register = function() {

    if ($scope.teamCredentials.nickname == "" || $scope.teamCredentials.password == "") {
      $scope.registerResponse = language.dictionary.blank_field;
    } else {
      $scope.registerResponse = "";
      $scope.registerRequest();
    }
  };
  $scope.resetResponse = function() {
    $scope.registerResponse = "";
  };
  $scope.registerResponse = "";
  $scope.registerRequest = function() {

    $.ajax({
      url: "/api/teams/register",
      type: 'POST',
      data: $scope.teamCredentials,
      dataType: 'JSON',
      error: function() {
        var response = language.dictionary.error;
        $scope.registerResponse = response;
        $scope.$apply();
      },
      success: function(res) {
        var resultCode = res.resultCode;

        var response;

        switch(resultCode) {
          case 1:
            response = "";
            window.location.reload();
            break;
          case -1:
            response = language.dictionary.registerError.email_phone;
            break;
          case -2:
            response = language.dictionary.registerError.nickname;
            break;
        }
        $scope.registerResponse = response;
        $scope.$apply();
      }
    });
  }
});

app.controller('TeamsForgotNicknameController', function($scope, language) {
  $scope.language = language;
  $scope.openNav = function() {
    $("#navDrawer").removeClass("fadeOut");
    $("#navDrawer").removeClass("hidden");
    $("#navDrawer").addClass("fadeIn");
  };
  $scope.closeNav = function() {
    $("#navDrawer").removeClass("fadeIn");
    $("#navDrawer").addClass("fadeOut");
    $("#navDrawer").addClass("hidden");
  };

  $scope.teamCredentials = {
    AUTH_TOKEN : "b44a0ce73a9258f233f8fd961304392bca91c266",
    lang : language.langFile,
    email_phone : "",
    name : "",
    webApp : true
  };
  $scope.funame = function() {
    if ($scope.teamCredentials.email_phone == "" || $scope.teamCredentials.nickname == "") {
      $scope.FUnameResponse = language.dictionary.blank_field;
    } else {
      $scope.FUnameResponse = "";
      $scope.funameRequest();
    }
  };
  $scope.resetResponse = function() {
    $scope.FUnameResponse = "";
  };
  $scope.FUnameResponse = "";
  $scope.funameRequest = function() {

    $.ajax({
      url: "/api/forgot_username",
      type: 'POST',
      data: $scope.teamCredentials,
      dataType: 'JSON',
      error: function() {
        $scope.FUnameResponse = language.dictionary.error;
        $scope.$apply();
      },
      success: function(res) {
        var resObj = decryptResponse(res);
        var resultCode = resObj.resultCode;

        var response;

        switch(resultCode) {
          case 1:
            response = resObj.username;
            $scope.forgotForm.$setPristine();
            $scope.teamCredentials = {
              AUTH_TOKEN : "b44a0ce73a9258f233f8fd961304392bca91c266",
              lang : language.langFile,
              email_phone : "",
              nickname : "",
              webApp : true
            };
            break;
          case -1:
            response = language.dictionary.unexistent;
            break;
        }
        $scope.FUnameResponse = response;
        $scope.$apply();
      }
    });
   }
 });

app.controller('TeamsForgotPasswordController', function($scope, language) {
  $scope.language = language;
  $scope.openNav = function() {
    $("#navDrawer").fadeIn(500);
    $("#navList").animate({width : '30%'}, 500);
  };
  $scope.closeNav = function() {
    $("#navDrawer").fadeOut(500);
    $("#navList").animate({width : '0%'}, 500);
  };

  $scope.teamCredentials = {
    AUTH_TOKEN : "e161de2f7209e21e31ec70cce6e6978f8db24316",
    lang : language.langFile,
    fullname : "",
    email_phone : "",
    nickname : "",
    webApp : true
  };
  $scope.fpass = function() {

    if ($scope.teamCredentials.email_phone == "" || $scope.teamCredentials.dob == "") {
      $scope.FPassResponse = language.dictionary.blank_field;
    } else {
      $scope.FPassResponse = "";
      $scope.fpassRequest();
    }
  };
  $scope.resetResponse = function() {
    $scope.FpassResponse = "";
  }
  $scope.FpassResponse = "";

  $scope.fpassRequest = function() {

    $.ajax({
      url: "/api/teams/forgot_password",
      type: 'POST',
      data: $scope.teamCredentials,
      dataType: 'JSON',
      error: function() {
        $scope.FpassResponse = language.dictionary.error;
        $scope.$apply();
      },
      success: function(res) {
        var resObj = decryptResponse(res);
        var resultCode = resObj.resultCode;

        var response;

        switch(resultCode) {
          case 1:
            response = language.dictionary.forgot_password_error.success;
            $scope.forgotForm.$setPristine();
            $scope.teamCredentials = {
              AUTH_TOKEN : "e161de2f7209e21e31ec70cce6e6978f8db24316",
              lang : language.langFile,
              fullname : "",
              email_phone : "",
              nickname : "",
              webApp : true
            };
            break;
          case -1:
            response = language.dictionary.unexistent;
            break;
          case -2:
            response = language.dictionary.forgot_password_error.spam(resObj.time_to_wait);
        }
        $scope.FpassResponse = response;
        $scope.$apply();
      }
    });
  }
});

app.controller('TeamsResetPasswordController', function($scope, language) {
  $scope.language = language;
  $scope.openNav = function() {
    $("#navDrawer").fadeIn(500);
    $("#navList").animate({width : '30%'}, 500);
  };
  $scope.closeNav = function() {
    $("#navDrawer").fadeOut(500);
    $("#navList").animate({width : '0%'}, 500);
  };
  $scope.teamCredentials = {
    AUTH_TOKEN : "ce4ee55bde2faf2ca977333d603545f3b8bffa61",
    lang : language.langFile,
    username : "",
    password : "",
    code : "",
    webApp : true
  };
  $scope.passToText = function() {
    $("#password").prop("type", "text");
  };
  $scope.textToPass = function() {
    $("#password").prop("type", "password");
  };
  $scope.rpass = function() {
    if ($scope.teamCredentials.username == "" || $scope.teamCredentials.password == "") {
      $scope.RPassResponse = language.dictionary.blank_field;
    } else {
      $scope.RPassResponse = "";
      $scope.rpassRequest();
    }
  };
  $scope.resetResponse = function() {
    $scope.RpassResponse = "";
  };
  $scope.RpassResponse = "";

  $scope.rpassRequest = function() {

    $.ajax({
      url: "/api/teams/reset_password",
      type: 'POST',
      data: $scope.teamCredentials,
      dataType: 'JSON',
      error: function() {
        $scope.RpassResponse = language.dictionary.error;
        $scope.$apply();
      },
      success: function(res) {
        var resObj = decryptResponse(res);
        var resultCode = resObj.resultCode;

        var response;

        switch(resultCode) {
          case 1:
            response = language.dictionary.reset_password_response.success;
            $scope.resetForm.$setPristine();
            $scope.teamCredentials = {
              AUTH_TOKEN : "ce4ee55bde2faf2ca977333d603545f3b8bffa61",
              lang : language.langFile,
              username : "",
              password : "",
              code : "",
              webApp : true
            };
            break;
          case -1:
            response = language.dictionary.reset_password_response.unexistent_user;
            break;
          case -2:
            response = language.dictionary.reset_password_response.unexistent_code;
        }
        $scope.RpassResponse = response;
        $scope.$apply();
      }
    });
  }
});

app.controller('SurveyAnswerController', function($scope, $rootScope, $timeout, $interval, surveyAnswerAPI, language, alertsAPI) {
  $scope.language = language;
  $scope.openNav = function() {
    $("#navDrawer").fadeIn(500);
    $("#navList").animate({width : '30%'}, 500);
  };
  $scope.closeNav = function() {
    $("#navDrawer").fadeOut(500);
    $("#navList").animate({width : '0%'}, 500);
  };
  $scope.countdown = 5;
  $scope.gameTime = 0;
  $scope.api = surveyAnswerAPI;
  $scope.api.getSurveyData(function(data) {
    $timeout(function() {
      $scope.api.loading = false;
      if (data.countdown) {
        $scope.gameTime = $scope.api.surveyDetails.labs.time;
        var countdown = $interval(function() {
          $scope.countdown -= 1;
          if ($scope.countdown == 0) {
            if (data.timed) {
              $scope.api.labsAnswers.elapsed = performance.now();
            }
            $scope.startGame();
            $interval.cancel(countdown);
            $scope.countdown -= 1;
            return;
          }
        }, 1000);
      }
    }, 1000);
  });
  $scope.game;
  $scope.startGame = function() {
    $scope.game = $interval(function() {
      $scope.gameTime -= 1;
      if ($scope.gameTime == 0) {
        $interval.cancel($scope.game);
        $scope.api.submitSurvey();
        return;
      }
    }, 1000);
  };
  var emojiRgx = "^[\u{1f300}-\u{1f5ff}\u{1f900}-\u{1f9ff}\u{1f600}-\u{1f64f}\u{1f680}-\u{1f6ff}\u{2600}-\u{26ff}\u{2700}-\u{27bf}\u{1f1e6}-\u{1f1ff}\u{1f191}-\u{1f251}\u{1f004}\u{1f0cf}\u{1f170}-\u{1f171}\u{1f17e}-\u{1f17f}\u{1f18e}\u{3030}\u{2b50}\u{2b55}\u{2934}-\u{2935}\u{2b05}-\u{2b07}\u{2b1b}-\u{2b1c}\u{3297}\u{3299}\u{303d}\u{00a9}\u{00ae}\u{2122}\u{23f3}\u{24c2}\u{23e9}-\u{23ef}\u{25b6}\u{23f8}-\u{23fa}]+$";
  $scope.emojiOnly = new RegExp(emojiRgx, "u");
  $scope.alert = alertsAPI;
  $scope.optionChecked = function(parent, index, event) {
    if (event.target.checked) {
      $scope.api.userAnswers[parent].push(index);
    } else {
      var ind = $scope.api.userAnswers[parent].indexOf(index);
      $scope.api.userAnswers[parent].splice(ind, 1);
    }
  };
  $scope.validateSurvey = function() {
    for (var answer in $scope.api.userAnswers) {
      if ($scope.api.userAnswers[answer] === null || $scope.api.userAnswers[answer] == "") {
        if ($scope.api.surveyDetails.questions[answer].required) {
          $scope.api.response = language.dictionary.blank_required;
          return false;
        }
      }
    }

    $interval.cancel($scope.game);
    $scope.api.submitSurvey();
  };
  $scope.$on('$destroy',function(){
    if($scope.game)
        $interval.cancel($scope.game);
  });
});

app.controller('PollAnswerController', function($scope, $rootScope, $timeout, $interval, pollAnswerAPI, language, alertsAPI) {
  $scope.language = language;
  $scope.api = pollAnswerAPI;
  $scope.countdown = 5;
  $scope.gameTime = 0;
  $scope.api.getPollData(function(data) {
    $timeout(function() {
      $scope.api.loading = false;
      if (data.countdown) {
        $scope.gameTime = $scope.api.pollDetails.labs.time;
        var countdown = $interval(function() {
          $scope.countdown -= 1;
          if ($scope.countdown == 0) {
            if (data.timed) {
              $scope.api.labsAnswers.elapsed = performance.now();
            }
            $scope.startGame();
            $interval.cancel(countdown);
            $scope.countdown -= 1;
            return;
          }
        }, 1000);
      }
    }, 1000);
  });
  $scope.alert = alertsAPI;
  $scope.openNav = function() {
    $("#navDrawer").fadeIn(500);
    $("#navList").animate({width : '30%'}, 500);
  };
  $scope.closeNav = function() {
    $("#navDrawer").fadeOut(500);
    $("#navList").animate({width : '0%'}, 500);
  };
  $scope.game;
  $scope.startGame = function() {
    $scope.game = $interval(function() {
      $scope.gameTime -= 1;
      if ($scope.gameTime == 0) {
        $interval.cancel($scope.game);
        $scope.currentAdditionalQuestion = null;
        $scope.api.submitPoll();
        return;
      }
    }, 1000);
  };
  $scope.currentAdditionalQuestion = null;
  $scope.voted = null;
  $scope.vote = function($index) {
    if ($scope.api.pollDetails.answered || $scope.api.request_pending) {
      return false;
    }

    $scope.voted = $index;

    if ($scope.api.pollDetails.labs.questions.length > 0) {
      $scope.currentAdditionalQuestion = 0;
      return;
    }

    $scope.api.request_pending = true;
    $scope.api.submitPoll($index);
  };
  $scope.voteAdditional = function($index) {
    if ($scope.api.pollDetails.answered || $scope.api.request_pending) {
      return false;
    }

    $scope.api.labsAnswers.responses[$scope.currentAdditionalQuestion] = $index;

    if ($scope.api.pollDetails.labs.questions.length - 1 != $scope.currentAdditionalQuestion) {
      $scope.currentAdditionalQuestion += 1;
      return;
    }

    $scope.api.request_pending = true;
    $scope.currentAdditionalQuestion = null;
    $interval.cancel($scope.game);
    $scope.api.submitPoll($scope.voted);
  };
  $scope.$on('$destroy',function(){
    if($scope.game)
        $interval.cancel($scope.game);
  });
});

app.controller('TeamPublicController', function($scope, language, teamPublicAPI) {
  $scope.api = teamPublicAPI;
  $scope.api.profileData();
  $scope.language = language;
});
