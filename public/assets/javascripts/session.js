"use strict";

var app = angular.module("survey-stacks", ['ngResource', 'ngRoute', 'ngCookies', 'ngAnimate', 'ngSanitize']);

app.config(['$compileProvider', function ($compileProvider) {
  $compileProvider.debugInfoEnabled(false);
}]);

app.config(['$sceProvider', function ($sceProvider) {
  $sceProvider.enabled(true);
}]);

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $locationProvider.hashPrefix('');
  $routeProvider
     .when('/', {
         templateUrl : '/views/main.html',
         controller  : 'MainController'
     })

     .when('/main', {
         templateUrl : '/views/main.html',
         controller  : 'MainController'
     })

     .when('/analytics', {
       templateUrl : '/views/analytics.html',
       controller  : 'AnalyticsController'
     })

     .when('/bookmarks', {
         templateUrl : '/views/bookmarks.html',
         controller  : 'BookmarksController'
     })

     .when('/labs', {
         templateUrl : '/views/labs.html',
         controller  : 'LabsController'
     })

     .when('/manage', {
         templateUrl : '/views/manage.html',
         controller  : 'ManageController'
     })

     .when('/polls', {
         templateUrl : '/views/polls.html',
         controller  : 'PollsController'
     })

     .when('/profile', {
         templateUrl : '/views/profile.html',
         controller  : 'ProfileController',
         css: '/stylesheet/.css'
     })

     .when('/redeem', {
         templateUrl : '/views/redeem.html',
         controller  : 'RedeemController'
     })

     .when('/settings', {
         templateUrl : '/views/settings.html',
         controller  : 'SettingsController'
     })

     .when('/teams', {
         templateUrl : '/views/teams.html',
         controller  : 'TeamsController'
     })

     .when('/preferences_survey', {
       templateUrl : '/views/preferences_survey.html',
       controller  : 'PreferencesSurveyController'
     })

     .when('/about_survey', {
       templateUrl : '/views/about_survey.html',
       controller  : 'AboutSurveyController'
     })

     .when('/payment_survey', {
       templateUrl : '/views/payment_survey.html',
       controller  : 'PaymentSurveyController'
     })

     .when('/survey_creator', {
         templateUrl : '/views/survey_creator.html',
         controller  : 'SurveyCreatorController'
     })

     .when('/poll_creator', {
         templateUrl : '/views/poll_creator.html',
         controller  : 'PollCreatorController'
     })

     .when('/surveys/:survey_token/edit', {
       templateUrl : '/views/editor.html',
       controller : 'EditorController'
     })

     .when('/polls/:poll_token/edit', {
       templateUrl : '/views/editor.html',
       controller : 'EditorController'
     })

     .when('/surveys/:survey_token/answer', {
       templateUrl : '/views/survey_answer.html',
       controller : 'SurveyAnswerController'
     })

     .when('/polls/:poll_token/answer', {
       templateUrl : '/views/poll_answer.html',
       controller : 'PollAnswerController'
     })

     .when('/surveys/:survey_token/report', {
       templateUrl : '/views/report.html',
       controller : 'ReportController'
     })

     .when('/polls/:poll_token/report', {
       templateUrl : '/views/report.html',
       controller : 'ReportController'
     })

     .when('/users/:user_validation_key/:username/profile', {
       templateUrl : '/views/profile_view_outline.html',
       controller  : 'ProfilePublicController',
     })

     .when('/teams/:team_validation_key/:nickname/profile', {
       templateUrl : '/views/team_view_outline.html',
       controller  : 'TeamPublicController',
     })

     .when('/surveys/:survey_token/receipt', {
       templateUrl : '/views/receipt.html',
       controller : 'ReceiptController'
     })

     .when('/logout', {
        controller: '',
        templateUrl: '/views/logout.html',
        resolve: {
          init: function() {
            window.location = "/73664f66bc9106da7d/logout";
          }
        }
     })

     .otherwise({
       redirectTo : "/"
     });

   $locationProvider.html5Mode({
       enabled: true,
       requireBase: true,
       rewriteLinks: true
   });
 }]);

app.directive("onErrorSrc", function() {
  return {
    link: function(scope, element, attrs) {
      element.bind('error', function() {
        if (attrs.src != attrs.onErrorSrc || attrs.src == "") {
          attrs.$set('src', attrs.onErrorSrc);
        }
      });
    }
  };
});

app.directive("showOnLoad", function() {
  return {
    restrict : "EA",
    link : function($scope, element, attrs) {
      element.bind('load', function() {
        $scope.api.loading = false;
        $scope.$apply();
      });
    }
  }
});

app.directive('onFileSelect', function() {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var onChangeFunc = scope.$eval(attrs.onFileSelect);
      element.bind('change', onChangeFunc);
    }
  };
});

app.directive('scroller', function () {
  return {
      restrict: 'A',
      link: function ($scope, element, attrs) {
          element.on('mouseenter', function () {
            $("#view").css("overflow", "hidden");
          });
          element.on('mouseleave', function () {
            $("#view").css("overflow", "auto");
          });
      }
  };
});

app.directive('ngDrag', function(surveyCreatorAPI) {
  return {
    restrict : 'A',
    scope : {
      dragfinished : "&dfFunc"
    },
    link: function(scope, element, attrs) {
      element.on('dragstart', function(event) {
        $(".order_item").addClass("shake");
        var index = $(event.target).attr('data-index');
        var question = $(event.target).attr('data-question');
        $("#listorder").attr('ng-data-original', index);
        $("#listorder").attr('ng-data-original-t', question);
      });
      element.on('dragenter', function(event) {
        var index = $(element).data('index');
        var index_of_original = $("#listorder").attr('ng-data-original');
        var text_of_original = $("#listorder").attr('ng-data-original-t');
        $(element).css("background-color", "#ccc");
      });
      element.on('dragover', function(event) {
        $(element).css("background-color", "#ccc");
        event.preventDefault();
      });
      element.on('dragleave', function(event) {
        $(".order_item").removeAttr("style");
      });
      element.on('dragend', function(event) {
        $(".order_item").removeClass("shake");
        $(".order_item").removeAttr("style");
      });
      element.on('drop', function(event) {
        event.preventDefault();
        $(".order_item").removeClass("shake");
        $(".order_item").removeAttr("style");
        $(".order_item").blur();
        var index = $(element).attr('data-index');
        var index_of_original = $("#listorder").attr('ng-data-original');
        var text_of_original = $("#listorder").attr('ng-data-original-t');
        scope.dragfinished({index_current : index_of_original, question : text_of_original, index_new : index});
      });
    }
  };
});

app.controller("SearchController", function($scope, $rootScope, $timeout, $window, $location, userAPI, searchAPI, language) {
  $scope.api = searchAPI;
  $scope.language = language;
  $scope.closeSearch = function() {
    if (mobileCheck()) {
      $("#toolbar_header").fadeIn(500);
      $("#toolbar_notification_container").fadeIn(10);
      $("#toolbar_wishlist_container").fadeIn(10);
    }
    $("#search_input").stop().animate({width: 0}, 400 );
    setTimeout(function() {
      $("#search_input").addClass("hidden");
      $("#toolbar_search").removeClass("hidden");
    }, 300);
  };
  $scope.searchEmpty = function() {
    if ($scope.usersEmpty() && $scope.companiesEmpty() && $scope.surveysEmpty() && $scope.rewardsEmpty()) {
      return true;
    } else {
      return false;
    }
  };
  $scope.usersEmpty = function() {
    var amountOfUsers = Object.keys($scope.api.usersSearchData).length;

    if (amountOfUsers == 0) {
      return true;
    } else {
      return false;
    }
  };
  $scope.companiesEmpty = function() {
    var amountOfCompanies = Object.keys($scope.api.companiesSearchData).length;

    if (amountOfCompanies == 0) {
      return true;
    } else {
      return false;
    }
  };
  $scope.surveysEmpty = function() {
    var amountOfSurveys = Object.keys($scope.api.surveysSearchData).length;

    if (amountOfSurveys == 0) {
      return true;
    } else {
      return false;
    }
  };
  $scope.rewardsEmpty = function() {
    var amountOfRewards = Object.keys($scope.api.rewardsSearchData).length;

    if (amountOfRewards == 0) {
      return true;
    } else {
      return false;
    }
  };
  $scope.bookmark = function($index, $event) {
    $event.stopPropagation();
    var bookmarked = $scope.api.surveysSearchData[$index].bookmarked;

    if (bookmarked) {
      $scope.api.surveysSearchData[$index].bookmarked = false;
    } else {
      $scope.api.surveysSearchData[$index].bookmarked = true;
    }
  };
  $scope.showData = function(token) {
    var tokenID = "#" + token;
    $(tokenID + "_search_description").fadeOut(50);
    $(tokenID + "_search_info").fadeIn(50, function() {
      $(tokenID + "_search_info").attr("style", "display: block !important");
    });
    $(tokenID + "_search_meta").fadeIn(50, function() {
      $(tokenID + "_search_meta").attr("style", "display: block !important");
    });
  };
  $scope.hideData = function(token) {
    var tokenID = "#" + token;
    $(tokenID + "_search_description").fadeIn(50);
    $(tokenID + "_search_info").removeAttr("style");
    $(tokenID + "_search_meta").removeAttr("style");
    $(tokenID + "_search_info").fadeOut(50);
    $(tokenID + "_search_meta").fadeOut(50);
    $timeout(function() {
      $(tokenID + "_search_info").addClass("hidden");
      $(tokenID + "_search_meta").addClass("hidden");
    }, 200);
  };

  $rootScope.$on('searchOpened', function(event, args) {
    $("#search_section").removeClass("hidden");
  });
  $rootScope.$on('searchClosed', function(event, args) {
    $scope.closeSearch();
    $("#search_section").addClass("hidden");
  });
  var mobileCheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);

    return check;
  };
});

app.controller("SessionController", function($scope, $rootScope, language, metadata, userAPI) {
  $scope.language = language;
  $scope.metadata = metadata;
  $scope.user = userAPI;
  $scope.showM = true;
  $rootScope.$on("searchOpened", function() {
    $scope.showM = false;
  });
  $scope.closeSearch = function($event) {
    if ($($event.target).hasClass("search") || $($event.target.parentElement).hasClass("search")) return;
    $scope.showM = true;
    $rootScope.$emit('searchClosed', "");
  };
  var ScrollPos = 0;
  $("#view").scroll(function () {
      var CurScrollPos = $(this).scrollTop();
      if (CurScrollPos > ScrollPos) {
        $("#creator_fab").fadeOut(100);
        $(".fabb").fadeOut(100);
        if ($("#survey_c").is(":visible")) {
          $("#fab_add_ico").html("&#xE145;");
          $("#survey_c").fadeOut(100);
          $("#poll_c").fadeOut(100);
        }
      } else {
        $("#creator_fab").fadeIn(100);
        $(".fabb").fadeIn(100);
      }
      ScrollPos = CurScrollPos;
  });
  $scope.openFabs = function() {
    if ($("#survey_c").is(":visible")) {
      $("#fab_add_ico").html("&#xE145;");
      $("#survey_c").fadeOut(300);
      $("#poll_c").fadeOut(300);
    } else {
      $("#fab_add_ico").html("&#xE14C;");
      $("#survey_c").show(300);
      $("#poll_c").show(300);
    }
  };
});

app.controller("NotificationsController", function($scope, $rootScope, $timeout, userAPI, socketAPI, language) {
  $scope.user = userAPI;
  $scope.language = language;
  $scope.listener = socketAPI;
  $rootScope.$on("expandNotifications", function(event, args) {
    $("#notification_container").show();
    $(".nicon").fadeIn();
    $("#notification_container").stop().animate({height : "40%", minHeight : "300px"}, 200);
  });
  $rootScope.$on("closeNotifications", function(event, args) {
    $(".nicon").fadeOut(50);
    $("#notification_container").stop().animate({height : "0%", minHeight : "0px"}, 200).css("display", "none");
  });
  $scope.empty_notifications = function() {
    var notifications = $scope.listener.notifications.list;
    if (notifications.length == 0) {
      return true;
    } else {
      return false;
    }
  };

});

app.controller("WishlistController", function($scope, $rootScope, $timeout, $injector, $http, userAPI, socketAPI, alertsAPI, language) {
  $scope.user = userAPI;
  $scope.language = language;
  $scope.wishlist = function(index) {
    var token = $scope.user.wishlist_info[index].token;
    if($scope.user.request_pending) return;
    $scope.user.request_pending = true;
    $http({
        method : "POST",
        url : "/api/user/wishlist",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "6da2c39b7d41fd5bb5d545173db09988df3e9364",
          reward_token : token,
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = res.data;
      if (response.resultCode == "1") {
        $scope.user.request_pending = false;
        userAPI.wishlist_info.splice(index, 1);
          if ($injector.has('redeemAPI')) {
            var redeemAPI = $injector.get('redeemAPI');
            redeemAPI.rewardsAfford.forEach(function(reward, i) {
              if (reward.token == token) {
                redeemAPI.rewardsAfford[i].wishlisted = false;
                return;
              }
            });
            redeemAPI.rewardsOther.forEach(function(reward, i) {
              if (reward.token == token) {
                redeemAPI.rewardsOther[i].wishlisted = false;
                return;
              }
            });
          }
      }
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  $rootScope.$on("expandWishlist", function(event, args) {
    $("#wishlist_container").show();
    $(".nicon").fadeIn();
    $("#wishlist_container").stop().animate({height : "40%", minHeight : "300px"}, 200);
  });
  $rootScope.$on("closeWishlist", function(event, args) {
    $(".nicon").fadeOut(50);
    $("#wishlist_container").stop().animate({height : "0%", minHeight : "0px"}, 200).css("display", "none");
  });
});

app.controller("ToolbarController", function($scope, $rootScope, $timeout, $http, userAPI, searchAPI, alertsAPI, socketAPI, language, metadata) {
  $scope.route = "";
  $scope.language = language;
  $scope.metadata = metadata;
  $scope.$on('$routeChangeStart', function($event, next, current) {
    $scope.route = next["$$route"].originalPath.split("/")[next["$$route"].originalPath.split("/").length - 1];
    $scope.metadata.title = "SurveyStacks";
    $scope.metadata.description = $scope.language.dictionary.description_metadata;
    if ($scope.route == "") {
      $scope.route = "main";
    }
    if (mobileCheck()) {
      $("#toolbar_header").fadeIn(500);
      $("#toolbar_notification_container").fadeIn(10);
      $("#toolbar_wishlist_container").fadeIn(10);

    }
    $("#search_input").stop().animate({width: 0}, 400 );
    setTimeout(function() {
      $("#search_input").addClass("hidden");
      $("#toolbar_search").removeClass("hidden");
      $rootScope.$emit('searchClosed', "");
    }, 300);
  });
  $scope.user = userAPI;
  $scope.search = searchAPI;
  $scope.listener = socketAPI;
  $scope.listener.digestCycle($scope);
  $scope.notifications_expanded = false;
  $scope.wishlist_expanded = false;
  $scope.openSearch = function() {
    $("#toolbar_search").addClass("hidden");
    $("#search_input").removeClass("hidden");
    if (mobileCheck()) {
      $("#search_input").stop().animate({width: 150}, 500 );
      $("#toolbar_header").fadeOut(10);
      $("#toolbar_notification_container").fadeOut(10);
      $("#toolbar_wishlist_container").fadeOut(10);
    } else {
      $("#search_input").stop().animate({width: 250}, 500 );
    }
    $("#search_input").focus();
    $rootScope.$emit('searchOpened', "");
  };
  $scope.openNav = function() {
    $(".side-nav").toggleClass("side-nav-block");
  };
  $scope.expandNotifications = function() {
    if ($scope.notifications_expanded) {
      $scope.notifications_expanded = false;
      $rootScope.$emit('closeNotifications', "");
    } else {
      $scope.notifications_expanded = true;
      if ($scope.wishlist_expanded) {
        $rootScope.$emit('closeWishlist', "");
        $scope.wishlist_expanded = false;
      }
      $rootScope.$emit('expandNotifications', "");
      if ($scope.listener.notifications.unread != 0) {
        $http({
          method : "POST",
          url : "/api/notifications/read",
          headers: {'Content-Type': 'application/json'},
          data : {
            AUTH_TOKEN : "b85cd747c19971d2c16e17fba1302212ea87deed",
            webApp : true,
            _csrf : userAPI.csrf
          }
        }).then(function success(res) {
          var response = res.data;
          $scope.listener.notifications.unread = 0;
        }, function error(res) {
          if (res.status == 500) {
            alertsAPI.show(language.dictionary.error_snackbar);
          } else {
            alertsAPI.show(language.dictionary.error_verify_snackbar);
          }
        });
      }
    }
  };
  $scope.expandWishlist = function() {
    if ($scope.wishlist_expanded) {
      $scope.wishlist_expanded = false;
      $rootScope.$emit('closeWishlist', "");
    } else {

      if ($scope.notifications_expanded) {
        $rootScope.$emit('closeNotifications', "");
        $scope.notifications_expanded = false;
      }
      $scope.wishlist_expanded = true;
      $rootScope.$emit('expandWishlist', "");
    }
  };

  var mobileCheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);

    return check;
  };
});

app.controller("AlertsController", function($scope, language, userAPI, alertsAPI) {
  $scope.api = alertsAPI;
});

app.controller("DrawerController", function($scope, $rootScope, $timeout, $http, $route, userAPI, alertsAPI, language) {
  $scope.route = "";
  $scope.$on('$routeChangeStart', function($event, next, current) {
    $scope.route = next["$$route"].originalPath.split("/")[next["$$route"].originalPath.split("/").length - 1];
    if ($scope.route == "") {
      $scope.route = "main";
    }
  });
  $scope.user = userAPI;
  $scope.trade = function() {
    $scope.user.request_pending = true;
    $http({
        method : "POST",
        url : "/api/user/trade",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "1fb87bc55c7531db41295a99d7b4f3e5",
          webApp : true,
          _csrf : userAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == "1") {
        $scope.user.request_pending = false;
        $scope.user.stacks_amount = response.stacks;
        $scope.user.coins_amount = response.coins;
      }
    }, function error(res) {
      $scope.user.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.controller("MobileController", function($scope) {
  $scope.mobileCheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);

    return check;
  };
});

app.controller('MainController', function($scope, $rootScope, language, userAPI, surveyAPI) {
  $scope.language = language;
  $scope.api = surveyAPI;
  $scope.api.getSurveyData();
  $scope.language = language;
  $scope.user = userAPI;
  $rootScope.$on('searchOpened', function(event, args) {
    $("#main_section").addClass("hidden");
  });
  $rootScope.$on('searchClosed', function(event, args) {
    $("#main_section").removeClass("hidden");
  });
});

app.controller('AnalyticsController', function($scope, $rootScope, $timeout, userAPI, language, analyticsAPI) {
  $scope.language = language;
  $scope.user = userAPI;
  $scope.api = analyticsAPI;
  $scope.api.getAnalyticsData();
  $scope.display_board = "";
  $scope.display_board_type = "";
  $scope.display_board_obj = {};
  $scope.show_downloads = false;
  $scope.show_format = false;
  $scope.toggleDownload = function() {
    if ($scope.download_format != "") return false;
    $scope.download_as = -1;
    $scope.show_format = false;
    $scope.show_downloads = !$scope.show_downloads;
  };
  $scope.download_as = -1;
  $scope.download_format = "";
  $scope.format = function(index) {
    $scope.download_as = index;
    $scope.show_format = true;
  };
  $scope.back = function() {
    $("#viewBoard").show();
    $scope.display_board = "";
    $scope.display_board_type = "";
    $scope.download_as = -1;
    $scope.download_format = "";
    $scope.show_downloads = false;
    $scope.show_format = false;
    $scope.display_board_obj = {};
  };
  $scope.viewAnalytics = function(parent, index) {
    var array;
    var token;
    var cardColumns = {
      one : [],
      two : [],
      three : []
    };
    DestroyCharts();

    if (parent == 0) {
      array = $scope.api.analyticsData.surveys[index];
      token = array.view.token;
      $scope.display_board_obj = array.analytics;
      $scope.display_board = token;
      $scope.display_board_type = "survey";
      $scope.surveyAnalytics(array, token);
    } else if (parent == 1) {
      array = $scope.api.analyticsData.polls[index];
      token = array.view.token;
      $scope.display_board_obj = array.analytics;
      $scope.display_board = token;
      $scope.display_board_type = "poll";
      $scope.pollAnalytics(array, token);
    }

    var c = 0;
    for (var i = 0; i < array.cards.length; i++) {
      if (c === 0) {
        cardColumns.one.push(array.cards[i]);
        c = 1;
      } else if (c === 1) {
        cardColumns.two.push(array.cards[i]);
        c = 2;
      } else if (c === 2) {
        cardColumns.three.push(array.cards[i]);
        c = 0;
      }
    }

    $scope.display_board_obj.cards = cardColumns;
    $("#viewBoard").hide();
  };
  $scope.showResponses = true;
  $scope.showRespondent = true;
  $scope.showCards = true;
  $scope.toggleResponses = function() {
    $scope.showResponses = !$scope.showResponses;
  };
  $scope.toggleRespondent = function() {
    $scope.showRespondent = !$scope.showRespondent;
  };
  $scope.toggleCards = function() {
    $scope.showCards = !$scope.showCards;
  };
  $scope.pollAnalytics = function(array, token) {
    $timeout(function() {
      PieChart(token+"_pie_chart", array.analytics.questionnaire.question, array.analytics.questionnaire.options, array.analytics.questionnaire.percentages);
      array.analytics.respondents.forEach(function(analytic,i) {
        var options = [];
        analytic.options.forEach(function(option) {
          if (option == "") option = "n/a";
          if (option.length == 0) option = "n/a";
          switch (analytic.name) {
            case "sex":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.sexes[option]);
              break;
            case "ethnicity":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.ethnicities[option]);
              break;
            case "education":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.education_levels[option]);
              break;
            case "relationship_status":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.relationship_statuses[option]);
              break;
            case "employment_status":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.employment_statuses[option]);
              break;
            case "religion":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.religions[option]);
              break;
            case "age_group":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.age_groups[option]);
              break;
            case "languages":
              if (option == "n/a") return options.push(option);
              if (Array.isArray(option)) {
                var t = option.join(" & ");
                var splits = t.split("&");

                for (var i=0; i < splits.length; i++) {
                  t = t.replace(option[i], language.globals.supportedLanguages[option[i]]);
                }
                options.push(t);
                return;
              }
              options.push(language.globals.supportedLanguages[option]);
              break;
          }
        });
        PieChart(token+i+"_pie_chart", language.dictionary[analytic.name], options, analytic.percentages);
      });
    }, 100);
  };
  $scope.surveyAnalytics = function(array, token) {
    $timeout(function() {
      LineChart(token+"_daily_bar_graph", language.dictionary.daily_r, language.dictionary.days, array.analytics.questionnaire.daily, {legend:false});
      LineChart(token+"_monthly_bar_graph", language.dictionary.monthly_r, language.dictionary.months, array.analytics.questionnaire.monthly, {legend:false});
      LineChart(token+"_yearly_bar_graph", language.dictionary.yearly_r, array.analytics.questionnaire.yearly.years, array.analytics.questionnaire.yearly.values, {legend:false});
      array.analytics.graphic_questions.forEach(function(graphic, i) {
        var r = Math.floor(Math.random() * 2);
        if (r == 0) {
          DonutChart(token+i+"_graphic", graphic.question, graphic.options, graphic.percentages);
        } else {
          PieChart(token+i+"_graphic", graphic.question, graphic.options, graphic.percentages);
        }
      });
      array.analytics.respondents.forEach(function(analytic,i) {
        var options = [];
        analytic.options.forEach(function(option) {
          if (option == "") option = "n/a";
          switch (analytic.name) {
            case "sex":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.sexes[option]);
              break;
            case "ethnicity":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.ethnicities[option]);
              break;
            case "education":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.education_levels[option]);
              break;
            case "relationship_status":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.relationship_statuses[option]);
              break;
            case "employment_status":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.employment_statuses[option]);
              break;
            case "religion":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.religions[option]);
              break;
            case "age_group":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.age_groups[option]);
              break;
            case "languages":
              if (option == "n/a") return options.push(option);
              if (Array.isArray(option)) {
                var t = option.join(" & ");
                var splits = t.split("&");

                for (var i=0; i < splits.length; i++) {
                  t = t.replace(option[i], language.globals.supportedLanguages[option[i]]);
                }
                options.push(t);
                return;
              }
              options.push(language.globals.supportedLanguages[option]);
              break;
          }
        });
        PieChart(token+i+"_pie_chart", language.dictionary[analytic.name], options, analytic.percentages);
      });
    }, 100);
  };
});

app.controller('BookmarksController', function($rootScope, $scope, $timeout, language, userAPI, bookmarksAPI) {
  $scope.language = language;
  $scope.user = userAPI;
  $scope.api = bookmarksAPI;
  $scope.api.gatherBookmarks();
  $scope.emptySurveyBookmark = function() {
    if ($scope.api.surveyBookmarkData.length == 0) {
      return true;
    } else {
      return false;
    }
  };
  $scope.emptyPollBookmark = function() {
    if ($scope.api.pollBookmarkData.length == 0) {
      return true;
    } else {
      return false;
    }
  };
  $scope.emptyBookmarks = function() {
    if ($scope.emptySurveyBookmark() && $scope.emptyPollBookmark()) {
      return true;
    } else {
      return false;
    }
  };
});

app.controller('LabsController', function($scope, $rootScope, userAPI, language, labsAPI) {
  $scope.language = language;
  $scope.user = userAPI;
  $scope.api = labsAPI;
  $scope.api.retrieveData();
  $scope.templates = ["/views/quiz.html", "/views/key_needed.html", "/views/emoji.html", "/views/game.html"];
  $scope.template = "";
  $scope.stepone = true;
  $scope.steptwo = false;
  $scope.initiated = false;
  $scope.options = {
    free : true,
    checkbox : true,
    radio : true,
    boolean : true,
    single : true
  };
  $scope.typeChosen = function(index) {
    $scope.api.createdQuestionnaire.lab_option = index;
    $scope.stepone = false;
    $scope.steptwo = true;
  };
  $scope.init = function(index) {
    var a = [0,1,2,3];
    if (!a.includes($scope.api.createdQuestionnaire.lab_option)) {
      $scope.initiated = false;
      $scope.stepone = true;
      $scope.steptwo = false;
      return;
    }
    if (index !== 0 && index !== 1) return;

    $scope.initiated = true;
    $scope.steptwo = false;

    if (index == 0) {
      $scope.api.createdQuestionnaire.type = "survey";
    } else if (index == 1) {
      $scope.api.createdQuestionnaire.type = "poll";
    }

    switch($scope.api.createdQuestionnaire.lab_option) {
      case 0:
        $scope.options = {
          free : true,
          checkbox : true,
          radio : true,
          boolean : true,
          single : true
        };
        $scope.api.createdQuestionnaire.method_options = {
          show_score : false,
          out_of : null
        };
        break;
      case 1:
        $scope.options = {
          free : true,
          checkbox : true,
          radio : true,
          boolean : true,
          single : true
        };
        $scope.api.createdQuestionnaire.method_options = {
          unique_keys : false
        };
        break;
      case 2:
        $scope.options = {
          free : true,
          checkbox : true,
          radio : true,
          boolean : false,
          single : true
        };
        $scope.api.createdQuestionnaire.method_options = {};
        break;
      case 3:
        $scope.options = {
          free : true,
          checkbox : true,
          radio : true,
          boolean : true,
          single : true
        };
        $scope.api.createdQuestionnaire.method_options = {
          timed : false,
          max_time : null,
          pollQuestionsExtra : []
        };
        break;
    }
    $scope.template = $scope.templates[$scope.api.createdQuestionnaire.lab_option];
  };
  $scope.back = function() {
    $scope.api.createdQuestionnaire.lab_option = null;
    $scope.initiated = false;
    $scope.stepone = true;
    $scope.steptwo = false;
    $scope.api.createdQuestionnaire.survey = {
      "freeResponseQuestions" : [],
      "checkboxResponseQuestions" : [],
      "booleanResponseQuestions" : [],
      "radioResponseQuestions" : [],
      "menuResponseQuestions" : [],
      "order" : []
    };
    $scope.api.createdQuestionnaire.poll = {
      "question" : "",
      "options" : []
    };
  };
  $scope.category_display = "";
  $scope.sex_display = "";
  $scope.ethnicity_display = "";
  $scope.age_display = "";
  $scope.questionOptions = {
    "type" : "",
    "index" : 0
  };
  $scope.fileSelected = function(elem) {
    var spl = elem.target.value.split("\\");
    $scope.api.createdQuestionnaire.details.image_file = spl[spl.length - 1];
    $scope.$apply();
  };
  $scope.questionOptionsE = function(type, index) {
    $scope.questionOptions.type = type;
    $scope.questionOptions.index = index;
    $scope.show_options_toolbar = true;
  };
  $scope.emptyQuestionOptionsB = function($event) {
    if ($(".response_input").is(":focus") || $event.target.parentNode.className === "optionsContainer" || !$scope.show_options_toolbar) {
      return;
    }
    for (var i = 0; i < $event.originalEvent.path.length; i++) {
      if ($event.originalEvent.path[i].className === "optionsContainer") {
        return;
      }
    }
    $scope.questionOptions.type = "";
    $scope.questionOptions.index = 0;
    $scope.show_options_toolbar = false;
  };
  $scope.addFreeResponse = function() {
    var object = {};
    switch($scope.api.createdQuestionnaire.lab_option) {
      case 0:
        object = {
          "question" : "",
          "options" : {
            "pattern" : "a",
            "required" : false,
            "algorithm" : "o",
            "answer" : "Lorem Ipsum",
            "worth" : 1
          }
        };
        break;
      case 2:
        object = {
          "question" : "",
          "options" : {
            "required" : false,
          }
        };
      default:
        object = {
          "question" : "",
          "options" : {
            "pattern" : "a",
            "required" : false,
          }
        };
    }
    $scope.api.createdQuestionnaire.survey.freeResponseQuestions.push(object);
    var order_name = "fr_"+($scope.api.createdQuestionnaire.survey.freeResponseQuestions.length - 1);
    $scope.api.createdQuestionnaire.survey.order.push(order_name);
  };
  $scope.addCheckboxResponse = function() {
    var object = {};
    switch($scope.api.createdQuestionnaire.lab_option) {
      case 0:
        object = {
          "question" : "",
          "responses" : [],
          "options" : {
            "required" : false,
            "answer" : [],
            "worth" : 1
          }
        };
        break;
      default:
        object = {
          "question" : "",
          "responses" : [],
          "options" : {
            "required" : false
          }
        };
    }
    $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions.push(object);
    var order_name = "cbr_"+($scope.api.createdQuestionnaire.survey.checkboxResponseQuestions.length - 1);
    $scope.api.createdQuestionnaire.survey.order.push(order_name);
  };
  $scope.addBooleanResponse = function() {
    var object = {};
    switch($scope.api.createdQuestionnaire.lab_option) {
      case 0:
        object = {
          "question" : "",
          "options" : {
            "required" : false,
            "answer" : 0,
            "worth" : 1
          }
        };
        break;
      default:
        object = {
          "question" : "",
          "options" : {
            "required" : false
          }
        };
    }
    $scope.api.createdQuestionnaire.survey.booleanResponseQuestions.push(object);
    var order_name = "br_"+($scope.api.createdQuestionnaire.survey.booleanResponseQuestions.length - 1);
    $scope.api.createdQuestionnaire.survey.order.push(order_name);
  };
  $scope.addRadioResponse = function() {
    var object = {};
    switch($scope.api.createdQuestionnaire.lab_option) {
      case 0:
        object = {
          "question" : "",
          "responses" : [],
          "options" : {
            "required" : false,
            "answer" : 0,
            "worth" : 1
          }
        };
        break;
      default:
        object = {
          "question" : "",
          "responses" : [],
          "options" : {
            "required" : false
          }
        };
    }
    $scope.api.createdQuestionnaire.survey.radioResponseQuestions.push(object);
    var order_name = "rr_"+($scope.api.createdQuestionnaire.survey.radioResponseQuestions.length - 1);
    $scope.api.createdQuestionnaire.survey.order.push(order_name);
  };
  $scope.addMenuResponse = function() {
    var object = {};
    switch($scope.api.createdQuestionnaire.lab_option) {
      case 0:
        object = {
          "question" : "",
          "responses" : [],
          "options" : {
            "required" : false,
            "answer" : 0,
            "worth" : 1
          }
        };
        break;
      default:
        object = {
          "question" : "",
          "responses" : [],
          "options" : {
            "required" : false
          }
        };
    }
    $scope.api.createdQuestionnaire.survey.menuResponseQuestions.push(object);
    var order_name = "mr_"+($scope.api.createdQuestionnaire.survey.menuResponseQuestions.length - 1);
    $scope.api.createdQuestionnaire.survey.order.push(order_name);
  };
  $scope.addCheckboxItemResponse = function($index) {
    $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions[$index]["responses"].push("");
  };
  $scope.addRadioItemResponse = function($index) {
    $scope.api.createdQuestionnaire.survey.radioResponseQuestions[$index]["responses"].push("");
  };
  $scope.addMenuItemResponse = function($index) {
    $scope.api.createdQuestionnaire.survey.menuResponseQuestions[$index]["responses"].push("");
  };
  $scope.addAnswer = function(parentIndex, index) {
    var question = $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions[parentIndex];
    if (question.responses.length < index) return;
    var value = question.responses[index];
    var partOf = question.responses.indexOf(value);
    if (partOf > -1) {
      $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions[parentIndex].options.answer.splice(index, 1);
    } else {
      $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions[parentIndex].options.answer.push(index);
    }
  };
  $scope.removeItem = function($index, type) {
    switch (type) {
      case "fR":
        $scope.api.createdQuestionnaire.survey.freeResponseQuestions.splice($index, 1);
        var order_name = "fr_"+$index;
        var index_of_order = $scope.api.createdQuestionnaire.survey.order.indexOf(order_name)
        $scope.api.createdQuestionnaire.survey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdQuestionnaire.survey.freeResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdQuestionnaire.survey.order.length; i ++) {
            var item_prop = $scope.api.createdQuestionnaire.survey.order[i].split("_");
            if (item_prop[0] != "fr") return;

            if (item_prop[1] <= $index) return;

            var index_of_item = $scope.api.createdQuestionnaire.survey.order.indexOf($scope.api.createdQuestionnaire.survey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "fr_"+(index_of_item_i);
            $scope.api.createdQuestionnaire.survey.order.splice(index_of_item,1,newname);
          }
        }
        break;
      case "cR":
        $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions.splice($index, 1);
        var order_name = "cbr_"+$index;
        var index_of_order = $scope.api.createdQuestionnaire.survey.order.indexOf(order_name)
        $scope.api.createdQuestionnaire.survey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdQuestionnaire.survey.checkboxResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdQuestionnaire.survey.order.length; i ++) {
            var item_prop = $scope.api.createdQuestionnaire.survey.order[i].split("_");
            if (item_prop[0] != "cbr") continue;

            if (item_prop[1] <= $index) continue;

            var index_of_item = $scope.api.createdQuestionnaire.survey.order.indexOf($scope.api.createdQuestionnaire.survey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "cbr_"+(index_of_item_i);
            $scope.api.createdQuestionnaire.survey.order.splice(index_of_item,1,newname);
          }
        }
        break;
      case "bR":
        $scope.api.createdQuestionnaire.survey.booleanResponseQuestions.splice($index, 1);
        var order_name = "br_"+$index;
        var index_of_order = $scope.api.createdQuestionnaire.survey.order.indexOf(order_name)
        $scope.api.createdQuestionnaire.survey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdQuestionnaire.survey.booleanResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdQuestionnaire.survey.order.length; i ++) {
            var item_prop = $scope.api.createdQuestionnaire.survey.order[i].split("_");
            if (item_prop[0] != "br") return;

            if (item_prop[1] <= $index) return;

            var index_of_item = $scope.api.createdQuestionnaire.survey.order.indexOf($scope.api.createdQuestionnaire.survey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "br_"+(index_of_item_i);
            $scope.api.createdQuestionnaire.survey.order.splice(index_of_item,1,newname);
          }
        }
        break;
      case "rR":
        $scope.api.createdQuestionnaire.survey.radioResponseQuestions.splice($index, 1);
        var order_name = "rr_"+$index;
        var index_of_order = $scope.api.createdQuestionnaire.survey.order.indexOf(order_name)
        $scope.api.createdQuestionnaire.survey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdQuestionnaire.survey.radioResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdQuestionnaire.survey.order.length; i ++) {
            var item_prop = $scope.api.createdQuestionnaire.survey.order[i].split("_");
            if (item_prop[0] != "rr") continue;

            if (item_prop[1] <= $index) continue;

            var index_of_item = $scope.api.createdQuestionnaire.survey.order.indexOf($scope.api.createdQuestionnaire.survey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "rr_"+(index_of_item_i);
            $scope.api.createdQuestionnaire.survey.order.splice(index_of_item,1,newname);
          }
        }
        break;
      case "mR":
        $scope.api.createdQuestionnaire.survey.menuResponseQuestions.splice($index, 1);
        var order_name = "mr_"+$index;
        var index_of_order = $scope.api.createdQuestionnaire.survey.order.indexOf(order_name)
        $scope.api.createdQuestionnaire.survey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdQuestionnaire.survey.menuResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdQuestionnaire.survey.order.length; i ++) {
            var item_prop = $scope.api.createdQuestionnaire.survey.order[i].split("_");
            if (item_prop[0] != "mr") continue;

            if (item_prop[1] <= $index) continue;

            var index_of_item = $scope.api.createdQuestionnaire.survey.order.indexOf($scope.api.createdQuestionnaire.survey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "mr_"+(index_of_item_i);
            $scope.api.createdQuestionnaire.survey.order.splice(index_of_item,1,newname);
          }
        }
        break;
    }
  };
  $scope.removeResponseItem = function($parentIndex, $index, type) {
    switch (type) {
      case "cR":
        $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions[$parentIndex]["responses"].splice($index, 1);
        if ($scope.api.createdQuestionnaire.lab_option === 0) {
          $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions[$parentIndex]["options"]["answer"].splice($index, 1);
        }
        break;
      case "rR":
        $scope.api.createdQuestionnaire.survey.radioResponseQuestions[$parentIndex]["responses"].splice($index, 1);
        if ($scope.api.createdQuestionnaire.lab_option === 0) {
          delete $scope.api.createdQuestionnaire.survey.booleanResponseQuestions[$parentIndex]["options"]["answer"][$index];
        }
        break;
      case "mR":
        $scope.api.createdQuestionnaire.survey.menuResponseQuestions[$parentIndex]["responses"].splice($index, 1);
        if ($scope.api.createdQuestionnaire.lab_option === 0) {
          delete $scope.api.createdQuestionnaire.survey.menuResponseQuestions[$parentIndex]["options"]["answer"][$index];
        }
        break;
    }
  };
  $scope.expandables = {
    "category_expand" : false,
    "sex_expand" : false,
    "ethnicity_expand" : false,
    "age_expand" : false,
    "image_list_expand" : false,
    "ud_expand" : false,
    "collected" : []
  };
  $scope.collect = function(value) {
    if ($scope.expandables.collected.indexOf(value) == -1) {
      $scope.expandables.collected.push(value);
    } else {
      var ind = $scope.expandables.collected.indexOf(value);
      $scope.expandables.collected.splice(ind, 1);
    }
  };
  $scope.show_options_toolbar = false;
  $scope.categories = $scope.api.categories;
  $scope.menuDisplay = {
    "question" : "",
    "responses" : []
  };
  $scope.setCategory = function($index, value, category) {
    $scope.category_display = category;
    $scope.api.createdQuestionnaire.details["category_token"] = value;
    $("#category").addClass("spanned");
    $scope.expandables.category_expand = false;
  };
  $scope.setSex = function(value, sex) {
    $scope.sex_display = sex;
    $scope.api.createdQuestionnaire.details["sex"] = value;
    $("#sex").addClass("spanned");
    $scope.expandables.sex_expand = false;
  };
  $scope.setEthnicity = function(value, ethnicity) {
    $scope.ethnicity_display = ethnicity;
    $scope.api.createdQuestionnaire.details["ethnicity"] = value;
    $("#ethnicity").addClass("spanned");
    $scope.expandables.ethnicity_expand = false;
  };
  $scope.setAgeGroup = function(value, age) {
    $scope.age_display = age;
    $scope.api.createdQuestionnaire.details["age_group"] = value;
    $("#age_group").addClass("spanned");
    $scope.expandables.age_expand = false;
  };
  $scope.hideMenu = function() {
    $(".menu").fadeOut(100);
  };
  $scope.dragfinished = function(index_current, question, index_new) {
    $scope.api.createdQuestionnaire.survey.order.splice(index_current, 1);
    $timeout(function() {
      $scope.$apply;
    }, 20);
    if (index_current < index_new) {
      $scope.api.createdQuestionnaire.survey.order.splice(index_new, 0, question);
    } else {
      $scope.api.createdQuestionnaire.survey.order.splice(index_new, 0, question);
    }
    $timeout(function() {
      $scope.$apply;
    }, 200);
  };
  $scope.getQuestionText = function(question) {
    var split = question.split("_");
    var type = split[0];
    var index = split[1];
    var question_text;

    switch (type) {
      case "fr":
        question_text = $scope.api.createdQuestionnaire.survey.freeResponseQuestions[index].question;
        break;
      case "cbr":
        question_text = $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions[index].question;
        break;
      case "br":
        question_text = $scope.api.createdQuestionnaire.survey.booleanResponseQuestions[index].question;
        break;
      case "rr":
        question_text = $scope.api.createdQuestionnaire.survey.radioResponseQuestions[index].question;
        break;
      case "mr":
        question_text = $scope.api.createdQuestionnaire.survey.menuResponseQuestions[index].question;
        break;
    }

    if (question_text == "") {
      question_text = $scope.language.dictionary.blank;
    }

    return question_text;
  };
  $scope.submitQuestionnaire = function() {
    if ($scope.api.createdQuestionnaire.type === "survey") {
       $scope.createSurvey();
    } else if ($scope.api.createdQuestionnaire.type === "poll") {
       $scope.createPoll();
    } else {
      return;
    }
  };
  $scope.createSurvey = function() {
    var details = $scope.api.createdQuestionnaire.details;
    var methodOptions = $scope.api.createdQuestionnaire.method_options;
    var responses = language.dictionary;
    var freeResponse = $scope.api.createdQuestionnaire.survey.freeResponseQuestions;
    var checkbox = $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions;
    var boolean = $scope.api.createdQuestionnaire.survey.booleanResponseQuestions;
    var radio = $scope.api.createdQuestionnaire.survey.radioResponseQuestions;
    var single = $scope.api.createdQuestionnaire.survey.menuResponseQuestions;
    var questions = [];
    var amountedTotal = 0;
    questions = questions.concat(freeResponse, checkbox, boolean, radio, single);

    $scope.api.response = "";

    if (details.name == "") {
      $scope.api.response = responses.nameless_survey;
      return;
    } else if (details.description == "") {
      $scope.api.response = responses.descriptionless_survey;
      return;
    } else if (details.description.length > 80) {
      $scope.api.response = responses.description_toolong;
      return;
    } else if (details.category_token == "") {
      $scope.api.response = responses.categoryless_survey;
      return;
    } else if (details.sex == "") {
      $scope.api.response = responses.sexless_survey;
      return;
    } else if (details.ethnicity == "") {
      $scope.api.response = responses.ethnicityless_survey;
      return;
    } else if (details.age_group == "") {
      $scope.api.response = responses.ageless_survey;
      return;
    } else if (noQuestions(freeResponse, checkbox, boolean, radio, single)) {
      $scope.api.response = responses.no_questions;
      return;
    } else if (emptyQuestionsA(freeResponse)) {
      $scope.api.response = responses.empty_questions('fr');
      return;
    } else if (emptyQuestionsB(checkbox)) {
      $scope.api.response = responses.empty_questions('cbr');
      return;
    } else if (emptyQuestionsA(boolean)) {
      $scope.api.response = responses.empty_questions('br');
      return;
    } else if (emptyQuestionsB(radio)) {
      $scope.api.response = responses.empty_questions('rr');
      return;
    } else if (emptyQuestionsB(single)) {
      $scope.api.response = responses.empty_questions('mir');
      return;
    } else if (emptyResponses(checkbox)) {
      $scope.api.response = responses.empty_responses('cbr');
      return;
    } else if (emptyResponses(radio)) {
      $scope.api.response = responses.empty_responses('rr');
      return;
    } else if (emptyResponses(single)) {
      $scope.api.response = responses.empty_responses('mir');
      return;
    }

    switch($scope.api.createdQuestionnaire.lab_option) {
      case 0:
        if (methodOptions.out_of == "" || methodOptions.out_of === null) {
          $scope.api.response = responses.quiz_bad_total;
          return;
        }
        for (var i = 0; i < questions.length; i++) {
          amountedTotal += questions[i].options.worth;
          if (questions[i].options.worth === "") {
            $scope.api.response = responses.quiz_bad_worth;
            return;
          }
          if (questions[i].options.answer === "" || questions[i].options.answer.length == 0) {
            $scope.api.response = responses.quiz_bad_answer;
            return;
          }
        }
        if (amountedTotal != methodOptions.out_of) {
          $scope.api.response = responses.quiz_bad_amounted;
          return;
        }
        break;
      case 1:
        break;
      case 2:
        break;
      case 3:
        if (methodOptions.max_time == "" || methodOptions.max_time === null || methodOptions.max_time < 5) {
          $scope.api.response = responses.game_bad_time;
          return;
        }
        break;
      default:
        return;
    }

    $scope.api.uploading = true;
    $scope.api.createQuestionnaire();
  };
  $scope.addPollOption = function() {
    $scope.api.createdQuestionnaire.poll.options.push("");
  };
  $scope.removePollItem = function($index) {
    $scope.api.createdQuestionnaire.poll.options.splice($index, 1);
  };
  $scope.addPollQuestion = function() {
    $scope.api.createdQuestionnaire.method_options.pollQuestionsExtra.push({
      question : "",
      options : []
    });
  };
  $scope.addPollOptionQ = function(index) {
    $scope.api.createdQuestionnaire.method_options.pollQuestionsExtra[index].options.push("");
  };
  $scope.removePollQuestion = function(index) {
    $scope.api.createdQuestionnaire.method_options.pollQuestionsExtra.splice(index, 1);
  };
  $scope.removePollItemQ = function(index) {
    $scope.api.createdQuestionnaire.method_options.pollQuestionsExtra[index].options.splice($index, 1);
  };
  $scope.createPoll = function() {
    var details = $scope.api.createdQuestionnaire.details;
    var question = $scope.api.createdQuestionnaire.poll;
    var responses = language.dictionary;

    $scope.api.response = "";

    if (details.name == "") {
      $scope.api.response = responses.nameless_survey;
      return;
    } else if (details.description == "") {
      $scope.api.response = responses.descriptionless_survey;
      return;
    } else if (details.description.length > 80) {
      $scope.api.response = responses.description_toolong;
      return;
    } else if (details.category == "") {
      $scope.api.response = responses.categoryless_survey;
      return;
    } else if (details.sex == "") {
      $scope.api.response = responses.sexless_survey;
      return;
    } else if (details.ethnicity == "") {
      $scope.api.response = responses.ethnicityless_survey;
      return;
    } else if (details.age_group == "") {
      $scope.api.response = responses.ageless_survey;
      return;
    } else if (question.question == "") {
      $scope.api.response = responses.blank_pq;
      return;
    }

    for (var i = 0; i < question.responses; i++) {
      if (question.responses[i] == "") {
        $scope.api.response = responses.blank_po;
        return;
      }
    }

    $scope.api.uploading = true;
    $scope.api.createQuestionnaire();
  };
  $scope.noHide = function(event) {
    event.stopPropagation();
  }
  $scope.blank_questions = function() {
    return $scope.api.createdQuestionnaire.survey.order.length === 0;
  };
  function noQuestions(freeResponse, checkbox, boolean, radio, single) {
    if (freeResponse.length > 0) return false;
    if (checkbox.length > 0) return false;
    if (boolean.length > 0) return false;
    if (radio.length > 0) return false;
    if (single.length > 0) return false;

    return true;
  };
  function emptyQuestionsA(questionsArray) {
    for (var i = 0; i < questionsArray.length; i++) {
      if (questionsArray[i] == "") {
        return true;
      }
    }

    return false;
  };
  function emptyQuestionsB(questionsArray) {
    for (var i = 0; i < questionsArray.length; i++) {
      if (questionsArray[i].question == "") {
        return true;
      }
    }

    return false;
  };
  function emptyResponses(questionsArray) {
    for (var i = 0; i < questionsArray.length; i++) {
      if (questionsArray[i].responses.length == 0) return true;
      for (var d = 0; d < questionsArray[i].responses.length; d++) {
        if (questionsArray[i].responses[d] == "") {
          return true;
        }
      }
    }
  }
});

app.controller('ManageController', function($scope, $rootScope, userAPI, language, surveyManagerAPI) {
  $scope.language = language;
  $scope.user = userAPI;
  $scope.api = surveyManagerAPI;
  $scope.api.gatherData();
  $scope.noSurveys = function() {
    return $scope.api.surveyData.length == 0;
  };
  $scope.noPolls = function() {
    return $scope.api.pollData.length == 0;
  };
});

app.controller('PollsController', function($scope, $rootScope, $timeout, language, userAPI, pollsAPI) {
  $scope.api = pollsAPI;
  $scope.api.getPollData();
  $scope.language = language;
  $scope.user = userAPI;
});

app.controller('ProfileController', function($scope, $rootScope, language, userAPI, profileAPI) {
  $scope.user = userAPI;
  $scope.api = profileAPI;
  $scope.api.profileData();
  $scope.language = language;
  $scope.fileSelected = function() {
    var file = $("#pfpSelect")[0].files[0];
    if (file == "" || typeof file == "undefined") {
      $("#ppa").fadeOut(200);
      $scope.api.newProfilePictureData = {};
    } else {
      $("#ppa").fadeIn(100);
      $scope.api.newProfilePictureData = file;
    }
  };
  $scope.changeProfilePicure = function() {
    if (!jQuery.isEmptyObject($scope.api.newProfilePictureData)) {
      var file = $scope.api.newProfilePictureData;
      var fileType = file.type;
      var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
      if ($.inArray(fileType, ValidImageTypes) < 0) {
           $("#ppa").html(language.dictionary.not_image);
      }
      $("#ppa").html(language.dictionary.updating_progress);
      $scope.api.request_pending = true;
      $scope.api.updateProfilePicture();
    } else {
      $("#ppa").fadeOut(200);
      $("#ppa").html(language.dictionary.change_profile_picture);
    }
  };
  $scope.stackFullAmount = function() {
    $(".stackImage").css({opacity : "0.7"});
    $(".totalStacksText").css({display : "block"});
  };
  $scope.stackFullAmountOut = function() {
    $(".stackImage").removeAttr("style");
    $(".totalStacksText").removeAttr("style");
  };
  $scope.coinFullAmount = function() {
    $(".coinImage").css({opacity : "0.7"});
    $(".totalCoinsText").css({display : "block"});
  };
  $scope.coinFullAmountOut = function() {
    $(".coinImage").removeAttr("style");
    $(".totalCoinsText").removeAttr("style");
  };
});

app.controller('RedeemController', function($scope, $rootScope, userAPI, redeemAPI, language) {
  $scope.api = redeemAPI;
  $scope.api.getRewardsData();
  $scope.user = userAPI;
  $scope.language = language;
  $scope.rewardClaimer = function(index) {
    if ($scope.api.request_pending) return false;
    if ($scope.api.rewardsAfford[index].progress == 100) {
      $scope.api.claimReward(index);
    }
  };
  $scope.margins = function() {
    var w = document.getElementById('main_list').offsetWidth;
    var i = (w / 410).toString().substr(0,1);
    var margins = (w - (410 * i)) / (2 * i);
    return margins + "px";
  };
  $scope.marginsn = function(i) {
    var w = document.getElementById('main_list').offsetWidth - i;
    var d = (w / 410).toString().substr(0,1);
    var margins = (w - (410 * d)) / (2 * d);

    return margins + "px";
  };
});

app.controller('SettingsController', function($scope, $rootScope, $timeout, $cookies, userAPI, language, settingsAPI) {
  $scope.language = language;
  $scope.api = settingsAPI;
  $scope.api.getSettingsData();
  $scope.user = userAPI;
  $scope.dpc = false;
  $scope.dp = function() {
    var dc = $("#delAccountInt").val();
    if (dc == $scope.api.nu) {
      $scope.dpc = true;
    }
  };
  $scope.updateAccountInfo = function($event) {
    if ($scope.api.email == "" || $("#email_ai").hasClass("ng-invalid-email")) {
      $("#email_ai").css({"border" : "1px solid red"});
      return false;
    } else if ($scope.api.phone_number == "") {
      $("#phone_number_ai").css({"border" : "1px solid red"});
      return false;
    }

    if (!$scope.request_pending) {
      if ($scope.api.email != "" && $scope.api.phone_number != "") {
        $(".aii").removeAttr("style");
        $scope.api.request_pending = true;
        $scope.api.updateAccountInfo();
      }
    }
  };
  $scope.updateContactInfo = function($event) {
    if ($("#email_ci").hasClass("ng-invalid-email")) {
      $("#email_ci").css({"border" : "1px solid red"});
      return false;
    }

    if (!$scope.request_pending) {
      if ($scope.api.email != "" && $scope.api.phone_number != "") {
        $(".cii").removeAttr("style");
        $scope.api.request_pending = true;
        $scope.api.updateContactInfo();
      }
    }
  };
  $scope.updateLabsMem = function($event) {
    if (!$scope.request_pending) {
      $scope.api.request_pending = true;
      $scope.api.updateLabsMem();
    }
  };
  $scope.updatePassword = function($event) {
    if ($scope.api.current_password == "") {
      $("#current_password_pi").css({"border" : "1px solid red"});
      return false;
    } else if ($scope.api.new_password == "") {
      $("#new_password_pi").css({"border" : "1px solid red"});
      return false;
    }

    if (!$scope.request_pending) {
      if ($scope.api.email != "" && $scope.api.phone_number != "") {
        $(".pii").removeAttr("style");
        $scope.api.request_pending = true;
        $scope.api.updatePassword();
      }
    }
  };
  $scope.updateNotifications = function($event) {
    if (!$scope.request_pending) {
      if ($scope.api.email != "" && $scope.api.phone_number != "") {
        $scope.api.request_pending = true;
        $scope.api.updateNotifications();
      }
    }
  };
  $rootScope.$on('searchOpened', function(event, args) {
    $("#main_section").addClass("hidden");
  });
  $rootScope.$on('searchClosed', function(event, args) {
    $("#main_section").removeClass("hidden");
  });
  $scope.desktopNotifications = function($event) {
    if (!Notification) {
      alert("Cannot Use Notifications with Current Browser");
      return;
    }

    if (Notification.permission === "denied") {
      $timeout(function() {
        $($event.target).prop('checked', false);
      }, 150);
      return;
    };

    if (Notification.permission !== "granted") {
      Notification.requestPermission().then(function(result) {
        if (result === 'denied') {
          $timeout(function() {
            $($event.target).prop('checked', false);
          }, 150);
          return;
        };

        if (result === 'default') {
          $timeout(function() {
            $($event.target).prop('checked', false);
          }, 150);
          return;
        }
        $cookies.put("dnotif", true);
      });
    };

    var checked = $($event.target).data().boxChecked;

    if (checked || checked == "true") {
      $($event.target).data("box-checked", false);
      $cookies.put("dnotif", false);
      return;
    }

    if (!checked || checked == "false") {
      $($event.target).data("box-checked", true);
      $cookies.put("dnotif", true);
      return;
    }
  };
});

app.controller('TeamsController', function($scope, $rootScope, language, userAPI, teamsAPI) {
  $scope.user = userAPI;
  $scope.api = teamsAPI;
  $scope.api.getTeams();
  $scope.language = language;
  $scope.teamCode = "";
  $scope.joinTeam = function() {
    $scope.api.join($scope, $scope.teamCode);
  };
});

app.controller('ProfilePublicController', function($scope, $rootScope, language, userAPI, profilePublicAPI) {
  $scope.user = userAPI;
  $scope.api = profilePublicAPI;
  $scope.api.profileData();
  $scope.language = language;
  $scope.stackFullAmount = function() {
    $(".stackImage").css({opacity : "0.7"});
    $(".totalStacksText").css({display : "block"});
  };
  $scope.stackFullAmountOut = function() {
    $(".stackImage").removeAttr("style");
    $(".totalStacksText").removeAttr("style");
  };
  $scope.coinFullAmount = function() {
    $(".coinImage").css({opacity : "0.7"});
    $(".totalCoinsText").css({display : "block"});
  };
  $scope.coinFullAmountOut = function() {
    $(".coinImage").removeAttr("style");
    $(".totalCoinsText").removeAttr("style");
  };
});

app.controller('SurveyCreatorController', function($scope, $rootScope, $timeout, userAPI, surveyCreatorAPI, language) {
  $scope.language = language;
  $scope.user = userAPI;
  $scope.api = surveyCreatorAPI;
  $scope.api.retrieveData();
  $scope.category_display = "";
  $scope.sex_display = "";
  $scope.ethnicity_display = "";
  $scope.age_display = "";
  $scope.questionOptions = {
    "type" : "",
    "index" : 0
  };
  $scope.fileSelected = function(elem) {
    var spl = elem.target.value.split("\\");
    $scope.api.createdSurvey.surveyDetails.image_file = spl[spl.length - 1];
    $scope.$apply();
  };
  $scope.questionOptionsE = function(type, index) {
    $scope.questionOptions.type = type;
    $scope.questionOptions.index = index;
    $scope.show_options_toolbar = true;
  };
  $scope.emptyQuestionOptionsB = function($event) {
    if ($(".response_input").is(":focus") || $event.target.parentNode.className === "optionsContainer" || !$scope.show_options_toolbar) {
      return;
    }
    for (var i = 0; i < $event.originalEvent.path.length; i++) {
      if ($event.originalEvent.path[i].className === "optionsContainer") {
        return;
      }
    }
    $scope.questionOptions.type = "";
    $scope.questionOptions.index = 0;
    $scope.show_options_toolbar = false;
  };
  $scope.addFreeResponse = function() {
    var object = {
      "question" : "",
      "options" : {
        "pattern" : "a",
        "required" : false
      }
    };
    $scope.api.createdSurvey.freeResponseQuestions.push(object);
    var order_name = "fr_"+($scope.api.createdSurvey.freeResponseQuestions.length - 1);
    $scope.api.createdSurvey.order.push(order_name);
  };
  $scope.addCheckboxResponse = function() {
    var object = {
      "question" : "",
      "responses" : [],
      "options" : {
        "required" : false
      }
    };
    $scope.api.createdSurvey.checkboxResponseQuestions.push(object);
    var order_name = "cbr_"+($scope.api.createdSurvey.checkboxResponseQuestions.length - 1);
    $scope.api.createdSurvey.order.push(order_name);
  };
  $scope.addBooleanResponse = function() {
    var object = {
      "question" : "",
      "responses" : [],
      "options" : {
        "required" : false
      }
    };
    $scope.api.createdSurvey.booleanResponseQuestions.push(object);
    var order_name = "br_"+($scope.api.createdSurvey.booleanResponseQuestions.length - 1);
    $scope.api.createdSurvey.order.push(order_name);
  };
  $scope.addRadioResponse = function() {
    var object = {
      "question" : "",
      "responses" : [],
      "options" : {
        "required" : false
      }
    };
    $scope.api.createdSurvey.radioResponseQuestions.push(object);
    var order_name = "rr_"+($scope.api.createdSurvey.radioResponseQuestions.length - 1);
    $scope.api.createdSurvey.order.push(order_name);
  };
  $scope.addMenuResponse = function() {
    var object = {
      "question" : "",
      "responses" : [],
      "options" : {
        "required" : false
      }
    };
    $scope.api.createdSurvey.menuResponseQuestions.push(object);
    var order_name = "mr_"+($scope.api.createdSurvey.menuResponseQuestions.length - 1);
    $scope.api.createdSurvey.order.push(order_name);
  };
  $scope.addCheckboxItemResponse = function($index) {
    $scope.api.createdSurvey.checkboxResponseQuestions[$index]["responses"].push("");
  };
  $scope.addRadioItemResponse = function($index) {
    $scope.api.createdSurvey.radioResponseQuestions[$index]["responses"].push("");
  };
  $scope.addMenuItemResponse = function($index) {
    $scope.api.createdSurvey.menuResponseQuestions[$index]["responses"].push("");
  };
  $scope.removeItem = function($index, type) {
    switch (type) {
      case "fR":
        $scope.api.createdSurvey.freeResponseQuestions.splice($index, 1);
        var order_name = "fr_"+$index;
        var index_of_order = $scope.api.createdSurvey.order.indexOf(order_name)
        $scope.api.createdSurvey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdSurvey.freeResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdSurvey.order.length; i ++) {
            var item_prop = $scope.api.createdSurvey.order[i].split("_");
            if (item_prop[0] != "fr") return;

            if (item_prop[1] <= $index) return;

            var index_of_item = $scope.api.createdSurvey.order.indexOf($scope.api.createdSurvey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "fr_"+(index_of_item_i);
            $scope.api.createdSurvey.order.splice(index_of_item,1,newname);
          }
        }
        break;
      case "cR":
        $scope.api.createdSurvey.checkboxResponseQuestions.splice($index, 1);
        var order_name = "cbr_"+$index;
        var index_of_order = $scope.api.createdSurvey.order.indexOf(order_name)
        $scope.api.createdSurvey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdSurvey.checkboxResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdSurvey.order.length; i ++) {
            var item_prop = $scope.api.createdSurvey.order[i].split("_");
            if (item_prop[0] != "cbr") continue;

            if (item_prop[1] <= $index) continue;

            var index_of_item = $scope.api.createdSurvey.order.indexOf($scope.api.createdSurvey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "cbr_"+(index_of_item_i);
            $scope.api.createdSurvey.order.splice(index_of_item,1,newname);
          }
        }
        break;
      case "bR":
        $scope.api.createdSurvey.booleanResponseQuestions.splice($index, 1);
        var order_name = "br_"+$index;
        var index_of_order = $scope.api.createdSurvey.order.indexOf(order_name)
        $scope.api.createdSurvey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdSurvey.booleanResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdSurvey.order.length; i ++) {
            var item_prop = $scope.api.createdSurvey.order[i].split("_");
            if (item_prop[0] != "br") return;

            if (item_prop[1] <= $index) return;

            var index_of_item = $scope.api.createdSurvey.order.indexOf($scope.api.createdSurvey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "br_"+(index_of_item_i);
            $scope.api.createdSurvey.order.splice(index_of_item,1,newname);
          }
        }
        break;
      case "rR":
        $scope.api.createdSurvey.radioResponseQuestions.splice($index, 1);
        var order_name = "rr_"+$index;
        var index_of_order = $scope.api.createdSurvey.order.indexOf(order_name)
        $scope.api.createdSurvey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdSurvey.radioResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdSurvey.order.length; i ++) {
            var item_prop = $scope.api.createdSurvey.order[i].split("_");
            if (item_prop[0] != "rr") continue;

            if (item_prop[1] <= $index) continue;

            var index_of_item = $scope.api.createdSurvey.order.indexOf($scope.api.createdSurvey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "rr_"+(index_of_item_i);
            $scope.api.createdSurvey.order.splice(index_of_item,1,newname);
          }
        }
        break;
      case "mR":
        $scope.api.createdSurvey.menuResponseQuestions.splice($index, 1);
        var order_name = "mr_"+$index;
        var index_of_order = $scope.api.createdSurvey.order.indexOf(order_name)
        $scope.api.createdSurvey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdSurvey.menuResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdSurvey.order.length; i ++) {
            var item_prop = $scope.api.createdSurvey.order[i].split("_");
            if (item_prop[0] != "mr") continue;

            if (item_prop[1] <= $index) continue;

            var index_of_item = $scope.api.createdSurvey.order.indexOf($scope.api.createdSurvey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "mr_"+(index_of_item_i);
            $scope.api.createdSurvey.order.splice(index_of_item,1,newname);
          }
        }
        break;
    }
  };
  $scope.removeResponseItem = function($parentIndex, $index, type) {
    switch (type) {
      case "cR":
        $scope.api.createdSurvey.checkboxResponseQuestions[$parentIndex]["responses"].splice($index, 1);
        break;
      case "rR":
        $scope.api.createdSurvey.radioResponseQuestions[$parentIndex]["responses"].splice($index, 1);
        break;
      case "mR":
        $scope.api.createdSurvey.menuResponseQuestions[$parentIndex]["responses"].splice($index, 1);
        break;
    }
  };
  $scope.expandables = {
    "category_expand" : false,
    "sex_expand" : false,
    "ethnicity_expand" : false,
    "age_expand" : false,
    "image_list_expand" : false,
    "ud_expand" : false,
    "billing_country_expand" : false,
    "billing_states_expand" : false,
    "collected" : []
  };
  $scope.collect = function(value) {
    if ($scope.expandables.collected.indexOf(value) == -1) {
      $scope.expandables.collected.push(value);
    } else {
      var ind = $scope.expandables.collected.indexOf(value);
      $scope.expandables.collected.splice(ind, 1);
    }
  };
  $scope.show_options_toolbar = false;
  $scope.categories = $scope.api.categories;
  $scope.states = {};
  $scope.state_display = "";
  $scope.country_display = "";
  $scope.menuDisplay = {
    "question" : "",
    "responses" : []
  };
  $scope.freeSurvey = function($event) {
    if ($scope.api.createdSurvey.paymentDetails.freeSurvey) {
      $scope.api.createdSurvey.paymentDetails.order_total = language.dictionary.zero_zero;
    } else {
      $scope.api.createdSurvey.paymentDetails.order_total = $scope.api.data.one_survey;
    }
  };
  $scope.setCategory = function($index, value, category) {
    $scope.category_display = category;
    $scope.api.createdSurvey.surveyDetails["category_token"] = value;
    $("#category").addClass("spanned");
    $scope.expandables.category_expand = false;
  };
  $scope.setSex = function(value, sex) {
    $scope.sex_display = sex;
    $scope.api.createdSurvey.surveyDetails["sex"] = value;
    $("#sex").addClass("spanned");
    $scope.expandables.sex_expand = false;
  };
  $scope.setEthnicity = function(value, ethnicity) {
    $scope.ethnicity_display = ethnicity;
    $scope.api.createdSurvey.surveyDetails["ethnicity"] = value;
    $("#ethnicity").addClass("spanned");
    $scope.expandables.ethnicity_expand = false;
  };
  $scope.setAgeGroup = function(value, age) {
    $scope.age_display = age;
    $scope.api.createdSurvey.surveyDetails["age_group"] = value;
    $("#age_group").addClass("spanned");
    $scope.expandables.age_expand = false;
  };
  $scope.setCountry = function(code) {
    if ($scope.api.createdSurvey.paymentDetails.cardDetails["billingCountry"] == code) {
      $scope.expandables.billing_country_expand = false;
      return false;
    }

    switch (code) {
      case "US":
        $scope.states = language.globals.US_states;
        break;
      case "MX":
        $scope.states = language.globals.MX_states;
        break;
      case "CA":
        $scope.states = language.globals.CA_states;
        break;
    }
    $("#country").addClass("spanned");
    $scope.api.createdSurvey.paymentDetails.cardDetails["billingState"] = "";
    $("#state").removeClass("spanned");
    $scope.api.createdSurvey.paymentDetails.cardDetails["billingCountry"] = code;
    $scope.country_display = language.globals.supportedCountries[code];
    $scope.expandables.billing_country_expand = false;
    $scope.state_display = "";
  };
  $scope.setState = function(code) {
    if ($scope.api.createdSurvey.paymentDetails.cardDetails["billingState"] == code) {
      $scope.expandables.billing_states_expand = false;
      return false;
    }

    $scope.api.createdSurvey.paymentDetails.cardDetails["billingState"] = code;
    $scope.state_display = $scope.states[code];
    $("#state").addClass("spanned");
    $scope.expandables.billing_states_expand = false;
  };
  $scope.hideMenu = function() {
    $(".menu").fadeOut(100);
  };
  $scope.dragfinished = function(index_current, question, index_new) {
    $scope.api.createdSurvey.order.splice(index_current, 1);
    $timeout(function() {
      $scope.$apply;
    }, 20);
    if (index_current < index_new) {
      $scope.api.createdSurvey.order.splice(index_new, 0, question);
    } else {
      $scope.api.createdSurvey.order.splice(index_new, 0, question);
    }
    $timeout(function() {
      $scope.$apply;
    }, 200);
  };
  $scope.getQuestionText = function(question) {
    var split = question.split("_");
    var type = split[0];
    var index = split[1];
    var question_text;

    switch (type) {
      case "fr":
        question_text = $scope.api.createdSurvey.freeResponseQuestions[index].question;
        break;
      case "cbr":
        question_text = $scope.api.createdSurvey.checkboxResponseQuestions[index].question;
        break;
      case "br":
        question_text = $scope.api.createdSurvey.booleanResponseQuestions[index].question;
        break;
      case "rr":
        question_text = $scope.api.createdSurvey.radioResponseQuestions[index].question;
        break;
      case "mr":
        question_text = $scope.api.createdSurvey.menuResponseQuestions[index].question;
        break;
    }

    if (question_text == "") {
      question_text = $scope.language.dictionary.blank;
    }

    return question_text;
  };
  $scope.createSurvey = function() {
    var details = $scope.api.createdSurvey.surveyDetails;
    var responses = language.dictionary;
    var freeResponse = $scope.api.createdSurvey.freeResponseQuestions;
    var checkbox = $scope.api.createdSurvey.checkboxResponseQuestions;
    var boolean = $scope.api.createdSurvey.booleanResponseQuestions;
    var radio = $scope.api.createdSurvey.radioResponseQuestions;
    var single = $scope.api.createdSurvey.menuResponseQuestions;
    var payment = $scope.api.createdSurvey.paymentDetails;

    $scope.api.response = "";

    if (details.name == "") {
      $scope.api.response = responses.nameless_survey;
      return;
    } else if (details.description == "") {
      $scope.api.response = responses.descriptionless_survey;
      return;
    } else if (details.description.length > 80) {
      $scope.api.response = responses.description_toolong;
      return;
    } else if (details.category_token == "") {
      $scope.api.response = responses.categoryless_survey;
      return;
    } else if (details.sex == "") {
      $scope.api.response = responses.sexless_survey;
      return;
    } else if (details.ethnicity == "") {
      $scope.api.response = responses.ethnicityless_survey;
      return;
    } else if (details.age_group == "") {
      $scope.api.response = responses.ageless_survey;
      return;
    } else if (noQuestions(freeResponse, checkbox, boolean, radio, single)) {
      $scope.api.response = responses.no_questions;
      return;
    } else if (emptyQuestionsA(freeResponse)) {
      $scope.api.response = responses.empty_questions('fr');
      return;
    } else if (emptyQuestionsB(checkbox)) {
      $scope.api.response = responses.empty_questions('cbr');
      return;
    } else if (emptyQuestionsA(boolean)) {
      $scope.api.response = responses.empty_questions('br');
      return;
    } else if (emptyQuestionsB(radio)) {
      $scope.api.response = responses.empty_questions('rr');
      return;
    } else if (emptyQuestionsB(single)) {
      $scope.api.response = responses.empty_questions('mir');
      return;
    } else if (emptyResponses(checkbox)) {
      $scope.api.response = responses.empty_responses('cbr');
      return;
    } else if (emptyResponses(radio)) {
      $scope.api.response = responses.empty_responses('rr');
      return;
    } else if (emptyResponses(single)) {
      $scope.api.response = responses.empty_responses('mir');
      return;
    }

    if (payment.cardSaved && !$scope.api.userData.payment_saved) {
      $scope.api.response = responses.no_payment_saved;
      return;
    }

    if (!payment.cardSaved && !payment.freeSurvey) {
      for (var detail in payment.cardDetails) {
        if (payment.cardDetails[detail] == "") {
          $scope.api.response = language.dictionary.billing_invalid;
          return;
        }
      }
    }

    if (!payment.freeSurvey) {
      if (payment.cardDetails.cardSecurityCode == "") {
        $scope.api.response = responses.empty_security_code;
        return;
      }
    }

    $scope.api.uploading = true;
    $("#place_order").html(language.dictionary.loading);
    $("#place_order").prop("disabled",true);
    $scope.api.createSurvey();
  };
  $scope.noHide = function(event) {
    event.stopPropagation();
  }
  $scope.blank_questions = function() {
    return $scope.api.createdSurvey.order.length === 0;
  };
  function noQuestions(freeResponse, checkbox, boolean, radio, single) {
    if (freeResponse.length > 0) return false;
    if (checkbox.length > 0) return false;
    if (boolean.length > 0) return false;
    if (radio.length > 0) return false;
    if (single.length > 0) return false;

    return true;
  };
  function emptyQuestionsA(questionsArray) {
    for (var i = 0; i < questionsArray.length; i++) {
      if (questionsArray[i] == "") {
        return true;
      }
    }

    return false;
  };
  function emptyQuestionsB(questionsArray) {
    for (var i = 0; i < questionsArray.length; i++) {
      if (questionsArray[i].question == "") {
        return true;
      }
    }

    return false;
  };
  function emptyResponses(questionsArray) {
    for (var i = 0; i < questionsArray.length; i++) {
      for (var d = 0; d < questionsArray[i].responses.length; d++) {
        if (questionsArray[i].responses[d] == "") {
          return true;
        }
      }
    }
  }
});

app.controller('PollCreatorController', function($scope, $rootScope, $timeout, userAPI, pollCreatorAPI, language) {
  $scope.language = language;
  $scope.user = userAPI;
  $scope.api = pollCreatorAPI;
  $scope.api.retrieveCategories();
  $scope.expandables = {
    "category_expand" : false,
    "sex_expand" : false,
    "ethnicity_expand" : false,
    "age_expand" : false,
    "image_list_expand" : false,
    "ud_expand" : false,
    "collected" : []
  };
  $scope.collect = function(value) {
    if ($scope.expandables.collected.indexOf(value) == -1) {
      $scope.expandables.collected.push(value);
    } else {
      var ind = $scope.expandables.collected.indexOf(value);
      $scope.expandables.collected.splice(ind, 1);
    }
  };
  $scope.menuDisplay = {
    "question" : "",
    "responses" : []
  };
  $scope.fileSelected = function(elem) {
    var spl = elem.target.value.split("\\");
    $scope.api.createdPoll.pollDetails.image_file = spl[spl.length - 1];
    $scope.$apply();
  };
  $scope.setCategory = function($index, value, category) {
    $scope.category_display = category;
    $scope.api.createdPoll.pollDetails["category_token"] = value;
    $("#category").addClass("spanned");
    $scope.expandables.category_expand = false;
  };
  $scope.setSex = function(value, sex) {
    $scope.sex_display = sex;
    $scope.api.createdPoll.pollDetails["sex"] = value;
    $("#sex").addClass("spanned");
    $scope.expandables.sex_expand = false;
  };
  $scope.setEthnicity = function(value, ethnicity) {
    $scope.ethnicity_display = ethnicity;
    $scope.api.createdPoll.pollDetails["ethnicity"] = value;
    $("#ethnicity").addClass("spanned");
    $scope.expandables.ethnicity_expand = false;
  };
  $scope.setAgeGroup = function(value, age) {
    $scope.age_display = age;
    $scope.api.createdPoll.pollDetails["age_group"] = value;
    $("#age_group").addClass("spanned");
    $scope.expandables.age_expand = false;
  };
  $scope.addPollOption = function() {
    $scope.api.createdPoll.pollQuestion.options.push("");
  };
  $scope.removePollItem = function($index) {
    $scope.api.createdPoll.pollQuestion.options.splice($index, 1);
  };
  $scope.createPoll = function() {
    var details = $scope.api.createdPoll.pollDetails;
    var question = $scope.api.createdPoll.pollQuestion;
    var responses = language.dictionary;

    $scope.api.response = "";

    if (details.name == "") {
      $scope.api.response = responses.nameless_survey;
      return;
    } else if (details.description == "") {
      $scope.api.response = responses.descriptionless_survey;
      return;
    } else if (details.description.length > 80) {
      $scope.api.response = responses.description_toolong;
      return;
    } else if (details.category == "") {
      $scope.api.response = responses.categoryless_survey;
      return;
    } else if (details.sex == "") {
      $scope.api.response = responses.sexless_survey;
      return;
    } else if (details.ethnicity == "") {
      $scope.api.response = responses.ethnicityless_survey;
      return;
    } else if (details.age_group == "") {
      $scope.api.response = responses.ageless_survey;
      return;
    } else if (question.question == "") {
      $scope.api.response = responses.blank_pq;
      return;
    }

    for (var i = 0; i < question.responses; i++) {
      if (question.responses[i] == "") {
        $scope.api.response = responses.blank_po;
        return;
      }
    }

    $scope.api.uploading = true;
    $("#place_order").html(language.dictionary.loading);
    $("#place_order").prop("disabled",true);

    $scope.api.createPoll();
  };
  $scope.noHide = function(event) {
    event.stopPropagation();
  }
});

app.controller('EditorController', function($scope, $rootScope, $timeout, userAPI, editorAPI, language, alertsAPI) {
  $scope.language = language;
  $scope.user = userAPI;
  $scope.api = editorAPI;
  $scope.api.getEditorData();
  $scope.alert = alertsAPI;
  $scope.add = function() {
    $scope.api.questionnaireData.details.sc_update += 10;
    $scope.api.edit.asm += 1;
  };
  $scope.remove = function() {
    $scope.api.questionnaireData.details.sc_update -= 10;
    $scope.api.edit.asm -= 1;
  };
  $scope.addK = function() {
    if (typeof $scope.api.edit.keys == "undefined") {
      $scope.api.edit.keys = 0;
    }
    $scope.api.edit.keys += 2;
  };
  $scope.removeK = function() {
    $scope.api.edit.keys -= 2;
    if ($scope.api.edit.keys < 0) {
      $scope.api.edit.keys = 0;
    }
  };
  $scope.delete = {
    deleting : false,
    text : ""
  };
  $scope.deletingF = function() {
    if ($scope.delete.text === $scope.api.questionnaireData.details.name) {
      $scope.api.edit.delete = true;
      $scope.api.request_pending = true;
      $scope.api.editSurvey();
    } else {
      $scope.delete.deleting = !$scope.delete.deleting;
    }
  };
  $scope.update = function() {
    $scope.api.request_pending = true;
    $scope.api.editSurvey();
  };
  $scope.deleteK = function() {
    if ($scope.delete.text == $scope.api.questionnaireData.details.name) {
      $("#deleteI").fadeOut(500);
      $("#delete").focus();
      $timeout(function() {
        $scope.delete.deleting = !$scope.delete.deleting;
      }, 500);
    }
  };
});

app.controller('ReceiptController', function($scope, $rootScope, $timeout, userAPI, receiptAPI, language, alertsAPI) {
  $scope.language = language;
  $scope.user = userAPI;
  $scope.api = receiptAPI;
  $scope.api.getReceiptData();
  $scope.alert = alertsAPI;
});

app.controller('PreferencesSurveyController', function($scope, $rootScope, $timeout, userAPI, preferencesSurveyAPI, language, alertsAPI) {
  $scope.language = language;
  $scope.user = userAPI;
  $scope.api = preferencesSurveyAPI;
  $scope.api.getPrefData();
  $scope.alert = alertsAPI;
    $scope.optionChecked = function(ioa, value, event) {
    var array;
    if (ioa == "0") {
      array = $scope.api.userAnswers.categories;
    } else if (ioa == "1") {
      array = $scope.api.userAnswers.companies;
    }

    if (event.target.checked) {
      array.push(value);
    } else {
      var ind = array.indexOf(value);
      array.splice(ind, 1);
    }
  };
  $scope.validateSurvey = function() {
    $scope.api.request_pending = true;
    $scope.api.submitSurvey();
  };
});

app.controller('AboutSurveyController', function($scope, $rootScope, $timeout, userAPI, aboutSurveyAPI, language, alertsAPI) {
  $scope.language = language;
  $scope.user = userAPI;
  $scope.api = aboutSurveyAPI;
  $scope.api.getAboutData();
  $scope.alert = alertsAPI;
  $scope.optionChecked = function(ioa, value, event) {
    var array;
    if (ioa == 0) {
      array = $scope.api.userAnswers.ethnicity;
    } else if (ioa == 1) {
      array = $scope.api.userAnswers.lang;
    }

    if (event.target.checked) {
      array.push(value);
    } else {
      var ind = array.indexOf(value);
      array.splice(ind, 1);
    }
  };
  $scope.validateSurvey = function() {
    $scope.api.request_pending = true;
    $scope.api.submitSurvey();
  };
});

app.controller('PaymentSurveyController', function($scope, $rootScope, $timeout, userAPI, paymentSurveyAPI, language, alertsAPI) {
  $scope.language = language;
  $scope.user = userAPI;
  $scope.api = paymentSurveyAPI;
  $scope.api.getPaymentData();
  $scope.alert = alertsAPI;
  $scope.optionChecked = function(parent, index, event) {
    if (event.target.checked) {
      $scope.api.userAnswers[parent].push(index);
    } else {
      var ind = $scope.api.userAnswers[parent].indexOf(index);
      $scope.api.userAnswers[parent].splice(ind, 1);
    }
  };
  $scope.states = function() {
    var country = $scope.api.userAnswers.country+"_states";
    return $scope.language.globals[country];
  };
  $scope.statesInit = function() {
    $scope.api.userAnswers.state = "";
  };
  $scope.validateSurvey = function() {
    for (var answer in $scope.api.userAnswers) {
      if ($scope.api.userAnswers[answer] == "") {
        $scope.api.response = language.dictionary.blank_required;
        return;
      }
    }

    $scope.api.response = "";
    $scope.api.submitSurvey();
  };
});

app.controller('PollAnswerController', function($scope, $rootScope, $timeout, $interval, userAPI, pollAnswerAPI, language, alertsAPI) {
  $scope.language = language;
  $scope.user = userAPI;
  $scope.api = pollAnswerAPI;
  $scope.alert = alertsAPI;
  $scope.countdown = 5;
  $scope.gameTime = 0;
  $scope.api.getPollData(function(data) {
    $timeout(function() {
      $scope.api.loading = false;
      if ($scope.api.pollDetails.answered || !$scope.api.pollDetails.available || !$scope.api.pollDetails.targeted) {
        $scope.countdown = -1;
        $scope.gameTime = null;
        return;
      }
      if (data.countdown) {
        $scope.gameTime = $scope.api.pollDetails.labs.time;
        var countdown = $interval(function() {
          $scope.countdown -= 1;
          if ($scope.countdown == 0) {
            if (data.timed) {
              $scope.api.labsAnswers.elapsed = performance.now();
            }
            $scope.startGame();
            $interval.cancel(countdown);
            $scope.countdown -= 1;
            return;
          }
        }, 1000);
      }
    }, 1000);
  });
  $scope.game;
  $scope.startGame = function() {
    $scope.game = $interval(function() {
      $scope.gameTime -= 1;
      if ($scope.gameTime == 0) {
        $interval.cancel($scope.game);
        $scope.currentAdditionalQuestion = null;
        $scope.api.submitPoll();
        return;
      }
    }, 1000);
  };
  $scope.$on('$destroy',function(){
    if($scope.game)
        $interval.cancel($scope.game);
  });
  $scope.currentAdditionalQuestion = null;
  $scope.voted = null;
  $scope.vote = function($index) {
    if ($scope.api.pollDetails.answered || $scope.api.request_pending) {
      return false;
    }

    $scope.voted = $index;

    if ($scope.api.pollDetails.labs.questions) {
      $scope.currentAdditionalQuestion = 0;
      return;
    }

    $scope.api.request_pending = true;
    $scope.api.submitPoll($index);
  };
  $scope.voteAdditional = function($index) {
    if ($scope.api.pollDetails.answered || $scope.api.request_pending) {
      return false;
    }

    $scope.api.labsAnswers.responses[$scope.currentAdditionalQuestion] = $index;

    if ($scope.api.pollDetails.labs.questions.length - 1 != $scope.currentAdditionalQuestion) {
      $scope.currentAdditionalQuestion += 1;
      return;
    }

    $scope.api.request_pending = true;
    $scope.currentAdditionalQuestion = null;
    $interval.cancel($scope.game);
    $scope.api.submitPoll($scope.voted);
  };
});

app.controller('SurveyAnswerController', function($scope, $rootScope, $timeout, $interval, userAPI, surveyAnswerAPI, language, alertsAPI) {
  $scope.language = language;
  $scope.user = userAPI;
  $scope.alert = alertsAPI;
  $scope.api = surveyAnswerAPI;
  $scope.api.getSurveyData(function(data) {
    $timeout(function() {
      if ($scope.api.surveyDetails.answered || !$scope.api.surveyDetails.available || !$scope.api.surveyDetails.targeted) {
        $scope.countdown = 0;
        return;
      }
      if (data.countdown) {
        $scope.gameTime = $scope.api.surveyDetails.labs.time;
        var countdown = $interval(function() {
          $scope.countdown -= 1;
          if ($scope.countdown == 0) {
            if (data.timed) {
              $scope.api.labsAnswers.elapsed = performance.now();
            }
            $scope.startGame();
            $interval.cancel(countdown);
            $scope.countdown -= 1;
            return;
          }
        }, 1000);
      }
      $scope.api.loading = false;
    }, 1000);
  });
  $scope.game;
  $scope.startGame = function() {
    $scope.game = $interval(function() {
      $scope.gameTime -= 1;
      if ($scope.gameTime == 0) {
        $interval.cancel($scope.game);
        $scope.api.submitSurvey();
        return;
      }
    }, 1000);
  };
  $scope.$on('$destroy',function(){
    $interval.cancel($scope.game);
  });
  var emojiRgx = "^[\u{1f300}-\u{1f5ff}\u{1f900}-\u{1f9ff}\u{1f600}-\u{1f64f}\u{1f680}-\u{1f6ff}\u{2600}-\u{26ff}\u{2700}-\u{27bf}\u{1f1e6}-\u{1f1ff}\u{1f191}-\u{1f251}\u{1f004}\u{1f0cf}\u{1f170}-\u{1f171}\u{1f17e}-\u{1f17f}\u{1f18e}\u{3030}\u{2b50}\u{2b55}\u{2934}-\u{2935}\u{2b05}-\u{2b07}\u{2b1b}-\u{2b1c}\u{3297}\u{3299}\u{303d}\u{00a9}\u{00ae}\u{2122}\u{23f3}\u{24c2}\u{23e9}-\u{23ef}\u{25b6}\u{23f8}-\u{23fa}]+$";
  $scope.emojiOnly = new RegExp(emojiRgx, "u");
  $scope.countdown = 5;
  $scope.gameTime = -1;
  $scope.optionChecked = function(parent, index, event) {
    if (event.target.checked) {
      $scope.api.userAnswers[parent].push(index);
    } else {
      var ind = $scope.api.userAnswers[parent].indexOf(index);
      $scope.api.userAnswers[parent].splice(ind, 1);
    }
  };
  $scope.validateSurvey = function() {
    for (var answer in $scope.api.userAnswers) {
      if ($scope.api.userAnswers[answer] === null || $scope.api.userAnswers[answer] == "") {
        if ($scope.api.surveyDetails.questions[answer].required) {
          $scope.api.response = language.dictionary.blank_required;
          return false;
        }
      }
    }

    $interval.cancel($scope.game);
    $scope.api.submitSurvey();
  };
});

app.controller('ReportController', function($scope, $rootScope, $timeout, userAPI, reportAPI, language, alertsAPI) {
  $scope.language = language;
  $scope.user = userAPI;
  $scope.api = reportAPI;
  $scope.api.getReportData();
  $scope.alert = alertsAPI;
  $scope.addReason = function(val, event) {
    if (event.target.checked) {
      $scope.api.userReport.reason.push(val);
    } else {
      var ind = $scope.api.userReport.reason.indexOf(val);
      $scope.api.userReport.reason.splice(ind, 1);
    }
  };
  $scope.validateSurvey = function() {
    if ($scope.api.userReport.reason.length == 0) {
      $scope.api.response = language.dictionary.blank_required;
      return false;
    }
    $scope.api.response = "";
    $scope.api.submitReport();
  };
});

app.controller('TeamPublicController', function($scope, language, teamPublicAPI) {
  $scope.api = teamPublicAPI;
  $scope.api.profileData();
  $scope.language = language;
});
