Chart.defaults.global.defaultFontFamily = "OpenSans-Light";

function randomColors(amount, options = colorOptions) {
  var array = [[], []];
  var delim = 1 / amount;
  var trans = 0;
  for (let i = 0; i < amount; i++) {
    var ranR = Math.floor(Math.random() * 235) + 10;
    var ranG = Math.floor(Math.random() * 235) + 10;
    var ranB = Math.floor(Math.random() * 235) + 10;
    trans += delim;
    if (options.tranparency) {
      var clr = "rgba("+ranR+","+ranG+","+ranB+","+trans+")";
      var clrl = "rgba("+(ranR-10)+","+(ranG-10)+","+(ranB-10)+","+trans+")";
    } else {
      var clr = "rgb("+ranR+","+ranG+","+ranB+")";
      var clrl = "rgb("+(ranR-10)+","+(ranG-10)+","+(ranB-10)+")";
    }
    array[0].push(clr);
    array[1].push(clrl);
  }
  return array;
}


var charts = [];

function DestroyCharts() {
  for(var i = 0; i < charts.length; i++) {
    charts[i].destroy();
  }
  charts = [];
}

var colorOptions = {
  tranparency : false
};

var defaultOptions = {
  legend : true,
  fill : false,
  title : true,
  zeroes : false,
  max : 1000,
  pie_percentage : true
};

function LineChart(token, label, labels, data, options = defaultOptions, multiDataLegend = []) {
  var id = token;
  var ctx = document.getElementById(id);
  var colorArray = randomColors(data.length, {tranparency:true});
  var realLabels = labels;
  var realData = data;
  const l = Array.from(labels);
  var ds = [];

  if (typeof data[0] === 'object') {
    for (var i = 0; i < data.length; i++) {
      ds.push({
        label: multiDataLegend[i],
        data: realData[i],
        fill: (typeof options.fill !== "undefined") ? options.fill : defaultOptions.fill,
        borderColor: colorArray[0][i],
        backgroundColor: colorArray[0][i],
        pointBackgroundColor: "#fff",
        pointRadius: 4,
        pointBorderWidth: 2,
        pointHoverRadius: 5,
        pointHoverBorderWidth: 2
      });
    }
  } else {
    for (var i = 0; i < l.length; i++) {
      if (realData.indexOf(0) != -1 && !options.zeroes) {
        let id = realData.indexOf(0);
        realLabels.splice(id, 1);
        realData.splice(id, 1);
      }
    }

    if (realData.length < 3) {
      BarGraph(token, label, realLabels, realData, options);
      return;
    }

    ds.push({
      label: [label],
      data: realData,
      fill: (typeof options.fill !== "undefined") ? options.fill : defaultOptions.fill,
      borderColor: colorArray[0],
      backgroundColor: colorArray[0]
    });
  }

  var lineChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: realLabels,
      datasets: ds
    },
    options: {
      title:{
        display: (typeof options.title !== "undefined") ? options.title : defaultOptions.title,
        text: label,
        fontSize : 18
      },
      responsive : false,
      scales: {
        xAxes: [{
          stacked: true,
          gridLines: {
            lineWidth: 0,
            color: "rgba(255,255,255,0)"
          }
        }],
        yAxes: [{
          stacked: false,
          ticks: {
            min: 0,
            max: (typeof data[0] === 'object') ? options.max : Math.max.apply(null, realData)
          },
          gridLines: {
            lineWidth: 0,
            color: "rgba(255,255,255,0)"
          }
        }]
      },
      width : 300,
      height : 300,
      legend : {
        display : (typeof options.legend !== "undefined") ? options.legend : defaultOptions.legend,
        fontSize : 16
      }
    }
  });
  charts.push(lineChart);
}

//BarGraph
function BarGraph(token, label, labels, data, options = defaultOptions) {
  var id = token;
  var ctx = document.getElementById(id);
  var colorArray = randomColors(labels.length);
  var showL = (labels < 3);
  var sets = [];
  var realLabels = [];

  for (var i = 0; i < labels.length; i++) {
    if (data[i] == 0 || data[i] == "" && !options.zeroes) {
      continue;
    }
    realLabels.push(labels[i]);
    sets.push({
      label : labels[i],
      backgroundColor: colorArray[0][i],
      hoverBackgroundColor : colorArray[1][i],
      hoverBorderColor : colorArray[1][i],
      borderWidth : 1,
      data: [data[i]]
    });
  }

  var ss = Math.max.apply(Math, data) / realLabels.length;

  var barChart = new Chart(ctx, {
    type: 'bar',
    data : {
      labels: realLabels,
      datasets: sets
    },
    options: {
      title:{
        display:  (typeof options.title !== "undefined") ? options.title : defaultOptions.title,
        text: label,
        fontSize : 18
      },
      responsive : false,
      width : 300,
      height : 300,
      animation : {
        animateRotate	: true,
        animateScale : true
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero:true,
            stepSize : ss
          },
          gridLines : {
            display : false
          },
        }],
        xAxes: [{
          ticks: {
            beginAtZero:true,
          },
          categoryPercentage: 1.0,
          barPercentage: 1.0,
          maxBarThickness : 40,
          gridLines : {
            display : false
          }
        }]
      },
      legend : {
        display : showL,
        fontSize : 16
      },
      tooltips: {
        callbacks: {
            title : function(tooltipItem, data) {
              return "";
            },
            label: function(tooltipItem, data) {
              return data.datasets[tooltipItem.datasetIndex].data[0];
            }
        }
      }
    }
  });
  charts.push(barChart);
}

//Donut Chart
function DonutChart(token, label, labels, data, options = defaultOptions) {
  var id = token;

  var ctx = document.getElementById(id);
  var colorArray = randomColors(labels.length);

  var donutChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
      labels: labels,
      datasets: [{
        label: [label],
        backgroundColor: colorArray[0],
        hoverBackgroundColor : colorArray[1],
        hoverBorderColor : colorArray[1],
        data: data
      }]
    },
    options: {
      title:{
        display: (typeof options.title !== "undefined") ? options.title : defaultOptions.title,
        text: label,
        wrap : false,
        fontSize : 18
      },
      responsive : false,
      width : 300,
      height : 300,
      animation : {
        animateRotate	: true,
        animateScale : true
      },
      legend : {
        display : (typeof options.legend !== "undefined") ? options.legend : defaultOptions.legend,
        fontSize : 16
      },
      tooltips: {
        callbacks: {
            label: function(tooltipItem, data) {
              return data.datasets[0].data[tooltipItem.index] + "%";
            }
        }
      }
    }
  });
  charts.push(donutChart);
}

//Pie Chart
function PieChart(token, label, labels, data, options = defaultOptions) {
  var id = token;

  var ctx = document.getElementById(id);
  var colorArray = randomColors(labels.length);

  var pieChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: labels,
      datasets: [{
        label: [label],
        backgroundColor: colorArray[0],
        hoverBackgroundColor : colorArray[1],
        hoverBorderColor : colorArray[1],
        data: data
      }]
    },
    options: {
      title:{
        display: (typeof options.title !== "undefined") ? options.title : defaultOptions.title,
        text: label,
        wrap : false,
        fontSize : 18
      },
      responsive : false,
      width : 300,
      height : 300,
      animation : {
        animateRotate	: true,
        animateScale : true
      },
      legend : {
        display : (typeof options.legend !== "undefined") ? options.legend : defaultOptions.legend,
        fontSize : 16
      },
      tooltips: {
        callbacks: {
            label: function(tooltipItem, data) {
              if (options.pie_percentage) {
                return data.datasets[0].data[tooltipItem.index]+"%";
              } else {
                return data.datasets[0].data[tooltipItem.index]+" "+data.labels[tooltipItem.index];
              }
            }
        }
      }
    }
  });

  charts.push(pieChart);
}
