var alphabet=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y",
"Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",",","!",
"?",".","#","@","$","%","^","&","*","(",")","-","_","+","=","{","}","[","]","\\","/","|","`","~","<",">",":",";","'",
'"',"1","2","3","4","5","6","7","8","9","0"];function shift(r){for(var e=[],t=r;t<alphabet.length;t++)e.push(alphabet[t]);
for(t=0;t<r;t++)e.push(alphabet[t]);return e}function loopD(r,e){var t={};Array.isArray(e)&&(t=[]);for(var a in e){var o=decrypt(r,a);
if(Array.isArray(e)&&(o=a),"object"!=typeof e[a]){var n=decrypt(r,e[a]);t[o]=n}else t[o]=loopD(r,e[a])}return t}
function decrypt(r,e){if("number"==typeof e||"boolean"==typeof e)return e;var t=shift(r),a="";for(var o in e){var n=e[o],p=alphabet.indexOf(n);a+=-1==p?" ":t[p]}return a}
function decryptResponse(r){var e=r.__s__,t={};for(var a in r)"resultCode"!=a&&"resultDate"!=a?"__s__"!=a&&(ke=decrypt(e,a),"object"!=typeof r[a]?(ve=decrypt(e,r[a]),t[ke]=ve):t[ke]=loopD(e,r[a])):t[a]=r[a];return t}

app.service('language', function() {
  var lang = window.location.hash.substr(1);
  var langFile = window.location.hash.substr(1);
  var langDir = "ltr";
  var langFull;
  switch (lang) {
    case "en":
      langFull = "English";
      lang = en;
      break;
    case "es":
      langFull = "Spanish";
      lang = es;
      break;
    default:
      langFull = "English";
      langFile = "en";
      lang = en;
  }

  this.langFull = langFull;
  this.langDir = langDir;
  this.langFile = langFile;
  this.globals = globals;
  this.dictionary = lang;
});

app.service('metadata', function($route, $cookies, $window, language) {
  var self = this;
  self.title = "SurveyStacks";
  self.description = "Answer Surveys and Recieve 'Stacks'! Ask Surveys and Recieve Stats Instantly";
});

app.service("alertsAPI", function($timeout, language) {
  var self = this;
  self.alert = "";
  self.show = function(text) {
    self.alert = text;
    $("#snackbar").fadeIn(400);

    $timeout(function() {
      $("#snackbar").fadeOut(400);
      self.alert = "";
    }, 2000);
  };
});


app.service("profilePublicAPI", function($http, $location, $window, $timeout, $interval, alertsAPI, language) {
  var self = this;
  self.profile = {
    pvk : $location.path().split("/")[2],
    username : $location.path().split("/")[3],
    profile_theme_url : "",
    profile_picture_url : "https://res.cloudinary.com/survey-stacks/image/upload/v1499041967/30af3b2db890ab2d3f96a34fa309c0ce/"+$location.path().split("/")[2]+"/profile_picture.png",
    motto : "",
    since : "",
  };
  self.achievements = {
    total_stacks : 0,
    total_coins : 0,
    total_surveys_answered : 0,
    total_questions_answered : 0,
    total_rewards_claimed : 0,
  };
  self.contactInfo = {
    email : "",
    phone_number : "",
    website : "",
    facebook : "",
    instagram : ""
  };
  self.loading = true;
  self.profile_exists = false;
  self.profileData = function() {
    self.loading = true;
    $http({
        method : "GET",
        url : "/api/users/"+self.profile.pvk+"/712b10c4f7ce4432af",
        headers: {'Content-Type': 'application/json'},
        params : {
          reference_code : "FridayEveningCloudySky",
          key : "995215",
          pvk : self.profile.pvk,
          username : self.profile.username,
          webApp : true,
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      var profileInfo = response.profileInfo;
      var achievementsInfo = response.achievementsInfo;
      var contactInfo = response.contactInfo;
      self.profile_exists = (response.resultCode === 1);
      self.profile.profile_picture_url = profileInfo.profile_picture_url;
      self.profile.motto = profileInfo.motto;
      self.profile.since = profileInfo.since;
      self.achievements.total_stacks = achievementsInfo.stacks_earned;
      self.achievements.total_coins = achievementsInfo.coins_earned;
      self.achievements.total_surveys_answered = achievementsInfo.surveys_answered;
      self.achievements.total_questions_answered = achievementsInfo.questions_answered;
      self.achievements.total_rewards_claimed = achievementsInfo.rewards_claimed;
      self.contactInfo.email = contactInfo.email;
      self.contactInfo.phone_number = contactInfo.phone_number;
      self.contactInfo.website = contactInfo.website;
      self.contactInfo.facebook = contactInfo.facebook;
      self.contactInfo.instagram = contactInfo.instagram;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        self.profile_exists = false;
      }
    });
  };
});

app.service("surveyAnswerAPI", function($timeout, $window, $http, $location, alertsAPI, language, metadata) {
  var self = this;
  self.surveyDetails = {};
  self.userAnswers = [];
  self.labsAnswers = {};
  self.request_pending = false;
  self.response = "";
  self.loading = true;
  self.location = window.location.pathname;
  self.getSurveyData = function(cb) {
    self.surveyDetails = {
      available : false,
      public : false,
      targeted : false,
      answered : false,
      token : $location.path().split("/")[2],
      lang : "",
      title : "",
      description : "",
      stacks : 0,
      questions : [],
      labs_created : false,
      labs_option : null,
      labs : {}
    };
    self.loading = true;
    $http({
          method : "GET",
          url : "/api/user/5a239f4c0ec530c497",
          headers: {'Content-Type': 'application/json'},
          params : {
            reference_code : "FridayNightMMS",
            key : "991201",
            lang : language.langFile,
            token : self.surveyDetails.token,
            webApp : true
          }
      }).then(function success(res) {
          var response = decryptResponse(res.data);
          var data = {};
          for (var q in response.survey_details.questions) {
            if (response.survey_details.questions[q].type != "fr") continue;
            response.survey_details.questions[q].pattern = new RegExp(response.survey_details.questions[q].pattern[0], response.survey_details.questions[q].pattern[1]);
          }
          self.surveyDetails = response.survey_details;
          self.userAnswers = response.answers_blank;
          if (self.surveyDetails.available) {
            metadata.title = self.surveyDetails.title;
            metadata.description = self.surveyDetails.description;
          } else {
            self.location = "landing_page";
            metadata.title = language.dictionary.unav_s;
          }

          if (self.surveyDetails.labs_option === 3) {
            self.labsAnswers.elapsed = 0;
            self.labsAnswers.elapsed_end = 0;
            data.timed = self.surveyDetails.labs.timed;
            data.countdown = (self.surveyDetails.labs.time > 0);
          } else if (self.surveyDetails.labs_option === 1) {
            self.labsAnswers.key = "";
          }

          cb(data);
      }, function error(res) {
          if (res.status == 500) {
            alertsAPI.show(language.dictionary.error_snackbar);
          } else {
            alertsAPI.show(language.dictionary.error_verify_snackbar);
          }
      });
  };
  self.submitSurvey = function() {
    if (self.surveyDetails.labs.timed) {
      self.labsAnswers.elapsed_end = performance.now();
    }
    $http({
        method : "POST",
        url : "/api/user/surveys/answer",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "7ccea46ffe440f91a95c84272c919fbe9057c80e",
          lang : language.langFile,
          token : self.surveyDetails.token,
          response : self.userAnswers,
          labs : self.labsAnswers,
          webApp : true,
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);

      if (response.resultCode == "1") {
        if (typeof response.score !== "undefined") {
          document.getElementById("submitResponses").remove();
          $("#answer_section").hide();
          $(".begin-timer").removeClass('ng-hide');
          $(".begin-timer").html((response.score * self.surveyDetails.labs.out_of)+"%") ;

          $timeout(function() {
            $window.location = "/landing_page";
            self.request_pending = false;
          }, 3000);
        } else {
          $window.location = "/landing_page";
          self.request_pending = false;
        }
      } else if (response.resultCode == "-1") {
        self.response = language.dictionary.wrong_key
      }
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("pollAnswerAPI", function($timeout, $window, $http, $location, alertsAPI, language, metadata) {
  var self = this;
  self.pollDetails = {};
  self.labsAnswers = {};
  self.request_pending = false;
  self.loading = true;
  self.location = window.location.pathname;
  self.getPollData = function(cb) {
    self.pollDetails = {
      available : false,
      public : false,
      targeted : false,
      answered : false,
      token : $location.path().split("/")[2],
      title : "",
      lang : "",
      description : "",
      coins : 0,
      image_url : "",
      question : "",
      options : [],
      analytics : {},
      next_poll : "",
      labs_created : false,
      labs_option : null,
      labs : {}
    };
    self.loading = true;
    $http({
          method : "GET",
          url : "/api/user/c3789b05570894bcfd",
          headers: {'Content-Type': 'application/json'},
          params : {
            reference_code : "SundayNightAC",
            key : "990220",
            lang : language.langFile,
            token : self.pollDetails.token,
            webApp : true
          }
      }).then(function success(res) {
          var response = decryptResponse(res.data);
          var data = {};
          self.pollDetails = response.poll_details;
          if (self.pollDetails.available) {
            metadata.title = self.pollDetails.title;
            metadata.description = self.pollDetails.description;
          } else {
            self.location = "landing_page";
            metadata.title = language.dictionary.unav_p;
          }

          if (self.pollDetails.labs_option === 3) {
            self.labsAnswers.elapsed = 0;
            self.labsAnswers.elapsed_end = 0;
            data.timed = self.pollDetails.labs.timed;
            data.countdown = (self.pollDetails.labs.time > 0);

            for (var i = 0; i < self.pollDetails.labs.questions.length; i++) {
              if (!self.labsAnswers.responses) {
                self.labsAnswers.responses = [];
              }
              self.labsAnswers.responses.push(null);
            }
          }

          cb(data);
      }, function error(res) {
          if (res.status == 500) {
            alertsAPI.show(language.dictionary.error_snackbar);
          } else {
            alertsAPI.show(language.dictionary.error_verify_snackbar);
          }
      });
  };
  self.submitPoll = function($index) {
    $http({
        method : "POST",
        url : "/api/user/polls/answer",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "f6642b19507252dbd126a770e9ea0265c3754bd5",
          lang : language.langFile,
          token : self.pollDetails.token,
          response : $index,
          labs : self.labsAnswers,
          webApp : true,
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      self.request_pending = false;
      if (response.resultCode == "1") {
        if (typeof response.score !== "undefined") {
          $("#description_section").hide();
          $("#quiz_span").hide();
          $(".time_span1").hide();
          $("#timer_span").removeClass("ng-hide");
          $(".begin-timer").removeClass('ng-hide');
          $(".begin-timer").html((response.score * 100)+"%");
          if (response.score == 0) {
            $(".begin-timer").css("background-color", "red");
            $(".begin-timer").css("border", "1px solid red");
          }
        }

        self.pollDetails.answered = true;
        $timeout(function() {
          self.pollDetails.analytics = response.analytics;
        }, 100);
      }
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("teamPublicAPI", function($http, $location, $window, $timeout, $interval, alertsAPI, language) {
  var self = this;
  self.profile = {
    tvk : $location.path().split("/")[2],
    name : "",
    nickname : $location.path().split("/")[3],
    team_theme_url : "",
    team_logo_url : "",
    description : "",
    since : "",
  };
  self.contactInfo = {
    email : "",
    phone_number : "",
    website : "",
    facebook : "",
    instagram : ""
  };
  self.additional = {
    verified : false,
    members : 0,
  };
  self.loading = true;
  self.team_exists = false;
  self.profileData = function() {
    self.loading = true;
    $http({
        method : "GET",
        url : "/api/teams/"+self.profile.tvk+"/a50cf95ced779706b6",
        headers: {'Content-Type': 'application/json'},
        params : {
          reference_code : "SundayNightTenSeven",
          key : "991821",
          tvk : self.profile.tvk,
          nickname : self.profile.nickname,
          webApp : true,
          lang : language.langFile
        }
    }).then(function success(res) {
      var response = decryptResponse(decryptResponse(res.data));
      if (response.resultCode == -1) {
        self.team_exists = false;
        self.loading = false;
        return;
      }
      self.team_exists = true;
      self.profile.name = response.profileData.name;
      self.profile.description = response.profileData.description;
      self.profile.team_theme_url = response.profileData.theme_url;
      self.profile.team_logo_url = response.profileData.logo_url;
      self.profile.since = response.profileData.since.toString();
      self.contactInfo = response.contactInfo;
      self.additional = response.additional;
      self.loading = false;
      $("#toolbar_header").html(self.profile.name);
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        self.team_exists = false;
      }
    });
  };
});
