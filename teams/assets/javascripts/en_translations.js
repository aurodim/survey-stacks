var en = {
  app_name : "SurveyStacks for Teams",
  description_metadata : "Ask Surveys and Recieve Stats Instantly",
  tabs : [
    {
      "value" : "analytics",
      "icon" : "&#xE6E1;",
      "translation" : "Analytics"
    },
    {
      "value" : "manage",
      "icon" : "&#xE22B;",
      "translation" : "Manage"
    },
    {
      "value" : "dashboard",
      "icon" : "&#xE871;",
      "translation" : "Dashboard"
    },
    {
      "value" : "labs",
      "icon" : "&#xE80E;",
      "translation" : "Labs"
    },
    {
      "value" : "members",
      "icon" : "&#xE7FB;",
      "translation" : "Members"
    },
    {
      "value" : "notifications",
      "icon" : "&#xE7F4;",
      "translation" : "Notifications"
    },
    {
      "value" : "settings",
      "icon" : "&#xE8B8;",
      "translation" : "Settings"
    }
  ],
  unexistentProfile : function(username) {
    return "<i>"+username+"</i>&nbsp;does not belong to any user";
  },
  unexistentTeam : function(nickname) {
    return "<i>"+nickname+"</i>&nbsp;is not an active team nickname";
  },
  logout : "Logout",
  logging_out : "Logging Out..",
  help : "Help",
  t_and_c : "Terms and Conditions",
  forums : "Forums",
  contact_us : "Contact Us",
  cookies_policy : "Cookies Policy",
  survey_creator : "Survey Creator",
  poll_creator : "Poll Creator",
  profile : "Profile",
  member_since : "Member Since",
  total_stacks : "Total Stacks",
  stacks : "Stacks",
  delete_ps : "Enter Questionnaire Name to Delete",
  update : "Update",
  delete : "Delete",
  description : "Description",
  updated_price : "Price",
  update_notification_settings : "Update Notification Settings",
  update_membership : "Update Membership",
  update_member_settings : "Update Member Settings",
  update_password : "Update Password",
  update_contact_info : "Update Contact Info",
  update_account_info : "Update Account Info",
  team_fullname : "Leader's Fullname",
  team_name : "Team Name",
  team_nickname : "Team Nickname",
  email_phone : "Email or Phone Number",
  team_description : "Team Description",
  team_theme_color : "Theme Color Hex",
  team_accent_color : "Accent Color Hex",
  email : "E-mail",
  phone : "Phone",
  team_code : "Team Code",
  current_password : "Current Password",
  new_password : "New Password",
  private_team : "Leader confirmation needed to join team?",
  team_open : "Open to join?",
  allow_desktop_notifications : "Allow Desktop notifications",
  email_phone_notifications : "Allow Notifications to E-mail or Phone Number",
  notification_freq : "Notification Frequency",
  auto_renew : "Auto Renew Membership",
  free : "Free",
  premium : "Premium",
  borrowed : "Shared Premium",
  expiration_date : "Expiration Date",
  price : "Price",
  credit_card_digit : "Last 4 digits of Card",
  facebook_username : "Facebook Username",
  instagram_username : "Instagram Username",
  website : "Website",
  survey_name : "Survey Name",
  category : "Category",
  survey_description : "Survey Description (80 character max)",
  public_survey : "Public Survey (SurveyStacks account not needed to respond)",
  survey_category : "Survey Category",
  blank_question_survey : "Blank Question",
  blank_pq : "Poll question cannot be left blank",
  blank_po : "Poll option cannot be left blank",
  q_name : "Questionnaire Name",
  category : "Category",
  q_description : "Questionnaire Description (80 character max)",
  public_q : "Public Questionnaire (SurveyStacks account not needed to respond)",
  q_category : "Questionnaire Category",
  create_q : "Create Questionnaire",
  getQuestionText : function(question) {
    var split = question.split("_");
    var type = split[0];
    var index = split[1];
    var question_type;

    switch (type) {
      case "fr":
        question_type = "(Free Response Question)";
        break;
      case "cbr":
        question_type = "(Checkbox Question)";
        break;
      case "br":
        question_type = "(Boolean Question)";
        break;
      case "rr":
        question_type = "(Radio Question)";
        break;
      case "mr":
        question_type = "(Single Item Question)";
        break;
    }

    return question_type;
  },
  blank : "Blank",
  nameless_survey : "Name field cannot be left blank",
  descriptionless_survey : "Please fill out description",
  categoryless_survey : "Please select a category",
  sexless_survey : "Please select a target sex",
  ethnicityless_survey : "Please select a target ethnicity",
  ageless_survey : "Please select a target age group",
  ud_group : "Respondent Data to Automatically Collect",
  imageless_survey : "Image URL is empty",
  problem_image : "The image URL is invalid",
  no_questions : "Survey must have at least one question",
  survey : "Survey",
  poll : "Poll",
  empty_questions : function(type) {
    var response = "";

    switch (type) {
      case 'fr':
        response = "Free";
        break;
      case 'cbr':
        response = "Checkbox";
        break;
      case 'br':
        response = "Boolean";
        break;
      case 'rr':
        response = "Radio";
        break;
      case 'mir':
        response = "Single Item";
        break;
    }

    return "There is a blank " + response + " Response question";
  },
  empty_responses : function(type) {
    var response = "";

    switch (type) {
      case 'cbr':
        response = "Checkbox";
        break;
      case 'rr':
        response = "Radio";
        break;
      case 'mir':
        response = "Single Item";
        break;
    }

    return "There is a " + response + " Response question with a blank response";
  },
  description_toolong : "Description cannot exceed 80 characters",
  target_sex : "Target Sex",
  target_ethnicity : "Target Ethnicity",
  target_age_group : "Target Age Group",
  any : "Any",
  sexes : {
    "male" : "Male",
    "female" : "Female",
    "both" : "Both"
  },
  ethnicities : {
    "a" : "Any",
    "w" : "White",
    "h_l" : "Hispanic or Latino",
    "b_aa" : "Black or African American",
    "na_an" : "Native American or American Indian",
    "a_pi" : "Asian / Pacific Islander",
    "o" : "Other"
  },
  age_groups : {
    "a" : "Any",
    "t" : "13-17",
    "lt" : "18-24",
    "ea" : "25-34",
    "ad" : "35-44",
    "la" : "45-54",
    "es" : "55-64",
    "s" : "65-74",
    "ls" : "75+"
  },
  text_pattern : "Response Type",
  patterns : {
    "a" : "All Text",
    "t" : "Text Only",
    "n" : "Numbers Only",
    "tn" : "Text and Numbers Only",
    "d" : "Date",
    "ts" : "Time",
    "h" : "Link"
  },
  survey_image : "Survey Image (only use images you are licensed to use)",
  q_image : "Questionnaire Image (only use images you are licensed to use)",
  select_image : "Select Image From Computer",
  free_response : "Free Response",
  question_here : "Enter Question",
  order : "Question Arrangement",
  add_question : "Add Question",
  checkbox_response : "Checkbox Response",
  respondent_answer : "Respondent answer here",
  add_response : "Add Response",
  answer_url : "Answer URL",
  boolean_response : "\"No\" or \"Yes\" Response",
  radio_response : "Radio Response",
  required : "Required",
  purchase_details : "Purchase Details",
  single_item_response : "Single Item Response",
  create_survey : "Create Survey",
  details : "Details",
  cover_image : "Questionnaire Image",
  create_poll : "Create Poll",
  poll_question : "Poll Question",
  add_poll_option : "Add Poll Option",
  poll_option : "Poll Option",
  poll_name : "Poll Name",
  poll_description : "Poll Description",
  name : "Name",
  public_poll : "Public Poll (SurveyStacks account not needed to respond)",
  poll_category : "Poll Category",
  poll_image : "Poll Image (only use images you are licensed to use)",
  daily_r : "Daily Responses",
  monthly_r : "Monthly Responses",
  yearly_r : "Yearly Responses",
  days : [
    "Mon",
    "Tue",
    "Wed",
    "Thu",
    "Fri",
    "Sat",
    "Sun"
  ],
  months : [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sept",
    "Oct",
    "Nov",
    "Dec"
  ],
  no_analytics : "You have not created any questionnaires",
  no_qs_created : "You have not created any questionnaires",
  click_p_to : "Click the green \"+\" button below to create a survey or poll!",
  surveys : "Surveys",
  polls : "Polls",
  not_editable : "Sorry! The survey/poll you are looking for does not exist!",
  analytics : "Analytics",
  download_as : [
    "As Shown",
    "Survey Analytics",
    "Graphics by Question",
    "Respondent Analytics",
    "Responses w/ User Data",
    "Responses",
    "Question Responses"
  ],
  download_as_p : [
    "As Shown",
    "Poll Analytics",
    "Respondent Analytics",
    "Responses w/ User Data",
  ],
  download_formats : [
    "JSON",
    "CSV",
    "XML"
  ],
  responses_by_user : "Responses By User",
  first_response : "First Response",
  total_responses : "Total Responses",
  responses : "Responses",
  graphics_questions : "Graphics By Question",
  average_responses_per_day : "Average Responses per Day",
  average_responses_per_user : "Average Answers per User",
  average_stacks_per_user : "Average Stacks per Response",
  last_response : "Last Response",
  respondent_analytics : "Respondent Analytics",
  error_snackbar : "Trouble Loading! Reload!",
  error_verify_snackbar : "Server Cannot Verify Credentials!",
  approve : "Approve",
  remove : "Remove",
  no_mem : "You do not have an active subscription. Our services cannot be used to the fullest",
  update_theme_image : "Update Theme Image",
  update_logo : "Update Team Logo/Icon",
  wis : "What is this?",
  rtbc : "Request to become a company",
  cs : ["N/A", "Pending. We will E-mail you with further details.", "Approved", "Denied"],
  new_notification : "New Notification",
  membersP : function(a) {
    if (a == 1) return "Member";
    return "Members"
  },
  surveys_created : "Surveys Created",
  polls_created : "Polls Created",
  mem_expires : "Membership Expires",
  total_members : "Total Members",
  membership : "Membership",
  qs_created : "Questionnaires Created",
  qs_responses : "Daily Questionnaire Responses",
  types_qs : "Types of Questionnaires Created",
  delete_account_pre : "Enter Team Name to Delete",
  account : "Account",
  notifications : "Notifications",
  team_logo : "Team Logo/Picture",
  team_theme : "Team Theme Image",
  contact_info : "Contact Info",
  change_password : "Change Password",
  company_status : "Company Status",
  members : "Members",
  // collectables
  fullname : "Full name",
  religion : "Religion",
  employment_status : "Employment Status",
  relationship_status : "Relationship Status",
  education_level : "Current or Highest Level of Education",
  spoken_languages : "Languages Spoken",
  ethnicity : "Ethnicity",
  sex : "Sex",
  birth_date : "Date of Birth",
  //labs
  labs : "Labs",
  labs_types : ["📝\xa0\xa0\xa0Quiz\xa0\xa0📝", "🔑\xa0\xa0\xa0Key Needed to Reply\xa0\xa0🔑", "⭐️\xa0\xa0\xa0Emoji Based\xa0\xa0❤️", "😎\xa0\xa0\xa0Game\xa0\xa0😎"],
  grading_algorithm : "Grading Algorithm",
  grading_algorithms : {
    "c" : "Contains",
    "bw" : "Begins With",
    "ew" : "Ends With",
    "o" : "Is Only",
    "pm" : "Percentage Match"
  },
  show_grade : "Show Grade to Respondent",
  possible_quiz_points : "Possible Quiz Points",
  correct_answer : "Correct Answer",
  no : "No",
  yes : "Yes",
  question_worth : "Question Worth",
  unique_keys : "Unique Keys",
  global_key : "Global Key",
  timed : "Timed",
  max_time : "Seconds to Answer",
  quiz_options : "Quiz Options",
  quiz_bad_total : "The total quiz score is invalid",
  quiz_bad_worth : "Questions cannot have an empty \"Question Worth\"",
  quiz_bad_answer : "Questions must have a valid answer",
  game_bad_time : "Game timer must be at least 5 seconds",
  quiz_bad_amounted : "The amounted questions' worth does not equal the total quiz score",
  // analytics
  education : "Education",
  education_levels : {
    "n_a" : "No schooling completed",
    "e" : "8th grade",
    "n_d" : "Some high school, no diploma",
    "ged" : "High school graduate, diploma or the equivalent",
    "cc_nd" : "Some college credit, no degree",
    "ttv_t" : "Trade/technical/vocational training",
    "a_d" : "Associate degree",
    "b_d" : "Bachelor’s degree",
    "m_d" : "Master’s degree",
    "p_d" : "Professional degree",
    "d_d" : "Doctorate degree"
  },
  relationship_statuses : {
    "s" : "Single",
    "r" : "In a relationship",
    "e" : "Engaged",
    "m" : "Married",
    "c" : "It's complicated",
    "o_r" : "In an open relationship",
    "w" : "Widowed",
    "d_p" : "In a domestic partnership",
    "c_u" : "In a civil union"
  },
  employment_statuses : {
    "s" : "Student",
    "n_l" : "Not employed & Not interested",
    "u" : "Unemployed",
    "e" : "Employed for wages",
    "se" : "Self-employed",
    "h" : "A homemaker",
    "m" : "Military",
    "r" : "Retired",
    "uw" : "Unable to work"
  },
  religions : {
    "c" : "Christian",
    "m" : "Muslim",
    "ct" : "Catholic",
    "b" : "Buddhist",
    "a" : "Atheist",
    "ag" : "Agnostic",
    "nr" : "Non-religious",
    "o" : "Other"
  },
  ethnicities : {
    "a" : "Any",
    "w" : "White",
    "h_l" : "Hispanic or Latino",
    "b_aa" : "Black or African American",
    "na_an" : "Native American or American Indian",
    "a_pi" : "Asian / Pacific Islander",
    "o" : "Other"
  },
  age_group : "Age Group",
  languages : "Languages",
  score : "Score",
  key_used : "Key Used",
  time_elapsed : "Answer Duration",
  answered_at : "Response Date"
};
