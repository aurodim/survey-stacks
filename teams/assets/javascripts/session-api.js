var alphabet=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y",
"Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",",","!",
"?",".","#","@","$","%","^","&","*","(",")","-","_","+","=","{","}","[","]","\\","/","|","`","~","<",">",":",";","'",
'"',"1","2","3","4","5","6","7","8","9","0"];function shift(r){for(var e=[],t=r;t<alphabet.length;t++)e.push(alphabet[t]);
for(t=0;t<r;t++)e.push(alphabet[t]);return e}function loopD(r,e){var t={};Array.isArray(e)&&(t=[]);for(var a in e){var o=decrypt(r,a);
if(Array.isArray(e)&&(o=a),"object"!=typeof e[a]){var n=decrypt(r,e[a]);t[o]=n}else t[o]=loopD(r,e[a])}return t}
function decrypt(r,e){if("number"==typeof e||"boolean"==typeof e)return e;var t=shift(r),a="";for(var o in e){var n=e[o],p=alphabet.indexOf(n);a+=-1==p?" ":t[p]}return a}
function decryptResponse(r){var e=r.__s__,t={};for(var a in r)"resultCode"!=a&&"resultDate"!=a?"__s__"!=a&&(ke=decrypt(e,a),"object"!=typeof r[a]?(ve=decrypt(e,r[a]),t[ke]=ve):t[ke]=loopD(e,r[a])):t[a]=r[a];return t}

app.service('language', function($route, $cookies, $window) {
  var self = this;
  self.langFile = $cookies.get("lang-team");
  self.langFull = "";
  self.langDir = "ltr";

  self.dictionary = en;
  self.globals = globals;
});

app.service('metadata', function($route, $cookies, $window, language) {
  var self = this;
  self.title = "SurveyStacks for Teams";
  self.description = language.dictionary.description_metadata;
});

app.service("alertsAPI", function($timeout, language) {
  var self = this;
  self.alert = "";
  self.show = function(text) {
    self.alert = text;
    $("#snackbar").fadeIn(300);

    $timeout(function() {
      $("#snackbar").fadeOut(300, function() {
        self.alert = "";
      });
    }, 2000);
  };
});

app.service('socketAPI', function ($cookies, $timeout, $http, alertsAPI, language) {
  var self = this;
  self.socket;
  self.desktop_notifications = $cookies.get('dnotif');
  self.$scope;
  self.unread_notif = false;
  self.digestCycle = function($scope) {
    self.$scope = $scope;
  };
  self.desktopNotification = function(notification) {

    if (self.desktop_notifications == "true" && Notification.permission === "granted") {
        var notification = new Notification(notification.title, {
          lang : language.langFile,
          dir : language.langDir,
          tag : notification.token,
          icon : "https://res.cloudinary.com/survey-stacks/image/upload/c_scale,w_512/v1510847720/icon_r.png",
          sound : "notificationSound",
          data : {
            type : notification.type,
            link : notification.url
          },
          body : notification.content

        });
        $(document).prop('title', language.dictionary.new_notification);

        $timeout(function() {
          notification.close();
        }, 5000);

        notification.onclick = function() {
          if (notification.data.link != "") {
            $window.location = notification.data.link;
          }
        };

        notification.onclose = function() {
          $(document).prop('title', 'SurveyStacks');
        };
    }
  };
  self.init = function(socket) {
    self.socket = io();
    self.socket.on(socket, function(todo) {
      $("#notifications_drawer_icon").addClass("bl_notif");
      self.unread_notif = "true";
      if (todo.type === "cn") {
        $http({
            method : "GET",
            url : "/api/notifications/e1760a7b88184d0517f11c900342e1c4fba47fdd",
            headers: {'Content-Type': 'application/json'},
            params : {
              reference_code : "TuesdayEveningPizza",
              key : "992018",
              lang : language.langFile,
              webApp : true
            }
        }).then(function success(res) {
            var response = decryptResponse(res.data);
            self.desktopNotification(response.notification);
        }, function error(res) {
          if (res.status == 500) {
            alertsAPI.show(language.dictionary.error_snackbar);
          } else {
            alertsAPI.show(language.dictionary.error_verify_snackbar);
          }
        });
      } else if (todo.type === "es") {
        window.location = "/logout";
      }
    });
  };
});

app.service("teamAPI", function($timeout, $cookies, $http, language, alertsAPI, socketAPI) {
  var self = this;
  self.name = "";
  self.desktop_notifications = $cookies.get('dnotif');
  self.csrf = "";
  self.team_logo_url = "";
  self.backgroundColor = "";
  self.backgroundColorDark = "";
  self.listener = socketAPI;
  self.membership_active = $cookies.get("sd-team");;
  self.loading = true;
  self.getTeamData = function() {
    self.loading = true;
    $http({
      method : "GET",
      url : "/api/team/b15356eaa49b47dc97",
      headers : {'Content-Type' : "application/json"},
      params : {
        reference_code : "MondayMidnightEarly",
        key : "991815",
        lang : language.langFile,
        webApp : true
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      self.name = response.name;
      self.csrf = response.csrf;
      self.membership_active = response.membership_active;
      self.team_logo_url = response.team_logo_url;
      self.backgroundColor = response.colors.theme;
      self.backgroundColorDark = response.colors.dark;
      socketAPI.unread_notif = response.unread;
      if (response.unread_notif == true) {
        $("#notifications_drawer_icon").addClass("bl_notif");
      }
      self.listener.init(self.socket);
      changeColor(self.backgroundColor);
      self.loading = false;
    }, function error(res) {
      if (res.status == 500) {
        window.location.href = "/500";
      } else {
        window.location.reload();
      }
    });
  };
  self.getTeamData();
});

app.service("analyticsAPI", function($timeout, $http, language, teamAPI, alertsAPI) {
  var self = this;
  self.loading = true;
  self.analyticsData = {
    surveys : [],
    polls : []
  };
  self.getAnalyticsData = function() {
    if (!teamAPI.membership_active) {
      self.loading = false;
      return;
    }
    self.loading = true;
    $http({
      method : "GET",
      url : "/api/team/605f4b89f6b804cf8b",
      headers : {'Content-Type' : "application/json"},
      params : {
        reference_code : "MondayEveningTwTw",
        key : "990822",
        lang : language.langFile,
        webApp : true
      }
    }).then(function success(res) {
      var response = res.data;
      self.analyticsData = response.analytics;
      self.loading = false;

      console.log(response);
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("dashboardAPI", function($timeout, $http, language, teamAPI, alertsAPI) {
  var self = this;
  self.loading = true;
  self.tips = {};
  self.graphics = {};
  self.gatherData = function(cb) {
    self.loading = true;
    $http({
          method : "GET",
          url : "/api/team/6322c3c2e2d3f2953f",
          headers: {'Content-Type': 'application/json'},
          params : {
            reference_code : "SundayEveningCelebBD",
            key : "991979",
            lang : language.langFile,
            webApp : true
          }
      }).then(function success(res) {
          var response = decryptResponse(res.data);
          self.tips = response.tips;
          self.graphics = response.graphics;
          cb(null);
      }, function error(res) {
          cb(true);
          if (res.status == 500) {
            alertsAPI.show(language.dictionary.error_snackbar);
          } else {
            alertsAPI.show(language.dictionary.error_verify_snackbar);
          }
      });
  };
});

app.service("labsAPI", function($timeout, $http, $window, language, teamAPI, alertsAPI) {
  var self = this;
  self.loading = false;
  self.response = "";
  self.categories = {};
  self.createdQuestionnaire = {
    "lab_option" : null,
    "type" : "",
    "details" : {
      "name" : "",
      "description" : "",
      "public" : false,
      "category_token" : "",
      "sex" : "",
      "ethnicity" : "",
      "age_group" : "",
      "image_url" : "",
      "image_file" : "",
      "data_collected" : {
        "fullname" : false,
        "email_phone" : false,
        "sex" : false,
        "ethinicity" : false,
        "languages" : false,
        "education" : false,
        "relationship" : false,
        "employment" : false,
        "religion" : false,
        "dob" : false
      }
    },
    "method_options" : {},
    "survey" : {
      "freeResponseQuestions" : [],
      "checkboxResponseQuestions" : [],
      "booleanResponseQuestions" : [],
      "radioResponseQuestions" : [],
      "menuResponseQuestions" : [],
      "order" : []
    },
    "poll" : {
      "question" : "",
      "options" : []
    }
  };
  self.retrieveData = function() {
    if (!teamAPI.membership_active) {
      self.loading = false;
      return;
    }
    self.loading = true;
    $http({
      method : "GET",
      url : "/api/4868b0b4105908817f",
      headers : {'Content-Type' : "application/json"},
      params : {
        reference_code : "FridayMorningPreMeet",
        key : "993421",
        lang : language.langFile,
        webApp : true
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      self.categories = response.categories;
      self.loading = false;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.createQuestionnaire = function() {
    var file;

    if ($("#imageFile").length) {
      file = $("#imageFile")[0].files[0];
    }

    if (typeof file === "undefined") {
      file = "";
    }

    var fd = new FormData();
    fd.append('AUTH_TOKEN', 'e9c1e045a911740aeb579bc40ac58d45e934866b');
    fd.append('qImage', file);
    fd.append('createdQuestionnaire', angular.toJson(self.createdQuestionnaire));
    fd.append('lang', language.langFile);
    fd.append('webApp', true);
    fd.append('_csrf', teamAPI.csrf);
    $http({
      method : "POST",
      url : "/api/team/labs/create",
      headers : {'Content-Type' : undefined},
      transformRequest: angular.identity,
      data : fd
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      var resultCode = response.resultCode;
      self.uploading = false;
      switch (resultCode) {
        case 1:
          $window.location.href = "/manage";
          break;
        case -1:
          self.response = language.dictionary.problem_image;
          break;
      }
    }, function error(res) {
      self.uploading = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("surveyManagerAPI", function($timeout, $http, language, teamAPI, alertsAPI) {
  var self = this;

  self.surveyData = [];
  self.pollData = [];
  self.loading = true;
  self.gatherData = function() {
    self.loading = true;
    $http({
          method : "GET",
          url : "/api/team/c6cc5f93ef1ecb9dff",
          headers: {'Content-Type': 'application/json'},
          params : {
            reference_code : "MondayEveningAte",
            key : "992816",
            lang : language.langFile,
            webApp : true
          }
      }).then(function success(res) {
          var response = decryptResponse(res.data);
          self.surveyData = response.surveys;
          self.pollData = response.polls;
          self.loading = false;
      }, function error(res) {
          if (res.status == 500) {
            alertsAPI.show(language.dictionary.error_snackbar);
          } else {
            alertsAPI.show(language.dictionary.error_verify_snackbar);
          }
      });
  };
});

app.service("membersAPI", function($timeout, $window, $http, teamAPI, alertsAPI, language) {
  var self = this;
  self.loading = true;
  this.amount = 0;
  this.awaiting = {
    one : [],
    two : [],
    three : []
  };
  this.membersOne = [];
  this.membersTwo = [];
  this.membersThree = [];
  self.getMembersData = function() {
    self.loading = true;
    $http({
      method : "GET",
      url : "/api/team/a15d61a2dbc11be816",
      headers : {'Content-Type' : "application/json"},
      params : {
        reference_code : "MondayEveningPreNight",
        key : "990840",
        lang : language.langFile,
        webApp : true
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      this.amount = response.members.length;
      var prev = 0;
      for (var i = 0; i < response.awaiting.length; i++) {
        if (prev == 0) {
          self.awaiting.one.push(response.awaiting[i]);
          prev = 1;
        } else if (prev == 1) {
          self.awaiting.two.push(response.awaiting[i]);
          prev = 2;
        } else if (prev == 2) {
          self.awaiting.three.push(response.awaiting[i]);
          prev = 0;
        }
      }
      prev = 0;
      for (var i = 0; i < response.members.length; i++) {
        if (prev == 0) {
          self.membersOne.push(response.members[i]);
          prev = 1;
        } else if (prev == 1) {
          self.membersTwo.push(response.members[i]);
          prev = 2;
        } else if (prev == 2) {
          self.membersThree.push(response.members[i]);
          prev = 0;
        }
      }
      self.loading = false;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.approveMember = function(c, i) {
    var t = "";
    if (i == 0) {
      t = self.awaiting.one[i].token;
    } else if (i == 1) {
      t = self.awaiting.two[i].token;
    } else {
      t = self.awaiting.three[i].token;
    }
    $http({
      method : "POST",
      url : "/api/team/members/approve",
      headers : {'Content-Type' : "application/json"},
      data : {
        AUTH_TOKEN : "a2aacaedb7f99b056bc842e862a0b10292b28731",
        at : t,
        lang : language.langFile,
        webApp : true,
        _csrf : teamAPI.csrf
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (i == 0) {
        t = self.awaiting.one.spice(i, 1);
      } else if (i == 1) {
        t = self.awaiting.two.spice(i, 1);
      } else {
        t = self.awaiting.three.spice(i, 1);
      }
      if (response.resultCode == -1) return;
      if (self.membersOne.length == 0) {
        self.membersOne.push(response.member);
      } else if (self.membersTwo == 0) {
        self.membersTwo.push(response.member);
      } else {
        self.membersThree.push(response.member);
      }
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.rejectMember = function(c, i) {
    var t = "";
    if (i == 0) {
      t = self.awaiting.one[i].token;
    } else if (i == 1) {
      t = self.awaiting.two[i].token;
    } else {
      t = self.awaiting.three[i].token;
    }
    $http({
      method : "POST",
      url : "/api/team/members/reject",
      headers : {'Content-Type' : "application/json"},
      data : {
        AUTH_TOKEN : "b87a09c4f08b2024bdcdf2f6be6a978b7dfd0247",
        at : t,
        lang : language.langFile,
        webApp : true,
        _csrf : teamAPI.csrf
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (i == 0) {
        t = self.awaiting.one.spice(i, 1);
      } else if (i == 1) {
        t = self.awaiting.two.spice(i, 1);
      } else {
        t = self.awaiting.three.spice(i, 1);
      }
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.removeMember = function(c, i) {
    var t = "";
    if (i == 0) {
      t = self.membersOne[i].token;
    } else if (i == 1) {
      t = self.membersTwo[i].token;
    } else {
      t = self.membersThree[i].token;
    }
    $http({
      method : "POST",
      url : "/api/team/members/remove",
      headers : {'Content-Type' : "application/json"},
      data : {
        AUTH_TOKEN : "c268c899b4038ff1249b8d3fc36e781f55ac8a90",
        mt : t,
        lang : language.langFile,
        webApp : true,
        _csrf : teamAPI.csrf
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (i == 0) {
        t = self.membersOne.spice(i, 1);
      } else if (i == 1) {
        t = self.membersTwo.spice(i, 1);
      } else {
        t = self.membersThree.spice(i, 1);
      }
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("notificationsAPI", function($timeout, $window, $http, teamAPI, alertsAPI, language) {
  var self = this;
  self.loading = false;
  self.notifications = [];
  self.gatherNotifications = function() {
    self.loading = true;
    $http({
          method : "GET",
          url : "/api/team/32d8d7c83b6d86deb1",
          headers: {'Content-Type': 'application/json'},
          params : {
            reference_code : "SundayDawnRevival",
            key : "991418",
            lang : language.langFile,
            webApp : true
          }
      }).then(function success(res) {
          var response = decryptResponse(res.data);
          self.notifications = response.notifications;
          self.loading = false;
      }, function error(res) {
          if (res.status == 500) {
            alertsAPI.show(language.dictionary.error_snackbar);
          } else {
            alertsAPI.show(language.dictionary.error_verify_snackbar);
          }
      });
  };
});

app.service("settingsAPI", function($timeout, $http, alertsAPI, teamAPI, language) {
  var self = this;
  self.nu = "";
  self.accountData = {};
  self.contactInfo = {};
  self.membersInfo = {};
  self.membershipInfo = {};
  self.notificationsInfo = {};
  self.imagesInfo = {
    logo : "",
    logo_file : {},
    theme : "",
    theme_file : {}
  };
  self.passwordInfo = {
    current : "",
    new : ""
  };
  self.cs = 0;
  self.loading = true;
  self.request_pending = false;
  self.gatherData = function() {
    self.loading = true;
    $http({
          method : "GET",
          url : "/api/team/c18909cf71f5d8018b",
          headers: {'Content-Type': 'application/json'},
          params : {
            reference_code : "MondayMidday",
            key : "998151",
            lang : language.langFile,
            webApp : true
          }
      }).then(function success(res) {
          var response = decryptResponse(res.data);
          self.nu = response.account_data.name;
          self.accountData = response.account_data;
          self.contactInfo = response.contact_info;
          self.membersInfo = response.members_info;
          self.membershipInfo = response.membership_info;
          self.notificationsInfo = response.notifications_info;
          self.cs = response.cs;
          self.loading = false;
      }, function error(res) {
          if (res.status == 500) {
            alertsAPI.show(language.dictionary.error_snackbar);
          } else {
            alertsAPI.show(language.dictionary.error_verify_snackbar);
          }
      });
  };
  self.updateAccountInfo = function($scope) {
    self.request_pending = true;
    $http({
      method : "POST",
      url : "/api/team/settings/account",
      headers : {'Content-Type' : "application/json"},
      data : {
        AUTH_TOKEN : "d057e41b8eba1c98da9a1b500da0a62fcc54e175",
        accountData : self.accountData,
        lang : language.langFile,
        webApp : true,
        _csrf : teamAPI.csrf
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == -1) {
        $scope.accountForm.email_tel.$setValidity("minlength", false);
      } else if (response.resultCode == -2) {
        $scope.accountForm.name.$setValidity("minlength", false);
      } else if (response.resultCode == -3) {
        $scope.accountForm.nickname.$setValidity("minlength", false);
      } else {
        $scope.accountForm.$setPristine();
      }
      self.request_pending = false;
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.updateContactInfo = function($scope) {
    self.request_pending = true;
    $http({
      method : "POST",
      url : "/api/team/settings/contact",
      headers : {'Content-Type' : "application/json"},
      data : {
        AUTH_TOKEN : "45650fd9ef136a387325c9b7952bb2821c3a2b1c",
        contactInfo : self.contactInfo,
        lang : language.langFile,
        webApp : true,
        _csrf : teamAPI.csrf
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      $scope.contactForm.$setPristine();
      self.request_pending = false;
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.updatePassword = function($scope) {
    self.request_pending = true;
    $http({
      method : "POST",
      url : "/api/team/settings/password",
      headers : {'Content-Type' : "application/json"},
      data : {
        AUTH_TOKEN : "317c7b2e1168d28bf51a53893b9b39199c3c158b",
        passwordInfo : self.passwordInfo,
        lang : language.langFile,
        webApp : true,
        _csrf : teamAPI.csrf
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == -1) {
        $scope.passwordForm["current-password"].$setValidity("maxlength", false);
      } else {
        $scope.passwordForm.$setPristine();
      }
      self.request_pending = false;
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.updateMembers = function($scope) {
    self.request_pending = true;
    $http({
      method : "POST",
      url : "/api/team/settings/members",
      headers : {'Content-Type' : "application/json"},
      data : {
        AUTH_TOKEN : "20c2017b0a9c303714781c4cf69cd241c7694f43",
        membersInfo : self.membersInfo,
        lang : language.langFile,
        webApp : true,
        _csrf : teamAPI.csrf
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      $scope.membersForm.$setPristine();
      self.request_pending = false;
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.updateNotifications = function($scope) {
    self.request_pending = true;
    $http({
      method : "POST",
      url : "/api/team/settings/notifications",
      headers : {'Content-Type' : "application/json"},
      data : {
        AUTH_TOKEN : "77eb2a3a66d0835701e4eed1dc85896e6f368a7c",
        notificationsInfo : self.notificationsInfo,
        lang : language.langFile,
        webApp : true,
        _csrf : teamAPI.csrf
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      $scope.notificationsForm.$setPristine();
      self.request_pending = false;
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.updateCompanyStatus = function($scope) {
    self.request_pending = true;
    $http({
      method : "POST",
      url : "/api/team/settings/company",
      headers : {'Content-Type' : "application/json"},
      data : {
        AUTH_TOKEN : "efcc9eebd845a186341388f16002cd96145a1f91",
        lang : language.langFile,
        webApp : true,
        _csrf : teamAPI.csrf
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      self.cs = 1;
      $scope.companyForm.$setPristine();
      self.request_pending = false;
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.updateLogoPicture = function($scope) {
    self.request_pending = true;
    var fd = new FormData();
    fd.append('AUTH_TOKEN', 'bed0985d51e32c4fd121437bfa52e2de3e9df38e');
    fd.append('logoPicture', self.imagesInfo.logo_file);
    fd.append('webApp', true);
    fd.append('_csrf', teamAPI.csrf);
    $http({
      method : "POST",
      url : "/api/team/settings/logo_image",
      headers : {'Content-Type' : undefined},
      transformRequest: angular.identity,
      data : fd
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      teamAPI.team_logo_url = response.imageURL;
      self.imagesInfo.logo_file = {};
      self.imagesInfo.logo = "";
      $scope.logoForm.$setPristine();
      self.request_pending = false;
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.updateThemePicture = function($scope) {
    self.request_pending = true;
    var fd = new FormData();
    fd.append('AUTH_TOKEN', '8e3d48071fc7338f837d5b65b614fa902800ef4e');
    fd.append('themePicture', self.imagesInfo.theme_file);
    fd.append('webApp', true);
    fd.append('_csrf', teamAPI.csrf);
    $http({
      method : "POST",
      url : "/api/team/settings/theme_image",
      headers : {'Content-Type' : undefined},
      transformRequest: angular.identity,
      data : fd
    }).then(function success(res) {
      self.request_pending = false;
      var response = decryptResponse(res.data);
      self.imagesInfo.theme_logo = {};
      self.imagesInfo.theme = "";
      $scope.themeForm.$setPristine();
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.delete = function() {
    self.request_pending = true;
    $http({
      method : "POST",
      url : "/api/team/settings/delete",
      headers : {'Content-Type' : "application/json"},
      data : {
        AUTH_TOKEN : "f3ec31b9db60050687ed4814b3a7df0bb73957be",
        lang : language.langFile,
        webApp : true,
        _csrf : teamAPI.csrf
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == "1") {
        window.location = "/landing_page";
      }
      self.request_pending = false;
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("surveyCreatorAPI", function($timeout, $window, $http, teamAPI, alertsAPI, language) {
  var self = this;
  self.categories = {};
  self.loading = true;
  self.uploading = false;
  self.response = "";
  self.createdSurvey = {
    surveyDetails : {
      "name" : "",
      "description" : "",
      "public" : false,
      "category_token" : "",
      "sex" : "",
      "ethnicity" : "",
      "age_group" : "",
      "image_url" : "",
      "image_file" : "",
      "data_collected" : {
        "fullname" : false,
        "email_phone" : false,
        "sex" : false,
        "ethinicity" : false,
        "languages" : false,
        "education" : false,
        "relationship" : false,
        "employment" : false,
        "religion" : false,
        "dob" : false
      }
    },
    freeResponseQuestions : [],
    checkboxResponseQuestions : [],
    booleanResponseQuestions : [],
    radioResponseQuestions : [],
    menuResponseQuestions : [],
    order : []
  };

  self.retrieveData = function() {
    if (!teamAPI.membership_active) {
      self.loading = false;
      return;
    }
    self.loading = true;
    $http({
      method : "GET",
      url : "/api/4868b0b4105908817f",
      headers : {'Content-Type' : "application/json"},
      params : {
        reference_code : "FridayMorningPreMeet",
        key : "993421",
        lang : language.langFile,
        webApp : true
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      self.categories = response.categories;
      self.loading = false;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.createSurvey = function() {
    var file;

    if ($("#imageFile")) {
      file = $("#imageFile")[0].files[0];
    }

    if (typeof file === "undefined") {
      file = "";
    }

    var fd = new FormData();
    fd.append('AUTH_TOKEN', '1491b201ac5943a80ccd77b9e59b6f542b2ae4b0');
    fd.append('surveyImage', file);
    fd.append('createdSurvey', angular.toJson(self.createdSurvey));
    fd.append('lang', language.langFile);
    fd.append('webApp', true);
    fd.append('_csrf', teamAPI.csrf);
    $http({
      method : "POST",
      url : "/api/team/surveys/create",
      headers : {'Content-Type' : undefined},
      transformRequest: angular.identity,
      data : fd
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      var resultCode = response.resultCode;
      self.uploading = false;
      switch (resultCode) {
        case 1:
          $window.location.href = "/manage";
          break;
        case -1:
          self.response = language.dictionary.problem_image;
          break;
      }
    }, function error(res) {
      self.uploading = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("pollCreatorAPI", function($timeout, $window, $http, teamAPI, alertsAPI, language) {
  var self = this;

  self.categories = {};
  self.loading = true;
  self.uploading = false;
  self.response = "";
  self.createdPoll = {
    pollDetails : {
      "name" : "",
      "public" : false,
      "description" : "",
      "category_token" : "",
      "sex" : "",
      "ethnicity" : "",
      "age_group" : "",
      "image_url" : "",
      "image_file" : "",
      "data_collected" : {
        "fullname" : false,
        "email_phone" : false,
        "sex" : false,
        "ethinicity" : false,
        "languages" : false,
        "education" : false,
        "relationship" : false,
        "employment" : false,
        "religion" : false,
        "dob" : false
      }
    },
    pollQuestion : {
      question : "",
      options : []
    }
  };

  self.retrieveCategories = function() {
    if (!teamAPI.membership_active) {
      self.loading = false;
      return;
    }
    self.loading = true;
    $http({
      method : "GET",
      url : "/api/4868b0b4105908817f",
      headers : {'Content-Type' : "application/json"},
      params : {
        reference_code : "FridayMorningPreMeet",
        key : "993421",
        lang : language.langFile,
        webApp : true
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);

      self.categories = response.categories;
      self.loading = false;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.createPoll = function() {
    var file;

    if ($("#imageFile")) {
      file = $("#imageFile")[0].files[0];
    }

    if (typeof file === "undefined") {
      file = "";
    }

    var fd = new FormData();
    fd.append('AUTH_TOKEN', '9a8a502f1fdc798bd8ee71dcf64013ae9b4efe85');
    fd.append('pollImage', file);
    fd.append('lang', language.langFile);
    fd.append('poll', angular.toJson(self.createdPoll));
    fd.append('webApp', true);
    fd.append('_csrf', teamAPI.csrf);
    $http({
      method : "POST",
      url : "/api/team/polls/create",
      headers : {'Content-Type' : undefined},
      transformRequest: angular.identity,
      data : fd
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      var resultCode = response.resultCode;
      self.uploading = false;

      switch (resultCode) {
        case 1:
          $window.location.href = "/manage";
          break;
        case -1:
          self.response = language.dictionary.problem_image;
          break;
      }
    }, function error(res) {
      self.uploading = false;

      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("editorAPI", function($timeout, $window, $http, $location, teamAPI, alertsAPI, language) {
  var self = this;

  self.questionnaireData = {
    "editable" : true,
    "type" : $location.path().split("/")[1],
    "q_token" : $location.path().split("/")[2],
    "ppa" : 0,
    "details" : {
      "name" : "",
      "description" : "",
      "image_url" : "",
      "answer_url" : "",
      "sc_amount" : 0,
      "sc_update" : 0,
      "labs_created" : false,
      "labs_type" : null
    },
    "labs" : {}
  };
  self.loading = true;
  self.request_pending = false;
  self.response = "";
  self.edit = {
    "delete" : false,
    "name" : "",
    "description" : "",
    "image_url" : "",
    "asm" : 0
  };
  self.getEditorData = function() {
    self.loading = true;
    self.questionnaireData.type = $location.path().split("/")[1];
    self.questionnaireData.q_token = $location.path().split("/")[2];
    $http({
      method : "GET",
      url : "/api/team/e46f6da77f6a868a04",
      headers : {'Content-Type' : "application/json"},
      params : {
        reference_code : "SaturdayPreDB",
        key : "990127",
        lang : language.langFile,
        type : self.questionnaireData.type,
        token : self.questionnaireData.q_token,
        webApp : true
      }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      self.questionnaireData.editable = response.editable;
      self.questionnaireData.ppa = response.ppa;
      self.questionnaireData.details.name = response.details.name;
      self.edit.name = response.details.name;
      self.questionnaireData.details.description = response.details.description;
      self.edit.description = response.details.description;
      self.questionnaireData.details.image_url = response.details.image_url;
      self.questionnaireData.details.answer_url = response.details.answer_url;
      self.edit.image_url = response.details.image_url;
      self.questionnaireData.details.sc_amount = response.details.sc_amount;
      self.questionnaireData.details.sc_update = response.details.sc_amount;
      self.questionnaireData.details.labs_created = response.details.labs_created;
      self.questionnaireData.details.labs_type = response.details.labs_type;

      self.questionnaireData.labs = response.labs;

      self.loading = false;
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
  self.editSurvey = function() {
    $http({
        method : "POST",
        url : "/api/team/surveys/edit",
        headers: {'Content-Type': 'application/json'},
        data : {
          AUTH_TOKEN : "5a1f61c030b13abfbe1d551299a2c1254771cccb",
          type : self.questionnaireData.type,
          token : self.questionnaireData.q_token,
          edit : self.edit,
          webApp : true,
          _csrf : teamAPI.csrf
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      self.request_pending = false;
      if (response.resultCode == "1") {
        self.edit = {
          "delete" : false,
          "name" : "",
          "description" : "",
          "image_url" : "",
          "asm" : 0
        };
        self.getEditorData();
      } else if (response.resultCode == "-1") {
        self.response = language.dictionary.problem_image;
      }
    }, function error(res) {
      self.request_pending = false;
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        alertsAPI.show(language.dictionary.error_verify_snackbar);
      }
    });
  };
});

app.service("profilePublicAPI", function($http, $location, $window, $timeout, $interval, alertsAPI, language) {
  var self = this;
  self.profile = {
    pvk : $location.path().split("/")[2],
    username : $location.path().split("/")[3],
    profile_theme_url : "",
    profile_picture_url : "https://res.cloudinary.com/survey-stacks/image/upload/v1499041967/30af3b2db890ab2d3f96a34fa309c0ce/"+$location.path().split("/")[2]+"/profile_picture.png",
    motto : "",
    since : "",
  };
  self.achievements = {
    total_stacks : 0,
    total_coins : 0,
    total_surveys_answered : 0,
    total_questions_answered : 0,
    total_rewards_claimed : 0,
  };
  self.contactInfo = {
    email : "",
    phone_number : "",
    website : "",
    facebook : "",
    instagram : ""
  };
  self.loading = true;
  self.profile_exists = false;
  self.profileData = function() {
    self.loading = true;
    $http({
        method : "GET",
        url : "/api/users/"+self.profile.pvk+"/712b10c4f7ce4432af",
        headers: {'Content-Type': 'application/json'},
        params : {
          reference_code : "FridayEveningCloudySky",
          key : "995215",
          pvk : self.profile.pvk,
          username : self.profile.username,
          webApp : true,
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      var profileInfo = response.profileInfo;
      var achievementsInfo = response.achievementsInfo;
      var contactInfo = response.contactInfo;
      self.profile_exists = (response.resultCode === 1);
      self.profile.profile_picture_url = profileInfo.profile_picture_url;
      self.profile.motto = profileInfo.motto;
      self.profile.since = profileInfo.since;
      self.achievements.total_stacks = achievementsInfo.stacks_earned;
      self.achievements.total_coins = achievementsInfo.coins_earned;
      self.achievements.total_surveys_answered = achievementsInfo.surveys_answered;
      self.achievements.total_questions_answered = achievementsInfo.questions_answered;
      self.achievements.total_rewards_claimed = achievementsInfo.rewards_claimed;
      self.contactInfo.email = contactInfo.email;
      self.contactInfo.phone_number = contactInfo.phone_number;
      self.contactInfo.website = contactInfo.website;
      self.contactInfo.facebook = contactInfo.facebook;
      self.contactInfo.instagram = contactInfo.instagram;
      self.loading = false;
      if (self.profile_exists) {
        $("#toolbar_header").html(self.profile.username);
      }
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        self.profile_exists = false;
      }
    });
  };
});

app.service("teamPublicAPI", function($http, $location, $window, $timeout, $interval, alertsAPI, language) {
  var self = this;
  self.profile = {
    tvk : $location.path().split("/")[2],
    name : "",
    nickname : $location.path().split("/")[3],
    team_theme_url : "",
    team_logo_url : "",
    description : "",
    since : "",
  };
  self.contactInfo = {
    email : "",
    phone_number : "",
    website : "",
    facebook : "",
    instagram : ""
  };
  self.additional = {
    verified : false,
    members : 0,
  };
  self.loading = true;
  self.team_exists = false;
  self.profileData = function() {
    self.loading = true;
    $http({
        method : "GET",
        url : "/api/teams/"+self.profile.tvk+"/a50cf95ced779706b6",
        headers: {'Content-Type': 'application/json'},
        params : {
          reference_code : "SundayNightTenSeven",
          key : "991821",
          tvk : self.profile.tvk,
          nickname : self.profile.nickname,
          webApp : true,
          lang : language.langFile
        }
    }).then(function success(res) {
      var response = decryptResponse(res.data);
      if (response.resultCode == -1) {
        self.team_exists = false;
        self.loading = false;
        return;
      }
      self.team_exists = true;
      self.profile.name = response.profileData.name;
      self.profile.description = response.profileData.description;
      self.profile.team_theme_url = response.profileData.theme_url;
      self.profile.team_logo_url = response.profileData.logo_url;
      self.profile.since = response.profileData.since.toString();
      self.contactInfo = response.contactInfo;
      self.additional = response.additional;
      self.loading = false;
      $("#toolbar_header").html(self.profile.name);
    }, function error(res) {
      if (res.status == 500) {
        alertsAPI.show(language.dictionary.error_snackbar);
      } else {
        self.team_exists = false;
      }
    });
  };
});
