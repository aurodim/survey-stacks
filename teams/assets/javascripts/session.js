"use strict";

var app = angular.module("survey-stacks-teams", ['ngResource', 'ngRoute', 'ngCookies', 'ngAnimate', 'ngSanitize']);

app.config(['$compileProvider', function ($compileProvider) {
  $compileProvider.debugInfoEnabled(false);
}]);

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $locationProvider.hashPrefix('');
  $routeProvider
     .when('/', {
         templateUrl : '/teams/views/dashboard.html',
         controller  : 'DashboardController'
     })

     .when('/dashboard', {
         templateUrl : '/teams/views/dashboard.html',
         controller  : 'DashboardController'
     })

     .when('/analytics', {
         templateUrl : '/teams/views/analytics.html',
         controller  : 'AnalyticsController'
     })

     .when('/labs', {
         templateUrl : '/teams/views/labs.html',
         controller  : 'LabsController'
     })

     .when('/manage', {
         templateUrl : '/views/manage.html',
         controller  : 'ManageController'
     })

     .when('/members', {
         templateUrl : '/teams/views/members.html',
         controller  : 'MembersController'
     })

     .when('/notifications', {
         templateUrl : '/teams/views/notifications.html',
         controller  : 'NotificationsController'
     })

     .when('/settings', {
         templateUrl : '/teams/views/settings.html',
         controller  : 'SettingsController'
     })

     .when('/survey_creator', {
         templateUrl : '/teams/views/survey_creator.html',
         controller  : 'SurveyCreatorController'
     })

     .when('/poll_creator', {
         templateUrl : '/teams/views/poll_creator.html',
         controller  : 'PollCreatorController'
     })

     .when('/surveys/:survey_token/edit', {
       templateUrl : '/teams/views/editor.html',
       controller : 'EditorController'
     })

     .when('/polls/:poll_token/edit', {
       templateUrl : '/teams/views/editor.html',
       controller : 'EditorController'
     })

     .when('/logout', {
        controller: '',
        templateUrl: '/views/logout.html',
        resolve: {
          init: function() {
            window.location = "/6bda982nndks62fgdba/teams/logout";
          }
        }
     })

     .when('/users/:user_validation_key/:username/profile', {
       templateUrl : '/views/profile_view_outline.html',
       controller  : 'ProfilePublicController',
     })

     .when('/teams/:team_validation_key/:nickname/profile', {
       templateUrl : '/views/team_view_outline.html',
       controller  : 'TeamPublicController',
     })

     .otherwise({
       redirectTo : "/dashboard"
     });

   $locationProvider.html5Mode({
       enabled: true,
       requireBase: true,
       rewriteLinks: true
   });
 }]);

app.directive("onErrorSrc", function() {
  return {
    link: function(scope, element, attrs) {
      element.bind('error', function() {
        if (attrs.src != attrs.onErrorSrc || attrs.src == "") {
          attrs.$set('src', attrs.onErrorSrc);
        }
      });
    }
  };
});

app.directive('onFileSelect', function() {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var onChangeFunc = scope.$eval(attrs.onFileSelect);
      element.bind('change', onChangeFunc);
    }
  };
});

app.directive("showOnLoad", function() {
  return {
    restrict : "EA",
    link : function($scope, element, attrs) {
      element.bind('load', function() {
        $scope.api.loading = false;
        $scope.$apply();
      });
    }
  }
});

app.directive('scroller', function () {
  return {
      restrict: 'A',
      link: function ($scope, element, attrs) {
          element.on('mouseenter', function () {
            $("#view").css("overflow", "hidden");
          });
          element.on('mouseleave', function () {
            $("#view").css("overflow", "auto");
          });
      }
  };
});

app.directive('ngDrag', function(surveyCreatorAPI) {
  return {
    restrict : 'A',
    scope : {
      dragfinished : "&dfFunc"
    },
    link: function(scope, element, attrs) {
      element.on('dragstart', function(event) {
        $(".order_item").addClass("shake");
        var index = $(event.target).attr('data-index');
        var question = $(event.target).attr('data-question');
        $("#listorder").attr('ng-data-original', index);
        $("#listorder").attr('ng-data-original-t', question);
      });
      element.on('dragenter', function(event) {
        var index = $(element).data('index');
        var index_of_original = $("#listorder").attr('ng-data-original');
        var text_of_original = $("#listorder").attr('ng-data-original-t');
        $(element).css("background-color", "#ccc");
      });
      element.on('dragover', function(event) {
        $(element).css("background-color", "#ccc");
        event.preventDefault();
      });
      element.on('dragleave', function(event) {
        $(".order_item").removeAttr("style");
      });
      element.on('dragend', function(event) {
        $(".order_item").removeClass("shake");
        $(".order_item").removeAttr("style");
      });
      element.on('drop', function(event) {
        event.preventDefault();
        $(".order_item").removeClass("shake");
        $(".order_item").removeAttr("style");
        $(".order_item").blur();
        var index = $(element).attr('data-index');
        var index_of_original = $("#listorder").attr('ng-data-original');
        var text_of_original = $("#listorder").attr('ng-data-original-t');
        scope.dragfinished({index_current : index_of_original, question : text_of_original, index_new : index});
      });
    }
  };
});

app.controller("SessionController", function($scope, language, metadata, teamAPI) {
  $scope.language = language;
  $scope.metadata = metadata;
  $scope.team = teamAPI;
  var ScrollPos = 0;
  $("#view").scroll(function () {
      var CurScrollPos = $(this).scrollTop();
      if (CurScrollPos > ScrollPos) {
        $(".fab_addd").hide(300);
        $(".fabb").hide(350);
        if ($("#survey_c").is(":visible")) {
          $("#fab_add_ico").html("&#xE145;");
          $("#survey_c").fadeOut(300);
          $("#poll_c").fadeOut(300);
        }
      } else {
        $(".fab_addd").show();
        $(".fabb").show(350);
      }
      ScrollPos = CurScrollPos;
  });
  $scope.openFabs = function() {
    if ($("#survey_c").is(":visible")) {
      $("#fab_add_ico").html("&#xE145;");
      $("#survey_c").fadeOut(300);
      $("#poll_c").fadeOut(300);
    } else {
      $("#fab_add_ico").html("&#xE14C;");
      $("#survey_c").show(300);
      $("#poll_c").show(300);
    }
  };
  $scope.fabHov = function(event) {
    $(event.currentTarget).css("background", $scope.team.backgroundColorDark);
    $(event.currentTarget).css("border-color", $scope.team.backgroundColorDark);
  };
  $scope.fabReg = function(event) {
    $(event.currentTarget).css("background", $scope.team.backgroundColor);
    $(event.currentTarget).css("border-color", $scope.team.backgroundColor);
  };
});

app.controller("ToolbarController", function($scope, $rootScope, $timeout, language, metadata, teamAPI) {
  $scope.route = "";
  $scope.metadata = metadata;
  $scope.language = language;
  $scope.team = teamAPI;
  $scope.$on('$routeChangeStart', function($event, next, current) {
    $scope.route = next["$$route"].originalPath.split("/")[next["$$route"].originalPath.split("/").length - 1];
    $scope.metadata.title = $scope.language.dictionary.app_name;
    $scope.metadata.description = $scope.language.dictionary.description_metadata;
    if ($scope.route == "") {
      $scope.route = "dashboard";
    }
  });
});

app.controller("AlertsController", function($scope, language, alertsAPI) {
  $scope.api = alertsAPI;
});

app.controller("DrawerController", function($scope, $rootScope, $timeout, $route, alertsAPI, language, teamAPI) {
  $scope.route = "";
  $scope.$on('$routeChangeStart', function($event, next, current) {
    $scope.route = next["$$route"].originalPath.split("/")[next["$$route"].originalPath.split("/").length - 1];
    if ($scope.route == "") {
      $scope.route = "dashboard";
    }
  });
  $scope.language = language;
  $scope.team = teamAPI;
  $scope.itemHov = function(event) {
    $(event.currentTarget).css("opacity", "0.5");
  };
  $scope.itemReg = function(event) {
    $(event.currentTarget).css("background", $scope.team.backgroundColorDark);
    $(event.currentTarget).css("opacity", "");
  };
});

app.controller('DashboardController', function($scope, $rootScope, $timeout, teamAPI, language, dashboardAPI) {
  $scope.language = language;
  $scope.team = teamAPI;
  $scope.api = dashboardAPI;
  $scope.api.gatherData(function(error) {
    if (error) return;

    LineChart("dash_daily", $scope.language.dictionary.qs_responses, $scope.language.dictionary.days, $scope.api.graphics.responses, {max:$scope.api.graphics.max, fill:true}, [$scope.language.dictionary.surveys, $scope.language.dictionary.polls]);
    PieChart("dash_comb", $scope.language.dictionary.types_qs, [$scope.language.dictionary.surveys, $scope.language.dictionary.polls], $scope.api.graphics.created, {legend:false, pie_percentage : false});
    $scope.api.loading = false;
  });
});

app.controller('AnalyticsController', function($scope, $rootScope, $timeout, teamAPI, language, analyticsAPI) {
  $scope.language = language;
  $scope.team = teamAPI;
  $scope.api = analyticsAPI;
  $scope.api.getAnalyticsData();
  $scope.display_board = "";
  $scope.display_board_type = "";
  $scope.display_board_obj = {};
  $scope.show_downloads = false;
  $scope.show_format = false;
  $scope.toggleDownload = function() {
    if ($scope.download_format != "") return false;
    $scope.download_as = -1;
    $scope.show_format = false;
    $scope.show_downloads = !$scope.show_downloads;
  };
  $scope.download_as = -1;
  $scope.download_format = "";
  $scope.format = function(index) {
    $scope.download_as = index;
    $scope.show_format = true;
  };
  $scope.back = function() {
    $("#viewBoard").show();
    $scope.display_board = "";
    $scope.display_board_type = "";
    $scope.download_as = -1;
    $scope.download_format = "";
    $scope.show_downloads = false;
    $scope.show_format = false;
    $scope.display_board_obj = {};
  };
  $scope.viewAnalytics = function(parent, index) {
    var array;
    var token;
    var cardColumns = {
      one : [],
      two : [],
      three : []
    };
    DestroyCharts();

    if (parent == 0) {
      array = $scope.api.analyticsData.surveys[index];
      token = array.view.token;
      $scope.display_board_obj = array.analytics;
      $scope.display_board = token;
      $scope.display_board_type = "survey";
      $scope.surveyAnalytics(array, token);
    } else if (parent == 1) {
      array = $scope.api.analyticsData.polls[index];
      token = array.view.token;
      $scope.display_board_obj = array.analytics;
      $scope.display_board = token;
      $scope.display_board_type = "poll";
      $scope.pollAnalytics(array, token);
    }

    var c = 0;
    for (var i = 0; i < array.cards.length; i++) {
      if (c === 0) {
        cardColumns.one.push(array.cards[i]);
        c = 1;
      } else if (c === 1) {
        cardColumns.two.push(array.cards[i]);
        c = 2;
      } else if (c === 2) {
        cardColumns.three.push(array.cards[i]);
        c = 0;
      }
    }

    $scope.display_board_obj.cards = cardColumns;
    $("#viewBoard").hide();
  };
  $scope.showResponses = true;
  $scope.showRespondent = true;
  $scope.showCards = true;
  $scope.toggleResponses = function() {
    $scope.showResponses = !$scope.showResponses;
  };
  $scope.toggleRespondent = function() {
    $scope.showRespondent = !$scope.showRespondent;
  };
  $scope.toggleCards = function() {
    $scope.showCards = !$scope.showCards;
  };
  $scope.pollAnalytics = function(array, token) {
    $timeout(function() {
      PieChart(token+"_pie_chart", array.analytics.questionnaire.question, array.analytics.questionnaire.options, array.analytics.questionnaire.percentages);
      array.analytics.respondents.forEach(function(analytic,i) {
        var options = [];
        analytic.options.forEach(function(option) {
          if (option == "") option = "n/a";
          if (option.length == 0) option = "n/a";
          switch (analytic.name) {
            case "sex":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.sexes[option]);
              break;
            case "ethnicity":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.ethnicities[option]);
              break;
            case "education":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.education_levels[option]);
              break;
            case "relationship_status":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.relationship_statuses[option]);
              break;
            case "employment_status":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.employment_statuses[option]);
              break;
            case "religion":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.religions[option]);
              break;
            case "age_group":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.age_groups[option]);
              break;
            case "languages":
              if (option == "n/a") return options.push(option);
              if (Array.isArray(option)) {
                var t = option.join(" & ");
                var splits = t.split("&");

                for (var i=0; i < splits.length; i++) {
                  t = t.replace(option[i], language.globals.supportedLanguages[option[i]]);
                }
                options.push(t);
                return;
              }
              options.push(language.globals.supportedLanguages[option]);
              break;
          }
        });
        PieChart(token+i+"_pie_chart", language.dictionary[analytic.name], options, analytic.percentages);
      });
    }, 100);
  };
  $scope.surveyAnalytics = function(array, token) {
    $timeout(function() {
      LineChart(token+"_daily_bar_graph", language.dictionary.daily_r, language.dictionary.days, array.analytics.questionnaire.daily, {legend:false});
      LineChart(token+"_monthly_bar_graph", language.dictionary.monthly_r, language.dictionary.months, array.analytics.questionnaire.monthly, {legend:false});
      LineChart(token+"_yearly_bar_graph", language.dictionary.yearly_r, array.analytics.questionnaire.yearly.years, array.analytics.questionnaire.yearly.values, {legend:false});
      array.analytics.graphic_questions.forEach(function(graphic, i) {
        var r = Math.floor(Math.random() * 2);
        if (r == 0) {
          DonutChart(token+i+"_graphic", graphic.question, graphic.options, graphic.percentages);
        } else {
          PieChart(token+i+"_graphic", graphic.question, graphic.options, graphic.percentages);
        }
      });
      array.analytics.respondents.forEach(function(analytic,i) {
        var options = [];
        analytic.options.forEach(function(option) {
          if (option == "") option = "n/a";
          switch (analytic.name) {
            case "sex":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.sexes[option]);
              break;
            case "ethnicity":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.ethnicities[option]);
              break;
            case "education":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.education_levels[option]);
              break;
            case "relationship_status":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.relationship_statuses[option]);
              break;
            case "employment_status":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.employment_statuses[option]);
              break;
            case "religion":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.religions[option]);
              break;
            case "age_group":
              if (option == "n/a") return options.push(option);
              options.push(language.dictionary.age_groups[option]);
              break;
            case "languages":
              if (option == "n/a") return options.push(option);
              if (Array.isArray(option)) {
                var t = option.join(" & ");
                var splits = t.split("&");

                for (var i=0; i < splits.length; i++) {
                  t = t.replace(option[i], language.globals.supportedLanguages[option[i]]);
                }
                options.push(t);
                return;
              }
              options.push(language.globals.supportedLanguages[option]);
              break;
          }
        });
        PieChart(token+i+"_pie_chart", language.dictionary[analytic.name], options, analytic.percentages);
      });
    }, 100);
  };
});

app.controller('LabsController', function($scope, $rootScope, teamAPI, language, labsAPI) {
  $scope.language = language;
  $scope.team = teamAPI;
  $scope.api = labsAPI;
  $scope.api.retrieveData();
  $scope.templates = ["/views/quiz.html", "/views/key_needed.html", "/views/emoji.html", "/views/game.html"];
  $scope.template = "";
  $scope.stepone = true;
  $scope.steptwo = false;
  $scope.initiated = false;
  $scope.options = {
    free : true,
    checkbox : true,
    radio : true,
    boolean : true,
    single : true
  };
  $scope.typeChosen = function(index) {
    $scope.api.createdQuestionnaire.lab_option = index;
    $scope.stepone = false;
    $scope.steptwo = true;
  };
  $scope.init = function(index) {
    var a = [0,1,2,3];
    if (!a.includes($scope.api.createdQuestionnaire.lab_option)) {
      $scope.initiated = false;
      $scope.stepone = true;
      $scope.steptwo = false;
      return;
    }
    if (index !== 0 && index !== 1) return;

    $scope.initiated = true;
    $scope.steptwo = false;

    if (index == 0) {
      $scope.api.createdQuestionnaire.type = "survey";
    } else if (index == 1) {
      $scope.api.createdQuestionnaire.type = "poll";
    }

    switch($scope.api.createdQuestionnaire.lab_option) {
      case 0:
        $scope.options = {
          free : true,
          checkbox : true,
          radio : true,
          boolean : true,
          single : true
        };
        $scope.api.createdQuestionnaire.method_options = {
          show_score : false,
          out_of : null
        };
        break;
      case 1:
        $scope.options = {
          free : true,
          checkbox : true,
          radio : true,
          boolean : true,
          single : true
        };
        $scope.api.createdQuestionnaire.method_options = {
          unique_keys : false
        };
        break;
      case 2:
        $scope.options = {
          free : true,
          checkbox : true,
          radio : true,
          boolean : false,
          single : true
        };
        $scope.api.createdQuestionnaire.method_options = {};
        break;
      case 3:
        $scope.options = {
          free : true,
          checkbox : true,
          radio : true,
          boolean : true,
          single : true
        };
        $scope.api.createdQuestionnaire.method_options = {
          timed : false,
          max_time : null,
          pollQuestionsExtra : []
        };
        break;
    }
    $scope.template = $scope.templates[$scope.api.createdQuestionnaire.lab_option];
  };
  $scope.back = function() {
    $scope.api.createdQuestionnaire.lab_option = null;
    $scope.initiated = false;
    $scope.stepone = true;
    $scope.steptwo = false;
    $scope.api.createdQuestionnaire.survey = {
      "freeResponseQuestions" : [],
      "checkboxResponseQuestions" : [],
      "booleanResponseQuestions" : [],
      "radioResponseQuestions" : [],
      "menuResponseQuestions" : [],
      "order" : []
    };
    $scope.api.createdQuestionnaire.poll = {
      "question" : "",
      "options" : []
    };
  };
  $scope.category_display = "";
  $scope.sex_display = "";
  $scope.ethnicity_display = "";
  $scope.age_display = "";
  $scope.questionOptions = {
    "type" : "",
    "index" : 0
  };
  $scope.fileSelected = function(elem) {
    var spl = elem.target.value.split("\\");
    $scope.api.createdQuestionnaire.details.image_file = spl[spl.length - 1];
    $scope.$apply();
  };
  $scope.questionOptionsE = function(type, index) {
    $scope.questionOptions.type = type;
    $scope.questionOptions.index = index;
    $scope.show_options_toolbar = true;
  };
  $scope.emptyQuestionOptionsB = function($event) {
    if ($(".response_input").is(":focus") || $event.target.parentNode.className === "optionsContainer" || !$scope.show_options_toolbar) {
      return;
    }
    for (var i = 0; i < $event.originalEvent.path.length; i++) {
      if ($event.originalEvent.path[i].className === "optionsContainer") {
        return;
      }
    }
    $scope.questionOptions.type = "";
    $scope.questionOptions.index = 0;
    $scope.show_options_toolbar = false;
  };
  $scope.addFreeResponse = function() {
    var object = {};
    switch($scope.api.createdQuestionnaire.lab_option) {
      case 0:
        object = {
          "question" : "",
          "options" : {
            "pattern" : "a",
            "required" : false,
            "algorithm" : "o",
            "answer" : "Lorem Ipsum",
            "worth" : 1
          }
        };
        break;
      case 2:
        object = {
          "question" : "",
          "options" : {
            "required" : false,
          }
        };
      default:
        object = {
          "question" : "",
          "options" : {
            "pattern" : "a",
            "required" : false,
          }
        };
    }
    $scope.api.createdQuestionnaire.survey.freeResponseQuestions.push(object);
    var order_name = "fr_"+($scope.api.createdQuestionnaire.survey.freeResponseQuestions.length - 1);
    $scope.api.createdQuestionnaire.survey.order.push(order_name);
  };
  $scope.addCheckboxResponse = function() {
    var object = {};
    switch($scope.api.createdQuestionnaire.lab_option) {
      case 0:
        object = {
          "question" : "",
          "responses" : [],
          "options" : {
            "required" : false,
            "answer" : [],
            "worth" : 1
          }
        };
        break;
      default:
        object = {
          "question" : "",
          "responses" : [],
          "options" : {
            "required" : false
          }
        };
    }
    $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions.push(object);
    var order_name = "cbr_"+($scope.api.createdQuestionnaire.survey.checkboxResponseQuestions.length - 1);
    $scope.api.createdQuestionnaire.survey.order.push(order_name);
  };
  $scope.addBooleanResponse = function() {
    var object = {};
    switch($scope.api.createdQuestionnaire.lab_option) {
      case 0:
        object = {
          "question" : "",
          "options" : {
            "required" : false,
            "answer" : 0,
            "worth" : 1
          }
        };
        break;
      default:
        object = {
          "question" : "",
          "options" : {
            "required" : false
          }
        };
    }
    $scope.api.createdQuestionnaire.survey.booleanResponseQuestions.push(object);
    var order_name = "br_"+($scope.api.createdQuestionnaire.survey.booleanResponseQuestions.length - 1);
    $scope.api.createdQuestionnaire.survey.order.push(order_name);
  };
  $scope.addRadioResponse = function() {
    var object = {};
    switch($scope.api.createdQuestionnaire.lab_option) {
      case 0:
        object = {
          "question" : "",
          "responses" : [],
          "options" : {
            "required" : false,
            "answer" : 0,
            "worth" : 1
          }
        };
        break;
      default:
        object = {
          "question" : "",
          "responses" : [],
          "options" : {
            "required" : false
          }
        };
    }
    $scope.api.createdQuestionnaire.survey.radioResponseQuestions.push(object);
    var order_name = "rr_"+($scope.api.createdQuestionnaire.survey.radioResponseQuestions.length - 1);
    $scope.api.createdQuestionnaire.survey.order.push(order_name);
  };
  $scope.addMenuResponse = function() {
    var object = {};
    switch($scope.api.createdQuestionnaire.lab_option) {
      case 0:
        object = {
          "question" : "",
          "responses" : [],
          "options" : {
            "required" : false,
            "answer" : 0,
            "worth" : 1
          }
        };
        break;
      default:
        object = {
          "question" : "",
          "responses" : [],
          "options" : {
            "required" : false
          }
        };
    }
    $scope.api.createdQuestionnaire.survey.menuResponseQuestions.push(object);
    var order_name = "mr_"+($scope.api.createdQuestionnaire.survey.menuResponseQuestions.length - 1);
    $scope.api.createdQuestionnaire.survey.order.push(order_name);
  };
  $scope.addCheckboxItemResponse = function($index) {
    $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions[$index]["responses"].push("");
  };
  $scope.addRadioItemResponse = function($index) {
    $scope.api.createdQuestionnaire.survey.radioResponseQuestions[$index]["responses"].push("");
  };
  $scope.addMenuItemResponse = function($index) {
    $scope.api.createdQuestionnaire.survey.menuResponseQuestions[$index]["responses"].push("");
  };
  $scope.addAnswer = function(parentIndex, index) {
    var question = $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions[parentIndex];
    if (question.responses.length < index) return;
    var value = question.responses[index];
    var partOf = question.responses.indexOf(value);
    if (partOf > -1) {
      $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions[parentIndex].options.answer.splice(index, 1);
    } else {
      $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions[parentIndex].options.answer.push(index);
    }
  };
  $scope.removeItem = function($index, type) {
    switch (type) {
      case "fR":
        $scope.api.createdQuestionnaire.survey.freeResponseQuestions.splice($index, 1);
        var order_name = "fr_"+$index;
        var index_of_order = $scope.api.createdQuestionnaire.survey.order.indexOf(order_name)
        $scope.api.createdQuestionnaire.survey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdQuestionnaire.survey.freeResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdQuestionnaire.survey.order.length; i ++) {
            var item_prop = $scope.api.createdQuestionnaire.survey.order[i].split("_");
            if (item_prop[0] != "fr") return;

            if (item_prop[1] <= $index) return;

            var index_of_item = $scope.api.createdQuestionnaire.survey.order.indexOf($scope.api.createdQuestionnaire.survey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "fr_"+(index_of_item_i);
            $scope.api.createdQuestionnaire.survey.order.splice(index_of_item,1,newname);
          }
        }
        break;
      case "cR":
        $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions.splice($index, 1);
        var order_name = "cbr_"+$index;
        var index_of_order = $scope.api.createdQuestionnaire.survey.order.indexOf(order_name)
        $scope.api.createdQuestionnaire.survey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdQuestionnaire.survey.checkboxResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdQuestionnaire.survey.order.length; i ++) {
            var item_prop = $scope.api.createdQuestionnaire.survey.order[i].split("_");
            if (item_prop[0] != "cbr") continue;

            if (item_prop[1] <= $index) continue;

            var index_of_item = $scope.api.createdQuestionnaire.survey.order.indexOf($scope.api.createdQuestionnaire.survey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "cbr_"+(index_of_item_i);
            $scope.api.createdQuestionnaire.survey.order.splice(index_of_item,1,newname);
          }
        }
        break;
      case "bR":
        $scope.api.createdQuestionnaire.survey.booleanResponseQuestions.splice($index, 1);
        var order_name = "br_"+$index;
        var index_of_order = $scope.api.createdQuestionnaire.survey.order.indexOf(order_name)
        $scope.api.createdQuestionnaire.survey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdQuestionnaire.survey.booleanResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdQuestionnaire.survey.order.length; i ++) {
            var item_prop = $scope.api.createdQuestionnaire.survey.order[i].split("_");
            if (item_prop[0] != "br") return;

            if (item_prop[1] <= $index) return;

            var index_of_item = $scope.api.createdQuestionnaire.survey.order.indexOf($scope.api.createdQuestionnaire.survey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "br_"+(index_of_item_i);
            $scope.api.createdQuestionnaire.survey.order.splice(index_of_item,1,newname);
          }
        }
        break;
      case "rR":
        $scope.api.createdQuestionnaire.survey.radioResponseQuestions.splice($index, 1);
        var order_name = "rr_"+$index;
        var index_of_order = $scope.api.createdQuestionnaire.survey.order.indexOf(order_name)
        $scope.api.createdQuestionnaire.survey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdQuestionnaire.survey.radioResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdQuestionnaire.survey.order.length; i ++) {
            var item_prop = $scope.api.createdQuestionnaire.survey.order[i].split("_");
            if (item_prop[0] != "rr") continue;

            if (item_prop[1] <= $index) continue;

            var index_of_item = $scope.api.createdQuestionnaire.survey.order.indexOf($scope.api.createdQuestionnaire.survey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "rr_"+(index_of_item_i);
            $scope.api.createdQuestionnaire.survey.order.splice(index_of_item,1,newname);
          }
        }
        break;
      case "mR":
        $scope.api.createdQuestionnaire.survey.menuResponseQuestions.splice($index, 1);
        var order_name = "mr_"+$index;
        var index_of_order = $scope.api.createdQuestionnaire.survey.order.indexOf(order_name)
        $scope.api.createdQuestionnaire.survey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdQuestionnaire.survey.menuResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdQuestionnaire.survey.order.length; i ++) {
            var item_prop = $scope.api.createdQuestionnaire.survey.order[i].split("_");
            if (item_prop[0] != "mr") continue;

            if (item_prop[1] <= $index) continue;

            var index_of_item = $scope.api.createdQuestionnaire.survey.order.indexOf($scope.api.createdQuestionnaire.survey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "mr_"+(index_of_item_i);
            $scope.api.createdQuestionnaire.survey.order.splice(index_of_item,1,newname);
          }
        }
        break;
    }
  };
  $scope.removeResponseItem = function($parentIndex, $index, type) {
    switch (type) {
      case "cR":
        $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions[$parentIndex]["responses"].splice($index, 1);
        if ($scope.api.createdQuestionnaire.lab_option === 0) {
          $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions[$parentIndex]["options"]["answer"].splice($index, 1);
        }
        break;
      case "rR":
        $scope.api.createdQuestionnaire.survey.radioResponseQuestions[$parentIndex]["responses"].splice($index, 1);
        if ($scope.api.createdQuestionnaire.lab_option === 0) {
          delete $scope.api.createdQuestionnaire.survey.booleanResponseQuestions[$parentIndex]["options"]["answer"][$index];
        }
        break;
      case "mR":
        $scope.api.createdQuestionnaire.survey.menuResponseQuestions[$parentIndex]["responses"].splice($index, 1);
        if ($scope.api.createdQuestionnaire.lab_option === 0) {
          delete $scope.api.createdQuestionnaire.survey.menuResponseQuestions[$parentIndex]["options"]["answer"][$index];
        }
        break;
    }
  };
  $scope.expandables = {
    "category_expand" : false,
    "sex_expand" : false,
    "ethnicity_expand" : false,
    "age_expand" : false,
    "image_list_expand" : false,
    "ud_expand" : false,
    "collected" : []
  };
  $scope.collect = function(value) {
    if ($scope.expandables.collected.indexOf(value) == -1) {
      $scope.expandables.collected.push(value);
    } else {
      var ind = $scope.expandables.collected.indexOf(value);
      $scope.expandables.collected.splice(ind, 1);
    }
  };
  $scope.show_options_toolbar = false;
  $scope.categories = $scope.api.categories;
  $scope.menuDisplay = {
    "question" : "",
    "responses" : []
  };
  $scope.setCategory = function($index, value, category) {
    $scope.category_display = category;
    $scope.api.createdQuestionnaire.details["category_token"] = value;
    $("#category").addClass("spanned");
    $scope.expandables.category_expand = false;
  };
  $scope.setSex = function(value, sex) {
    $scope.sex_display = sex;
    $scope.api.createdQuestionnaire.details["sex"] = value;
    $("#sex").addClass("spanned");
    $scope.expandables.sex_expand = false;
  };
  $scope.setEthnicity = function(value, ethnicity) {
    $scope.ethnicity_display = ethnicity;
    $scope.api.createdQuestionnaire.details["ethnicity"] = value;
    $("#ethnicity").addClass("spanned");
    $scope.expandables.ethnicity_expand = false;
  };
  $scope.setAgeGroup = function(value, age) {
    $scope.age_display = age;
    $scope.api.createdQuestionnaire.details["age_group"] = value;
    $("#age_group").addClass("spanned");
    $scope.expandables.age_expand = false;
  };
  $scope.hideMenu = function() {
    $(".menu").fadeOut(100);
  };
  $scope.dragfinished = function(index_current, question, index_new) {
    $scope.api.createdQuestionnaire.survey.order.splice(index_current, 1);
    $timeout(function() {
      $scope.$apply;
    }, 20);
    if (index_current < index_new) {
      $scope.api.createdQuestionnaire.survey.order.splice(index_new, 0, question);
    } else {
      $scope.api.createdQuestionnaire.survey.order.splice(index_new, 0, question);
    }
    $timeout(function() {
      $scope.$apply;
    }, 200);
  };
  $scope.getQuestionText = function(question) {
    var split = question.split("_");
    var type = split[0];
    var index = split[1];
    var question_text;

    switch (type) {
      case "fr":
        question_text = $scope.api.createdQuestionnaire.survey.freeResponseQuestions[index].question;
        break;
      case "cbr":
        question_text = $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions[index].question;
        break;
      case "br":
        question_text = $scope.api.createdQuestionnaire.survey.booleanResponseQuestions[index].question;
        break;
      case "rr":
        question_text = $scope.api.createdQuestionnaire.survey.radioResponseQuestions[index].question;
        break;
      case "mr":
        question_text = $scope.api.createdQuestionnaire.survey.menuResponseQuestions[index].question;
        break;
    }

    if (question_text == "") {
      question_text = $scope.language.dictionary.blank;
    }

    return question_text;
  };
  $scope.submitQuestionnaire = function() {
    if ($scope.api.createdQuestionnaire.type === "survey") {
       $scope.createSurvey();
    } else if ($scope.api.createdQuestionnaire.type === "poll") {
       $scope.createPoll();
    } else {
      return;
    }
  };
  $scope.createSurvey = function() {
    var details = $scope.api.createdQuestionnaire.details;
    var methodOptions = $scope.api.createdQuestionnaire.method_options;
    var responses = language.dictionary;
    var freeResponse = $scope.api.createdQuestionnaire.survey.freeResponseQuestions;
    var checkbox = $scope.api.createdQuestionnaire.survey.checkboxResponseQuestions;
    var boolean = $scope.api.createdQuestionnaire.survey.booleanResponseQuestions;
    var radio = $scope.api.createdQuestionnaire.survey.radioResponseQuestions;
    var single = $scope.api.createdQuestionnaire.survey.menuResponseQuestions;
    var questions = [];
    var amountedTotal = 0;
    questions = questions.concat(freeResponse, checkbox, boolean, radio, single);

    $scope.api.response = "";

    if (details.name == "") {
      $scope.api.response = responses.nameless_survey;
      return;
    } else if (details.description == "") {
      $scope.api.response = responses.descriptionless_survey;
      return;
    } else if (details.description.length > 80) {
      $scope.api.response = responses.description_toolong;
      return;
    } else if (details.category_token == "") {
      $scope.api.response = responses.categoryless_survey;
      return;
    } else if (details.sex == "") {
      $scope.api.response = responses.sexless_survey;
      return;
    } else if (details.ethnicity == "") {
      $scope.api.response = responses.ethnicityless_survey;
      return;
    } else if (details.age_group == "") {
      $scope.api.response = responses.ageless_survey;
      return;
    } else if (noQuestions(freeResponse, checkbox, boolean, radio, single)) {
      $scope.api.response = responses.no_questions;
      return;
    } else if (emptyQuestionsA(freeResponse)) {
      $scope.api.response = responses.empty_questions('fr');
      return;
    } else if (emptyQuestionsB(checkbox)) {
      $scope.api.response = responses.empty_questions('cbr');
      return;
    } else if (emptyQuestionsA(boolean)) {
      $scope.api.response = responses.empty_questions('br');
      return;
    } else if (emptyQuestionsB(radio)) {
      $scope.api.response = responses.empty_questions('rr');
      return;
    } else if (emptyQuestionsB(single)) {
      $scope.api.response = responses.empty_questions('mir');
      return;
    } else if (emptyResponses(checkbox)) {
      $scope.api.response = responses.empty_responses('cbr');
      return;
    } else if (emptyResponses(radio)) {
      $scope.api.response = responses.empty_responses('rr');
      return;
    } else if (emptyResponses(single)) {
      $scope.api.response = responses.empty_responses('mir');
      return;
    }

    switch($scope.api.createdQuestionnaire.lab_option) {
      case 0:
        if (methodOptions.out_of == "" || methodOptions.out_of === null) {
          $scope.api.response = responses.quiz_bad_total;
          return;
        }
        for (var i = 0; i < questions.length; i++) {
          amountedTotal += questions[i].options.worth;
          if (questions[i].options.worth === "") {
            $scope.api.response = responses.quiz_bad_worth;
            return;
          }
          if (questions[i].options.answer === "" || questions[i].options.answer.length == 0) {
            $scope.api.response = responses.quiz_bad_answer;
            return;
          }
        }
        if (amountedTotal != methodOptions.out_of) {
          $scope.api.response = responses.quiz_bad_amounted;
          return;
        }
        break;
      case 1:
        break;
      case 2:
        break;
      case 3:
        if (methodOptions.max_time == "" || methodOptions.max_time === null || methodOptions.max_time < 5) {
          $scope.api.response = responses.game_bad_time;
          return;
        }
        break;
      default:
        return;
    }

    $scope.api.uploading = true;
    $scope.api.createQuestionnaire();
  };
  $scope.addPollOption = function() {
    $scope.api.createdQuestionnaire.poll.options.push("");
  };
  $scope.removePollItem = function($index) {
    $scope.api.createdQuestionnaire.poll.options.splice($index, 1);
  };
  $scope.addPollQuestion = function() {
    $scope.api.createdQuestionnaire.method_options.pollQuestionsExtra.push({
      question : "",
      options : []
    });
  };
  $scope.addPollOptionQ = function(index) {
    $scope.api.createdQuestionnaire.method_options.pollQuestionsExtra[index].options.push("");
  };
  $scope.removePollQuestion = function(index) {
    $scope.api.createdQuestionnaire.method_options.pollQuestionsExtra.splice(index, 1);
  };
  $scope.removePollItemQ = function(index) {
    $scope.api.createdQuestionnaire.method_options.pollQuestionsExtra[index].options.splice($index, 1);
  };
  $scope.createPoll = function() {
    var details = $scope.api.createdQuestionnaire.details;
    var question = $scope.api.createdQuestionnaire.poll;
    var responses = language.dictionary;

    $scope.api.response = "";

    if (details.name == "") {
      $scope.api.response = responses.nameless_survey;
      return;
    } else if (details.description == "") {
      $scope.api.response = responses.descriptionless_survey;
      return;
    } else if (details.description.length > 80) {
      $scope.api.response = responses.description_toolong;
      return;
    } else if (details.category == "") {
      $scope.api.response = responses.categoryless_survey;
      return;
    } else if (details.sex == "") {
      $scope.api.response = responses.sexless_survey;
      return;
    } else if (details.ethnicity == "") {
      $scope.api.response = responses.ethnicityless_survey;
      return;
    } else if (details.age_group == "") {
      $scope.api.response = responses.ageless_survey;
      return;
    } else if (question.question == "") {
      $scope.api.response = responses.blank_pq;
      return;
    }

    for (var i = 0; i < question.responses; i++) {
      if (question.responses[i] == "") {
        $scope.api.response = responses.blank_po;
        return;
      }
    }

    $scope.api.uploading = true;
    $scope.api.createQuestionnaire();
  };
  $scope.noHide = function(event) {
    event.stopPropagation();
  }
  $scope.blank_questions = function() {
    return $scope.api.createdQuestionnaire.survey.order.length === 0;
  };
  function noQuestions(freeResponse, checkbox, boolean, radio, single) {
    if (freeResponse.length > 0) return false;
    if (checkbox.length > 0) return false;
    if (boolean.length > 0) return false;
    if (radio.length > 0) return false;
    if (single.length > 0) return false;

    return true;
  };
  function emptyQuestionsA(questionsArray) {
    for (var i = 0; i < questionsArray.length; i++) {
      if (questionsArray[i] == "") {
        return true;
      }
    }

    return false;
  };
  function emptyQuestionsB(questionsArray) {
    for (var i = 0; i < questionsArray.length; i++) {
      if (questionsArray[i].question == "") {
        return true;
      }
    }

    return false;
  };
  function emptyResponses(questionsArray) {
    for (var i = 0; i < questionsArray.length; i++) {
      if (questionsArray[i].responses.length == 0) return true;
      for (var d = 0; d < questionsArray[i].responses.length; d++) {
        if (questionsArray[i].responses[d] == "") {
          return true;
        }
      }
    }
  }
});

app.controller('ManageController', function($scope, $rootScope, teamAPI, language, surveyManagerAPI) {
  $scope.language = language;
  $scope.team = teamAPI;
  $scope.api = surveyManagerAPI;
  $scope.api.gatherData();
  $scope.noSurveys = function() {
    return $scope.api.surveyData.length == 0;
  };
  $scope.noPolls = function() {
    return $scope.api.pollData.length == 0;
  };
});

app.controller('MembersController', function($scope, $rootScope, teamAPI, language, membersAPI) {
  $scope.language = language;
  $scope.team = teamAPI;
  $scope.api = membersAPI;
  $scope.api.getMembersData();

});

app.controller('NotificationsController', function($scope, language, teamAPI, notificationsAPI) {
  $scope.team = teamAPI;
  $scope.api = notificationsAPI;
  $scope.api.gatherNotifications();
  $scope.language = language;

});

app.controller('SettingsController', function($scope, $timeout, language, teamAPI, settingsAPI) {
  $scope.language = language;
  $scope.team = teamAPI;
  $scope.api = settingsAPI;
  $scope.api.gatherData();
  $scope.dpc = false;
  $scope.dp = function() {
    var dc = $("#delAccountInt").val();
    if (dc == $scope.api.nu) {
      $scope.dpc = true;
    }
  };
  $scope.changeBG = function() {
    $scope.team.backgroundColor = $scope.api.accountData.theme_color;
  };
  $scope.changeBCD = function() {
    $scope.team.backgroundColorDark = $scope.api.accountData.accent_color;
  };
  $scope.fileSelectedL = function(elem) {
    var file = $("#logo_image")[0].files[0];
    if (file == "" || typeof file == "undefined") return;

    var spl = elem.target.value.split("\\");
    $scope.api.imagesInfo.logo = spl[spl.length - 1];
    $scope.api.imagesInfo.logo_file = file;
    $scope.$apply();
  };
  $scope.fileSelectedT = function(elem) {
    var file = $("#theme_image")[0].files[0];
    if (file == "" || typeof file == "undefined") return;

    var spl = elem.target.value.split("\\");
    $scope.api.imagesInfo.theme = spl[spl.length - 1];
    $scope.api.imagesInfo.theme_file = file;
    $scope.$apply();
  };
  $scope.updateLogo = function() {
    $scope.api.updateLogoPicture($scope);
  }
  $scope.updateTheme = function() {
    $scope.api.updateThemePicture($scope);
  }
  $scope.desktopNotifications = function($event) {
    if (!Notification) {
      alert("Cannot Use Notifications with Current Browser");
      return;
    }

    if (Notification.permission === "denied") {
      $timeout(function() {
        $($event.target).prop('checked', false);
      }, 150);
      return;
    };

    if (Notification.permission !== "granted") {
      Notification.requestPermission().then(function(result) {
        if (result === 'denied') {
          $timeout(function() {
            $($event.target).prop('checked', false);
          }, 150);
          return;
        };

        if (result === 'default') {
          $timeout(function() {
            $($event.target).prop('checked', false);
          }, 150);
          return;
        }
        $cookies.put("dnotif", true);
      });
    };

    var checked = $($event.target).data().boxChecked;

    if (checked || checked == "true") {
      $($event.target).data("box-checked", false);
      $cookies.put("dnotif", false);
      return;
    }

    if (!checked || checked == "false") {
      $($event.target).data("box-checked", true);
      $cookies.put("dnotif", true);
      return;
    }
  };
});

app.controller('SurveyCreatorController', function($scope, $rootScope, $timeout, teamAPI, surveyCreatorAPI, language) {
  $scope.language = language;
  $scope.team = teamAPI;
  $scope.api = surveyCreatorAPI;
  $scope.api.retrieveData();
  $scope.category_display = "";
  $scope.sex_display = "";
  $scope.ethnicity_display = "";
  $scope.age_display = "";
  $scope.questionOptions = {
    "type" : "",
    "index" : 0
  };
  $scope.fileSelected = function(elem) {
    var spl = elem.target.value.split("\\");
    $scope.api.createdSurvey.surveyDetails.image_file = spl[spl.length - 1];
    $scope.$apply();
  };
  $scope.questionOptionsE = function(type, index) {
    $scope.questionOptions.type = type;
    $scope.questionOptions.index = index;
    $scope.show_options_toolbar = true;
  };
  $scope.emptyQuestionOptionsB = function($event) {
    if ($(".response_input").is(":focus") || $event.target.parentNode.className === "optionsContainer" || !$scope.show_options_toolbar) {
      return;
    }
    for (var i = 0; i < $event.originalEvent.path.length; i++) {
      if ($event.originalEvent.path[i].className === "optionsContainer") {
        return;
      }
    }
    $scope.questionOptions.type = "";
    $scope.questionOptions.index = 0;
    $scope.show_options_toolbar = false;
  };
  $scope.addFreeResponse = function() {
    var object = {
      "question" : "",
      "options" : {
        "pattern" : "a",
        "required" : false
      }
    };
    $scope.api.createdSurvey.freeResponseQuestions.push(object);
    var order_name = "fr_"+($scope.api.createdSurvey.freeResponseQuestions.length - 1);
    $scope.api.createdSurvey.order.push(order_name);
  };
  $scope.addCheckboxResponse = function() {
    var object = {
      "question" : "",
      "responses" : [],
      "options" : {
        "required" : false
      }
    };
    $scope.api.createdSurvey.checkboxResponseQuestions.push(object);
    var order_name = "cbr_"+($scope.api.createdSurvey.checkboxResponseQuestions.length - 1);
    $scope.api.createdSurvey.order.push(order_name);
  };
  $scope.addBooleanResponse = function() {
    var object = {
      "question" : "",
      "responses" : [],
      "options" : {
        "required" : false
      }
    };
    $scope.api.createdSurvey.booleanResponseQuestions.push(object);
    var order_name = "br_"+($scope.api.createdSurvey.booleanResponseQuestions.length - 1);
    $scope.api.createdSurvey.order.push(order_name);
  };
  $scope.addRadioResponse = function() {
    var object = {
      "question" : "",
      "responses" : [],
      "options" : {
        "required" : false
      }
    };
    $scope.api.createdSurvey.radioResponseQuestions.push(object);
    var order_name = "rr_"+($scope.api.createdSurvey.radioResponseQuestions.length - 1);
    $scope.api.createdSurvey.order.push(order_name);
  };
  $scope.addMenuResponse = function() {
    var object = {
      "question" : "",
      "responses" : [],
      "options" : {
        "required" : false
      }
    };
    $scope.api.createdSurvey.menuResponseQuestions.push(object);
    var order_name = "mr_"+($scope.api.createdSurvey.menuResponseQuestions.length - 1);
    $scope.api.createdSurvey.order.push(order_name);
  };
  $scope.addCheckboxItemResponse = function($index) {
    $scope.api.createdSurvey.checkboxResponseQuestions[$index]["responses"].push("");
  };
  $scope.addRadioItemResponse = function($index) {
    $scope.api.createdSurvey.radioResponseQuestions[$index]["responses"].push("");
  };
  $scope.addMenuItemResponse = function($index) {
    $scope.api.createdSurvey.menuResponseQuestions[$index]["responses"].push("");
  };
  $scope.removeItem = function($index, type) {
    switch (type) {
      case "fR":
        $scope.api.createdSurvey.freeResponseQuestions.splice($index, 1);
        var order_name = "fr_"+$index;
        var index_of_order = $scope.api.createdSurvey.order.indexOf(order_name)
        $scope.api.createdSurvey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdSurvey.freeResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdSurvey.order.length; i ++) {
            var item_prop = $scope.api.createdSurvey.order[i].split("_");
            if (item_prop[0] != "fr") return;

            if (item_prop[1] <= $index) return;

            var index_of_item = $scope.api.createdSurvey.order.indexOf($scope.api.createdSurvey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "fr_"+(index_of_item_i);
            $scope.api.createdSurvey.order.splice(index_of_item,1,newname);
          }
        }
        break;
      case "cR":
        $scope.api.createdSurvey.checkboxResponseQuestions.splice($index, 1);
        var order_name = "cbr_"+$index;
        var index_of_order = $scope.api.createdSurvey.order.indexOf(order_name)
        $scope.api.createdSurvey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdSurvey.checkboxResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdSurvey.order.length; i ++) {
            var item_prop = $scope.api.createdSurvey.order[i].split("_");
            if (item_prop[0] != "cbr") continue;

            if (item_prop[1] <= $index) continue;

            var index_of_item = $scope.api.createdSurvey.order.indexOf($scope.api.createdSurvey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "cbr_"+(index_of_item_i);
            $scope.api.createdSurvey.order.splice(index_of_item,1,newname);
          }
        }
        break;
      case "bR":
        $scope.api.createdSurvey.booleanResponseQuestions.splice($index, 1);
        var order_name = "br_"+$index;
        var index_of_order = $scope.api.createdSurvey.order.indexOf(order_name)
        $scope.api.createdSurvey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdSurvey.booleanResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdSurvey.order.length; i ++) {
            var item_prop = $scope.api.createdSurvey.order[i].split("_");
            if (item_prop[0] != "br") return;

            if (item_prop[1] <= $index) return;

            var index_of_item = $scope.api.createdSurvey.order.indexOf($scope.api.createdSurvey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "br_"+(index_of_item_i);
            $scope.api.createdSurvey.order.splice(index_of_item,1,newname);
          }
        }
        break;
      case "rR":
        $scope.api.createdSurvey.radioResponseQuestions.splice($index, 1);
        var order_name = "rr_"+$index;
        var index_of_order = $scope.api.createdSurvey.order.indexOf(order_name)
        $scope.api.createdSurvey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdSurvey.radioResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdSurvey.order.length; i ++) {
            var item_prop = $scope.api.createdSurvey.order[i].split("_");
            if (item_prop[0] != "rr") continue;

            if (item_prop[1] <= $index) continue;

            var index_of_item = $scope.api.createdSurvey.order.indexOf($scope.api.createdSurvey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "rr_"+(index_of_item_i);
            $scope.api.createdSurvey.order.splice(index_of_item,1,newname);
          }
        }
        break;
      case "mR":
        $scope.api.createdSurvey.menuResponseQuestions.splice($index, 1);
        var order_name = "mr_"+$index;
        var index_of_order = $scope.api.createdSurvey.order.indexOf(order_name)
        $scope.api.createdSurvey.order.splice(index_of_order, 1);

        if ($index <= ($scope.api.createdSurvey.menuResponseQuestions.length)) {
          for (var i = 0; i < $scope.api.createdSurvey.order.length; i ++) {
            var item_prop = $scope.api.createdSurvey.order[i].split("_");
            if (item_prop[0] != "mr") continue;

            if (item_prop[1] <= $index) continue;

            var index_of_item = $scope.api.createdSurvey.order.indexOf($scope.api.createdSurvey.order[i]);
            var index_of_item_i = parseInt(item_prop[1]) - 1;
            var newname = "mr_"+(index_of_item_i);
            $scope.api.createdSurvey.order.splice(index_of_item,1,newname);
          }
        }
        break;
    }
  };
  $scope.removeResponseItem = function($parentIndex, $index, type) {
    switch (type) {
      case "cR":
        $scope.api.createdSurvey.checkboxResponseQuestions[$parentIndex]["responses"].splice($index, 1);
        break;
      case "rR":
        $scope.api.createdSurvey.radioResponseQuestions[$parentIndex]["responses"].splice($index, 1);
        break;
      case "mR":
        $scope.api.createdSurvey.menuResponseQuestions[$parentIndex]["responses"].splice($index, 1);
        break;
    }
  };
  $scope.expandables = {
    "category_expand" : false,
    "sex_expand" : false,
    "ethnicity_expand" : false,
    "age_expand" : false,
    "image_list_expand" : false,
    "ud_expand" : false,
    "collected" : []
  };
  $scope.collect = function(value) {
    if ($scope.expandables.collected.indexOf(value) == -1) {
      $scope.expandables.collected.push(value);
    } else {
      var ind = $scope.expandables.collected.indexOf(value);
      $scope.expandables.collected.splice(ind, 1);
    }
  };
  $scope.show_options_toolbar = false;
  $scope.categories = $scope.api.categories;
  $scope.menuDisplay = {
    "question" : "",
    "responses" : []
  };
  $scope.setCategory = function($index, value, category) {
    $scope.category_display = category;
    $scope.api.createdSurvey.surveyDetails["category_token"] = value;
    $("#category").addClass("spanned");
    $scope.expandables.category_expand = false;
  };
  $scope.setSex = function(value, sex) {
    $scope.sex_display = sex;
    $scope.api.createdSurvey.surveyDetails["sex"] = value;
    $("#sex").addClass("spanned");
    $scope.expandables.sex_expand = false;
  };
  $scope.setEthnicity = function(value, ethnicity) {
    $scope.ethnicity_display = ethnicity;
    $scope.api.createdSurvey.surveyDetails["ethnicity"] = value;
    $("#ethnicity").addClass("spanned");
    $scope.expandables.ethnicity_expand = false;
  };
  $scope.setAgeGroup = function(value, age) {
    $scope.age_display = age;
    $scope.api.createdSurvey.surveyDetails["age_group"] = value;
    $("#age_group").addClass("spanned");
    $scope.expandables.age_expand = false;
  };
  $scope.hideMenu = function() {
    $(".menu").fadeOut(100);
  };
  $scope.dragfinished = function(index_current, question, index_new) {
    $scope.api.createdSurvey.order.splice(index_current, 1);
    $timeout(function() {
      $scope.$apply;
    }, 20);
    if (index_current < index_new) {
      $scope.api.createdSurvey.order.splice(index_new, 0, question);
    } else {
      $scope.api.createdSurvey.order.splice(index_new, 0, question);
    }
    $timeout(function() {
      $scope.$apply;
    }, 200);
  };
  $scope.getQuestionText = function(question) {
    var split = question.split("_");
    var type = split[0];
    var index = split[1];
    var question_text;

    switch (type) {
      case "fr":
        question_text = $scope.api.createdSurvey.freeResponseQuestions[index].question;
        break;
      case "cbr":
        question_text = $scope.api.createdSurvey.checkboxResponseQuestions[index].question;
        break;
      case "br":
        question_text = $scope.api.createdSurvey.booleanResponseQuestions[index].question;
        break;
      case "rr":
        question_text = $scope.api.createdSurvey.radioResponseQuestions[index].question;
        break;
      case "mr":
        question_text = $scope.api.createdSurvey.menuResponseQuestions[index].question;
        break;
    }

    if (question_text == "") {
      question_text = $scope.language.dictionary.blank;
    }

    return question_text;
  };
  $scope.createSurvey = function() {
    var details = $scope.api.createdSurvey.surveyDetails;
    var responses = language.dictionary;
    var freeResponse = $scope.api.createdSurvey.freeResponseQuestions;
    var checkbox = $scope.api.createdSurvey.checkboxResponseQuestions;
    var boolean = $scope.api.createdSurvey.booleanResponseQuestions;
    var radio = $scope.api.createdSurvey.radioResponseQuestions;
    var single = $scope.api.createdSurvey.menuResponseQuestions;

    $scope.api.response = "";

    if (details.name == "") {
      $scope.api.response = responses.nameless_survey;
      return;
    } else if (details.description == "") {
      $scope.api.response = responses.descriptionless_survey;
      return;
    } else if (details.description.length > 80) {
      $scope.api.response = responses.description_toolong;
      return;
    } else if (details.category_token == "") {
      $scope.api.response = responses.categoryless_survey;
      return;
    } else if (details.sex == "") {
      $scope.api.response = responses.sexless_survey;
      return;
    } else if (details.ethnicity == "") {
      $scope.api.response = responses.ethnicityless_survey;
      return;
    } else if (details.age_group == "") {
      $scope.api.response = responses.ageless_survey;
      return;
    } else if (noQuestions(freeResponse, checkbox, boolean, radio, single)) {
      $scope.api.response = responses.no_questions;
      return;
    } else if (emptyQuestionsA(freeResponse)) {
      $scope.api.response = responses.empty_questions('fr');
      return;
    } else if (emptyQuestionsB(checkbox)) {
      $scope.api.response = responses.empty_questions('cbr');
      return;
    } else if (emptyQuestionsA(boolean)) {
      $scope.api.response = responses.empty_questions('br');
      return;
    } else if (emptyQuestionsB(radio)) {
      $scope.api.response = responses.empty_questions('rr');
      return;
    } else if (emptyQuestionsB(single)) {
      $scope.api.response = responses.empty_questions('mir');
      return;
    } else if (emptyResponses(checkbox)) {
      $scope.api.response = responses.empty_responses('cbr');
      return;
    } else if (emptyResponses(radio)) {
      $scope.api.response = responses.empty_responses('rr');
      return;
    } else if (emptyResponses(single)) {
      $scope.api.response = responses.empty_responses('mir');
      return;
    }

    $scope.api.uploading = true;
    $scope.api.createSurvey();
  };
  $scope.noHide = function(event) {
    event.stopPropagation();
  }
  $scope.blank_questions = function() {
    return $scope.api.createdSurvey.order.length === 0;
  };
  function noQuestions(freeResponse, checkbox, boolean, radio, single) {
    if (freeResponse.length > 0) return false;
    if (checkbox.length > 0) return false;
    if (boolean.length > 0) return false;
    if (radio.length > 0) return false;
    if (single.length > 0) return false;

    return true;
  };
  function emptyQuestionsA(questionsArray) {
    for (var i = 0; i < questionsArray.length; i++) {
      if (questionsArray[i] == "") {
        return true;
      }
    }

    return false;
  };
  function emptyQuestionsB(questionsArray) {
    for (var i = 0; i < questionsArray.length; i++) {
      if (questionsArray[i].question == "") {
        return true;
      }
    }

    return false;
  };
  function emptyResponses(questionsArray) {
    for (var i = 0; i < questionsArray.length; i++) {
      for (var d = 0; d < questionsArray[i].responses.length; d++) {
        if (questionsArray[i].responses[d] == "") {
          return true;
        }
      }
    }
  }
});

app.controller('PollCreatorController', function($scope, $rootScope, $timeout, teamAPI, pollCreatorAPI, language) {
  $scope.language = language;
  $scope.team = teamAPI;
  $scope.api = pollCreatorAPI;
  $scope.api.retrieveCategories();
  $scope.category_display = "";
  $scope.sex_display = "";
  $scope.ethnicity_display = "";
  $scope.age_display = "";
  $scope.expandables = {
    "category_expand" : false,
    "sex_expand" : false,
    "ethnicity_expand" : false,
    "age_expand" : false,
    "image_list_expand" : false,
    "ud_expand" : false,
    "collected" : []
  };
  $scope.collect = function(value) {
    if ($scope.expandables.collected.indexOf(value) == -1) {
      $scope.expandables.collected.push(value);
    } else {
      var ind = $scope.expandables.collected.indexOf(value);
      $scope.expandables.collected.splice(ind, 1);
    }
  };
  $scope.menuDisplay = {
    "question" : "",
    "responses" : []
  };
  $scope.fileSelected = function(elem) {
    var spl = elem.target.value.split("\\");
    $scope.api.createdPoll.pollDetails.image_file = spl[spl.length - 1];
    $scope.$apply();
  };
  $scope.setCategory = function($index, value, category) {
    $scope.category_display = category;
    $scope.api.createdPoll.pollDetails["category_token"] = value;
    $("#category").addClass("spanned");
    $scope.expandables.category_expand = false;
  };
  $scope.setSex = function(value, sex) {
    $scope.sex_display = sex;
    $scope.api.createdPoll.pollDetails["sex"] = value;
    $("#sex").addClass("spanned");
    $scope.expandables.sex_expand = false;
  };
  $scope.setEthnicity = function(value, ethnicity) {
    $scope.ethnicity_display = ethnicity;
    $scope.api.createdPoll.pollDetails["ethnicity"] = value;
    $("#ethnicity").addClass("spanned");
    $scope.expandables.ethnicity_expand = false;
  };
  $scope.setAgeGroup = function(value, age) {
    $scope.age_display = age;
    $scope.api.createdPoll.pollDetails["age_group"] = value;
    $("#age_group").addClass("spanned");
    $scope.expandables.age_expand = false;
  };
  $scope.addPollOption = function() {
    $scope.api.createdPoll.pollQuestion.options.push("");
  };
  $scope.removePollItem = function($index) {
    $scope.api.createdPoll.pollQuestion.options.splice($index, 1);
  };
  $scope.createPoll = function() {
    var details = $scope.api.createdPoll.pollDetails;
    var question = $scope.api.createdPoll.pollQuestion;
    var responses = language.dictionary;

    $scope.api.response = "";

    if (details.name == "") {
      $scope.api.response = responses.nameless_survey;
      return;
    } else if (details.description == "") {
      $scope.api.response = responses.descriptionless_survey;
      return;
    } else if (details.description.length > 80) {
      $scope.api.response = responses.description_toolong;
      return;
    } else if (details.category == "") {
      $scope.api.response = responses.categoryless_survey;
      return;
    } else if (details.sex == "") {
      $scope.api.response = responses.sexless_survey;
      return;
    } else if (details.ethnicity == "") {
      $scope.api.response = responses.ethnicityless_survey;
      return;
    } else if (details.age_group == "") {
      $scope.api.response = responses.ageless_survey;
      return;
    } else if (question.question == "") {
      $scope.api.response = responses.blank_pq;
      return;
    }

    for (var i = 0; i < question.responses; i++) {
      if (question.responses[i] == "") {
        $scope.api.response = responses.blank_po;
        return;
      }
    }

    $scope.api.uploading = true;
    $scope.api.createPoll();
  };
  $scope.noHide = function(event) {
    event.stopPropagation();
  }
});

app.controller('EditorController', function($scope, language, teamAPI, editorAPI) {
  $scope.language = language;
  $scope.team = teamAPI;
  $scope.api = editorAPI;
  $scope.api.getEditorData();
  $scope.add = function() {
    $scope.api.questionnaireData.details.sc_update += 10;
    $scope.api.edit.asm += 1;
  };
  $scope.remove = function() {
    $scope.api.questionnaireData.details.sc_update -= 10;
    $scope.api.edit.asm -= 1;
  };
  $scope.addK = function() {
    if (typeof $scope.api.edit.keys == "undefined") {
      $scope.api.edit.keys = 0;
    }
    $scope.api.edit.keys += 2;
  };
  $scope.removeK = function() {
    $scope.api.edit.keys -= 2;
    if ($scope.api.edit.keys < 0) {
      $scope.api.edit.keys = 0;
    }
  };
  $scope.delete = {
    deleting : false,
    text : ""
  };
  $scope.deletingF = function() {
    if ($scope.delete.text === $scope.api.questionnaireData.details.name) {
      $scope.api.edit.delete = true;
      $scope.api.request_pending = true;
      $scope.api.editSurvey();
    } else {
      $scope.delete.deleting = !$scope.delete.deleting;
    }
  };
  $scope.update = function() {
    $scope.api.request_pending = true;
    $scope.api.editSurvey();
  };
  $scope.deleteK = function() {
    if ($scope.delete.text == $scope.api.questionnaireData.details.name) {
      $("#deleteI").fadeOut(500);
      $("#delete").focus();
      $timeout(function() {
        $scope.delete.deleting = !$scope.delete.deleting;
      }, 500);
    }
  };
});

app.controller('ProfilePublicController', function($scope, language, profilePublicAPI) {
  $scope.api = profilePublicAPI;
  $scope.api.profileData();
  $scope.language = language;
});

app.controller('TeamPublicController', function($scope, language, teamPublicAPI) {
  $scope.api = teamPublicAPI;
  $scope.api.profileData();
  $scope.language = language;
});
