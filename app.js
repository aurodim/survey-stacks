const nodemailer = require("nodemailer");
      routes = require("./routes.js");
      sockets = require("./sockets.js");
      multer = require("multer");
      compression = require('compression');
      fs = require("fs");
      path = require("path");
      storage = multer.diskStorage({
        destination: function (req, file, cb) {
          if (!fs.existsSync("./tmp")) {
            fs.mkdirSync("./tmp");
            cb(null,'./tmp');
          } else {
            cb(null,'./tmp');
          }
        },
        filename: function (req, file, cb) {
          cb(null, file.fieldname + '-' + Date.now());
        }
      });
require("dotenv").load();

global.favicon = require('serve-favicon');
global.mongoose = require("mongoose");
global.twilio = require("twilio");
global.path = require("path");
global.bodyParser = require("body-parser");
global.fileUpload = multer({storage : storage});
global.cookieParser = require("cookie-parser");
global.cloudinary = require("cloudinary");
global.promise = require('bluebird');
global.crypto = require('crypto');
global.adler = require('adler32-js');
global.express = require("express");
global.expressTimeout = require("connect-timeout");
global.async = require("async");
global.fs = require("fs");
global.app = express();
global.server  = require('http').Server(app);
global.io = require("socket.io")(server);
global.rootRequire = function(name) {
  return require(__dirname + name);
};
global.transporter = nodemailer.createTransport({
  service : "Gmail",
  auth : {
    user : process.env.GMAIL_USERNAME,
    pass : process.env.GMAIL_PASSWORD
  }
});
global.TWILIO_PHONE_NUMBER = process.env.TWILIO_PHONE_NUMBER;
global.client = new twilio(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN);

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET
});

var options = {
  autoIndex: true, 
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 500,
  poolSize: 10,
  bufferMaxEntries: 0,
};

mongoose.connect(process.env.MONGODB_URI, options, function(error) {
  if (error) {
    console.log(error);
    throw error;
  }
});

routes();
sockets();

app.enable('trust proxy');
app.use(compression());
app.set('view cache', true);
server.listen(process.env.PORT || 3000);
