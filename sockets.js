var onsite = 0;

module.exports = function() {
  io.on('connection', function(socket){
    onsite++;

    io.emit("056101ea.2a56cbf3d54a751094f419540928136459546babbf1c07c43e6eca66b21ddd6dd0a370edc93eeebaec73dc7064ac5845", {
      type : "at",
      value : onsite
    });
    
    socket.on('disconnect', function() {
      onsite--;
      io.emit("056101ea.2a56cbf3d54a751094f419540928136459546babbf1c07c43e6eca66b21ddd6dd0a370edc93eeebaec73dc7064ac5845", {
        type : "rt",
        value : onsite
      });
    });
  });
};
