var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');
var Schema = mongoose.Schema;

var reportsSchema = new Schema({
  report_id : {type : String, required : true, unique : true, trim : true, default : ""},
  reported_key : {type : String, required : true, unique : false, trim : true, default : ""},
  reported_type : {type : String, required : true, unique : false, trim : true, default : ""},
  reported_by : {type : String, required : true, unique : false, default : ""},
  reason : {type : Array, required : true, unique : false, trim : true, default : ""},
  additional : {type : String, required : false, unique : false, trim : true, default : ""},
  resolved : {type : Boolean, required : false, unique : false, default : false},
  created_at : {type : Date},
  updated_at : {type : Date, required : true, default : Date.now}
});

var Reports = mongoose.model("reports", reportsSchema);

module.exports = Reports;
