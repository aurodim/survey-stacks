var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');
var Schema = mongoose.Schema;

var pollSchema = new Schema({
  poll_token : {type : String, unique : true, required : true, default : ""},
  creator_key : {type : String, unique : false, required : true, default : ""},
  poll_details : {
    public_poll : {type : Boolean, unique : false, required : true, default : false},
    lang : {type : String, unique : false, required : true, default : ""},
    coins : {type : Number, unique : false, required : false, default : 0},
    name : {type : String, unique : false, required : true, default : ""},
    description : {type : String, unique : false, required : true, default : ""},
    category_token : {type : String, unique : false, required : true, default : ""},
    target_sex : {type : String, unique : false, required : true, default : "both"},
    target_ethnicity : {type : String, unique : false, required : true, default : "any"},
    target_age_group : {type : String, unique : false, required : true, default : "a"},
    image_url : {type : String, unique : false, required : false, default : ""},
    answer_url : {type : String, unique : false, required : true, default : ""},
    collected : {type : Array, unique : false, required : true},
    lab_created : {type : Boolean, unique : false, required : false, default : false}
  },
  labs : {
    type : {type : String, unique : false, required : true},
    timed : {type : Boolean, unique : false, required : false},
    max_time : {type : Number, unique : false, required : false, min : 0},
    gameQuestions : [],
    show_score : {type : Boolean, unique : false, required : false},
    answer : {type : Number, unique : false, required : false}
  },
  question_details : {
    question : {type : String, unique : false, required : true, default : ""},
    options : {type : Array, unique : false, required : true, default : []}
  },
  responses : {type : Array, unique : false, required : false, default : []},
  times_reported : {type : Number, unique : false, required : false, default : 0},
  blocked : {type : Boolean, unique : false, required : false, default : false},
  deleted : {type : Boolean, unique : false, required : false, default : false},
  created_at : {type : Date},
  updated_at : {type : Date, required : true, default : Date.now}
});

var Polls = mongoose.model("polls", pollSchema);

module.exports = Polls;
