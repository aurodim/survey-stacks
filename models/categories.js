var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');
var Schema = mongoose.Schema;

var categorySchema = new Schema({
  category_token : {type : String, required : true, unique : true, default : ""},
  category_value : {type : String, required : true, unique : true, default : ""},
  category_name : {type : String, required : true, unique : true, default : ""},
  added_by : {type : String, required : true, unique : false, default : ""},
  approved : {type : Boolean, required : false, unique : false, default : false},
  approved_by : {type : String, required : false, unique : false, default : ""},
  created_at : {type : Date},
  update_at : {type : Date, default : Date.now}
});

var Categories = mongoose.model("categories", categorySchema, "categories");

module.exports = Categories;
