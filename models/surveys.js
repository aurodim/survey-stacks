var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');
var Schema = mongoose.Schema;

var textResponseSchema = new Schema({
  question : {type : String, unique : false, required : true, default : ""},
  options : {
    required : {type : Boolean, unique : false, required : true, default : false},
    pattern : {type : String, unique : false, required : true, default : "a"},
    algorithm : {type : String, unique : false, required : false},
    answer : {type : String, unique : false, required : false},
    worth : {type : Number, unique : false, required : false, min : 0}
  }
});

var booleanResponseSchema = new Schema({
  question : {type : String, unique : false, required : true, default : ""},
  options : {
    required : {type : Boolean, unique : false, required : true, default : false},
    correct_answer : {type : Number, unique : false, required : false},
    worth : {type : Number, unique : false, required : false, min : 0}
  }
});

var optionsResponseSchema = new Schema({
  question : {type : String, unique : false, required : true, default : ""},
  responses : {type : Array, unique : false, required : true, default : []},
  options : {
    required : {type : Boolean, unique : false, required : true, default : false},
    correct_answer : {type : Array, unique : false, required : false},
    worth : {type : Number, unique : false, required : false, min : 0}
  }
});

var surveysSchema = new Schema({
  survey_token : {type : String, unique : true, required : true, default : ""},
  creator_key : {type : String, unique : false, required : true, default : ""},
  survey_details : {
    public_survey : {type : Boolean, unique : false, required : true, default : false},
    lang : {type : String, unique : false, required : true, default : ""},
    stacks : {type : Number, unique : false, required : false, default : 0, min : 0},
    name : {type : String, unique : false, required : true, default : ""},
    description : {type : String, unique : false, required : true, default : ""},
    category_token : {type : String, unique : false, required : true, default : ""},
    target_sex : {type : String, unique : false, required : true, default : "both"},
    target_ethnicity : {type : String, unique : false, required : true, default : "any"},
    target_age_group : {type : String, unique : false, required : true, default : "a"},
    image_url : {type : String, unique : false, required : false, default : ""},
    answer_url : {type : String, unique : false, required : true, default : ""},
    collected : {type : Array, unique : false, required : true},
    lab_created : {type : Boolean, unique : false, required : false, default : false}
  },
  labs : {
    type : {type : String, unique : false, required : true},
    timed : {type : Boolean, unique : false, required : false},
    max_time : {type : Number, unique : false, required : false, min : 0},
    unique_keys : {type : Boolean, unique : false, required : false},
    keys : {type : Array, unique : false, required : false},
    show_score : {type : Boolean, unique : false, required : false},
    out_of : {type : Number, unique : false, required : false, min : 0},
  },
  question_details : {
    order : {type : Array, unique : false, required : false, default : []}, // fr -> free response; mr -> menu response; cr -> checkbox response; br -> boolean response; rr -> radio response;
    amount : {type : Number, unique : false, required : false, default : 0},
    free_response : [textResponseSchema],
    checkbox : [optionsResponseSchema],
    boolean : [booleanResponseSchema],
    radio : [optionsResponseSchema],
    menu : [optionsResponseSchema]
  },
  payment_details : {
    free : {type : Boolean, unique : false, required : true, default : false},
    survey_charge : {type : Number, unique : false, required : true, default : 1.00},
    extra_stacks : {type : Number, unique : false, required : true, default : 0.00},
    order_total : {type : Number, unique : false, required : true, default : 0.00},
    receipt_token : {type : String, unique : true, required : true, default : ""}
  },
  responses : {type : Array, unique : false, required : false, default : []},
  times_reported : {type : Number, unique : false, required : false, default : 0},
  blocked : {type : Boolean, unique : false, required : false, default : false},
  deleted : {type : Boolean, unique : false, required : false, default : false},
  created_at : {type : Date},
  updated_at : {type : Date, required : true, default : Date.now}
});

var Surveys = mongoose.model("surveys", surveysSchema);

module.exports = Surveys;
