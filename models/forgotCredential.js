var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');
var Schema = mongoose.Schema;

var forgotCredentialKeys = new Schema({
  validation_key : {type : String, required : true, unique : false, trim : true},
  key : {type : String, required : true, unique : false, trim : true},
  username : {type : String, required : true, unique : false, trim : true},
  used : {type : Boolean, required : true},
  expired : {type : Boolean, required : true},
  created_at : Date,
  updated_at : Date
});

var ForgotCredentialKeys = mongoose.model("forgot_credential_keys", forgotCredentialKeys, "forgot_credential_keys");

module.exports = ForgotCredentialKeys;
