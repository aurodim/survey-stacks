var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');
var Schema = mongoose.Schema;

var responseSchema = new Schema({
  response_token : {type : Schema.Types.ObjectId, required : true, unique : true},
  respondent_token : {type : String, required : true, unique : false, default : ""},
  respondent_ip : {type : String, required : true, unique : false, default : ""},
  questionnaire_type : {type : String, required : true, unique : false, default : ""},
  questionnaire_token : {type : String, required : true, unique : false, default : ""},
  response_lang : {type : String, required : true, unique : false, default : ""},
  currency_recieved : {type : Number, required : true, unique : false, default : 0},
  labs : {
    time_elapsed : {type : Number, required : false, unique : false},
    score : {type : Number, required : false, unique : false, min : 0},
    key_used : {type : String, required : false, unique : false}
  },
  collected : {
    fullname : {type : String, required : false, unique : false},
    religion : {type : String, required : false, unique : false},
    employment_status : {type : String, required : false, unique : false},
    relationship_status : {type : String, required : false, unique : false},
    education_level : {type : String, required : false, unique : false},
    spoken_languages : {type : String, required : false, unique : false},
    ethinicity : {type : String, required : false, unique : false},
    sex : {type : String, required : false, unique : false},
    birth_date : {type : String, required : false, unique : false},
  },
  responses : [],
  created_at : {type : Date},
  updated_at : {type : Date, required : true, default : Date.now}
});

var Responses = mongoose.model("responses", responseSchema);

module.exports = Responses;
