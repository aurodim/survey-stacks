var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');
var Schema = mongoose.Schema;

var constantsSchema = new Schema({
  stacks_xrt : {type : Number, unique : true, required : true, default : 1.00, min : 0, max : 3.00},
  coins_xrt : {type : Number, unique : true, required : true, default : 1.00, min : 0, max : 3.00},
  one_survey : {type : Number, unique : true, required : true, default : 2.00, min : 1.00, max : 5.00},
  labs_mem : {type : Number, unique : true, required : true, default : 2.00, min : 1.00, max : 100.00}, // labs membership
  ppa : {type : Number, unique : true, required : true, default : 2.00, min : 1.00, max : 5.00}, // price per addition
  cpp : {type : Number, unique : true, required : true, default : 5.00, min : 1.00, max : 20.0}, // poll reward
  sc_xrt : {type : Number, unique : true, required : true, default : 10, min : 0, max : 100000}, // survey conversion
  created_at : {type : Date},
  updated_at : {type : Date, required : true, default : Date.now()}
});

var Constants = mongoose.model("constants", constantsSchema);

module.exports = Constants;

Constants.count({}, function(error, amount) {
  if (error || amount != "0") return;

  var constant = new Constants({
    stacks_xrt : 1,
    coins_xrt : 1,
    one_survey : 2.00,
    labs_mem : 2.00,
    ppa : 2.00,
    cpp : 5.00,
    sc_xrt : 10,
    created_at : Date.now(),
    updated_at : Date.now()
  });

  constant.save(function (err) {
    if (err) return;
  })
});
