var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');
var Schema = mongoose.Schema;

var notificationsSchema = new Schema({
  reciever : {type : String, required : true, unique : false, trim : true, default : ""},
  notification_token : {type : String, required : true, unique : true, trim : true, default : ""},
  read : {type : Boolean, required : false, unique : false, trim : true, default : false},
  type : {type : String, required : false, unique : false, trim : true, default : ""},
  title : {type : String, required : false, unique : false, trim : true},
  content : {type : String, required : false, unique : false, trim : true},
  url : {type : String, required : false, unique : false, trim : true, default : ""},
  created_at : {type : Date},
  updated_at : {type : Date, required : true, default : Date.now}
});

var Notifications = mongoose.model("notifications", notificationsSchema);

module.exports = Notifications;
