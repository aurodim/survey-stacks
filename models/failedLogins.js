var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');
var Schema = mongoose.Schema;

var failedLoginsSchema = new Schema({
    key :  {type : String, required : true, unique : false},
    username : {type : String, required : true, unique : false, trim : true},
    ip_address : {type : String, required : true, unique : false},
    created_at : {type : Date, required : true, unique : false},
});

var FailedLogins = mongoose.model("failed_logins", failedLoginsSchema);

module.exports = FailedLogins;
