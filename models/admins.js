var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');
var Schema = mongoose.Schema;

var adminsSchema = new Schema({
  authorization_key : {type : String, required : true, unique : true, trim : true, default : ""},
  fullname : {type : String, required : true, unique : false, trim : true, default : ""},
  email_address : {type : String, required : true, unique : false, trim : true, default : ""},
  phone_number : {type : String, required : true, unique : false, trim : true, default : ""},
  password : {type : String, required : true, unique : false, default : ""},
  restriction : {type : String, required : true, unique : false, default : "junior"},
  created_at : {type : Date},
  updated_at : {type : Date, required : true, default : Date.now}
});

var Admins = mongoose.model("admins", adminsSchema);

module.exports = Admins;
