var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');
var Schema = mongoose.Schema;

var rewardsSchema = new Schema({
  reward_token : {type : String, unique : true, required : true, default : ""},
  quantity : {type : Number, unique : false, required : false, default : 0, min : 0},
  image_url : {type : String, unique : false, required : true, default : ""},
  redeem_keys : {type : Array, unique : false, required : true, default : []},
  description : {type : String, unique : true, required : true, default : ""},
  cost : {type : Number, unique : false, required : true, default : 0.00},
  discount : {type : Number, unique : false, required : false, default : 1, min : 0, max : 1},
  times_redeemed : {type : Number, unique : false, required : false, default : 0, min : 0},
  created_at : {type : Date},
  updated_at : {type : Date, required : true, default : Date.now}
});

var Rewards = mongoose.model("rewards", rewardsSchema);

module.exports = Rewards;
