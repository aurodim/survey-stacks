var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');
var Schema = mongoose.Schema;

var companiesSchema = new Schema({
  team_token : {type : String, required : true, unique : true, default : ""},
  company_value : {type : String, required : true, unique : true, default : ""},
  company_name : {type : String, required : true, unique : true, default : ""},
  added_by : {type : String, required : true, unique : false, default : ""},
  approved : {type : Boolean, required : false, unique : false, default : false},
  approved_by : {type : String, required : false, unique : false, default : ""},
  created_at : {type : Date},
  updated_at : {type : Date, default : Date.now}
});

var Companies = mongoose.model("companies", companiesSchema);

module.exports = Companies;
