var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');
var Schema = mongoose.Schema;

/*
  keys will follow the different protocols of IP Addresses but in a different format to minize the chance of having similar keys

  user key -> xxxx . xxx . >x
    random generated 4 digit code
    unique 3 digit code to signify date in which user signed up
    x previous user key
  user session key -> xxxx . xxx . x>
    4 digit code hashed first and last name
    3 digit code to signify user account
    old session code
  user_auth_key -> xxx . xx . >x
    constant code
    Adler32 -> timestamp
    old user_auth_key
*/

var profilesSchema = new Schema({
  user_key : {type : String, required : true, unique : true},
  user_session_key : {type : String, required : true, unique : true},
  user_auth_key : {type : String, required : true, unique : true},
  profile_theme_url : {type : String, required : false, unique : false, default : "https://res.cloudinary.com/survey-stacks/image/upload/v1515239424/constants/theme.png"},
  profile_picture_url : {type : String, required : false, unique : false, default : ""},
  fullname : {type : String, required : false, unique : false, trim : true, default : ""},
  email_phone : {type : String, required : false, unique : true, trim : true, default : ""},
  username : {type : String, required : false, unique : true, trim : true, default : ""},
  password : {type : String, required : false, unique : false, default : ""},
  motto : {type : String, required : false, unique : false, default : ""},
  contact_info : {
    phone_number : {type : String, required : false, unique : false, default : ""},
    email : {type : String, required : false, unique : false, default : ""},
    website : {type : String, required : false, unique : false, default : ""},
    facebook : {type : String, required : false, unique : false, default : ""},
    instagram : {type : String, required : false, unique : false, default : ""}
  },
  setup_completed : {
    preferences : {type : Boolean, required : false, default : false},
    about : {type : Boolean, required : false, default : false},
    payment : {type : Boolean, required : false, default : false},
  },
  about : {
    sex : {type : String, required : false, unique : false, default : ""},
    ethnicity : {type : Array, required : false, unique : false, default : []},
    languages : {type : Array, required : false, unique : false, default : []},
    education : {type : String, required : false, unique : false, default : ""},
    relationship_status : {type : String, required : false, unique : false, default : ""},
    employment_status : {type : String, required : false, unique : false, default : ""},
    religion : {type : String, required : false, unique : false, default : ""},
    birth_date : {type : String, required : false, unique : false, default : ""},
  },
  preferences : {
    lang : {type : String, required : false, unique : false, default : ""},
    monosex : {type : Boolean, required : false, unique : false, default : false},
    translated : {type : Boolean, required : false, unique : false, default : false},
    categories_tokens : {type : Array, required : false, unique : false, default : []},
    companies_tokens : {type : Array, required : false, unique : false, default : []},
  },
  payment : {
    labs_membership : {type : Boolean, required : false, unique : false, default : false},
    // stripe code 
    labs_expire : {type : Date}
  },
  notification_settings : {
    email_message : {type : Boolean, default : false},
    match_preferences : {type : Boolean, default : false}
  },
  achievements : {
    lifetime_stacks : {type : Number, required : false, default : 0, min : 0},
    lifetime_coins : {type : Number, required : false, default : 0, min : 0},
    surveys_answered : {type : Number, required : false, default : 0, min : 0},
    questions_answered : {type : Number, required : false, default : 0, min : 0},
    rewards_claimed : {type : Number, required : false, default : 0, min : 0}
  },
  stacks : {type : Number, required : false, default : 0, min : 0},
  coins : {type : Number, required : false, default : 0, min : 0},
  bookmarks : {type : Array, required : false, default : []},
  bookmarks_polls : {type : Array, required : false, default : []},
  reports : {type : Array, required : false, default : []},
  wishlist : {type : Array, required : false, default : []},
  redeemed : {type : Array, required : false, default : []},
  surveys_answered : {type : Array, required : false, default : []},
  surveys_created : {type : Array, required : false, default : []},
  polls_answered : {type : Array, required : false, default : []},
  polls_created : {type : Array, required : false, default : []},
  login_attempts : {type : Number, required : false, default : 0, min : 0},
  deleted : {type : Boolean, required : false, unique : false, default : false},
  created_at : {type : Date},
  updated_at : {type : Date, required : true, default : Date.now}
});

var Profiles = mongoose.model("profiles", profilesSchema);

module.exports = Profiles;
