var mongoose = require("mongoose");
mongoose.Promise = require('bluebird');
var Schema = mongoose.Schema;

var teamsSchema = new Schema({
  team_key : {type : String, required : true, unique : true},
  team_session_key : {type : String, required : true, unique : true},
  team_auth_key : {type : String, required : true, unique : true},
  team_picture_url : {type : String, required : false, unique : false, default : ""},
  team_theme_url : {type : String, required : false, unique : false, default : ""},
  fullname : {type : String, required : false, unique : false, trim : true, default : ""},
  name : {type : String, required : false, unique : false, trim : true, default : ""},
  email_phone : {type : String, required : false, unique : true, trim : true, default : ""},
  nickname : {type : String, required : false, unique : true, trim : true, default : ""},
  password : {type : String, required : false, unique : false, default : ""},
  description : {type : String, required : false, unique : false, trim : true, default : ""},
  company_status : {type : Number, required : false, unique : false, default : 0}, // 0 -> No, 1 -> Pending, 2 -> confirmed, -1 -> denied
  contact_info : {
    phone_number : {type : String, required : false, unique : false, default : ""},
    email : {type : String, required : false, unique : false, default : ""},
    website : {type : String, required : false, unique : false, default : ""},
    facebook : {type : String, required : false, unique : false, default : ""},
    instagram : {type : String, required : false, unique : false, default : ""}
  },
  member_options : {
    open : {type : Boolean, required : false, default : false},
    public : {type : Boolean, required : false, default : false},
    code : {type : String, required : false, unique : true, default : "", min : 4, max : 12}
  },
  tbc : {type : Array, required : false, default : []},
  members : {type : Array, required : false, default : []},
  preferences : {
    lang : {type : String, required : false, unique : false, default : ""},
    theme_color : {type : String, required : false, unique : false, default : "#2ecc71"},
    theme_dark : {type : String, required : false, unique : false, default : "#27ae60"}
  },
  membership : {
    type : {type : Number, required : false, unique : false, default : 0}, // 0 -> FREE
    auto_bill : {type : Boolean, required : false, unique : false, default : false},
    last_four : {type : String, required : false, unique : false, default : "", max : 4},
    expiration_date : {type : String, required : false, unique : false, default : ""},
    paid : {type : String, required : false, unique : false, default : ""}
  },
  notification_settings : {
    email_phone : {type : Boolean, default : false},
    notification_freq : {type : Number, default : 1, min : 1, max : 50}
  },
  surveys_created : {type : Array, required : false, default : []},
  polls_created : {type : Array, required : false, default : []},
  login_attempts : {type : Number, required : false, default : 0, min : 0},
  deleted : {type : Boolean, required : false, default : false},
  created_at : {type : Date},
  updated_at : {type : Date, default : Date.now}
});

var Teams = mongoose.model("teams", teamsSchema);

module.exports = Teams;
