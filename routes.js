global.modelRequire = function(name) {
  return require(__dirname + "/models/" + name);
};

global.rootRequire = function(name) {
  return require(__dirname + name);
};

// <editor-fold> CONSTANT FILES
// client server files
const globals = rootRequire("/controller/globals.js");
const analyticsDownload = rootRequire("/controller/analyticsDownload.js");
const RateLimit = require('express-rate-limit');
const limiter = new RateLimit({
  windowMs: 15*60*1000, // 15 minutes
  max: 100, // limit each IP to 100 requests per windowMs
  delayMs: 0 // disable delaying - full speed until the max limit is reached
});
const Functions = rootRequire("/controller/globalFunctions.js");

const aboutData = rootRequire("/controller/users/GET/aboutData.js");
      paymentData = rootRequire("/controller/users/GET/paymentData.js");
      sessionData = rootRequire("/controller/users/GET/sessionData.js");
      profileData = rootRequire("/controller/users/GET/profileData.js");
      manageData = rootRequire("/controller/users/GET/manageData.js");
      bookmarksData = rootRequire("/controller/users/GET/bookmarksData.js");
      labsData = rootRequire("/controller/users/GET/labsData.js");
      editorData = rootRequire("/controller/users/GET/editorData.js");
      receiptData = rootRequire("/controller/users/GET/receiptData.js");
      surveyData = rootRequire("/controller/users/GET/surveyData.js");
      pollAnswerData = rootRequire("/controller/users/GET/pollAnswerData.js");
      pollAnswerPublicData = rootRequire("/controller/users/GET/pollAnswerPublicData.js");
      surveyAnswerData = rootRequire("/controller/users/GET/surveyAnswerData.js");
      surveysAnswerPublicData = rootRequire("/controller/users/GET/surveyAnswerPublicData.js");
      reportData = rootRequire("/controller/users/GET/reportData.js");
      pollData = rootRequire("/controller/users/GET/pollData.js");
      analyticsData = rootRequire("/controller/users/GET/analyticsData.js");
      searchData = rootRequire("/controller/users/GET/searchData.js");
      rewardsData = rootRequire("/controller/users/GET/rewardsData.js");
      settingsData = rootRequire("/controller/users/GET/settingsData.js");
      preferencesData = rootRequire("/controller/users/GET/preferencesData.js");
      publicProfileData = rootRequire("/controller/users/GET/publicProfileData.js");
      teamsGetter = rootRequire("/controller/users/GET/teamsGetter.js");

const register = rootRequire("/controller/users/POST/register.js");
      login = rootRequire("/controller/users/POST/login.js");
      forgotUsername = rootRequire("/controller/users/POST/forgotUsername.js");
      forgotPassword = rootRequire("/controller/users/POST/forgotPassword.js");
      resetPassword = rootRequire("/controller/users/POST/resetPassword.js");
      aboutSurvey = rootRequire("/controller/users/POST/aboutSurvey.js");
      preferencesSurvey = rootRequire("/controller/users/POST/preferencesSurvey.js");
      paymentSurvey = rootRequire("/controller/users/POST/paymentSurvey.js");
      setupPreferences = rootRequire("/controller/users/POST/setupPreferences.js");
      labsCreator = rootRequire("/controller/users/POST/labsCreator.js");
      surveyCreator = rootRequire("/controller/users/POST/surveyCreator.js");
      pollCreator = rootRequire("/controller/users/POST/pollCreator.js");
      pollAnswer = rootRequire("/controller/users/POST/pollAnswer.js");
      wishlistReward = rootRequire("/controller/users/POST/wishlist.js");
      surveyAnswer = rootRequire("/controller/users/POST/surveyAnswer.js");
      reportQuestionnaire = rootRequire("/controller/users/POST/report.js");
      claimReward = rootRequire("/controller/users/POST/redeem.js");
      trader = rootRequire("/controller/users/POST/trader.js");
      editQuestionnaire = rootRequire("/controller/users/POST/editor.js");
      bookmarkQuestionnaire = rootRequire("/controller/users/POST/bookmark.js");
      settingsProfilePictureUpdate = rootRequire("/controller/users/POST/settingsProfilePictureUpdate.js");
      settingsProfileMottoUpdate = rootRequire("/controller/users/POST/settingsProfileMottoUpdate.js");
      settingsContactInfo = rootRequire("/controller/users/POST/settingsContactInfo.js");
      settingsLabsMembership = rootRequire("/controller/users/POST/settingsLabsMem.js");
      settingsPassword = rootRequire("/controller/users/POST/settingsPassword.js");
      settingsNotifications = rootRequire("/controller/users/POST/settingsNotifications.js");
      settingsAccountInfo = rootRequire("/controller/users/POST/settingsAccountInfo.js");
      teamsManager = rootRequire("/controller/users/POST/teamsManager.js");
      deleteAccount = rootRequire("/controller/users/POST/deleteAccount.js");

// team server files
const teamNotifications = rootRequire("/controller/teams/GET/notifications.js");
      teamSession = rootRequire("/controller/teams/GET/session.js");
      teamAnalytics = rootRequire("/controller/teams/GET/analytics.js");
      teamDashboard = rootRequire("/controller/teams/GET/dashboard.js");
      teamManage = rootRequire("/controller/teams/GET/manager.js");
      teamMembers = rootRequire("/controller/teams/GET/members.js");
      teamSettings = rootRequire("/controller/teams/GET/settings.js");
      teamEditQ = rootRequire("/controller/teams/GET/editor.js");
      teamProfile = rootRequire("/controller/teams/GET/profile.js");

const registerTeams = rootRequire("/controller/teams/POST/register.js");
      loginTeams = rootRequire("/controller/teams/POST/login.js");
      forgotNicknameTeams = rootRequire("/controller/teams/POST/forgotNickname.js");
      forgotPasswordTeams = rootRequire("/controller/teams/POST/forgotPassword.js");
      resetPasswordTeams = rootRequire("/controller/teams/POST/resetPassword.js");
      teamSurveyCreator = rootRequire("/controller/teams/POST/surveyCreator.js");
      teamPollCreator = rootRequire("/controller/teams/POST/pollCreator.js");
      teamLabsCreator = rootRequire("/controller/teams/POST/labs.js");
      teamMembersApprove = rootRequire("/controller/teams/POST/memApprove.js");
      teamMembersReject = rootRequire("/controller/teams/POST/memReject.js");
      teamMembersRemove = rootRequire("/controller/teams/POST/memRemove.js");
      teamAccountData = rootRequire("/controller/teams/POST/account.js");
      teamContactInfo = rootRequire("/controller/teams/POST/contact.js");
      teamPasswordUpdate = rootRequire("/controller/teams/POST/password.js");
      teamMembersUpdate = rootRequire("/controller/teams/POST/updateMembers.js");
      teamNotificationsUpdate = rootRequire("/controller/teams/POST/notifications.js");
      teamCompanyStatusUpdate = rootRequire("/controller/teams/POST/company.js");
      settingsLogoPictureUpdate = rootRequire("/controller/teams/POST/updateLogoPicture.js");
      settingsThemePictureUpdate = rootRequire("/controller/teams/POST/updateThemePicture.js");
      teamDelete = rootRequire("/controller/teams/POST/delete.js");
      teamUpdateQ = rootRequire("/controller/teams/POST/editor.js");

// admin server files

const adminLogin = rootRequire("/controller/admins/POST/login.js");
      adminCreator = rootRequire("/controller/admins/POST/create.js");
      adminValidate = rootRequire("/controller/admins/POST/validate.js");
      adminPromote = rootRequire("/controller/admins/POST/promote.js");
      adminDelete = rootRequire("/controller/admins/POST/delete.js");
      adminSessionData = rootRequire("/controller/admins/GET/session.js");
      adminsGather = rootRequire("/controller/admins/GET/admins.js");
      adminUserSearch = rootRequire("/controller/admins/GET/users.js");
      adminTeamsSearch = rootRequire("/controller/admins/GET/teams.js");

const csrf = require("csurf");
const helmet = require('helmet')

// </editor-fold> CONSTANT FILES

module.exports = function() {

// <editor-fold> CONSTANT FUNCTIONS

  app.use(helmet());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: true}));
  app.use(cookieParser(process.env.COOKIES_ENCRYPTION));

  app.use(favicon(__dirname + '/public/assets/images/favicon.ico'));
  app.use('/manifest.json', express.static(__dirname + '/public/manifest.json'));
  app.use('/fonts', express.static(__dirname + '/public/assets/fonts'));
  app.use('/images', express.static(__dirname + '/public/assets/images'));
  app.use('/teams/images', express.static(__dirname + '/teams/assets/images'));
  app.use('/teams/javascript', express.static(__dirname + '/teams/assets/javascripts'));
  app.use('/teams/stylesheets', express.static(__dirname + '/teams/assets/stylesheets'));
  app.use('/teams/views', express.static(__dirname + '/teams/view'));
  app.use('/javascript', express.static(__dirname + '/public/assets/javascripts'));
  app.use('/stylesheets', express.static(__dirname + '/public/assets/stylesheets'));
  app.use('/views', express.static(__dirname + '/public/view'));
  app.use('/admin/javascript', express.static(__dirname + '/admin/assets/javascripts'));
  app.use('/admin/stylesheets', express.static(__dirname + '/admin/assets/stylesheets'));
  app.use('/admin/views', express.static(__dirname + '/admin/view'));
  app.use('/manifest.json', express.static(__dirname + '/public/manifest.json'));
  app.use('/session-service-worker.js', express.static(__dirname + '/public/session-service-worker.js'));
  app.use('/teams/views', express.static(__dirname + '/teams/view'));
  app.use('/session', express.static(__dirname + '/public/view/session'));

  app.use(function(req, res, next) {
    res.set('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    next();
  });
  app.use('/api/', limiter);
  app.use('/api/', function (error, req, res, next) {
    if (error.code == "EBADCSRFTOKEN" && req.method == "POST") return noAuthToken(res);
    next();
  });
  app.use('/api/', function(req, res, next) {
    if (req.method == "POST" && typeof req.body.webApp != "undefined") {
      next();
      return;
    }

    if (!req.xhr && req.headers.accept.indexOf('json') == -1 && req.headers["x-requested-with"] != "XMLHttpRequest") {
      return res.status(404).sendFile('404.html', {root: path.join(__dirname, './public/view/')});
    }

    if (typeof req.query.reference_code == "undefined" || typeof req.query.key == "undefined" || typeof req.query.lang == "undefined" || typeof req.query.webApp == "undefined") return noAuthToken(res);

    next();
  });

  var csrfProtection = csrf({cookie : true});

  /*
    *
    * GLOBAL FUNCTIONS FOR ROUTES
    *
  */

  function loggedIn(req, res) {
    if (!req.signedCookies["sk"] && !req.signedCookies["lk"] && !req.signedCookies["vk"] && !req.cookies.un && !req.signedCookies["dt"] && !req.cookies.sd && !req.cookies.lang && Functions.createUserKey(req.cookies.un) != req.signedCookies["vk"]) {
      return false;
    } else if (!req.signedCookies["sk"] ) {
      clearCookies(res);
      return false;
    } else if (!req.signedCookies["lk"]) {
      clearCookies(res);
      return false;
    } else if (!req.signedCookies["vk"]) {
      clearCookies(res);
      return false;
    } else if (!req.cookies.un) {
      clearCookies(res);
      return false;
    } else if (!req.signedCookies["dt"]) {
      clearCookies(res);
      return false;
    } else if (!req.cookies.sd) {
      clearCookies(res);
      return false;
    } else if (!req.cookies.lang) {
      clearCookies(res);
      return false;
    } else if (Functions.createUserKey(req.cookies.un) != req.signedCookies["vk"]) {
      clearCookies(res);
      return false;
    } else {
      return true;
    }
  }
  function teamLoggedIn(req, res) {
    if (!req.signedCookies["sk-team"] && !req.signedCookies["lk-team"] && Functions.createTeamKey(req.cookies["nn-team"]) != req.signedCookies["vk-team"] && !req.signedCookies["vk-team"] && !req.cookies["nn-team"] && !req.signedCookies["dt-team"] && !req.cookies["sd-team"] && !req.cookies["lang-team"]) {
      return false;
    } else if (!req.signedCookies["sk-team"]) {
      clearTeamCookies(res);
      return false;
    } else if (!req.signedCookies["lk-team"]) {
      clearTeamCookies(res);
      return false;
    } else if (!req.signedCookies["vk-team"]) {
      clearTeamCookies(res);
      return false;
    } else if (!req.cookies["nn-team"]) {
      clearTeamCookies(res);
      return false;
    } else if (!req.signedCookies["dt-team"]) {
      clearTeamCookies(res);
      return false;
    } else if (!req.cookies["sd-team"]) {
      clearTeamCookies(res);
      return false;
    } else if (!req.cookies["lang-team"]) {
      clearTeamCookies(res);
      return false;
    } else if (Functions.createTeamKey(req.cookies["nn-team"]) != req.signedCookies["vk-team"]) {
      clearTeamCookies(res);
      return false;
    } else {
      return true;
    }
  }
  function adminSession(req, res) {
    if (Object.keys(req.cookies).length == 0 && Object.keys(req.signedCookies).length == 0) {
      return false;
    } else if (!req.signedCookies["aak"]) {
      return false;
    } else if (!req.signedCookies["asst"]) {
      return false;
    } else {
      return true;
    }
  }

  function clearCookies(res) {
    res.clearCookie("sk");
    res.clearCookie("lk");
    res.clearCookie("vk");
    res.clearCookie("un");
    res.clearCookie("dt");
    res.clearCookie("sd");
    res.clearCookie("dnotif");
    res.clearCookie("lang");
    res.clearCookie("_csrf");
    res.clearCookie("io");
  }
  function clearTeamCookies(res) {
    res.clearCookie("sk-team");
    res.clearCookie("lk-team");
    res.clearCookie("vk-team");
    res.clearCookie("nn-team");
    res.clearCookie("dt-team");
    res.clearCookie("sd-team");
    res.clearCookie("dnotif");
    res.clearCookie("lang-team");
    res.clearCookie("_csrf");
    res.clearCookie("io");
  }
  function clearAdminCookies(res) {
    res.clearCookie("aak");
    res.clearCookie("asst");
    res.clearCookie("_csrf");
    res.clearCookie("io");
  }

  app.get("/api/4868b0b4105908817f", function(req, res) {
    if (req.query.reference_code === "FridayMorningPreMeet" && req.query.key == "993421" && typeof req.query.lang != "undefined" && typeof req.query.webApp != "undefined") {
      var ip = req.ip;
      var lang = req.query.lang;
      var webApp = (req.query.webApp == 'true');

      async.parallel({
          categories : function(callback) {
            globals.categories(lang, callback);
          }
      }, function(err, results) {
          if (err == "d") return callbackSendResponse(500, "callback", res);

          var response = {
            "resultCode" : "1",
            "resultDate" : new Date(),
            "categories" : results.categories,
            "webApp" : webApp
          };

          callbackSendResponse(200, response, res);
      });
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/b574be1ff5d0b12f2f", function(req, res) {
    if (req.query.reference_code == "ThursdayEveningLightBlueSky" && req.query.key == "990304") {
      var cookies = req.cookies;
      var ip = req.ip;
      var lang = req.query.lang;
      var webApp = (req.query.webApp == 'true');

      async.parallel({
          categories : function(callback) {
            globals.categories(lang, callback);
          },
          constants : function(callback) {
            globals.constants(callback);
          }
      }, function(err, results) {
          if (err == "d") return callbackSendResponse(500, "callback", res);

          var response = {
            "resultCode" : "1",
            "resultDate" : new Date(),
            "one_survey" : results.constants.one_survey.toFixed(2),
            "categories" : results.categories,
            "webApp" : webApp
          };

          callbackSendResponse(200, response, res);
      });
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/analytics/:ps/:id/:type/:format/download", function(req, res) {
    if (loggedIn(req, res) && ((req.params.ps == 'survey' && ['0','1','2','3','4','5','6'].includes(req.params.type))||(req.params.ps == 'poll' && ['0','1','2','3'].includes(req.params.type))) && ["JSON", "CSV", "XML"].includes(req.params.format)) {
      var id = req.params.id;
      var ps = req.params.ps;
      var type = req.params.type.toLowerCase().replace(/\s/g,'');
      var format = req.params.format.toLowerCase();
      var filename = "./temp/analytics/"+id+"-"+type+"."+format;
      var ip = req.ip;
      // var lang = req.query.lang;
      var validationKey = req.signedCookies['vk'];

      analyticsDownload.init(ip, validationKey, ps, id, type, format, function(error, result) {
        if (error) return res.status(404).send("not found");
        fs.writeFile(filename, result, function(error) {
          if(error) throw error;
          res.download(filename, function(error) {
            fs.unlink(filename, function (err) {
              if (err) throw err;
            });
          });
        });
      });
    } else {
      res.status(200).sendFile('404.html', {root: path.join(__dirname, './public/view/')});
    }
  });
  app.get("/api/notifications/e1760a7b88184d0517f11c900342e1c4fba47fdd", function(req, res) {
    if ((req.query.reference_code == "TuesdayEveningPizza" && req.query.key == "992018" && typeof req.query.webApp != "undefined")) {

      var ip = req.ip;
      var lang = req.params.lang;
      var sessionKey;
      var webApp = (req.query.webApp == 'true');

      if ((loggedIn(req, res) || teamLoggedIn(req, res))) {
        sessionKey = req.signedCookies['sk'];
      } else {
        if (typeof req.params.session_key != "undefined") {
          sessionKey = req.params.session_key;
        } else {
          return wrongAuthToken(res);
        }
      }

      async.parallel({
          notification : function(callback) {
            globals.notification(lang, sessionKey, callback);
          }
      }, function(err, results) {
          if (err == "d") return callbackSendResponse(500, "error", res);

          var response = {
            "resultCode" : "1",
            "resultDate" : new Date(),
            "notification" : results.notification,
            "webApp" : webApp
          };

          callbackSendResponse(200, response, res);
      });
    } else {
      wrongAuthToken(res);
    }
  });
  app.post("/api/notifications/read", function(req, res) {
    if ((loggedIn(req, res) || teamLoggedIn(req, res)) && (req.body.AUTH_TOKEN == "b85cd747c19971d2c16e17fba1302212ea87deed")) {

      var ip = req.ip;
      var sessionKey;
      var webApp = (req.query.webApp == 'true');
      sessionKey = req.signedCookies['sk'];

      async.parallel({
          read : function(callback) {
            globals.read(sessionKey, callback);
          }
      }, function(err) {
          if (err == "d") return callbackSendResponse(500, "error", res);

          var response = {
            "resultCode" : "1",
            "resultDate" : new Date(),
            "webApp" : webApp
          };

          callbackSendResponse(200, response, res);
      });
    } else {
      wrongAuthToken(res);
    }
  });


// </editor-fold>


  // app.get('/.well-known/acme-challenge/yLvv4TVU_h9fpb12vOVteW-can01PZ8LIlb4BE23-2U', function(req, res) {
  //   res.send('yLvv4TVU_h9fpb12vOVteW-can01PZ8LIlb4BE23-2U.vHGiQeEJK2QWVIkI6fOOlmxcfzLj1rU5Jz-9u85v7Mk');
  // });

// <editor-fold> ROUTES
  app.get('/07e2025d/05aa01fb', function(req, res) {
    if (loggedIn(req, res) || teamLoggedIn(req, res)) {
      res.status(404).sendFile('404.html', {root: path.join(__dirname, '/public/view/')});
    } else {
      res.status(200).sendFile('init.html', {root: path.join(__dirname, '/admin/view/')});
    }
  });

  app.get(['/2383052a', '/2383052a/', '/2383052a/home', '/2383052a/user-traffic', '/2383052a/reports', '/2383052a/income', '/2383052a/categories', '/2383052a/users', '/2383052a/companies', '/2383052a/admins'], function(req, res) {
    if (adminSession(req, res)) {
      res.status(200).sendFile('admin.html', {root: path.join(__dirname, '/admin/view/')});
    } else {
      res.status(404).sendFile('404.html', {root: path.join(__dirname, '/public/view/')});
    }
  });

  app.get(['/', '/survey_creator', '/poll_creator', '/surveys/:survey_token/edit', '/polls/:poll_token/edit', '/surveys/:survey_token/receipt', '/analytics', '/labs', '/manage', '/settings', '/logout', '/users/:user_validation_key/:username/profile', '/teams/:team_validation_key/:nickname/profile'], function(req, res) {
    if (loggedIn(req, res)) {
      res.status(200).sendFile('session.html', {root: path.join(__dirname, '/public/view/')});
    } else if (teamLoggedIn(req, res)) {
      res.status(200).sendFile('session.html', {root: path.join(__dirname, '/teams/view/')});
    } else {
      res.status(200).sendFile('public.html', {root: path.join(__dirname, '/public/view/')});
    }
  });

  app.get(['/surveys/:survey_token/answer', '/polls/:poll_token/answer'], function(req, res){
    if (loggedIn(req, res)) {
      res.status(200).sendFile('session.html', {root: path.join(__dirname, '/public/view/')});
    } else {
      res.status(200).sendFile('public.html', {root: path.join(__dirname, '/public/view/')});
    }
  });

  app.get(['/login', '/reset_password', '/register', '/forgot_password', '/forgot_username', '/landing_page', '/teams/login',
    '/teams/register', '/teams/forgot_nickname', '/teams/forgot_password', '/teams/reset_password'], function(req, res) {
    if (loggedIn(req, res) || teamLoggedIn(req, res)) {
      res.redirect('/');
    } else {
      res.status(200).sendFile('public.html', {root: path.join(__dirname, '/public/view/')});
    }
  });

  app.get(['/main', '/bookmarks', '/polls', '/profile', '/redeem', '/teams', '/preferences_survey', '/about_survey', '/payment_survey','/surveys/:survey_token/report', '/polls/:poll_token/report'], function(req, res) {
   if (loggedIn(req, res)) {
     res.status(200).sendFile('session.html', {root: path.join(__dirname, '/public/view/')});
   } else {
     res.redirect('/');
   }
  });

  app.get(['/dashboard', '/manage', '/members', '/notifications'], function(req, res) {
   if (teamLoggedIn(req, res)) {
     res.status(200).sendFile('session.html', {root: path.join(__dirname, '/teams/view/')});
   } else {
     res.redirect('/');
   }
  });

  app.get('/robots.txt', function (req, res) {
    res.type('text/plain');
    res.status(200).sendFile('robots.txt', {root: __dirname});
  });

  app.get('/404', function(req, res) {
    res.status(404).sendFile('404.html', {root: path.join(__dirname, '/public/view/')});
  });

  app.get('/500', function(req, res) {
    res.status(500).sendFile('500.html', {root: path.join(__dirname, '/public/view/')});
  });

// </editor-fold>

// <editor-fold> USER FILES
// <editor-fold> GET
  app.get('/73664f66bc9106da7d/logout', function(req, res) {
    if (loggedIn(req, res)) {
      clearCookies(res)
      res.redirect("/login");
    } else {
      res.redirect("/login");
    }
  });
  app.get("/api/user/b0058c9ee07d3b5a49", csrfProtection, function(req, res) {
    if (req.query.reference_code === "WednesdayNightClearSky" && req.query.key == "990178") {
      var cookies = req.cookies;
      var ip = req.ip;
      var webApp = (req.query.webApp == 'true');
      var lang = req.query.lang;
      var csrfToken = req.csrfToken();
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }

      sessionData.initialize(ip, validationKey, authKey, sessionKey, lang, webApp, csrfToken, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/users/:user_validation_key/712b10c4f7ce4432af", function(req, res) {
    if (req.query.reference_code == "FridayEveningCloudySky" && req.query.key == "995215" && req.query.pvk && req.query.username) {
      var ip = req.ip;
      var webApp = (req.query.webApp == 'true');
      var username = req.query.username;
      var validationKey = req.query.pvk;

      publicProfileData.initialize(ip, validationKey, username, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/1acef401f0ac771d72", function(req, res) {
    if (req.query.reference_code === "MondayNightDark" && req.query.key == "999666") {
      var cookies = req.cookies;
      var ip = req.ip;
      var webApp = (req.query.webApp == 'true');
      var lang = req.query.lang;
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }

      aboutData.initialize(ip, validationKey, authKey, sessionKey, lang, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/a4fbedfaf5414d9795", function(req, res) {
    if (req.query.reference_code === "TuesdayAfternoonQuarterToEvening" && req.query.key == "929647") {
      var cookies = req.cookies;
      var ip = req.ip;
      var webApp = (req.query.webApp == 'true');
      var lang = req.query.lang;
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }

      paymentData.initialize(ip, validationKey, authKey, sessionKey, lang, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/d61beb4fc45900f94d", function(req, res) {
    if (req.query.reference_code === "ThursdayNightBrightMoon" && req.query.key == "991756") {
      var cookies = req.cookies;
      var ip = req.ip;
      var webApp = (req.query.webApp == 'true');
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      profileData.initialize(ip, validationKey, authKey, sessionKey, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/0357e6e320980d2d8a", function(req, res) {
    if (req.query.reference_code === "FridayEvening1124" && req.query.key == "991124") {
      var cookies = req.cookies;
      var ip = req.ip;
      var webApp = (req.query.webApp == 'true');
      var lang = req.query.lang;
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      rewardsData.initialize(ip, validationKey, authKey, sessionKey, lang, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/4bc78765e700fc4c2b", function(req, res) {
    if (req.query.reference_code === "WednesdayNightThanksgivingEve" && req.query.key == "954262") {
      var cookies = req.cookies;
      var ip = req.ip;
      var webApp = (req.query.webApp == 'true');
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      manageData.initialize(ip, validationKey, authKey, sessionKey, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/d634cb4a34e7746fdc", function(req, res) {
    if (req.query.reference_code === "SaturdayAfternoonCloudy" && req.query.key == "021299") {
      var cookies = req.cookies;
      var ip = req.ip;
      var webApp = (req.query.webApp == 'true');
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      bookmarksData.initialize(ip, validationKey, authKey, sessionKey, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/3ebf12a3d9917ba93b", function(req, res) {
    if (req.query.reference_code === "SundaySnowingSlow" && req.query.key == "990211") {
      var cookies = req.cookies;
      var ip = req.ip;
      var webApp = (req.query.webApp == 'true');
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      labsData.initialize(ip, validationKey, authKey, sessionKey, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/afdc2625c2facd6326", function(req, res) {
    if (req.query.reference_code === "Thanksgiving3am" && req.query.key == "991123" && (req.query.type == "surveys" || req.query.type == "polls") && typeof req.query.token != "undefined") {
      var cookies = req.cookies;
      var ip = req.ip;
      var lang = req.query.lang;
      var type = req.query.type;
      var token = req.query.token;
      var webApp = (req.query.webApp == 'true');
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      editorData.initialize(ip, validationKey, authKey, sessionKey, lang, type, token, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/0695d378ae2144f4d4", function(req, res) {
    if (req.query.reference_code === "Thanksgiving1pm" && req.query.key == "991213" && (req.query.type == "surveys" || req.query.type == "polls") && typeof req.query.token != "undefined") {
      var cookies = req.cookies;
      var ip = req.ip;
      var lang = req.query.lang;
      var type = req.query.type;
      var token = req.query.token;
      var webApp = (req.query.webApp == 'true');
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      receiptData.initialize(ip, validationKey, authKey, sessionKey, lang, type, token, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/75076262024E2A793D", function(req, res) {
    if (req.query.reference_code == "TuesdayEveningSunnyClouds" && req.query.key == "991223") {
      var cookies = req.cookies;
      var ip = req.ip;
      var webApp = (req.query.webApp == 'true');
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      settingsData.initialize(ip, validationKey, authKey, sessionKey, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/9e476d177153b44685", function(req, res) {
    if (req.query.reference_code === "MondayEveningBeautifulSky" && req.query.key == "991212") {
      var cookies = req.cookies;
      var ip = req.ip;
      var lang = req.query.lang;
      var webApp = (req.query.webApp == 'true');
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      surveyData.initialize(ip, validationKey, authKey, sessionKey, lang, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/1c18d221496174a6aa", function(req, res) {
    if (req.query.reference_code === "SundayEveningBTS" && req.query.key == "991126") {
      var cookies = req.cookies;
      var ip = req.ip;
      var lang = req.query.lang;
      var webApp = (req.query.webApp == 'true');
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      pollData.initialize(ip, validationKey, authKey, sessionKey, lang, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/41562427f26404d237", function(req, res) {
    if (req.query.reference_code === "TuesdayEveningWindy" && req.query.key == "991205") {
      var cookies = req.cookies;
      var ip = req.ip;
      var lang = req.query.lang;
      var webApp = (req.query.webApp == 'true');
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
        } else {
          return noAuthToken(res)
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res)
        }
      }
      analyticsData.initialize(ip, validationKey, authKey, sessionKey, lang, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/00b169f453b6d34063", function(req, res) {
    if (req.query.reference_code === "SaturdayEveningLate" && req.query.key == "991318") {
      var cookies = req.cookies;
      var ip = req.ip;
      var lang = req.query.lang;
      var webApp = (req.query.webApp == 'true');
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
        } else {
          return noAuthToken(res)
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res)
        }
      }
      teamsGetter.initialize(ip, validationKey, authKey, sessionKey, lang, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/8f5f974a5fda1c42fb", function(req, res) {
    if (req.query.reference_code === "SaturdayMidnight2" && req.query.key == "991202" && typeof req.query.token != "undefined" && req.query.type == "surveys" || req.query.type == "polls") {
      var cookies = req.cookies;
      var ip = req.ip;
      var lang = req.query.lang;
      var webApp = (req.query.webApp == 'true');
      var token = req.query.token;
      var type = req.query.type;
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
        } else {
          return noAuthToken(res)
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res)
        }
      }
      reportData.initialize(ip, validationKey, authKey, sessionKey, lang, type, token, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/6c25ce5369fc7c0a5b", function(req, res) {
    if (req.query.reference_code === "FridayNightLunaCat" && req.query.key == "990675") {
      var cookies = req.cookies;
      var ip = req.ip;
      var webApp = (req.query.webApp == 'true');
      var lang = req.query.lang;
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      preferencesData.initialize(ip, validationKey, authKey, sessionKey, lang, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/c3789b05570894bcfd", function(req, res) {
    if (req.query.reference_code === "SundayNightAC" && req.query.key == "990220" && typeof req.query.token != "undefined") {
      var cookies = req.cookies;
      var ip = req.ip;
      var webApp = (req.query.webApp == 'true');
      var lang = req.query.lang;
      var token = req.query.token;
      var user;
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
          user = true;
        } else {
          user = false;
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
          user = true;
        } else {
          user = false;
        }
      }

      if (user) {
        pollAnswerData.initialize(ip, validationKey, authKey, sessionKey, lang, token, webApp, callbackSendResponse, res);
      } else {
        pollAnswerPublicData.initialize(ip, lang, token, webApp, callbackSendResponse, res);
      }
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/5a239f4c0ec530c497", function(req, res) {
    if (req.query.reference_code === "FridayNightMMS" && req.query.key == "991201" && typeof req.query.token != "undefined") {
      var cookies = req.cookies;
      var ip = req.ip;
      var webApp = (req.query.webApp == 'true');
      var lang = req.query.lang;
      var token = req.query.token;
      var user;
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
          user = true;
        } else {
          user = false;
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
          user = true;
        } else {
          user = false;
        }
      }

      if (user) {
        surveyAnswerData.initialize(ip, validationKey, authKey, sessionKey, lang, token, webApp, callbackSendResponse, res);
      } else {
        surveysAnswerPublicData.initialize(ip, lang, token, webApp, callbackSendResponse, res);
      }
    } else {
      wrongAuthToken(res);
    }
  });
  app.get("/api/user/f02d69d2b65a3a17f4", function(req, res) {
    if (req.query.reference_code === "WednesdayNightEmptySky" && req.query.key == "990128" && typeof req.query.searchQuery != "undefined" && typeof req.query.searchConstraint != "undefined") {
      var cookies = req.cookies;
      var ip = req.ip;
      var webApp = (req.query.webApp == 'true');
      var lang = req.query.lang;
      var searchQuery = req.query.searchQuery;
      var searchConstraint = req.query.searchConstraint;
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (loggedIn(req, res)) {
          validationKey = req.signedCookies["vk"];
          authKey = req.signedCookies["lk"];
          sessionKey = req.signedCookies["sk"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      searchData.initialize(ip, lang, validationKey, authKey, sessionKey, webApp, searchQuery, searchConstraint, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
// </editor-fold>
// <editor-fold> POST
  app.post("/api/register", function(req, res) {
    if (typeof req.body.lang != "undefined" && typeof req.body.fullname != "undefined" && typeof req.body.email_phone != "undefined" && typeof req.body.username != "undefined"
       && typeof req.body.password != "undefined" && typeof req.body.dob != "undefined") {

      var req_auth_token = req.body.AUTH_TOKEN;
          ip = req.ip;
          lang = req.body.lang;
          fullname = req.body.fullname;
          email_phone = req.body.email_phone;
          username = req.body.username;
          password = req.body.password;
          dob = req.body.dob;
          webApp = (req.body.webApp == "true" || req.body.webApp === true);

      if(req_auth_token !== "ff6e14a65abe095a37bd7e203da37183553a0ae4") {
        return noAuthToken(res);
      } else {
        register.initialize(ip, lang, fullname, email_phone, username, password, dob, webApp, callbackSendResponse, res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/login", function(req, res) {
    if (typeof req.body.lang != "undefined" && typeof req.body.username != "undefined" && typeof req.body.password != "undefined") {

      var req_auth_token = req.body.AUTH_TOKEN;
          ip = req.ip;
          lang = req.body.lang;
          username = req.body.username.toLowerCase();
          password = req.body.password;
          webApp = (req.body.webApp == "true" || req.body.webApp === true);

      if(req_auth_token !== "86d6b440f80df257d08375ac9719c24a7828e075") {
        return noAuthToken(res);
      } else {
        login.initialize(ip, lang, username, password, webApp, callbackSendResponse, res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/forgot_username", function(req, res) {
    if (typeof req.body.lang != "undefined" && typeof req.body.email_phone != "undefined" && typeof req.body.dob != "undefined") {
      var req_auth_token = req.body.AUTH_TOKEN;
          ip = req.ip;
          lang = req.body.lang.trim().toLowerCase();
          email_phone = req.body.email_phone.trim();
          dob = req.body.dob;

      if (req_auth_token !== "b7a1992ab58f4923369f3a53f0b73d02f71368f1") {
        return noAuthToken(res);
      } else {
        forgotUsername.initialize(ip, lang, email_phone, dob, callbackSendResponse, res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/forgot_password", function(req, res) {
    if (typeof req.body.lang != "undefined" && typeof req.body.username != "undefined" && typeof req.body.email_phone != "undefined" && typeof req.body.dob != "undefined") {
      var req_auth_token = req.body.AUTH_TOKEN;
          ip = req.ip;
          lang = req.body.lang.trim().toLowerCase();
          username = req.body.username.trim().toLowerCase();
          email_phone = req.body.email_phone.trim().toLowerCase();
          dob = req.body.dob.trim();
          webApp = (req.body.webApp == "true" || req.body.webApp === true);

      if (req_auth_token !== "106b965c50810171f7ad65198ebeb5c1aa153e7f") {
        return noAuthToken(res);
      } else {
        forgotPassword.initialize(ip, lang, username, email_phone, dob, webApp, callbackSendResponse,res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/reset_password", function(req, res){
    if(typeof req.body.lang != "undefined" && typeof req.body.username != "undefined" && typeof req.body.password != "undefined" && typeof req.body.code != "undefined") {
      var req_auth_token = req.body.AUTH_TOKEN;
          ip = req.ip;
          lang = req.body.lang.trim().toLowerCase();
          username = req.body.username.trim().toLowerCase();
          new_password = req.body.password;
          code = req.body.code.trim();

      if (req_auth_token !== "1598e8e31a66516d8fd27f15a6190904a31382c9") {
        return noAuthToken(res);
      } else {
        resetPassword.initialize(ip, lang, username, code, new_password, callbackSendResponse,res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/settings/about_survey", function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.about_completed != "undefined" && typeof req.body.userAnswers.sex != "undefined" && typeof req.body.userAnswers.ethnicity != "undefined" &&
    typeof req.body.userAnswers.lang != "undefined" && typeof req.body.userAnswers.education != "undefined" && typeof req.body.userAnswers.relationship != "undefined" && typeof req.body.userAnswers.employment != "undefined" && typeof req.body.userAnswers.religion != "undefined") {
      if (req.body.AUTH_TOKEN === "f877990c0256d27d0ab9c28f05eaf6d3b7748502") {
        var cookies = req.cookies;
        var ip = req.ip;
        var about_completed = req.body.about_completed;
        var userAnswers = req.body.userAnswers;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        aboutSurvey.initialize(ip, validationKey, authKey, sessionKey, about_completed, userAnswers, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/settings/payment_survey", function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.payment_completed != "undefined" && typeof req.body.userAnswers.digits != "undefined" && typeof req.body.userAnswers.name != "undefined" &&
     typeof req.body.userAnswers.exp != "undefined" && typeof req.body.userAnswers.street_address != "undefined" && typeof req.body.userAnswers.apt != "undefined" && typeof req.body.userAnswers.city != "undefined" && typeof req.body.userAnswers.country != "undefined") {
      if (req.body.AUTH_TOKEN === "dd1fe2a3422d0b1e8b767377e428d12c9098b917") {
        var cookies = req.cookies;
        var ip = req.ip;
        var payment_completed = req.body.payment_completed;
        var userAnswers = req.body.userAnswers;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        paymentSurvey.initialize(ip, validationKey, authKey, sessionKey, payment_completed, userAnswers, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/settings/delete", function(req, res) {
    if (req.body.AUTH_TOKEN) {
      if (req.body.AUTH_TOKEN === "fb11b53abcc1b8f696ae7103337e466b5cc28f2a") {
        var cookies = req.cookies;
        var ip = req.ip;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        deleteAccount.initialize(ip, validationKey, authKey, sessionKey, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/user/labs/create', fileUpload.single('qImage'), function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && (typeof req.file != "undefined" || req.body.qImage == "") && typeof req.body.createdQuestionnaire != "undefined") {
      if (req.body.AUTH_TOKEN === "1ff46b49c380c942733b4d89ac2c86f9602affa5") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var questionnaire = req.body.createdQuestionnaire;
        var image = req.file;
        if (typeof req.file === "undefined") {
          image = "";
        }
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk-team"];
            authKey = req.signedCookies["lk-team"];
            sessionKey = req.signedCookies["sk-team"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
            validationKey = req.query.validation_key;
            authKey = req.query.auth_key;
            sessionKey = req.query.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        labsCreator.initialize(ip, validationKey, authKey, sessionKey, lang, image, questionnaire, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/surveys/create", fileUpload.single('surveyImage'), function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && typeof req.body.createdSurvey != "undefined" && (typeof req.file != "undefined" || req.body.surveyImage == "") &&
     typeof req.body.createdSurvey.surveyDetails != "undefined" && typeof req.body.createdSurvey.freeResponseQuestions != "undefined" && typeof req.body.createdSurvey.checkboxResponseQuestions != "undefined" &&
     typeof req.body.createdSurvey.booleanResponseQuestions != "undefined" && typeof req.body.createdSurvey.radioResponseQuestions != "undefined" && typeof req.body.createdSurvey.menuResponseQuestions != "undefined" &&
     typeof req.body.createdSurvey.order != "undefined" && typeof req.body.createdSurvey.paymentDetails != "undefined") {

      if (req.body.AUTH_TOKEN === "5d83912d2037f1c843caa61125c59a3b2be4f7d0") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var createdSurvey = req.body.createdSurvey;
        var image = req.file;
        if (typeof req.file === "undefined") {
          image = "";
        }
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        surveyCreator.initialize(ip, validationKey, authKey, sessionKey, lang, image, createdSurvey, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/polls/create", fileUpload.single('pollImage'), function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && typeof req.body.poll != "undefined" && (typeof req.file != "undefined" || req.body.pollImage == "") &&
     typeof req.body.poll.pollDetails != "undefined" && typeof req.body.poll.pollQuestion != "undefined") {

      if (req.body.AUTH_TOKEN === "0c60247b9e8d9e2d67bf8f1ea07894c63af6ddca") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var createdPoll = req.body.poll;
        var image = req.file;
        if (typeof req.file === "undefined") {
          image = "";
        }
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        pollCreator.initialize(ip, validationKey, authKey, sessionKey, lang, image, createdPoll, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/polls/answer", function(req, res) {
    if (req.body.AUTH_TOKEN && req.body.lang != "undefined" && req.body.token != "undefined" && req.body.response != "undefined" && req.body.labs != "undefined") {
      if (req.body.AUTH_TOKEN === "f6642b19507252dbd126a770e9ea0265c3754bd5") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var token = req.body.token;
        var response = req.body.response;
        var labs = req.body.labs;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var user;
        var validationKey = "";
        var authKey = "";
        var sessionKey = "";

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
            user = true;
          } else {
            user = false;
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
            user = true;
          } else {
            user = false;
          }
        }

        pollAnswer.initialize(ip, validationKey, authKey, sessionKey, lang, token, response, labs, user, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/surveys/answer", function(req, res) {
    if (req.body.AUTH_TOKEN && req.body.lang != "undefined" && req.body.token != "undefined" && req.body.response != "undefined" && req.body.labs != "undefined") {
      if (req.body.AUTH_TOKEN === "7ccea46ffe440f91a95c84272c919fbe9057c80e") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var token = req.body.token;
        var response = req.body.response;
        var labs = req.body.labs;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var user;
        var validationKey = "";
        var authKey = "";
        var sessionKey = "";

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
            user = true;
          } else {
            user = false;
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
            user = true;
          } else {
            user = false;
          }
        }

        surveyAnswer.initialize(ip, validationKey, authKey, sessionKey, lang, token, response, labs, user, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/wishlist", function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.reward_token != "undefined") {
      if (req.body.AUTH_TOKEN === "6da2c39b7d41fd5bb5d545173db09988df3e9364") {
        var cookies = req.cookies;
        var ip = req.ip;
        var token = req.body.reward_token;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        wishlistReward.initialize(ip, validationKey, authKey, sessionKey, token, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/report", function(req, res) {
    if (req.body.AUTH_TOKEN && (req.body.type == "surveys" || req.body.type == "polls") && typeof req.body.token != "undefined"
    && typeof req.body.report != "undefined" && typeof req.body.report.reason != "undefined" && typeof req.body.report.additional != "undefined") {
      if (req.body.AUTH_TOKEN === "d1c8c9bd26d6083413e43b80948525ce05cab0ba") {
        var cookies = req.cookies;
        var ip = req.ip;
        var type = req.body.type;
        var token = req.body.token;
        var report = req.body.report;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        reportQuestionnaire.initialize(ip, validationKey, authKey, sessionKey, type, token, report, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/surveys/edit", function(req, res) {
    if (req.body.AUTH_TOKEN && (req.body.type == "surveys" || req.body.type == "polls") && typeof req.body.token != "undefined" && typeof req.body.edit != "undefined" &&
     typeof req.body.edit.delete != "undefined" && typeof req.body.edit.name != "undefined" && typeof req.body.edit.description != "undefined" && typeof req.body.edit.image_url != "undefined" && typeof req.body.edit.asm != "undefined") {
      if (req.body.AUTH_TOKEN === "b922425ee2940e6af1564f105a6911d1c757a3a6") {
        var cookies = req.cookies;
        var ip = req.ip;
        var type = req.body.type;
        var token = req.body.token;
        var edit = req.body.edit;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        editQuestionnaire.initialize(ip, validationKey, authKey, sessionKey, type, token, edit, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/teams/join", function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.code != "undefined") {
      if (req.body.AUTH_TOKEN === "e956030f6144dc88e583646a24cdfba30d4aebd2") {
        var cookies = req.cookies;
        var ip = req.ip;
        var code = req.body.code;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        teamsManager.initializeJoin(ip, validationKey, authKey, sessionKey, code, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/teams/leave", function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.token != "undefined") {
      if (req.body.AUTH_TOKEN === "5faaa9f469a85c282b6ec117cc3a8bae32a4fcec") {
        var cookies = req.cookies;
        var ip = req.ip;
        var token = req.body.token;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        teamsManager.initializeLeave(ip, validationKey, authKey, sessionKey, token, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/bookmark", function(req, res) {
    if (req.body.AUTH_TOKEN && (req.body.type == "survey" || req.body.type == "poll") && typeof req.body.token != "undefined") {
      if (req.body.AUTH_TOKEN === "c0905eeacea2815d5a7877f4fe35a38c94c81e46") {
        var cookies = req.cookies;
        var ip = req.ip;
        var type = req.body.type;
        var token = req.body.token;
        var webApp = (req.body.webApp == "true" || req.body.webApp == true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        bookmarkQuestionnaire.initialize(ip, validationKey, authKey, sessionKey, type, token, webApp, callbackSendResponse, res);
      } else {
        console.log("HDIHD");

        wrongAuthToken(res);
      }
    } else {
      console.log("dbdjdbd");
      wrongParams(res);
    }
  });
  app.post("/api/user/trade", function(req, res) {
    if (req.body.AUTH_TOKEN) {
      if (req.body.AUTH_TOKEN === "1fb87bc55c7531db41295a99d7b4f3e5") {
        var cookies = req.cookies;
        var ip = req.ip;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        trader.initialize(ip, validationKey, authKey, sessionKey, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/redeem", function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.reward_token != "undefined") {
      if (req.body.AUTH_TOKEN === "318f00d5aaa397e312d4e88773128b25") {
        var cookies = req.cookies;
        var ip = req.ip;
        var token = req.body.reward_token;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        claimReward.initialize(ip, validationKey, authKey, sessionKey, token, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/settings/profile_picture", fileUpload.single('profilePicture'), function (req, res, next) {
    if (req.body.AUTH_TOKEN && req.file) {
      if (req.body.AUTH_TOKEN === "29cf0a5f4e05b9cbcb7e3b08da0109d576b8d33f") {
        var cookies = req.cookies;
        var ip = req.ip;
        var file = req.file;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        settingsProfilePictureUpdate.initialize(ip, validationKey, authKey, sessionKey, file, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/settings/motto", function (req, res, next) {
    if (req.body.AUTH_TOKEN && req.body.motto) {
      if (req.body.AUTH_TOKEN === "n0102esa2el2l1ed9a78a7m4fe35a38c94c81e46") {
        var cookies = req.cookies;
        var ip = req.ip;
        var motto = req.body.motto;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        settingsProfileMottoUpdate.initialize(ip, validationKey, authKey, sessionKey, motto, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/settings/account", function(req, res) {
    if (req.body.AUTH_TOKEN && req.body.ep) {
      if (req.body.AUTH_TOKEN === "684f09b83c13cfc2249d637db86e0bea0e007af1") {
        var cookies = req.cookies;
        var ip = req.ip;
        var ep = req.body.ep;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        settingsAccountInfo.initialize(ip, validationKey, authKey, sessionKey, ep, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/settings/contact", function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.email != 'undefined' && typeof req.body.phone_number != 'undefined' && typeof req.body.website != 'undefined' && typeof req.body.facebook != 'undefined' && typeof req.body.instagram != 'undefined' && typeof req.body.webApp != 'undefined') {
      if (req.body.AUTH_TOKEN === "7da38a622ab17c090389d806efa1c785a2243977") {
        var cookies = req.cookies;
        var ip = req.ip;
        var email = req.body.email;
        var phone_number = req.body.phone_number;
        var website = req.body.website;
        var facebook = req.body.facebook;
        var instagram = req.body.instagram;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        settingsContactInfo.initialize(ip, validationKey, authKey, sessionKey, email, phone_number, website, facebook, instagram, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/settings/labs", function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.webApp != 'undefined') {
      if (req.body.AUTH_TOKEN === "39b95a7aea064635f4f6fec22d45bddfe05be85d") {
        var cookies = req.cookies;
        var ip = req.ip;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        settingsLabsMembership.initialize(ip, validationKey, authKey, sessionKey, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/settings/password", function(req, res) {
    if (req.body.AUTH_TOKEN && req.body.current_password && req.body.new_password) {
      if (req.body.AUTH_TOKEN === "a6e86db25db01eb61f9c8b4950b3914ee2f33e87") {
        var cookies = req.cookies;
        var ip = req.ip;
        var current_password = req.body.current_password;
        var new_password = req.body.new_password;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        settingsPassword.initialize(ip, validationKey, authKey, sessionKey, current_password, new_password, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/settings/notifications", function(req, res) {
    if (req.body.AUTH_TOKEN && req.body.email_message_notifications != "undefined" && req.body.match_preferences_notifications != "undefined" && req.body.lang) {
      if (req.body.AUTH_TOKEN === "d4b015c242d095eb62b0acabfac0a8f88d7c9cef") {
        var cookies = req.cookies;
        var ip = req.ip;
        var email_message_notifications = req.body.email_message_notifications;
        var match_preferences_notifications = req.body.match_preferences_notifications;
        var lang = req.body.lang;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        settingsNotifications.initialize(ip, validationKey, authKey, sessionKey, email_message_notifications, match_preferences_notifications, lang, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/user/settings/preferences_survey", function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.preferences_completed != "undefined" && typeof req.body.preferences.lang != "undefined" && typeof req.body.preferences.monosex_filter != "undefined" && typeof req.body.preferences.translated_filter != "undefined" &&
    typeof req.body.preferences.categories != "undefined" && typeof req.body.preferences.companies != "undefined") {
      if (req.body.AUTH_TOKEN === "6de84acd7ebbc319dbd884ce6ddb816172d9b5fc") {
        var cookies = req.cookies;
        var ip = req.ip;
        var preferences_completed = req.body.preferences_completed;
        var userAnswers = req.body.preferences;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (loggedIn(req, res)) {
            validationKey = req.signedCookies["vk"];
            authKey = req.signedCookies["lk"];
            sessionKey = req.signedCookies["sk"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (req.body.validation_key && req.body.auth_key && req.body.session_key) {
            validationKey = req.body.validation_key;
            authKey = req.body.auth_key;
            sessionKey = req.body.session_key;
          } else {
            return noAuthToken(res);
          }
        }

        preferencesSurvey.initialize(ip, validationKey, authKey, sessionKey, preferences_completed, userAnswers, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
//</editor-fold>
// </editor-fold>

// <editor-fold> TEAM FILES
  app.get('/6bda982nndks62fgdba/teams/logout', function(req, res) {
    if (teamLoggedIn(req, res)) {
      clearTeamCookies(res)
      res.redirect("/teams/login");
    } else {
      res.redirect("/teams/login");
    }
  });
  app.get('/api/team/b15356eaa49b47dc97', csrfProtection, function(req, res) {
    if (req.query.reference_code == "MondayMidnightEarly" && req.query.key == "991815") {
      var ip = req.ip;
      var cookies = req.cookies;
      var webApp = (req.query.webApp == 'true');
      var lang = req.query.lang;
      var csrfToken = req.csrfToken();
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (teamLoggedIn(req, res)) {
          validationKey = req.signedCookies["vk-team"];
          authKey = req.signedCookies["lk-team"];
          sessionKey = req.signedCookies["sk-team"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      teamSession.initialize(ip, validationKey, authKey, sessionKey, lang, webApp, csrfToken, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get('/api/team/605f4b89f6b804cf8b', function(req, res) {
    if (req.query.reference_code == "MondayEveningTwTw" && req.query.key == "990822") {
      var ip = req.ip;
      var cookies = req.cookies;
      var webApp = (req.query.webApp == 'true');
      var lang = req.query.lang;
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (teamLoggedIn(req, res)) {
          validationKey = req.signedCookies["vk-team"];
          authKey = req.signedCookies["lk-team"];
          sessionKey = req.signedCookies["sk-team"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      teamAnalytics.initialize(ip, validationKey, authKey, sessionKey, lang, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get('/api/team/6322c3c2e2d3f2953f', function(req, res) {
    if (req.query.reference_code == "SundayEveningCelebBD" && req.query.key == "991979") {
      var ip = req.ip;
      var cookies = req.cookies;
      var webApp = (req.query.webApp == 'true');
      var lang = req.query.lang;
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (teamLoggedIn(req, res)) {
          validationKey = req.signedCookies["vk-team"];
          authKey = req.signedCookies["lk-team"];
          sessionKey = req.signedCookies["sk-team"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      teamDashboard.initialize(ip, validationKey, authKey, sessionKey, lang, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get('/api/team/c6cc5f93ef1ecb9dff', function(req, res) {
    if (req.query.reference_code == "MondayEveningAte" && req.query.key == "992816") {
      var ip = req.ip;
      var cookies = req.cookies;
      var webApp = (req.query.webApp == 'true');
      var lang = req.query.lang;
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (teamLoggedIn(req, res)) {
          validationKey = req.signedCookies["vk-team"];
          authKey = req.signedCookies["lk-team"];
          sessionKey = req.signedCookies["sk-team"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      teamManage.initialize(ip, validationKey, authKey, sessionKey, lang, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get('/api/team/a15d61a2dbc11be816', function(req, res) {
    if (req.query.reference_code == "MondayEveningPreNight" && req.query.key == "990840") {
      var ip = req.ip;
      var cookies = req.cookies;
      var webApp = (req.query.webApp == 'true');
      var lang = req.query.lang;
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (teamLoggedIn(req, res)) {
          validationKey = req.signedCookies["vk-team"];
          authKey = req.signedCookies["lk-team"];
          sessionKey = req.signedCookies["sk-team"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      teamMembers.initialize(ip, validationKey, authKey, sessionKey, lang, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get('/api/team/32d8d7c83b6d86deb1', function(req, res) {
    if (req.query.reference_code == "SundayDawnRevival" && req.query.key == "991418") {
      var ip = req.ip;
      var cookies = req.cookies;
      var webApp = (req.query.webApp == 'true');
      var lang = req.query.lang;
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (teamLoggedIn(req, res)) {
          validationKey = req.signedCookies["vk-team"];
          authKey = req.signedCookies["lk-team"];
          sessionKey = req.signedCookies["sk-team"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      teamNotifications.initialize(ip, validationKey, authKey, sessionKey, lang, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get('/api/team/c18909cf71f5d8018b', function(req, res) {
    if (req.query.reference_code == "MondayMidday" && req.query.key == "998151") {
      var ip = req.ip;
      var cookies = req.cookies;
      var webApp = (req.query.webApp == 'true');
      var lang = req.query.lang;
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (teamLoggedIn(req, res)) {
          validationKey = req.signedCookies["vk-team"];
          authKey = req.signedCookies["lk-team"];
          sessionKey = req.signedCookies["sk-team"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      teamSettings.initialize(ip, validationKey, authKey, sessionKey, lang, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get('/api/team/e46f6da77f6a868a04', function(req, res) {
    if (req.query.reference_code == "SaturdayPreDB" && req.query.key == "990127" && typeof req.query.type != "undefined" && typeof req.query.token != "undefined") {
      var ip = req.ip;
      var cookies = req.cookies;
      var webApp = (req.query.webApp == 'true');
      var lang = req.query.lang;
      var type = req.query.type;
      var token = req.query.token;
      var validationKey;
      var authKey;
      var sessionKey;

      if (webApp) {
        if (teamLoggedIn(req, res)) {
          validationKey = req.signedCookies["vk-team"];
          authKey = req.signedCookies["lk-team"];
          sessionKey = req.signedCookies["sk-team"];
        } else {
          return noAuthToken(res);
        }
      } else {
        if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
          validationKey = req.query.validation_key;
          authKey = req.query.auth_key;
          sessionKey = req.query.session_key;
        } else {
          return noAuthToken(res);
        }
      }
      teamEditQ.initialize(ip, validationKey, authKey, sessionKey, lang, type, token, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get('/api/teams/:team_validation_key/a50cf95ced779706b6', function(req, res) {
    if (req.query.reference_code == "SundayNightTenSeven" && req.query.key == "991821" && typeof req.query.tvk != "undefined" && typeof req.query.nickname != "undefined") {
      var ip = req.ip;
      var cookies = req.cookies;
      var webApp = (req.query.webApp == 'true');
      var lang = req.query.lang;
      var token = req.query.tvk;
      var nickname = req.query.nickname;

      teamProfile.initialize(ip, lang, token, nickname, webApp, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });

  app.post('/api/teams/register', function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && typeof req.body.fullname != "undefined" && typeof req.body.email_phone != "undefined" && typeof req.body.name != "undefined" && typeof req.body.nickname != "undefined" && typeof req.body.password != "undefined") {
      if (req.body.AUTH_TOKEN === "ed3d5417c418918e311ccb9e4c3e95ba660a30b7") {

        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var fullname = req.body.fullname;
        var email_phone = req.body.email_phone;
        var name = req.body.name;
        var nickname = req.body.nickname;
        var password = req.body.password;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        registerTeams.initialize(ip, lang, fullname, email_phone, name, nickname, password, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/teams/login', function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && typeof req.body.nickname != "undefined" && typeof req.body.password != "undefined") {
      if (req.body.AUTH_TOKEN === "949eb67256cd10eef1b152c73e5e67aaedecf3ac") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var nickname = req.body.nickname;
        var password = req.body.password;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        loginTeams.initialize(ip, lang, nickname, password, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/teams/forgot_nickname', function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && typeof req.body.email_phone != "undefined" && typeof req.body.name != "undefined") {
      if (req.body.AUTH_TOKEN === "b44a0ce73a9258f233f8fd961304392bca91c266") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var email_phone = req.body.email_phone;
        var name = req.body.name;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        forgotNicknameTeams.initialize(ip, lang, email_phone, name, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/teams/forgot_password', function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && typeof req.body.fullname != "undefined" && typeof req.body.email_phone != "undefined" && typeof req.body.nickname != "undefined") {
      if (req.body.AUTH_TOKEN === "e161de2f7209e21e31ec70cce6e6978f8db24316") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var fullname = req.body.fullname;
        var email_phone = req.body.email_phone;
        var nickname = req.body.nickname;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        forgotPasswordTeams.initialize(ip, lang, fullname, email_phone, nickname, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/teams/reset_password', function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && typeof req.body.code != "undefined" && typeof req.body.nickname != "undefined" && typeof req.body.password != "undefined") {
      if (req.body.AUTH_TOKEN === "ce4ee55bde2faf2ca977333d603545f3b8bffa61") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var code = req.body.code;
        var nickname = req.body.nickname;
        var password = req.body.password;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        resetPasswordTeams.initialize(ip, lang, code, nickname, password, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/team/surveys/create', fileUpload.single('surveyImage'), function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && (typeof req.file != "undefined" || req.body.surveyImage == "") && typeof req.body.createdSurvey != "undefined") {
      if (req.body.AUTH_TOKEN === "1491b201ac5943a80ccd77b9e59b6f542b2ae4b0") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var survey = req.body.createdSurvey;
        var image = req.file;
        if (typeof req.file === "undefined") {
          image = "";
        }
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        if (webApp) {
          if (teamLoggedIn(req, res)) {
            validationKey = req.signedCookies["vk-team"];
            authKey = req.signedCookies["lk-team"];
            sessionKey = req.signedCookies["sk-team"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
            validationKey = req.query.validation_key;
            authKey = req.query.auth_key;
            sessionKey = req.query.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        teamSurveyCreator.initialize(ip, validationKey, authKey, sessionKey, lang, image, survey, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/team/polls/create', fileUpload.single('pollImage'), function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && (typeof req.file != "undefined" || req.body.pollImage == "") && typeof req.body.poll != "undefined") {
      if (req.body.AUTH_TOKEN === "9a8a502f1fdc798bd8ee71dcf64013ae9b4efe85") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var poll = req.body.poll;
        var image = req.file;
        if (typeof req.file === "undefined") {
          image = "";
        }
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        if (webApp) {
          if (teamLoggedIn(req, res)) {
            validationKey = req.signedCookies["vk-team"];
            authKey = req.signedCookies["lk-team"];
            sessionKey = req.signedCookies["sk-team"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
            validationKey = req.query.validation_key;
            authKey = req.query.auth_key;
            sessionKey = req.query.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        teamPollCreator.initialize(ip, validationKey, authKey, sessionKey, lang, image, poll, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/team/labs/create', fileUpload.single('qImage'), function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && (typeof req.file != "undefined" || req.body.qImage == "") && typeof req.body.createdQuestionnaire != "undefined") {
      if (req.body.AUTH_TOKEN === "e9c1e045a911740aeb579bc40ac58d45e934866b") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var questionnaire = req.body.createdQuestionnaire;
        var image = req.file;
        if (typeof req.file === "undefined") {
          image = "";
        }
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        if (webApp) {
          if (teamLoggedIn(req, res)) {
            validationKey = req.signedCookies["vk-team"];
            authKey = req.signedCookies["lk-team"];
            sessionKey = req.signedCookies["sk-team"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
            validationKey = req.query.validation_key;
            authKey = req.query.auth_key;
            sessionKey = req.query.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        teamLabsCreator.initialize(ip, validationKey, authKey, sessionKey, lang, image, questionnaire, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/team/members/approve', function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && typeof req.body.at != "undefined") {
      if (req.body.AUTH_TOKEN === "d057e41b8eba1c98da9a1b500da0a62fcc54e175") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var token = req.body.at;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        if (webApp) {
          if (teamLoggedIn(req, res)) {
            validationKey = req.signedCookies["vk-team"];
            authKey = req.signedCookies["lk-team"];
            sessionKey = req.signedCookies["sk-team"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
            validationKey = req.query.validation_key;
            authKey = req.query.auth_key;
            sessionKey = req.query.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        teamMembersApprove.initialize(ip, validationKey, authKey, sessionKey, token, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/team/members/reject', function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && typeof req.body.at != "undefined") {
      if (req.body.AUTH_TOKEN === "d057e41b8eba1c98da9a1b500da0a62fcc54e175") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var token = req.body.at;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        if (webApp) {
          if (teamLoggedIn(req, res)) {
            validationKey = req.signedCookies["vk-team"];
            authKey = req.signedCookies["lk-team"];
            sessionKey = req.signedCookies["sk-team"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
            validationKey = req.query.validation_key;
            authKey = req.query.auth_key;
            sessionKey = req.query.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        teamMembersReject.initialize(ip, validationKey, authKey, sessionKey, token, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/team/members/remove', function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && typeof req.body.mt != "undefined") {
      if (req.body.AUTH_TOKEN === "d057e41b8eba1c98da9a1b500da0a62fcc54e175") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var token = req.body.at;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        if (webApp) {
          if (teamLoggedIn(req, res)) {
            validationKey = req.signedCookies["vk-team"];
            authKey = req.signedCookies["lk-team"];
            sessionKey = req.signedCookies["sk-team"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
            validationKey = req.query.validation_key;
            authKey = req.query.auth_key;
            sessionKey = req.query.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        teamMembersRemove.initialize(ip, validationKey, authKey, sessionKey, token, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/team/settings/account', function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && typeof req.body.accountData != "undefined" && typeof req.body.accountData.fullname != "undefined" && typeof req.body.accountData.description != "undefined"
    && typeof req.body.accountData.name != "undefined" && typeof req.body.accountData.email_phone != "undefined" && typeof req.body.accountData.nickname != "undefined" && typeof req.body.accountData.accent_color != "undefined") {
      if (req.body.AUTH_TOKEN === "d057e41b8eba1c98da9a1b500da0a62fcc54e175") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var account = req.body.accountData;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        if (webApp) {
          if (teamLoggedIn(req, res)) {
            validationKey = req.signedCookies["vk-team"];
            authKey = req.signedCookies["lk-team"];
            sessionKey = req.signedCookies["sk-team"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
            validationKey = req.query.validation_key;
            authKey = req.query.auth_key;
            sessionKey = req.query.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        teamAccountData.initialize(ip, validationKey, authKey, sessionKey, account, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/team/settings/contact', function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && typeof req.body.contactInfo != "undefined" && typeof req.body.contactInfo.email != "undefined" && typeof req.body.contactInfo.phone_number != "undefined"
    && typeof req.body.contactInfo.website != "undefined" && typeof req.body.contactInfo.facebook != "undefined" && typeof req.body.contactInfo.website != "undefined") {
      if (req.body.AUTH_TOKEN === "45650fd9ef136a387325c9b7952bb2821c3a2b1c") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var contact = req.body.contactInfo;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        if (webApp) {
          if (teamLoggedIn(req, res)) {
            validationKey = req.signedCookies["vk-team"];
            authKey = req.signedCookies["lk-team"];
            sessionKey = req.signedCookies["sk-team"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
            validationKey = req.query.validation_key;
            authKey = req.query.auth_key;
            sessionKey = req.query.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        teamContactInfo.initialize(ip, validationKey, authKey, sessionKey, contact, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/team/settings/password', function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && typeof req.body.passwordInfo != "undefined" && typeof req.body.passwordInfo.current != "undefined" && typeof req.body.passwordInfo.new != "undefined") {
      if (req.body.AUTH_TOKEN === "317c7b2e1168d28bf51a53893b9b39199c3c158b") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var password = req.body.passwordInfo;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        if (webApp) {
          if (teamLoggedIn(req, res)) {
            validationKey = req.signedCookies["vk-team"];
            authKey = req.signedCookies["lk-team"];
            sessionKey = req.signedCookies["sk-team"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
            validationKey = req.query.validation_key;
            authKey = req.query.auth_key;
            sessionKey = req.query.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        teamPasswordUpdate.initialize(ip, validationKey, authKey, sessionKey, password, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/team/settings/members', function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && typeof req.body.membersInfo != "undefined" && typeof req.body.membersInfo.code != "undefined"
        && typeof req.body.membersInfo.public != "undefined" && typeof req.body.membersInfo.open != "undefined") {
      if (req.body.AUTH_TOKEN === "20c2017b0a9c303714781c4cf69cd241c7694f43") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var members = req.body.membersInfo;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        if (webApp) {
          if (teamLoggedIn(req, res)) {
            validationKey = req.signedCookies["vk-team"];
            authKey = req.signedCookies["lk-team"];
            sessionKey = req.signedCookies["sk-team"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
            validationKey = req.query.validation_key;
            authKey = req.query.auth_key;
            sessionKey = req.query.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        teamMembersUpdate.initialize(ip, validationKey, authKey, sessionKey, members, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/team/settings/notifications', function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined" && typeof req.body.notificationsInfo != "undefined" && typeof req.body.notificationsInfo.email_phone != "undefined"
        && typeof req.body.notificationsInfo.freq != "undefined") {
      if (req.body.AUTH_TOKEN === "77eb2a3a66d0835701e4eed1dc85896e6f368a7c") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var notifications = req.body.notificationsInfo;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        if (webApp) {
          if (teamLoggedIn(req, res)) {
            validationKey = req.signedCookies["vk-team"];
            authKey = req.signedCookies["lk-team"];
            sessionKey = req.signedCookies["sk-team"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
            validationKey = req.query.validation_key;
            authKey = req.query.auth_key;
            sessionKey = req.query.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        teamNotificationsUpdate.initialize(ip, validationKey, authKey, sessionKey, notifications, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/team/settings/company', function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.lang != "undefined") {
      if (req.body.AUTH_TOKEN === "efcc9eebd845a186341388f16002cd96145a1f91") {
        var cookies = req.cookies;
        var ip = req.ip;
        var lang = req.body.lang;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        if (webApp) {
          if (teamLoggedIn(req, res)) {
            validationKey = req.signedCookies["vk-team"];
            authKey = req.signedCookies["lk-team"];
            sessionKey = req.signedCookies["sk-team"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
            validationKey = req.query.validation_key;
            authKey = req.query.auth_key;
            sessionKey = req.query.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        teamCompanyStatusUpdate.initialize(ip, validationKey, authKey, sessionKey, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/team/settings/logo_image", fileUpload.single('logoPicture'), function (req, res, next) {
    if (req.body.AUTH_TOKEN && typeof req.file != "undefined") {
      if (req.body.AUTH_TOKEN === "bed0985d51e32c4fd121437bfa52e2de3e9df38e") {
        var cookies = req.cookies;
        var ip = req.ip;
        var file = req.file;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (teamLoggedIn(req, res)) {
            validationKey = req.signedCookies["vk-team"];
            authKey = req.signedCookies["lk-team"];
            sessionKey = req.signedCookies["sk-team"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
            validationKey = req.query.validation_key;
            authKey = req.query.auth_key;
            sessionKey = req.query.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        settingsLogoPictureUpdate.initialize(ip, validationKey, authKey, sessionKey, file, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post("/api/team/settings/theme_image", fileUpload.single('themePicture'), function (req, res, next) {
    if (req.body.AUTH_TOKEN && typeof req.file != "undefined") {
      if (req.body.AUTH_TOKEN === "8e3d48071fc7338f837d5b65b614fa902800ef4e") {
        var cookies = req.cookies;
        var ip = req.ip;
        var file = req.file;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);
        var validationKey;
        var authKey;
        var sessionKey;

        if (webApp) {
          if (teamLoggedIn(req, res)) {
            validationKey = req.signedCookies["vk-team"];
            authKey = req.signedCookies["lk-team"];
            sessionKey = req.signedCookies["sk-team"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
            validationKey = req.query.validation_key;
            authKey = req.query.auth_key;
            sessionKey = req.query.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        settingsThemePictureUpdate.initialize(ip, validationKey, authKey, sessionKey, file, webApp, callbackSendResponse, res);
      } else {
        wrongAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/team/settings/delete', function(req, res) {
    if (req.body.AUTH_TOKEN) {
      if (req.body.AUTH_TOKEN === "f3ec31b9db60050687ed4814b3a7df0bb73957be") {
        var cookies = req.cookies;
        var ip = req.ip;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        if (webApp) {
          if (teamLoggedIn(req, res)) {
            validationKey = req.signedCookies["vk-team"];
            authKey = req.signedCookies["lk-team"];
            sessionKey = req.signedCookies["sk-team"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
            validationKey = req.query.validation_key;
            authKey = req.query.auth_key;
            sessionKey = req.query.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        teamDelete.initialize(ip, validationKey, authKey, sessionKey, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });
  app.post('/api/team/surveys/edit', function(req, res) {
    if (req.body.AUTH_TOKEN && typeof req.body.type != "undefined" && typeof req.body.token != "undefined" && typeof req.body.edit != "undefined") {
      if (req.body.AUTH_TOKEN === "5a1f61c030b13abfbe1d551299a2c1254771cccb") {
        var cookies = req.cookies;
        var ip = req.ip;
        var type = req.body.type;
        var token = req.body.token;
        var edit = req.body.edit;
        var webApp = (req.body.webApp == "true" || req.body.webApp === true);

        if (webApp) {
          if (teamLoggedIn(req, res)) {
            validationKey = req.signedCookies["vk-team"];
            authKey = req.signedCookies["lk-team"];
            sessionKey = req.signedCookies["sk-team"];
          } else {
            return noAuthToken(res);
          }
        } else {
          if (typeof req.query.validation_key != "undefined" && typeof req.query.auth_key != "undefined" && typeof req.query.session_key != "undefined") {
            validationKey = req.query.validation_key;
            authKey = req.query.auth_key;
            sessionKey = req.query.session_key;
          } else {
            return noAuthToken(res);
          }
        }
        teamUpdateQ.initialize(ip, validationKey, authKey, sessionKey, type, token, edit, webApp, callbackSendResponse, res);
      } else {
        return noAuthToken(res);
      }
    } else {
      wrongParams(res);
    }
  });

// </editor-fold>

// <editor-fold> ADMIN FILES
  app.get('/4a8c9c790110e4/admins/logout', function(req, res) {
    if (adminSession(req, res) ) {
      clearAdminCookies(res)
      res.redirect("/");
    } else {
      res.status(404).sendFile('404.html', {root: path.join(__dirname, './public/view/')});
    }
  });

  app.get("/api/5757c384c37f705ef28f68ed0e5932830c0a3393/0b3c02e5", csrfProtection, function(req, res) {
    if (adminSession(req, res) && req.query.reference_code === "FridayLateNight" && req.query.key == "996652") {
      var cookies = req.cookies;
      var ip = req.ip;
      var csrfToken = req.csrfToken();
      var authKey = req.signedCookies["aak"];
      var st = req.signedCookies["asst"];

      adminSessionData.initialize(ip, authKey, csrfToken, st, callbackSendResponse, res);
    } else {
      wrongAuthToken(res);
    }
  });
  app.get('/api/fc8a073f6d80f71db6bbfcc1030728f66c14270a9a76a467d7589f7db5c496bf/categories/gather', function(req, res) {
    if (adminSession(req, res) && req.query.reference_code === "SaturdayEarlyMidnight" && req.query.key == "998787") {
      var authKey = req.signedCookies["aak"];

      adminValidate.initialize(authKey, function(error) {
        if (error == "d") return callbackSendResponse(500, "error", res);
        if (error == "a") return callbackSendResponse(4001, "no auth", res);

        var flow = new globals.category();

        flow.gather(function(error, cats, toval) {
          if (error) return callbackSendResponse(500, "error", res);
          var response = {"resultCode" : "1", "resultDate" : new Date(), "categories" : cats, "toval" : toval};
          callbackSendResponse(200, response, res);
        });
      });
    } else {
      noAdminAuth(res);
    }
  });
  app.get('/api/ce26827e838bfa07ba61a7d5480d9b3c005d7d000885362d139608eeabedf8a1/admins/gather', function(req, res) {
    if (adminSession(req, res) && req.query.reference_code === "SaturdayMidMidnight" && req.query.key == "997623") {
      var authKey = req.signedCookies["aak"];
      var ip = req.ip;

      adminValidate.initialize(authKey, function(error) {
        if (error == "d") return callbackSendResponse(500, "error", res);
        if (error == "a") return callbackSendResponse(4001, "no auth", res);


        adminsGather.initialize(ip, callbackSendResponse, res);
      });
    } else {
      noAdminAuth(res);
    }
  });
  app.get('/api/49c266baaaa70981ea188fa714d5c40cf13830d786a861c9943ae0d26a7f3fe9/gather/users', function(req, res) {
    if (adminSession(req, res) && req.query.reference_code === "SaturdayFourPastMorning" && req.query.key == "997763" && typeof req.query.keyword != "undefined") {
      var authKey = req.signedCookies["aak"];
      var ip = req.ip;
      var keyword = req.query.keyword;

      adminValidate.initialize(authKey, function(error) {
        if (error == "d") return callbackSendResponse(500, "error", res);
        if (error == "a") return callbackSendResponse(4001, "no auth", res);


        adminUserSearch.initialize(ip, keyword, callbackSendResponse, res);
      });
    } else {
      noAdminAuth(res);
    }
  });
  app.get('/api/49c266baaaa70981ea188fa714d5c40cf13830d786a861c9943ae0d26a7f3fe9/gather/teams', function(req, res) {
    if (adminSession(req, res) && req.query.reference_code === "SaturdayFourMorning" && req.query.key == "998712" && typeof req.query.keyword != "undefined") {
      var authKey = req.signedCookies["aak"];
      var ip = req.ip;
      var keyword = req.query.keyword;

      adminValidate.initialize(authKey, function(error) {
        if (error == "d") return callbackSendResponse(500, "error", res);
        if (error == "a") return callbackSendResponse(4001, "no auth", res);


        adminTeamsSearch.initialize(ip, keyword, callbackSendResponse, res);
      });
    } else {
      noAdminAuth(res);
    }
  });

  app.post('/api/5757c384c37f705ef28f68ed0e5932830c0a3393/05aa01fb', function(req, res) {
    if (req.body.AUTH_TOKEN == "db26bf5f39cd4f8aa2126b89593d544bcbea1c9b" && typeof req.body.clav != "undefined" && typeof req.body.aptor != "undefined") {
      var ip = req.ip;
      var key = req.body.clav;
      var pass = req.body.aptor;

      adminLogin.initialize(ip, key, pass, callbackSendResponse, res);
    } else {
      noAdminAuth(res);
    }
  });
  app.post('/api/b793d93c5acf46a759583d7db31b4affe3073c3f/admins/create', function(req, res) {
    if (adminSession(req, res) && req.body.AUTH_TOKEN == "beade2400869fabe8986aa701d23d96cea74c62470a71a" && typeof req.body.name != "undefined" && typeof req.body.email != "undefined" && typeof req.body.phone_number != "undefined" && typeof req.body.password != "undefined") {
      var ip = req.ip;
      var admin_key = req.signedCookies["aak"];
      var name = req.body.name;
      var email = req.body.email;
      var phone = req.body.phone_number;
      var password = req.body.password;

      adminCreator.initialize(ip, admin_key, name, email, phone, password, callbackSendResponse, res);
    } else {
      noAdminAuth(res);
    }
  });
  app.post('/api/ce26827e838bfa07ba61a7d5480d9b3c005d7d000885362d139608eeabedf8a1/admins/promote', function(req, res) {
    if (adminSession(req, res) && req.body.AUTH_TOKEN == "ea0c5ee7255ad0e9d6c90ecef57cdc46815c7eed" && typeof req.body.name != "undefined" && typeof req.body.token != "undefined") {
      var ip = req.ip;
      var authKey = req.signedCookies["aak"];
      var name = req.body.name;
      var token = req.body.token;
      token = escape(token);

      adminValidate.initialize(authKey, function(error) {
        if (error == "d") return callbackSendResponse(500, "error", res);
        if (error == "a") return callbackSendResponse(4001, "no auth", res);

        adminPromote.initialize(ip, name, token, callbackSendResponse, res);
      });
    } else {
      noAdminAuth(res);
    }
  });
  app.post('/api/ce26827e838bfa07ba61a7d5480d9b3c005d7d000885362d139608eeabedf8a1/admins/delete', function(req, res) {
    if (adminSession(req, res) && req.body.AUTH_TOKEN == "fe322a94e43e7f0d6bed69bb9ef652a9a4068a32" && typeof req.body.name != "undefined" && typeof req.body.token != "undefined") {
      var ip = req.ip;
      var authKey = req.signedCookies["aak"];
      var name = req.body.name;
      var token = req.body.token;
      token = escape(token);

      adminValidate.initialize(authKey, function(error) {
        if (error == "d") return callbackSendResponse(500, "error", res);
        if (error == "a") return callbackSendResponse(4001, "no auth", res);

        adminDelete.initialize(ip, name, token, callbackSendResponse, res);
      });
    } else {
      noAdminAuth(res);
    }
  });
  app.post('/api/fc8a073f6d80f71db6bbfcc1030728f66c14270a9a76a467d7589f7db5c496bf/categories/create', function(req, res) {
    if (adminSession(req, res) && (req.body.AUTH_TOKEN == "38ad924ff50a62c30a6e5c8815a61375775419f4" && typeof req.body.names != "undefined")) {
      var authKey = req.signedCookies["aak"];
      var namesArray = req.body.names.split(",");

      adminValidate.initialize(authKey, function(error) {
        if (error == "d") return callbackSendResponse(500, "error", res);
        if (error == "a") return callbackSendResponse(4001, "no auth", res);

        var flow = new globals.category();

        flow.create(authKey, namesArray, function(error_code, error_message, catd) {
          var errResponse = {"resultCode" : "-1", "resultDate" : new Date(), "error_code" : error_code, "error_message" : error_message}
          var response = {"resultCode" : "1", "resultDate" : new Date(), "catd" : catd};

          if (error_code != null || error_message != null) return callbackSendResponse(200, errResponse, res);

          callbackSendResponse(200, response, res);
        });
      });
    } else {
      noAdminAuth(res);
    }
  });
  app.post('/api/fc8a073f6d80f71db6bbfcc1030728f66c14270a9a76a467d7589f7db5c496bf/categories/approve', function(req, res) {
    if (adminSession(req, res) && (req.body.AUTH_TOKEN == "6e66da3ad5ba1ebeb81bc36b80c020a873a12ffb" && typeof req.body.catkey != "undefined")) {
      var authKey = req.signedCookies["aak"];
      var catkey = req.body.catkey;

      adminValidate.initialize(authKey, function(error) {
        if (error == "d") return callbackSendResponse(500, "error", res);
        if (error == "a") return callbackSendResponse(4001, "no auth", res);

        var flow = new globals.category();

        flow.approve(authKey, catkey, function(error, catd) {
          if (error) return callbackSendResponse(500, "error", res);

          var response = {"resultCode" : "1", "resultDate" : new Date(), "catd" : catd};

          callbackSendResponse(200, response, res);
        });
      });
    } else {
      noAdminAuth(res);
    }
  });
  app.post('/api/fc8a073f6d80f71db6bbfcc1030728f66c14270a9a76a467d7589f7db5c496bf/categories/rename', function(req, res) {
    if (adminSession(req, res) && (req.body.AUTH_TOKEN == "a883d369309b661dc69153b2e5877d6e447235a8" && typeof req.body.catkey != "undefined" && typeof req.body.rename != "undefined")) {

      var authKey = req.signedCookies["aak"];
      var catkey = req.body.catkey;
      var rename = req.body.rename;

      adminValidate.initialize(authKey, function(error) {
        if (error == "d") return callbackSendResponse(500, "error", res);
        if (error == "a") return callbackSendResponse(4001, "no auth", res);

        var flow = new globals.category();

        flow.rename(catkey, rename, function(error) {
          if (error) return callbackSendResponse(500, "error", res);

          var response = {"resultCode" : "1", "resultDate" : new Date()};

          callbackSendResponse(200, response, res);
        });
      });
    } else {
      noAdminAuth(res);
    }
  });
  app.post('/api/fc8a073f6d80f71db6bbfcc1030728f66c14270a9a76a467d7589f7db5c496bf/categories/delete', function(req, res) {
    if (adminSession(req, res) && (req.body.AUTH_TOKEN == "1cc90394f99c3579cab2d4ce35f6de6d53039942" && typeof req.body.catkey != "undefined")) {
      var authKey = req.signedCookies["aak"];
      var catkey = req.body.catkey;

      adminValidate.initialize(authKey, function(error) {
        if (error == "d") return callbackSendResponse(500, "error", res);
        if (error == "a") return callbackSendResponse(4001, "no auth", res);

        var flow = new globals.category();

        flow.delete(catkey, function(error) {
          if (error) return callbackSendResponse(500, "error", res);

          var response = {"resultCode" : "1", "resultDate" : new Date()};

          callbackSendResponse(200, response, res);
        });
      });
    } else {
      noAdminAuth(res);
    }
  });
  app.post('/api/dbc72abdff8f97982b9fa0fcc8ce6bc86c822e7ae25e279b9e9de5c3658acf4f/companies/create', function(req, res) {
    if (adminSession(req, res) && (typeof req.body.key != "undefined" && typeof req.body.team_token != "undefined" && typeof req.body.name != "undefined")) {

      var token = req.body.team_token;
      var name = req.body.name;
      var flow = new globals.company();

      flow.create(token, name, function(error_code, error_message) {
        var errResponse = {"resultCode" : "1", "resultDate" : new Date(), "error_code" : error_code, "error_message" : error_message}
        var response = {"resultCode" : "1", "resultDate" : new Date()};

        if (error_code || error_message) return callbackSendResponse(200, errResponse, res);

        callbackSendResponse(200, response, res);
      });
    } else {
      noAdminAuth(res);
    }
  });
  app.post('/api/dbc72abdff8f97982b9fa0fcc8ce6bc86c822e7ae25e279b9e9de5c3658acf4f/companies/delete', function(req, res) {
    if (adminSession(req, res) && (typeof req.body.key != "undefined" && typeof req.body.team_token != "undefined")) {

      var token = req.body.team_token;
      var flow = new globals.company();

      flow.delete(token, function(error_code, error_message) {
        var errResponse = {"resultCode" : "1", "resultDate" : new Date(), "error_code" : error_code, "error_message" : error_message}
        var response = {"resultCode" : "1", "resultDate" : new Date()};

        if (error_code || error_message) return callbackSendResponse(200, errResponse, res);

        callbackSendResponse(200, response, res);
      });
    } else {
      noAdminAuth(res);
    }
  });

// </editor-fold>

  app.get('*', function(req, res) {
    res.status(404).sendFile('404.html', {root: path.join(__dirname, '/public/view/')});
  });
};

// <editor-fold> RESPONSE SENDING
function wrongParams(res) {
  response = [];

  resultDate = new Date();

  resultArray = {"resultCode" : -1, "resultDate" : resultDate, "status" : "WRONG/MISSING PARAMS --- YOU DID NOT SEND THE CORRECT OR REQUIRED PARAMS IN ORDER TO COMPLETE YOUR REQUEST"};

  response.push(resultArray);

  responseJSON = {"ERROR":response};
  responseJSONParse = JSON.stringify(responseJSON);

  console.log(responseJSONParse);
  res.status(403).json(responseJSONParse);
}

function noAuthToken(res) {
  response = [];

  resultDate = new Date();

  resultArray = {"resultCode" : -1, "resultDate" : resultDate, "status" : "NO AUTH_TOKEN PROVIDED---CANNOT ACCESS API WITHOUT PERMISSION OR A AUTH_TOKEN"};

  response.push(resultArray);

  responseJSON = {"ERROR":response};
  responseJSONParse = JSON.stringify(responseJSON);

  console.log(responseJSONParse);
  res.status(401).json(responseJSONParse);
}

function wrongAuthToken(res) {
  var response = [];

  var resultDate = new Date();

  var resultArray = {"resultCode" : -1, "resultDate" : resultDate, "status" : "WRONG CREDENTIALS OR AUTHORIZATION TOKEN PROVIDED---CANNOT ACCESS SECURE USER DATA WITHOUT PERMISSION"};

  response.push(resultArray);

  var responseJSON = {"ERROR":response};
  var responseJSONParse = JSON.stringify(responseJSON);

  console.log(responseJSONParse);
  res.status(401).json(responseJSONParse);
}

function noAdminAuth(res) {
  response = [];

  resultDate = new Date();

  resultArray = {"resultCode" : -1, "resultDate" : resultDate, "status" : "CANNOT ACCESS ADMIN PANEL IF YOU DO NOT HAVE THE AUTHORIZATION TO"};

  response.push(resultArray);

  responseJSON = {"ERROR":response};
  responseJSONParse = JSON.stringify(responseJSON);

  console.log(responseJSONParse);
  res.status(401).json(responseJSONParse);
}

function databaseError(res) {
  response = [];

  resultDate = new Date();

  resultArray = {"resultCode" : -99999, "resultDate" : resultDate, "status" : "DATABASE IS UNDER MAINTANANCE---SORRY FOR THE INCONVINIENCE"};

  response.push(resultArray);

  responseJSON = {"ERROR":response};
  responseJSONParse = JSON.stringify(responseJSON);

  console.log(responseJSONParse);
  res.status(500).json(responseJSONParse);
}

function callbackSendResponse(code, response, res) {
  switch (code) {
    case 200:
      response = JSON.parse(JSON.stringify(response));
      if (response.webApp && typeof response.webApp !== "undefined") {
        response = Functions.encryptResponse(response);
      }
      console.log(response);
      res.status(200).json(response);
      break;
    case 500:
      databaseError(res);
      break;
    case 401:
      wrongAuthToken(res);
      break;
    case 4001:
      noAdminAuth(res);
      break;
  }
}

// </editor-fold>
