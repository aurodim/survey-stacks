var functions = {};

functions.AverageResponses = AverageResponses;
functions.AverageDailyResponses = AverageDailyResponses;
functions.DayResponses = DayResponses;
functions.MonthResponses = MonthResponses;
functions.YearResponses = YearResponses;
functions.GraphicQuestions = GraphicQuestions;
functions.RespondentAnalyzer = RespondentAnalyzer;
functions.AverageCurrency = AverageCurrency;
functions.ResponseOrganizer = ResponseOrganizer;
functions.PercentagesPoll = PercentagesPoll;
functions.QuestionsAssort = QuestionsAssort;
functions.ResponsesAssort = ResponsesAssort;
module.exports = functions;

function AverageResponses(responses) {
  var a = 0;
  for (var i = 0; i < responses.length; i++) {
    for (var r = 0; r < responses[i].length; r++) {
      if (responses[i][r] !== "" && responses[i][r] !== null) {
        a+=1;
      }
    }
  }

  var avr = a / responses.length;
  if (typeof avr.toString().split(".")[1] != "undefined") {
    avr = avr.toFixed(2);
  } else {
    avr = avr.toFixed(0);
  }
  return avr
}

function AverageCurrency(currency) {
  var t = 0;
  currency.forEach(function(amount) {
    t += amount;
  });

  return (t / currency.length).toFixed(0);
}

function AverageDailyResponses(dates) {
  if (dates.length == 0) return 0;

  var ud = [];

  for (var i = 0; i < dates.length; i++) {
    if (ud.indexOf(dates[i].getDay()) == -1) {
      ud.push(dates[i].getDay());
    }
  }

  var avr = dates.length / ud.length;
  if (typeof avr.toString().split(".")[1] != "undefined") {
    avr = avr.toFixed(2);
  } else {
    avr = avr.toFixed(0);
  }

  return avr;
}

function DayResponses(dates) {
  var daily = [0,0,0,0,0,0,0];

  dates.forEach(function(date, i) {
    date = new Date(date);
    var dow = date.getDay();
    if (dow === 0) dow = 7;

    daily[dow - 1]+=1;
  });

  return daily;
}

function MonthResponses(dates) {
  var monthly = [0,0,0,0,0,0,0,0,0,0,0,0];

  dates.forEach(function(date, i) {
    date = new Date(date);
    var mow = date.getMonth() + 1;

    monthly[mow-1]+=1;
  });

  return monthly;
}

function YearResponses(dates) {
  var yearly = [];
  var values = [];

  dates.forEach(function(date, i) {
    date = new Date(date);
    var y = date.getFullYear();

    if (!values.includes(y)) {
      values.push(y);
      yearly.push(1);
    } else {
      var d = values.indexOf(y);
      yearly[d]+=1;
    }
  });

  var a = [values, yearly];

  return a;
}

function GraphicQuestions(order, questions, responses) {
  var graphic_questions = [];

  order.forEach(function(q, i) {
    if (q.split("_")[0] == "fr") return true;
    var ioq = q.split("_")[1];
    var type = q.split("_")[0];
    var obj;

    if (type == "br") {
      obj = {
        question : questions[type][ioq].question,
        options : ["False", "True"],
        percentages : ResponsePercentages(i, undefined, responses)
      };
    } else {
      obj = {
        question : questions[type][ioq].question,
        options : questions[type][ioq].responses,
        percentages : ResponsePercentages(i, questions[type][ioq].responses, responses)
      };
    }

    if (obj.question.length > 30) {
      obj.question = obj.question.substr(0,30) + "...";
    }

    graphic_questions.push(obj);
  });

  return graphic_questions;
}

function ResponsePercentages(index, options, responses) {
  var isBR = false;
  var array;
  if (options === undefined) {
    array = [0, 0];
    options = ["false", "true"];
    isBR = true;
  } else {
    array = new Array(options.length);
  }

  responses.forEach(function(response) {
    if (isBR) {
      var ind = options.indexOf(response[index]);
      array[ind] += 1;
    } else {
      if (array[response[index]] === undefined) array[response[index]] = 0;
      array[response[index]] += 1;
    }
  });

  for (var i = 0; i < array.length; i++) {
    if (array[i] === undefined) array[i] = 0;
    array[i] = ((array[i] / responses.length) * 100).toFixed(2);
    if (array[i].split(".")[1] == "00") array[i] = array[i].split(".")[0];
  }

  return array;
}

function RespondentAnalyzer(about) {
  var respondent_analyzer = [];
  for (var about_item in about) {
    var obj = {
      name : about_item,
      options : RespondentPercentages(about[about_item])[0],
      percentages : RespondentPercentages(about[about_item])[1]
    };

    respondent_analyzer.push(obj);
  }

  return respondent_analyzer;
}

function RespondentPercentages(users_info) {
  var values = [];
  var amounts = [];

  users_info.forEach(function(info) {
    if (!values.includes(info)) {
      values.push(info);
      amounts.push(1);
      return true;
    }

    var index = values.indexOf(info);
    amounts[index]+=1;
  });

  for (var i = 0; i < amounts.length; i++) {
    amounts[i] = ((amounts[i] / users_info.length) * 100).toFixed(2);
  }

  var a = [values, amounts];

  return a;
}

function ResponseOrganizer(order, questions, responses) {
  var questions_a = [];
  var responses_a = [];

  order.forEach(function(q, i) {
    var t = q.split("_")[0];
    var ind = q.split("_")[1];

    questions_a.push(questions[t][ind].question);
    responses_a.push([]);

    responses.forEach(function(response) {
      if (Array.isArray(response[i])) {
        var arr = [];
        response[i].forEach(function(restores) {
          arr.push(questions[t][ind].responses[restores]);
        });
        responses_a[i].push(arr);
      } else if (t != "fr" && t != "br") {
        responses_a[i].push(questions[t][ind].responses[response[i]]);
      } else {
        responses_a[i].push(response[i]);
      }
    });
  });

  var  a = [questions_a, responses_a];

  return a;
}

function PercentagesPoll(length, responses) {
  var amount = new Array(length);
  var percentages = new Array(length);

  responses.forEach(function(response) {
    if (amount[response] === undefined) amount[response] = 0;
    amount[response]+=1;
  });

  amount.forEach(function(value, i) {
    if (value === undefined) value = 0;
    var percent = ((value / responses.length) * 100).toFixed(2);
    percentages[i] = percent;
  });

  for (var i = 0; i < amount.length; i++) {
    if (typeof amount[i] == "undefined") {
      amount[i] = i;
      percentages[i] = 0;
    }
  }

  var a = [amount, percentages];

  return a;
}

function QuestionsAssort(order, questions) {
  var questions_a = [];

  order.forEach(function(q, i) {
    var t = q.split("_")[0];
    var ind = q.split("_")[1];

    questions_a.push(questions[t][ind].question);
  });

  return questions_a;
}

function ResponsesAssort(order, questions, response) {
  var responses_a = {};

  order.forEach(function(q, i) {
    var t = q.split("_")[0];
    var ind = q.split("_")[1];

    if (Array.isArray(response[i])) {
      var arr = [];
      response[i].forEach(function(restores) {
        arr.push(questions[t][ind].responses[restores]);
      });
      responses_a[i] = arr;
    } else if (t != "fr" && t != "br") {
      responses_a[i] = questions[t][ind].responses[response[i]];
    } else {
      responses_a[i] = response[i];
    }
  });

  return responses_a;
}
