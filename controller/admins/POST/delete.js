"use strict";

var Admins = modelRequire("admins.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Admin() {
  this.ip = "";
  this.admin_key = "";
  this.fullname = "";
}

var currentAdmin;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, name, token, callback, res) {
  currentAdmin = new Admin();
  token = token.substr(0, token.length - 4);
  currentAdmin.ip = ip;
  currentAdmin.fullname = name;
  currentAdmin.admin_key = Functions.regexEscape(token);

  deleteAdmin(callback, res);
}

function deleteAdmin(callback, res) {
  var regex = new RegExp('^'+currentAdmin.admin_key, '');
  Admins.find({authorization_key : regex, fullname : currentAdmin.fullname}).remove(function(error) {
    if (error) return callback(500, "error", res);

    sendResponse(callback, res);
  });
}

function sendResponse(callback, res) {
  var resultCode = resultCode;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {
    "resultCode" : 1,
    "resultDate" : resultDate
  };

  callback(200, resultArray, res);
}
