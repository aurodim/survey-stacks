"use strict";

var Admins = modelRequire("admins.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Admin() {
  this.admin_key = "";
  this.authKey = "";
  this.name = "";
  this.email = "";
  this.phone = "";
  this.password = "";
}

var currentAdmin;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, admin_key, name, email, phone, password, callback, res) {
  currentAdmin = new Admin();
  currentAdmin.admin_key = admin_key;
  currentAdmin.authKey = Functions.createAdminAuthKey(name, password);
  currentAdmin.name = name;
  currentAdmin.email = email.toLowerCase();
  currentAdmin.phone = Functions.phoneNumberNormalize(phone);
  currentAdmin.password = Functions.hashedPasswordAdminCreator(password);

  Admins.count({authorization_key : currentAdmin.admin_key}, function(error, amount) {
    if (error) return callback(500, "error", res);
    if (amount != "1") return callback(4001, "no auth", res);

    createAdmin(callback, res);
  });
}

function createAdmin(callback, res) {
  Admins.find({$or : [{fullname : currentAdmin.fullname}, {email_address : currentAdmin.email}, {phone_number : currentAdmin.phone}]}).remove(function(error) {
    if (error) return callback(500, "error", res);

    var newAdmin = new Admins({
      authorization_key : currentAdmin.authKey,
      fullname : currentAdmin.name,
      email_address : currentAdmin.email,
      phone_number : currentAdmin.phone,
      password : currentAdmin.password,
      restriction : "junior",
      created_at : new Date(),
      updated_at : new Date()
    });

    newAdmin.save(function(error) {
      if (error) return callback(500, "error", res);

      sendResponse(callback, res);
    });
  })
}

function sendResponse(callback, res) {
  var resultCode = resultCode;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {
    "resultCode" : 1,
    "resultDate" : resultDate,
    "new_admin" : {
      "held" : 0,
      "heldapp" : 0,
      "name" : currentAdmin.name,
      "status" : "junior",
      "key" : (currentAdmin.authKey.substr(0, currentAdmin.authKey.length - 4) + "****")
    }
  };

  callback(200, resultArray, res);
}
