"use strict";

var Admins = modelRequire("admins.js");

function Admin() {
  this.ip = "";
  this.authKey = "";
}

var currentAdmin;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(auth_key, callback) {
  currentAdmin = new Admin();
  currentAdmin.authKey = auth_key;

  verifyAccount(callback);
}

function verifyAccount(callback) {
  Admins.count({authorization_key : currentAdmin.authKey}, function(error, amount) {
    if (error) return callback("d");
    if (amount == "0" || amount != "1") {
      res.clearCookie("aak");
      res.clearCookie("asst");
      res.clearCookie("_csrf");
      res.clearCookie("io");
      return callback("a");
    }

    callback(null);
  });
}
