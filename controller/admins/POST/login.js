"use strict";

var Admins = modelRequire("admins.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Admin() {
  this.ip = "";
  this.authKey = "";
  this.password = "";
  this.hashedPassword = "";
}

var currentAdmin;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, auth_key, password, callback, res) {
  currentAdmin = new Admin();
  currentAdmin.ip = ip;
  currentAdmin.authKey = auth_key;
  currentAdmin.password = password;
  currentAdmin.hashedPassword = Functions.hashedPasswordAdminCreator(password);

  verifyAccount(callback, res);
}

function verifyAccount(callback, res) {
  Admins.count({authorization_key : currentAdmin.authKey, password : currentAdmin.hashedPassword}, function(error, amount) {

    if (error) return callback(500, "error", res);
    if (amount == "0" || amount == null) return callback(4001, "no_auth", res);

    sendResponse(callback, res);
  });
}

function sendResponse(callback, res) {
  var resultCode = resultCode;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {
    "resultCode" : 1,
    "resultDate" : resultDate
  };

  var cookieNameAuth = "aak";
  var cookieNameDuration = "asst";
  var date = new Date();
  var d = new Date(date.getTime() + 43200000);

  res.cookie(cookieNameAuth, currentAdmin.authKey, { maxAge: 43200000, httpOnly: true, signed : true});
  res.cookie(cookieNameDuration, d, { maxAge: 43200000, httpOnly: true, signed : true});


  callback(200, resultArray, res);
}
