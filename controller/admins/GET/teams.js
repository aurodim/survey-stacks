"use strict";

var Teams = modelRequire("teams.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Admin() {
  this.ip = "";
  this.teams = [];
  this.keyword = "";
}

var currentAdmin;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, keyword, callback, res) {
  currentAdmin = new Admin();
  currentAdmin.ip = ip;
  currentAdmin.keyword = Functions.regexEscape(keyword);

  gatherUsers(callback, res);
}

function gatherUsers(callback, res) {
  var regex = new RegExp(currentAdmin.keyword);

  Teams.find({nickname : regex}, function(error, teams) {
    if (error) return callback(500, "error", res);

    for (var i = 0; i < teams.length; i++) {
      var team = teams[i];

      currentAdmin.teams.push({
        nickname : team.nickname,
    		expand : false,
        fullname : team.fullname,
        name : team.name,
        email_phone : team.email_phone,
        since : new Date(team.created_at).toLocaleDateString()
      });
    }

    sendResponse(callback, res);
  });
}

function sendResponse(callback, res) {
  var resultCode = resultCode;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {
    "resultCode" : 1,
    "resultDate" : resultDate,
    "teams" : currentAdmin.teams
  };

  callback(200, resultArray, res);
}
