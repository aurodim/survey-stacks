"use strict";

var Admins = modelRequire("admins.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Admin() {
  this.ip = "";
  this.authKey = "";
  this.name = "";
  this.csrf = "";
  this.restriction = "";
  this.expiration = 0;
  this.st = "";
}

var currentAdmin;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, auth_key, csrfToken, st, callback, res) {
  currentAdmin = new Admin();
  currentAdmin.ip = ip;
  currentAdmin.authKey = auth_key;
  currentAdmin.csrf = csrfToken;
  currentAdmin.st = st;

  verifyAccount(callback, res);
}

function verifyAccount(callback, res) {
  Admins.findOne({authorization_key : currentAdmin.authKey}, function(error, admin) {
    if (error) return callback(500, "error", res);
    if (admin == null || admin == "") {
      res.clearCookie("aak");
      res.clearCookie("asst");
      res.clearCookie("_csrf");
      res.clearCookie("io");
      return callback(4001, "no auth", res);
    }

    var d = new Date();
    d = d.getTime();
    var ed = new Date(currentAdmin.st);
    ed = ed.getTime();
    currentAdmin.name = admin.fullname;
    currentAdmin.restriction = admin.restriction;
    currentAdmin.expiration = Math.floor((ed - d) / 1000);

    sendResponse(callback, res);
  });
}

function sendResponse(callback, res) {
  var resultCode = resultCode;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {
    "resultCode" : 1,
    "resultDate" : resultDate,
    "data" : {
      "name" : currentAdmin.name,
      "csrf" : currentAdmin.csrf,
      "restriction" : currentAdmin.restriction,
      "expiration" : currentAdmin.expiration
    }
  };

  callback(200, resultArray, res);
}
