"use strict";

var Admins = modelRequire("admins.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Admin() {
  this.ip = "";
  this.admins = [];
}

var currentAdmin;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, callback, res) {
  currentAdmin = new Admin();
  currentAdmin.ip = ip;

  gatherAdmins(callback, res);
}

function gatherAdmins(callback, res) {
  Admins.find({}, function(error, admins) {
    if (error) return callback(500, "error", res);

    for (var i = 0; i < admins.length; i++) {
      var admin = admins[i];

      currentAdmin.admins.push({
        held : 0,
        heldapp : 0,
        name : admin.fullname,
        status : admin.restriction,
        key : admin.authorization_key.substr(0, admin.authorization_key.length - 4) + "****"
      });
    }

    sendResponse(callback, res);
  });
}

function sendResponse(callback, res) {
  var resultCode = resultCode;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {
    "resultCode" : 1,
    "resultDate" : resultDate,
    "admins" : currentAdmin.admins
  };

  callback(200, resultArray, res);
}
