"use strict";

var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Admin() {
  this.ip = "";
  this.users = [];
  this.keyword = "";
}

var currentAdmin;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, keyword, callback, res) {
  currentAdmin = new Admin();
  currentAdmin.ip = ip;
  currentAdmin.keyword = Functions.regexEscape(keyword);

  gatherUsers(callback, res);
}

function gatherUsers(callback, res) {
  var regex = new RegExp(currentAdmin.keyword);

  Profiles.find({username : regex}, function(error, users) {
    if (error) return callback(500, "error", res);

    for (var i = 0; i < users.length; i++) {
      var user = users[i];

      currentAdmin.users.push({
        username : user.username,
    		expand : false,
    		fullname : user.fullname,
    		email_phone : user.email_phone,
        stacks : user.stacks,
        coins : user.coins,
        attempts : user.login_attempts,
        since : new Date(user.created_at).toLocaleDateString()
      });
    }

    sendResponse(callback, res);
  });
}

function sendResponse(callback, res) {
  var resultCode = resultCode;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {
    "resultCode" : 1,
    "resultDate" : resultDate,
    "users" : currentAdmin.users
  };

  callback(200, resultArray, res);
}
