var Categories = modelRequire("categories.js");
var Constants = modelRequire("constants.js");
var Notifications = modelRequire("notifications.js");
var Companies = modelRequire("companies.js");
var Functions = rootRequire("/controller/globalFunctions.js");

var service = {};

service.categories = CategoriesFunc;
service.constants = ConstantsFunc;
service.companies = CompaniesFunc;
service.category = CategoryFunc;
service.company = CompanyFunc;
service.notification = NotificationFunc;
service.read = ReadFunc;
module.exports = service;

function CategoriesFunc(lang, callback) {
  Categories.find({}, function(error, categories) {
    if (error) return callback("d", null);

    let categories_v = {};
    categories.forEach(function(category) {
      categories_v[category.category_token] = category.category_name;
    });

    callback(null, categories_v);
  });
}

function CategoryFunc() {
  this.gather = function(callback) {
    var toval = [];
    var categoriesa = [];
    Categories.find({}, function(error, categories) {
      if (error) return callback(true, null, null);

      for (var i = 0; i < categories.length; i++) {
        var category = categories[i];
        if (category.approved == true) {
          categoriesa.push({
            token : category.category_token,
            text : category.category_name
          });
        } else {
          toval.push({
        		text : category.category_name,
            token : category.category_token,
            held : 0,
        		validated : false
        	});
        }
      }

      callback(null, categoriesa, toval);
    });
  };
  this.create = function(by, arr, callback) {
    var categoriesArray = [];
    var catd = [];

    Categories.count({category_name : {$in : arr}}, function(error, amount) {
      if (error) return callback(error.code, error.message, null);
      if (amount != "0") return callback(10001, "DUPLICATE CATEGORY", null);

      arr.forEach(function(name) {
        var token = Functions.createCategoryToken(name);

        let category = new Categories({
          category_token : token,
          category_value : name.toLowerCase().trim().replace(/ /g, "_"),
          category_name : name.trim(),
          added_by : by,
          approved : false,
          created_at : new Date(),
          update_at : new Date()
        });

        categoriesArray.push(category);
        catd.push({
      		text : name,
          token : token,
          held : 0,
      		validated : false
      	})
      });

      Categories.insertMany(categoriesArray, function(error) {
        if (error) return callback(error.code, error.message, null);

        callback(null, null, catd)
      });
    })
  };
  this.approve = function(auth, catkey, callback) {
    Categories.findOne({category_token : catkey}, function(error, category) {
      if (error) return callback(true, null);

      category.approved_by = auth;
      category.approved = true;
      category.update_at = new Date();

      category.save(function(error) {
        if (error) return callback(true, null);

        var n = {
          token : category.category_token,
          text : category.category_name,
          rename : category.category_name
        };

        callback(null, n);
      });
    });
  };
  this.rename = function(catkey, rename, callback) {
    Categories.findOne({category_token : catkey}, function(error, category) {
      if (error) return callback(true);
      if (category == null || category == "") return callback(true);

      category.category_value = rename.toLowerCase().trim().replace(/ /g, "_");
      category.category_name = rename.trim();
      category.update_at = new Date();

      category.save(function(error) {
        if (error) return callback(true);

        callback(null);
      });
    });
  };
  this.delete = function(key, callback) {
    Categories.remove({category_token : key, approved : false}, function(error) {
      if (error) return callback(true);

      callback(false);
    }).limit(1);
  };
}

function CompanyFunc() {
  this.create = function(tt, name, callback) {

    var company = new Companies({
      team_token : tt,
      company_value : name.toLowerCase().replace(/ /g, "_"),
      company_name : name,
      created_at : new Date(),
      updated_at : new Date()
    });

    company.save(function(error) {
      if (!error) return callback(null, null)
    }).catch(function(error) {
      if (error) return callback(error.code, error.message);
    });
  };
  this.delete = function(key, callback) {
    Companies.remove({team_token : key}, function(error) {
      if (error) return callback(500, "ERROR");

      callback(null, null);
    }).limit(1);
  };
}

function ConstantsFunc(callback) {
  Constants.find(function(error, constants) {
    if (error) return callback("d", null);

    var result = {};

    for (var ck in constants[0]) {
      result[ck] = constants[0][ck];
    }

    callback(null, result);
  }).limit(1);
}

function CompaniesFunc(lang, callback) {
  Companies.find({}, function(error, companies) {
    if (error) return callback("d", null);

    let companies_v = {};
    companies.forEach(function(company) {
      companies_v[company.company_token] = company.company_name;
    });

    callback(null, companies_v);
  });
}

function NotificationFunc(lang, sessionKey, callback) {
  Notifications.findOne({reciever : sessionKey}, function(error, notification) {
    if (error) return callback("d", null);
    if (notification == null || notification == "") return callback(null, {});

    var notif = {
      type : notification.type,
      title : notification.title,
      content : notification.content,
      url : notification.url,
      token : notification.notification_token
    };

    callback(null, notif);
  }).sort({"created_at" : -1});
}

function ReadFunc(sessionKey, callback) {
  Notifications.find({reciever : sessionKey, read : false}, function(error, notifications) {
    if (error) return callback("d");

    for(var i = 0; i < notifications.length; i++) {
      var notification = notifications[i];
      notification.read = true;
      notification.save(function(error) {

      });
    }

    global.io.emit(sessionKey, "mar");
    callback(null);
  });
}
