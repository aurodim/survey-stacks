module.exports = {
    registationEmailSubject : "SurveyStacks DevTeam",
    registrationEmailBody : "<h1>Thank You!</h1> <p>We want you to know we vaue you as a member! <br/> Please enjoy this app and answer surveys!</p> <a href='" + process.env.DOMAIN_HTTPS + "/registerError'>Click this link if you did not create an account</a>",
    registrationSMSBody : "Hello! SurveyStacks DevTeam here! Thank you for becoming a member!",
    loginAccountLocked : "WARNING: Your SurveyStacks account has been locked due to a series of unsuccessful logins attempts. Visit " + process.env.DOMAIN_HTTPS + "/accountLocked for help and more information",
    forgotPasswordEmailSubject : "Forgot Password---SurveyStacks DevTeam",
    forgotPasswordEmailBody : function(username, CredentialKey) {
      var fpbody = "<h1>SurveyStacks DevTeam to the Rescue!</h1> <p>Hey, " + username + " we want you to restore your password in the easiest way posible. Therefore, we have sent you a six digit code to help you. <br />Here is your Six-Digit Code: " + CredentialKey +
                    "<br/> Please do not share this key with anyone!!! <br/>If you did not request a code <a href='"+process.env.DOMAIN_HTTPS+"/forgotPasswordHelp'>Click here</a> and we will have our team deal with this issue. If anything goes wrong we will let you know as soon as possible";
      return fpbody;
    },
    forgotPasswordSMSBody : function(username, CredentialKey) {
      var fpbody = "SurveyStacks DevTeam to the Rescue! \n Hey, " + username + " we want you to restore your password in the easiest way posible. \n Here is your Six-Digit Code: " + CredentialKey +
                    "\n Do not share this key with anyone!!! \n If you did not request a code click here: "+process.env.DOMAIN_HTTPS+"/forgotPasswordHelp and our team will deal with this issue. If anything goes wrong we will let you know as soon as possible";
      return fpbody;
    },
    categories : [
      "Beauty",
      "Business",
      "Culinary",
      "Customer Satisfaction",
      "Education",
      "Employee Satisfaction",
      "Event Planning",
      "Free",
      "Human Resources",
      "Marketing",
      "Market Research",
      "Net Promoter",
      "Non-Profit",
    ],
    company_registration_subject : function (company_name) {
      var fpbody = "SurveyStacks DevTeam to the Rescue! \n Hey, " + company_name;
      return fpbody;
    },
    company_registration_body : function(company_name, company_value) {
      var fpbody = "SurveyStacks DevTeam to the Rescue! \n Hey, " + company_name;
      return fpbody;
    },
    company_locked_subject : function(name) {

    },
    company_locked_body : function(name) {

    },
    sampleEmailNotification : "SurveyStacks Notification Trigger",
    sampleEmailNotificationBody : "<h1>Hey!</h1> <p>You turned on 'E-mail Notifications'. To turn them off go to your SurveyStacks settings and uncheck 'E-mail Notifications' <br>We will notify you about things that go on in our app and new updates!</p> <p>Thank you for using SurveyStacks.</p> <p><b>SurveyStacksDevTeam</b></p>",
    sampleSMSNotificationBody : "Hey, SurveyStacksDevTeam here! 'Text Message Notifications' is on! To turn them off go to your SurveyStacks settings and uncheck 'Text Message Notifications'. We will notify you about things that go on in our app and new updates! Thank you for using SurveyStacks.",
    surveyBody : function(edit, receipt) {
      var body = "<h3>All Set!</h3> <p>Your survey is ready to be answered. We have attached the link where you can find your receipt and the link where you can edit this email. If you are unsatisfied with the amount of Stacks in your survey you can purchase more. <br>Feel free to contact us if you have any questions.</p> <a href='"
      + edit + "'>Edit Survey</a> <a href='" + receipt + "'>Survey Receipt</a>";
      return body;
    },
    claimReward : function(name, key) {
      return "Your " + name + " Gift Card is ready for use!\nGift Code: " + key;
    }
}
