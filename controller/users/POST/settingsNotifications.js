"use strict";

var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");

var currentUser = {
  ip : "",
  userKey : "",
  userSessionKey : "",
  userAuthKey : "",
  lang : "",
  email_phone : "",
  webApp : false,
  notificationsInfo : {
    email_message_notifications : "",
    match_preferences_notifications : "",
  }
};

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, user_validation_key, user_auth_key, user_session_key, email_message_notifications, match_preferences_notifications, lang, webApp, callback, res) {
  currentUser.ip = ip;
  currentUser.userKey = user_validation_key;
  currentUser.userSessionKey = user_session_key;
  currentUser.userAuthKey = user_auth_key;
  lang = lang.trim().toLocaleLowerCase();
  try {
    currentUser.lang = rootRequire("/controller/" + lang + ".js");
  } catch(e) {
    currentUser.langAbbr = "en";
    currentUser.lang = rootRequire("/controller/en.js");
  }
  currentUser.webApp = webApp;
  currentUser.notificationsInfo.email_message_notifications = email_message_notifications;
  currentUser.notificationsInfo.match_preferences_notifications = match_preferences_notifications;

  verifyData(callback, res);
}

function verifyData(callback, res) {
  Profiles.count({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, count) {
    if (error) {
      callback(500, "error", res);
    }

    if (count == "1") {
      updateUserData(callback, res);
    } else {
      res.clearCookie("sk");
      res.clearCookie("lk");
      res.clearCookie("vk");
      res.clearCookie("un");
      res.clearCookie("dt");
      res.clearCookie("sd");
      res.clearCookie("dnotif");
      callback(401, "wrongMatch", res);
    }
  });
}

function updateUserData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      callback(500, "error", res);
    }

    var profile = profiles[0];
    currentUser.email_phone = profile.email_phone;

    profile.notification_settings.email_message = currentUser.notificationsInfo.email_message_notifications;
    profile.notification_settings.match_preferences = currentUser.notificationsInfo.match_preferences_notifications;
    profile.updated_at = new Date();

    profile.save(function(error) {
      if (error) {
        callback(500, "error", res);
      } else {
        if (currentUser.notificationsInfo.email_message_notifications && currentUser.email_phone.includes("@")) {
          sendEmailNotification();
        }

        if (currentUser.notificationsInfo.email_message_notifications && !currentUser.email_phone.includes("@")) {
          sendTextMessageNotification();
        }

        sendResponse(callback, res);
      }
    });
  });
}

function sendEmailNotification() {
  var mailOptions = {
    from : "'SurveyRushDevTeam' <no-reply@surveyrush.com>",
    to : currentUser.email_phone,
    subject : currentUser.lang.sampleEmailNotification,
    html : currentUser.lang.sampleEmailNotificationBody
  };

  transporter.sendMail(mailOptions);
}

function sendTextMessageNotification() {
  client.messages.create({
    to : currentUser.email_phone,
    from : process.env.TWILIO_PHONE_NUMBER,
    body : currentUser.lang.sampleSMSNotificationBody
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {}
  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate
  };

  callback(200, resultArray, res);
}
