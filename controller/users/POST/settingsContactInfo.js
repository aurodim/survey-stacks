"use strict";

var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");


function User() {
  this.ip = "";
  this.userKey = "";
  this.userSessionKey = "";
  this.userAuthKey = "";
  this.webApp = false;
  this.contactInfo = {
    email : "",
    phone_number : "",
    website : "",
    facebook : "",
    instagram : ""
  };
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, user_validation_key, user_auth_key, user_session_key, email, phone_number, website, facebook, instagram, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = user_validation_key;
  currentUser.userAuthKey = user_auth_key;
  currentUser.userSessionKey = user_session_key;
  currentUser.webApp = webApp;
  currentUser.contactInfo.email = email;
  currentUser.contactInfo.phone_number = phone_number;
  currentUser.contactInfo.website = website;
  currentUser.contactInfo.facebook = facebook;
  currentUser.contactInfo.instagram = instagram;

  verifyData(callback, res);
}

function verifyData(callback, res) {
  Profiles.findOne({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profile) {
    if (error) return callback(500, "error", res);
    if (profile === "" || profile == null) return callback(401, "wrongMatch", res);

    profile.contact_info.email = currentUser.contactInfo.email;
    profile.contact_info.phone_number = Functions.phoneNumberNormalize(currentUser.contactInfo.phone_number);
    profile.contact_info.website = currentUser.contactInfo.website;
    profile.contact_info.facebook = currentUser.contactInfo.facebook;
    profile.contact_info.instagram = currentUser.contactInfo.instagram;
    profile.updated_at = new Date();

    profile.save(function(error) {
      if (error) {
        callback(500, "error", res);
      } else {
        sendResponse(callback, res);
      }
    });
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {}
  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate
  };

  callback(200, resultArray, res);
}
