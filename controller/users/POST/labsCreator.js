"use strict";

var Profiles = modelRequire("profiles.js");
var Surveys = modelRequire("surveys.js");
var Polls = modelRequire("polls.js");
var sizeOf = require('image-size');
var Functions = rootRequire("/controller/globalFunctions.js");


function User() {
  this.ip = "";
  this.validation_token = "";
  this.auth_token = "";
  this.session_token = "";
  this.q_token = "";
  this.q_stacks = 0;
  this.q_type = "";
  this.image_id = "";
  this.lang = "";
  this.webApp = false;
  this.image;
  this.survey = {};
  this.poll = {};
  this.questionnaire = {};
  this.labs = {};
  this.collected = [];
  this.poll_coins = 5;
}

var currentUser;

var services = {};

services.initialize = initialize;
module.exports = services;

function initialize(ip, validation_token, auth_token, session_token, lang, image, q, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.validation_token = validation_token;
  currentUser.auth_token = auth_token;
  currentUser.session_token = session_token;
  currentUser.lang = lang;
  currentUser.webApp = webApp;
  currentUser.image = image;
  currentUser.questionnaire = JSON.parse(q);
  currentUser.q_type = currentUser.questionnaire.type;
  currentUser.survey = currentUser.questionnaire.survey;
  currentUser.poll = currentUser.questionnaire.poll;
  if (currentUser.q_type === "survey") {
    currentUser.q_token = Functions.createSurveyToken(validation_token, "1");
    currentUser.q_stacks = Functions.determineStacks(currentUser.survey.freeResponseQuestions, currentUser.survey.checkboxResponseQuestions, currentUser.survey.booleanResponseQuestions, currentUser.survey.radioResponseQuestions, currentUser.survey.menuResponseQuestions);
  } else {
    currentUser.q_token = Functions.createPollToken(validation_token, "1");
  }
  var collected = [];
  for (var key in currentUser.questionnaire.details.data_collected) {
    if (currentUser.questionnaire.details.data_collected[key]) {
      collected.push(key);
    }
  }
  currentUser.collected = collected;
  if (currentUser.questionnaire.lab_option == 0) {
    if (currentUser.q_type === "survey") {
      currentUser.labs = {
        type : "quiz",
        show_score : currentUser.questionnaire.method_options.show_score,
        out_of : currentUser.questionnaire.method_options.out_of
      };
    } else {
      var ind = currentUser.poll.options.indexOf(currentUser.questionnaire.method_options.answer);
      if (ind == -1) return callback(500, "error", res);
      currentUser.labs = {
        type : "quiz",
        show_score : currentUser.questionnaire.method_options.show_score,
        answer : ind
      };
    }
  } else if (currentUser.questionnaire.lab_option == 1) {
    currentUser.labs = {
      type : "key_needed",
      unique_keys : currentUser.questionnaire.method_options.unique,
      keys : [Functions.createQAnswerKey()],
    };
  } else if (currentUser.questionnaire.lab_option == 2) {
    currentUser.labs = {
      type : "emoji"
    };
  } else if (currentUser.questionnaire.lab_option == 3) {
    if (currentUser.q_type === "survey") {
      currentUser.labs = {
        type : "game",
        timed : currentUser.questionnaire.method_options.timed,
        max_time : currentUser.questionnaire.method_options.max_time
      };
    } else {
      if (currentUser.questionnaire.method_options.pollQuestionsExtra.length > 0) {
        currentUser.poll_coins = currentUser.poll_coins * currentUser.questionnaire.method_options.pollQuestionsExtra.length;
      }
      currentUser.labs = {
        type : "game",
        timed : currentUser.questionnaire.method_options.timed,
        max_time : currentUser.questionnaire.method_options.max_time,
        gameQuestions : currentUser.questionnaire.method_options.pollQuestionsExtra
      };
    }
  }

  verifyProfile(callback, res);
}

function getCoins() {
  Constants.find(function(error, constants) {
    if (error) {
      currentUser.poll_coins = 5;
    } else {
      var constant = constants[0];
      currentUser.poll_coins = constant.cpp;
    }
  }).limit(1);
}

function verifyProfile(callback, res) {
  Profiles.findOne({deleted : false, user_key : currentUser.validation_token, user_session_key : currentUser.session_token, user_auth_key : currentUser.auth_token, "payment.labs_membership" : true}, function(error, profile) {
    if (error) return callback(500, "error", res);
    if (profile == null || profile == "") return callback(401, "wrongMatch", res);

    if (currentUser.image !== "") {
      // image we give
      uploadImageF(callback, res);
    } else if (currentUser.questionnaire.details.image_url != "") {
      uploadImage(callback, res);
    } else {
      if (currentUser.q_type === "survey") {
        uploadSurvey(callback, res);
      } else {
        uploadPoll(callback, res);
      }
    }
  });
}

function uploadImageF(callback, res) {
  var dimensions = sizeOf(process.cwd()+"/"+currentUser.image.path);
  var height = dimensions.height;
  var width = dimensions.width;
  var folder;
  var tags;
  if (dimensions.height > 1600 || dimensions.width > 1600) {
    height = dimensions.height / 2;
    width = dimensions.width / 2;
  }
  if (currentUser.q_type === "survey") {
    currentUser.image_id = process.env.CLOUDINARY_SURVEYS_FOLDER + "/" + currentUser.q_token + "/survey_image";
    tags = ['user_'+currentUser.validation_token, currentUser.q_token, "surveys", "labs"];
    folder = process.env.CLOUDINARY_SURVEYS_FOLDER;
  } else {
    currentUser.image_id = process.env.CLOUDINARY_POLLS_FOLDER + "/" + currentUser.q_token + "/poll_image";
    tags = ['user_'+currentUser.validation_token, currentUser.q_token, "polls", "labs"];
    folder = process.env.CLOUDINARY_POLLS_FOLDER;
  }
  cloudinary.v2.uploader.upload(
    currentUser.image.path,
    function(error, result) {
      if (typeof error != "undefined" && error != "undefined") return sendResponse(-1, callback, res);

      fs.unlink(process.cwd()+"/"+currentUser.image.path, function(error) {

      });

      currentUser.questionnaire.details.image_url = result.secure_url;

      if (currentUser.q_type === "survey") {
        uploadSurvey(callback, res);
      } else {
        uploadPoll(callback, res);
      }
  },
    {
      public_id: currentUser.image_id,
      width: width,
      height: height,
      folder :  folder,
      tags: tags
    }
  );
}

function uploadImage(callback, res) {
  var folder;
  var tags;
  if (currentUser.q_type === "survey") {
    currentUser.image_id = process.env.CLOUDINARY_SURVEYS_FOLDER + "/" + currentUser.q_token + "/survey_image";
    tags = ['user_'+currentUser.validation_token, currentUser.q_token, "surveys", "labs"];
    folder = process.env.CLOUDINARY_SURVEYS_FOLDER;
  } else {
    currentUser.image_id = process.env.CLOUDINARY_POLLS_FOLDER + "/" + currentUser.q_token + "/poll_image";
    tags = ['user_'+currentUser.validation_token, currentUser.q_token, "polls", "labs"];
    folder = process.env.CLOUDINARY_POLLS_FOLDER;
  }
  cloudinary.v2.uploader.upload(
    currentUser.questionnaire.details.image_url,
    {
      public_id: currentUser.image_id,
      width: 800,
      height: 800,
      folder :  folder,
      tags: tags
    },
    function(error, result) {

      if (typeof error != "undefined" && error != "undefined") return sendResponse(-1, callback, res);

      currentUser.questionnaire.details.image_url = result.secure_url;

      if (currentUser.q_type === "survey") {
        uploadSurvey(callback, res);
      } else {
        uploadPoll(callback, res);
      }
    }
  );
}

function uploadSurvey(callback, res) {
  var userSurvey = currentUser.survey;
  var Survey = new Surveys({
    survey_token : currentUser.q_token,
    creator_key : currentUser.validation_token,
    survey_details : {
      public_survey : currentUser.questionnaire.details.public,
      lang : currentUser.lang,
      stacks : currentUser.q_stacks,
      name : currentUser.questionnaire.details.name,
      description : currentUser.questionnaire.details.description,
      category_token : currentUser.questionnaire.details.category_token,
      target_sex : currentUser.questionnaire.details.sex,
      target_ethnicity : currentUser.questionnaire.details.ethnicity,
      target_age_group : currentUser.questionnaire.details.age_group,
      image_url : currentUser.questionnaire.details.image_url,
      answer_url : Functions.answerURL(currentUser.q_token, 0),
      collected : currentUser.collected,
      lab_created : true
    },
    labs : currentUser.labs,
    question_details : {
      order : userSurvey.order,
      amount : userSurvey.order.length,
      free_response : userSurvey.freeResponseQuestions,
      checkbox : userSurvey.checkboxResponseQuestions,
      boolean : userSurvey.booleanResponseQuestions,
      radio : userSurvey.radioResponseQuestions,
      menu : userSurvey.menuResponseQuestions
    },
    payment_details : {
      free : false,
      survey_charge : "0.00",
      order_total : "0.00",
      receipt_token : Functions.receiptToken()
    },
    created_at : new Date(),
    updated_at : new Date()
  });

  Survey.save(function(error) {
    if (error) return callback(500, "error", res);

    updateProfile(callback, res);
  });
}

function uploadPoll(callback, res) {
  var userPoll = currentUser.poll;
  var Poll = new Polls({
    poll_token : currentUser.q_token,
    creator_key : currentUser.validation_token,
    poll_details : {
      public_poll : currentUser.questionnaire.details.public,
      lang : currentUser.lang,
      coins : currentUser.poll_coins,
      name : currentUser.questionnaire.details.name,
      description : currentUser.questionnaire.details.description,
      category_token : currentUser.questionnaire.details.category_token,
      target_sex : currentUser.questionnaire.details.sex,
      target_ethnicity : currentUser.questionnaire.details.ethnicity,
      target_age_group : currentUser.questionnaire.details.age_group,
      image_url : currentUser.questionnaire.details.image_url,
      answer_url : Functions.answerURL(currentUser.q_token, 1),
      collected : currentUser.collected,
      lab_created : true
    },
    labs : currentUser.labs,
    question_details : {
      question : userPoll.question,
      options : userPoll.options
    },
    created_at : new Date(),
    updated_at : new Date()
  });

  Poll.save(function(error) {
    if (error) return callback(500, "error", res);

    updateProfile(callback, res);
  });
}

function updateProfile(callback, res) {
  Profiles.findOne({deleted : false, user_key : currentUser.validation_token, user_session_key : currentUser.session_token, user_auth_key : currentUser.auth_token}, function(error, profile) {
    if (error) return callback(500, "error", res);
    if (profile == null || profile == "") return callback(401, "no auth", res);

    if (currentUser.q_type === "survey") {
      profile.surveys_created.push(currentUser.q_token);
    } else {
      profile.polls_created.push(currentUser.q_token);
    }
    profile.updated_at = new Date();

    profile.save(function(error) {
      if (error) return callback(500, "error", res);

      sendResponse(1, callback, res);
    });
  });
}

function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {"resultCode" : resultCode, "resultDate" : resultDate};

  callback(200, resultArray, res);
}
