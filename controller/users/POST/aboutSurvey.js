"use strict";

var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");
var Constants = modelRequire("constants.js");

var currentUser = {
  ip : "",
  user_validation_key : "",
  user_auth_key : "",
  user_session_key : "",
  about_completed : false,
  stacks : 0,
  userAnswers : {
    sex : "",
    ethnicity : [],
    lang : [],
    education : "",
    relationship : "",
    employment : "",
    religion : ""
  },
  webApp : false
};

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, user_validation_key, user_auth_key, user_session_key, about_completed, userAnswers, webApp, callback, res) {
  currentUser.ip = ip;
  user_validation_key = user_validation_key.trim();
  user_auth_key = user_auth_key.trim();
  user_session_key = user_session_key.trim();

  currentUser.user_validation_key = user_validation_key;
  currentUser.user_auth_key =  user_auth_key;
  currentUser.user_session_key = user_session_key;
  currentUser.about_completed = about_completed;
  currentUser.userAnswers.sex = userAnswers.sex;
  currentUser.userAnswers.ethnicity = userAnswers.ethnicity;
  currentUser.userAnswers.lang = userAnswers.lang;
  currentUser.userAnswers.education = userAnswers.education;
  currentUser.userAnswers.relationship = userAnswers.relationship;
  currentUser.userAnswers.employment = userAnswers.employment;
  currentUser.userAnswers.religion = userAnswers.religion;
  currentUser.webApp = webApp;

  stackExchange();
  verifyInsertData(callback, res);
}

function stackExchange() {
  Constants.find(function(error, constants) {
    if (error) {
      currentUser.stacks = 10;
    } else {
      var constant = constants[0];
      currentUser.stacks = constant.stacks_xrt * 10;
    }
  }).limit(1);
}

function verifyInsertData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.user_validation_key, user_session_key : currentUser.user_session_key, user_auth_key : currentUser.user_auth_key}, function(error, profiles) {
    if (error) {
      callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];

        if (!profile.setup_completed.about) {
          profile.stacks += 10;
          profile.achievements.lifetime_stacks += 10;
        }

        profile.setup_completed.about = true;
        profile.about.sex = currentUser.userAnswers.sex;
        profile.about.ethnicity = currentUser.userAnswers.ethnicity;
        profile.about.languages = currentUser.userAnswers.lang;
        profile.about.education = currentUser.userAnswers.education;
        profile.about.relationship_status = currentUser.userAnswers.relationship;
        profile.about.employment_status = currentUser.userAnswers.employment;
        profile.about.religion = currentUser.userAnswers.religion;

        profile.save(function(error) {
          if (error) {
            callback(500, "error", res);
          } else {
            if (profile.setup_completed.preferences && profile.setup_completed.about && currentUser.webApp) {
              res.cookie("sd", true);
            }
            sendResponse(callback, res);
          }
        });
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {}

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate
  };

  callback(200, resultArray, res);
}
