"use strict";

var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.validation_key = "";
  this.session_key = "";
  this.auth_key = "";
  this.webApp = false;
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.validation_key = validation_key;
  currentUser.auth_key = auth_key;
  currentUser.session_key = session_key;
  currentUser.webApp = webApp;

  verifyData(callback, res);
}

function verifyData(callback, res) {
  Profiles.findOne({deleted : false, user_key : currentUser.validation_key, user_session_key : currentUser.session_key, user_auth_key : currentUser.auth_key}, function(error, profile) {
    if (error) return callback(500, "error", res);
    if (profile == null || profile == "") return callback(401, "wrongMatch", res);

    profile.deleted = true;
    profile.updated_at = new Date();

    profile.save(function(error) {
      if (error) return callback(500, "error", res);

      res.clearCookie("sk");
      res.clearCookie("lk");
      res.clearCookie("vk");
      res.clearCookie("un");
      res.clearCookie("dt");
      res.clearCookie("sd");
      res.clearCookie("dnotif");
      res.clearCookie("lang");
      res.clearCookie("_csrf");
      res.clearCookie("io");

      sendResponse(1, callback, res);
    });
  });
}

function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {"resultCode" : resultCode, "resultDate" : resultDate, "webApp" : currentUser.webApp};

  callback(200, resultArray, res);

  global.io.emit(currentUser.session_key, "es");
}
