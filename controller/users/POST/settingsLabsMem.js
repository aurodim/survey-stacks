"use strict";

var Profiles = modelRequire("profiles.js");
var Constants = modelRequire("constants.js");
var Functions = rootRequire("/controller/globalFunctions.js");


function User() {
  this.ip = "";
  this.userKey = "";
  this.userSessionKey = "";
  this.userAuthKey = "";
  this.webApp = false;
  this.labs_membership = false;
  this.labs_expire = 0;
  this.labs_price = 2.00;
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, user_validation_key, user_auth_key, user_session_key, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = user_validation_key;
  currentUser.userAuthKey = user_auth_key;
  currentUser.userSessionKey = user_session_key;
  currentUser.webApp = webApp;

  verifyData(callback, res);
}

function verifyData(callback, res) {
  Profiles.findOne({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profile) {
    if (error) return callback(500, "error", res);
    if (profile === "" || profile == null) return callback(401, "wrongMatch", res);

    if (profile.payment.labs_membership) {
      profile.payment.labs_membership = false;
      profile.payment.labs_expire = "";
      currentUser.labs_membership = false;
      currentUser.labs_expire = "";
    } else {
      // charge mem
      profile.payment.labs_membership = true;
      profile.payment.labs_expire = membershipExpire(1);
      currentUser.labs_membership = true;
      currentUser.labs_expire = membershipExpire(1).getTime();
    }


    profile.save(function(error) {
      if (error) return callback(500, "error", res);

      if (!currentUser.labs_membership) {
        return getPrice(callback, res);
      }

      sendResponse(1, callback, res);
    });
  });
}

function membershipExpire(months) {
  var date = new Date();
  var d = date.getDate();
  date.setMonth(date.getMonth() + (+months));
  if (date.getDate() != d) {
    date.setDate(0);
  }

  return date;
}

function getPrice(callback, res) {
  Constants.findOne({}, function(error, constants) {
    if (error) return;

    currentUser.labs_price = constants.labs_mem;

    sendResponse(1, callback, res);
  });
}


function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {}
  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "labs_membership" : currentUser.labs_membership,
    "labs_expire" : currentUser.labs_expire,
    "labs_price" : currentUser.labs_price
  };

  callback(200, resultArray, res);
}
