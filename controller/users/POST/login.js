"use strict";

var Profiles = modelRequire("profiles.js");
var FailedLogins = modelRequire("failedLogins.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.lang = "";
  this.langAbbr = "";
  this.userSessionKey = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.username = "";
  this.password = "";
  this.hashedPassword = "";
  this.setup_completed = false;
  this.webApp = false;
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, lang, username, password, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  lang = lang.trim();
  username = username.trim();
  password = password.trim();
  try {
    currentUser.lang = rootRequire("/controller/" + lang.toLowerCase() + ".js");
  } catch(e) {
    currentUser.lang = rootRequire("/controller/en.js");
  }
  currentUser.langAbbr = lang;
  currentUser.userKey = Functions.createUserKey(username);
  currentUser.username = username.toLowerCase();
  currentUser.password = password;
  currentUser.hashedPassword = Functions.hashedPasswordCreator(password);
  currentUser.webApp = webApp;

  verifyAccount(callback, res);
}

function verifyAccount(callback, res) {
  Profiles.findOne({deleted : false, user_key : currentUser.userKey, username : currentUser.username}, function(error, profile) {
    if (error) return callback(500, "error", res);
    if (profile == null || profile == "") return sendResponse(-1, callback, res);

    if (currentUser.hashedPassword != profile.password) return insertFailedLogin(profile, callback, res);

    if (profile.login_attempts > 4) return sendResponse(-3, callback, res);

    profile.login_attempts = 0;
    profile.updated_at = new Date();
    currentUser.userAuthKey = profile.user_auth_key;
    currentUser.userSessionKey = profile.user_session_key;
    currentUser.setup_completed = (profile.setup_completed.about && profile.setup_completed.preferences);

    profile.save(function(error) {
      if (error) return callback(500, "error", res);

      sendResponse(1, callback, res);
    });
  });
}

function insertFailedLogin(profile, callback, res) {
  var newFailedLogin = new FailedLogins({
    key : currentUser.userKey,
    username : currentUser.username,
    ip_address : currentUser.ip,
    created_at : new Date(),
  });

  newFailedLogin.save(function(error) {
    if (error) return callback(500, "error", res);

    profile.login_attempts += 1;
    profile.save(function(error) {
      if (error) return callback(500, "error", res);

      sendResponse(-2, callback, res);
    });
  });
}


function sendResponse(resultCode, callback, res) {
  var resultCode = resultCode;
  var resultArray = {}
  var resultDate = new Date();

  if (resultCode == 1) {

      resultArray = {
        "webApp" : currentUser.webApp,
        "resultCode" : resultCode,
        "resultDate" : resultDate,
        "profile_auth_data" : {
          "un" : currentUser.username,
          "vk" : currentUser.userKey,
          "sk" : currentUser.userSessionKey,
          "lk" : currentUser.userAuthKey,
          "sd" : currentUser.setup_completed
        }
      };

      var cookieNameAuth = "lk";
      var cookieNameSession = "sk";
      var cookieNameValidation = "vk";
      var cookieNameusername = "un";
      var cookieNameDate = "dt";
      var cookieNameSetupComplete = "sd";
      var cookieNameDesktopNotifications = "dnotif";
      var cookieNameLang = "lang";
      var date = new Date();

      if (currentUser.webApp) {
        res.cookie(cookieNameSession, currentUser.userSessionKey, { maxAge: (86400000 * 365.25), httpOnly: true, signed : true});  // secure : true, domain : 'http://www.surveystacks.com'
        res.cookie(cookieNameAuth, currentUser.userAuthKey, { maxAge: (86400000 * 365.25), httpOnly: true, signed : true});
        res.cookie(cookieNameValidation, currentUser.userKey, { maxAge: (86400000 * 365.25), httpOnly: true, signed : true});
        res.cookie(cookieNameusername, currentUser.username, { maxAge: (86400000 * 365.25), httpOnly: false});
        res.cookie(cookieNameLang, currentUser.langAbbr, { maxAge: (86400000 * 365.25), httpOnly: false});
        res.cookie(cookieNameDate, date.toString(), { maxAge: (86400000 * 365.25), httpOnly: true, signed : true});
        res.cookie(cookieNameSetupComplete, currentUser.setup_completed, { maxAge: (86400000 * 365.25), httpOnly: false});
        res.cookie(cookieNameDesktopNotifications, false, { maxAge: (86400000 * 365.25), httpOnly: false});
      }

      callback(200, resultArray, res);
  } else {
    resultArray = {
      "webApp" : currentUser.webApp,
      "resultCode" : resultCode,
      "resultDate" : resultDate,
      "profile_auth_data" : {
        "un" : "",
        "vk" : "",
        "sk" : "",
        "lk" : "",
        "sd" : ""
      }
    };

    callback(200, resultArray, res);
  }
}
