"use strict";

var Profiles = modelRequire("profiles.js");
var Polls = modelRequire("polls.js");
var Surveys = modelRequire("surveys.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.webApp = false;
  this.token = "";
  this.type = "";
  this.bookmarked = false;
  this.profile = {};
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, type, token, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.token = token;
  currentUser.type = type;
  currentUser.webApp = webApp;

  verifyGatherData(callback, res);
}

function verifyGatherData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];


        currentUser.profile = profile;

        if (currentUser.type == "survey") {
          verifySurvey(callback, res);
        } else {
          verifyPoll(callback, res);
        }
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function verifySurvey(callback, res) {
  Surveys.count({survey_token : currentUser.token}, function(error, count) {
    if (error) return callback(500, "error", res);
    if (count != 1) return callback(500, "error", res);

    currentUser.bookmarked = !currentUser.profile.bookmarks.includes(currentUser.token);
    if (currentUser.profile.bookmarks.includes(currentUser.token)) {
      var i = currentUser.profile.bookmarks.indexOf(currentUser.token);
      currentUser.profile.bookmarks.splice(i, 1);
    } else {
      currentUser.profile.bookmarks.push(currentUser.token);
    }
    currentUser.profile.updated_at = new Date();
    currentUser.profile.save(function(error) {
      if (error) return callback(500, "error", res);

      sendResponse(callback, res);
    });
  });
}

function verifyPoll(callback, res) {
  Polls.count({poll_token : currentUser.token}, function(error, count) {
    if (error) return callback(500, "error", res);
    if (count != 1) return callback(500, "error", res);

    currentUser.bookmarked = !currentUser.profile.bookmarks_polls.includes(currentUser.token);
    if (currentUser.profile.bookmarks_polls.includes(currentUser.token)) {
      var i = currentUser.profile.bookmarks_polls.indexOf(currentUser.token);
      currentUser.profile.bookmarks_polls.splice(i, 1);
    } else {
      currentUser.profile.bookmarks_polls.push(currentUser.token);
    }
    currentUser.profile.updated_at = new Date();
    currentUser.profile.save(function(error) {
      if (error) return callback(500, "error", res);

      sendResponse(callback, res);
    });
  });
}

function sendResponse(callback, res) {
  var resultArray = {}
  var resultCode = 1;
  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "bookmarked" : currentUser.bookmarked
  };

  callback(200, resultArray, res);
}
