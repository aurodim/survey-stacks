"use strict";

var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");

var currentUser = {
  ip : "",
  userKey : "",
  userAuthKey : "",
  userSessionKey : "",
  webApp : false,
  ep : "",
};

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, user_validation_key, auth_key, session_key, ep, webApp, callback, res) {
  currentUser.ip = ip;
  currentUser.userKey = user_validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.webApp = webApp;
  currentUser.ep = ep;

  updateUserData(callback, res);
}

function updateUserData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    }

    if (profiles.length != "1") return callback(401, "error", res);

    var profile = profiles[0];
    if (currentUser.ep.includes("@")) {
      profile.email_phone = currentUser.ep.toLocaleLowerCase();
    } else {
      profile.email_phone = Functions.phoneNumberNormalize(currentUser.ep);
    }
    profile.updated_at = new Date();

    profile.save(function(error) {
      if (error) {
        callback(500, "error", res);
      } else {
        sendResponse(callback, res);
      }
    });
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {}
  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate
  };

  callback(200, resultArray, res);
}
