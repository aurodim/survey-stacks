"use strict";

var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");

var currentUser = {
  ip : "",
  user_validation_key : "",
  user_auth_key : "",
  user_session_key : "",
  gender : "",
  language : "",
  categories : {},
  companies : {},
  webApp : false
};

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, user_validation_key, user_auth_key, user_session_key, gender, language, categories, companies, webApp, callback, res) {
  currentUser.ip = ip;
  user_validation_key = user_validation_key.trim();
  user_auth_key = user_auth_key.trim();
  user_session_key = user_session_key.trim();

  currentUser.user_validation_key = user_validation_key;
  currentUser.user_auth_key =  user_auth_key;
  currentUser.user_session_key = user_session_key;
  currentUser.gender = gender;
  currentUser.language = language;
  currentUser.categories = categories;
  currentUser.companies = companies;
  currentUser.webApp = webApp;

  verifyInsertData(callback, res);
}

function verifyInsertData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.user_validation_key, user_session_key : currentUser.user_session_key, user_auth_key : currentUser.user_auth_key}, function(error, profiles) {
    if (error) {
      callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];
        profile.setup_completed.preferences = true;
        profile.preferences.gender = currentUser.gender;
        profile.preferences.language = currentUser.language;
        profile.preferences.categories_tokens = currentUser.categories.tokens;
        profile.preferences.categories = currentUser.categories.values;
        profile.preferences.companies_tokens = currentUser.companies.tokens;
        profile.preferences.companies = currentUser.companies.values;

        profile.save(function(error) {
          if (error) {
            callback(500, "error", res);
          } else {
            if (profile.setup_completed.preferences && profile.setup_completed.address && webApp) {
              res.cookie("sd", true);
            }
            res.cookie("lang", currentUser.language);
            sendResponse(callback, res);
          }
        })
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate
  };

  callback(200, resultArray, res);
}
