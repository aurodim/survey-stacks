"use strict";

var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");
var ForgotCredentialKeys = modelRequire("forgotCredential.js");

function User() {
  this.ip = "";
  this.lang = "";
  this.user_key = "";
  this.username = "";
  this.email_phone = "";
  this.dob = "";
  this.second_left = "0";
  this.webApp = false;
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, lang, username, email_phone, dob, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  try {
    currentUser.lang = rootRequire("/controller/" + lang.toLowerCase() + ".js");
  } catch(e) {
    currentUser.lang = rootRequire("/controller/en.js");
  }
  currentUser.user_key = Functions.createUserKey(username);
  currentUser.username = username;
  if (email_phone.includes("@")) {
    currentUser.email_phone = email_phone.toLowerCase();
  } else {
    currentUser.email_phone = Functions.phoneNumberNormalize(email_phone);
  }
  currentUser.dob = dob;
  currentUser.webApp = webApp;

  verifyData(callback, res);
}

function verifyData(callback, res) {
  Profiles.findOne({deleted : false, user_key : currentUser.user_key, username : currentUser.username, email_phone : currentUser.email_phone, "about.birth_date" : currentUser.dob}, function(error, profile) {
    if (error) return callback(500, "error", res);
    if (profile == null || profile == "") return sendResponse(-1, callback, res);

    checkForSpam(callback, res);
  });
}

function checkForSpam(callback, res) {
  ForgotCredentialKeys.findOne({key : currentUser.user_key, username : currentUser.username, used : false, expired : false}, function(error, credentialKey) {
    if (error) return callback(500, "error", res);
    if (credentialKey == null || credentialKey == "") return insertCredentialKey(callback, res);

    var current_time = new Date();
    var created_time = new Date(credentialKey.created_at);
    var difference = Math.round((current_time - created_time) / 1000);

    if (difference >= 60) {
      credentialKey.expired = true;
      credentialKey.save(function(error, data) {
        if (error) {
        } else {
          currentUser.second_left = "0";
          insertCredentialKey(callback, res);
        }
      });
    } else if (difference < 60) {
      currentUser.second_left = 60 - difference;
      sendResponse(-2, callback, res);
    }
  }).sort({created_at : -1});
}

function insertCredentialKey(callback, res) {
  var CredentialKey = Functions.generateCredentialKey();

  var newForgotCredentialKey = new ForgotCredentialKeys({
    validation_key : CredentialKey,
    key : currentUser.user_key,
    username : currentUser.username,
    used : false,
    expired : false,
    created_at : new Date(),
    updated_at : new Date()
  });

  newForgotCredentialKey.save(function(error, result) {
    if (error) {
      callback("error", res);
    } else {
      if (currentUser.email_phone.includes("@")) {
        sendEmail(CredentialKey, callback, res);
      } else {
        sendSMS(CredentialKey, callback, res);
      }
    }
  });
}

function sendEmail(CredentialKey, callback, res) {
  var mailOptions = {
    from : "'SurveyStacksDevTeam' <no-reply@surveyrush.com>",
    to : currentUser.email_phone,
    subject : currentUser.lang.forgotPasswordEmailSubject,
    html : currentUser.lang.forgotPasswordEmailBody(currentUser.username, CredentialKey)
  };

  transporter.sendMail(mailOptions);
  sendResponse(1, callback, res);
}

function sendSMS(CredentialKey, callback, res) {
  client.messages.create({
    to : currentUser.email_phone,
    from : process.env.TWILIO_PHONE_NUMBER,
    body : currentUser.lang.forgotPasswordSMSBody(currentUser.username, CredentialKey)
  }, function(error) {
    if (error) return callback(500, "error", res);

    sendResponse(1, callback, res);
  });
}

function sendResponse(resultCode, callback, res) {
  var resultCode = resultCode;
  var resultArray = {}
  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "time_to_wait" : currentUser.second_left
  };

  callback(200, resultArray, res);
}
