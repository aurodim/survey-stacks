"use strict";

var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");
var ForgotCredentialKeys = modelRequire("forgotCredential.js");

function User() {
  this.ip = "";
  this.lang = "";
  this.user_key = "";
  this.username = "";
  this.code = "";
  this.new_password = "";
}

var currentUser;
var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, lang, username, code, new_password, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  try {
    currentUser.lang = rootRequire("/controller/" + lang.toLowerCase() + ".js");
  } catch(e) {
    currentUser.lang = rootRequire("/controller/en.js");
  }
  currentUser.user_key = Functions.createUserKey(username);
  currentUser.username = username;
  currentUser.code = code;
  currentUser.new_password = Functions.hashedPasswordCreator(new_password);

  console.log(currentUser);

  verifyData(callback, res);
}

function verifyData(callback, res) {
  Profiles.findOne({deleted : false, user_key : currentUser.user_key, username : currentUser.username}, function(error, profile) {
    if (error) return callback(500, "error", res);
    if (profile == null || profile == "") return sendResponse(-1, callback, res);

    ForgotCredentialKeys.findOne({validation_key : currentUser.code, key : currentUser.user_key, username : currentUser.username, used : false, expired : false}, function(error, key) {
      if (error) return callback(500, "error", res);
      if (key == null || key == "") return sendResponse(-2, callback, res);

      profile.password = currentUser.new_password;
      profile.updated_at = new Date();

      key.used = true;
      key.expired = true;
      key.updated_at = new Date();

      key.save(function(error) {
        if (error) return callback(500, "error", res);

        profile.save(function(error) {
          if (error) return callback(500, "error", res);

          sendResponse(1, callback, res);
        });
      });

    }).sort({created_at : -1});
  });
}

function sendResponse(resultCode, callback, res) {
  var resultCode = resultCode;
  var resultArray = {}

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate
  };

  callback(200, resultArray, res);
}
