"use strict";

var Profiles = modelRequire("profiles.js");
var Surveys = modelRequire("surveys.js");
var Constants = modelRequire("constants.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.user_validation_token = "";
  this.user_auth_token = "";
  this.user_session_token = "";
  this.survey_token = "";
  this.survey_paid = false;
  this.survey_stacks = 0;
  this.survey_total = 0.00;
  this.image_id = "";
  this.lang = "";
  this.email = "";
  this.webApp = false;
  this.image;
  this.collected = [];
  this.survey = {};
};

var currentUser;

var services = {};

services.initialize = initialize;
module.exports = services;

function initialize(ip, user_validation_token, user_auth_token, user_session_token, lang, image, survey, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.user_validation_token = user_validation_token;
  currentUser.user_auth_token = user_auth_token;
  currentUser.user_session_token = user_session_token;
  currentUser.lang = lang;
  currentUser.webApp = webApp;
  currentUser.image = image;
  currentUser.survey = survey;
  currentUser.survey_token = Functions.createSurveyToken(user_validation_token, "1");

  currentUser.survey_paid = !survey.paymentDetails.freeSurvey;
  if (!survey.paymentDetails.freeSurvey) {
    currentUser.survey_stacks = Functions.determineSips(survey.freeResponseQuestions, survey.checkboxResponseQuestions, survey.booleanResponseQuestions, survey.radioResponseQuestions, survey.menuResponseQuestions);
  } else {
    currentUser.survey_stacks = 1;
  }

  if (!survey.paymentDetails.freeSurvey) {
    getOrderTotal();
  }
  var collected = [];
  for (var key in currentUser.survey.details.data_collected) {
    if (currentUser.survey.details.data_collected[key]) {
      collected.push(key);
    }
  }
  currentUser.collected = collected;
  verifyProfile(callback, res);
}

function getOrderTotal() {
  Constants.find(function(error, constants) {
    if (error) {
      currentUser.survey_total = 1.00;
    } else {
      var constant = constants[0];
      currentUser.survey_total = (constant.one_survey).toFixed(2);
    }
  }).limit(1);
}

function verifyProfile(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.user_validation_token, user_session_key : currentUser.user_session_token, user_auth_key : currentUser.user_auth_token}, function(error, profiles) {
    if (error) return callback(500, "error", res);

    if (profiles.length == 0) return callback(401, "wrongMatch", res);

    var profile = profiles[0];
    currentUser.email = profile.email;
    if (!profile.setup_completed.payment && currentUser.survey.paymentDetails.cardSaved) {
      sendResponse(-1, callback, res);
      return;
    }

    // verify card -> createSurvey -> charge

    if (currentUser.image !== "") {
      // image we give
      uploadImageF(callback, res);
    } else if (currentUser.survey.surveyDetails.image_url != "") {
      uploadImage(callback, res);
    } else {
      uploadSurvey(callback, res);
    }
  }).limit(1);
}

function uploadImageF(callback, res) {
  var dimensions = sizeOf(process.cwd()+"/"+currentUser.image.path);
  var height = dimensions.height;
  var width = dimensions.width;
  if (dimensions.height > 1600 || dimensions.width > 1600) {
    height = dimensions.height / 2;
    width = dimensions.width / 2;
  }
  currentUser.image_id = process.env.CLOUDINARY_SURVEYS_FOLDER + "/" + currentUser.survey_token + "/survey_image";
  cloudinary.v2.uploader.upload(
    currentUser.image.path,
    function(error, result) {
      if (typeof error != "undefined" && error != "undefined") return sendResponse(-1, callback, res);

      fs.unlink(process.cwd()+"/"+currentUser.image.path, function(error) {

      });

      currentUser.survey.surveyDetails.image_url = result.secure_url;

      uploadSurvey(callback, res);
    },
    {
      public_id: currentUser.image_id,
      width: width,
      height: height,
      folder :  process.env.CLOUDINARY_SURVEYS_FOLDER,
      tags: ['user_'+currentUser.validation_token, currentUser.survey_token, "surveys"]
    }
  );
}

function uploadImage(callback, res) {
  currentUser.image_id = process.env.CLOUDINARY_SURVEYS_FOLDER + "/" + currentUser.survey_token + "/survey_image";
  cloudinary.v2.uploader.upload(
    currentUser.survey.surveyDetails.image_url,
    {
      public_id: currentUser.image_id,
      width: 800,
      height: 800,
      folder :  process.env.CLOUDINARY_SURVEYS_FOLDER,
      tags: ['user_'+currentUser.user_validation_token, currentUser.survey_token, "surveys"]
    },
    function(error, result) {

      if (typeof error != "undefined" && error != "undefined") return sendResponse(-2, callback, res);

      currentUser.survey.surveyDetails.image_url = result.secure_url;
      uploadSurvey(callback, res);
    }
  );
}

function uploadSurvey(callback, res) {
  var userSurvey = currentUser.survey;
  var Survey = new Surveys({
    survey_token : currentUser.survey_token,
    creator_key : currentUser.user_validation_token,
    survey_details : {
      public_survey : userSurvey.surveyDetails.public,
      lang : currentUser.lang,
      stacks : currentUser.survey_stacks,
      name : userSurvey.surveyDetails.name,
      description : userSurvey.surveyDetails.description,
      category_token : userSurvey.surveyDetails.category_token,
      target_sex : userSurvey.surveyDetails.sex,
      target_ethnicity : userSurvey.surveyDetails.ethnicity,
      target_age_group : userSurvey.surveyDetails.age_group,
      image_url : currentUser.survey.surveyDetails.image_url,
      answer_url : Functions.answerURL(currentUser.survey_token, 0),
      lab_created : false,
      collected : currentTeam.collected
    },
    question_details : {
      order : userSurvey.order,
      amount : userSurvey.order.length,
      free_response : userSurvey.freeResponseQuestions,
      checkbox : userSurvey.checkboxResponseQuestions,
      boolean : userSurvey.booleanResponseQuestions,
      radio : userSurvey.radioResponseQuestions,
      menu : userSurvey.menuResponseQuestions
    },
    payment_details : {
      free : userSurvey.paymentDetails.freeSurvey,
      paid : !userSurvey.paymentDetails.freeSurvey,
      analytics : !userSurvey.paymentDetails.freeSurvey,
      survey_charge : currentUser.survey_total,
      order_total : currentUser.survey_total,
      receipt_token : Functions.receiptToken()
    },
    created_at : new Date(),
    updated_at : new Date()
  });

  Survey.save(function(error) {
    if (error) return callback(500, "error", res);

    updateProfile(callback, res);
  });
}

function updateProfile(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.user_validation_token, user_session_key : currentUser.user_session_token, user_auth_key : currentUser.user_auth_token}, function(error, profiles) {
    if (error) return callback(500, "error", res);

    var profile = profiles[0];
    profile.surveys_created.push(currentUser.survey_token);
    profile.updated_at = new Date();

    profile.save(function(error) {
      if (error) return callback(500, "error", res);

      if (!currentUser.survey.paymentDetails.freeSurvey) {
        return chargeSurvey(callback, res);
      }

      sendResponse(1, callback, res);
    });
  }).limit(1);
}

function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {}
  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate
  };

  callback(200, resultArray, res);
}

function chargeSurvey(callback, res) {
  // notify();
}

function notify(callback, res) {
  var langFile;
  try {
    langFile = require("./" + currentUser.lang.toLowerCase() + ".js");
  } catch(e) {
    langFile = require("./en.js");
  }

  var mailOptions = {
    from : "'SurveyStacksDevTeam' <no-reply@surveystacks.com>",
    to : currentUser.email_address,
    subject : langFile.registationEmailSubject,
    html : langFile.surveyBody(Function.editURL(currentUser.survey_token, 0), Functions.receiptURL(currentUser.survey_token), 0)
  };

  transporter.sendMail(mailOptions);

  sendResponse(1, callback, res);
}
