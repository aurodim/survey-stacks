"use strict";

var Profiles = modelRequire("profiles.js");
var Rewards = modelRequire("rewards.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.webApp = false;
  this.token = "";
  this.wishlisted = false;
  this.wishlistObj = {
    token : "",
    title : "",
    progress : 0,
    wishlisted : false
  };
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, token, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.token = token;
  currentUser.webApp = webApp;

  verifyGatherData(callback, res);
}

function verifyGatherData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];
        profile.updated_at = new Date();

        if (profile.wishlist.includes(currentUser.token)) {
          currentUser.wishlisted = false;
          var i = profile.wishlist.indexOf(currentUser.token);
          profile.wishlist.splice(i, 1);

          profile.save(function(error) {
            if (error) return callback(500, "error", res);

            sendResponse(callback, res);
          });
        } else {
          currentUser.wishlisted = true;
          profile.wishlist.push(currentUser.token);

          Rewards.findOne({reward_token : currentUser.token}, function(error, reward) {
            if (error) return callback(500, "error", res);
            if (!reward) return callback(500, "error", res);

            currentUser.wishlistObj = {
              token : currentUser.token,
              title : reward.description,
              progress : ((profile.stacks / (reward.cost * reward.discount)) * 100).toFixed(0),
              wishlisted : true
            };

            if (currentUser.wishlistObj.progress > 100) {
              currentUser.wishlistObj.progress = 100;
            }

            profile.save(function(error) {
              if (error) return callback(500, "error", res);

              sendResponse(callback, res);
            });
          });
        }
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function sendResponse(callback, res) {
  var resultArray = {}
  var resultCode = 1;
  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "wishlisted" : currentUser.wishlisted,
    "item" : currentUser.wishlistObj
  };

  callback(200, resultArray, res);
}
