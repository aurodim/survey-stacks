"use strict";

var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");
var Constants = modelRequire("constants.js");

var currentUser = {
  ip : "",
  user_validation_key : "",
  user_auth_key : "",
  user_session_key : "",
  about_completed : false,
  stacks : 0,
  userAnswers : {
    card_type : "",
    digits : "",
    name : "",
    exp : "",
    street_address : "",
    apt : "",
    city : "",
    zipcode : "",
    state : "",
    country : ""
  },
  webApp : false
};

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, user_validation_key, user_auth_key, user_session_key, payment_completed, userAnswers, webApp, callback, res) {
  currentUser.ip = ip;
  user_validation_key = user_validation_key.trim();
  user_auth_key = user_auth_key.trim();
  user_session_key = user_session_key.trim();

  currentUser.user_validation_key = user_validation_key;
  currentUser.user_auth_key =  user_auth_key;
  currentUser.user_session_key = user_session_key;
  currentUser.payment_completed = payment_completed;
  currentUser.userAnswers.digits = userAnswers.digits.replace(/\s+/g, '');
  currentUser.userAnswers.card_type = Functions.cardType(currentUser.userAnswers.digits);
  currentUser.userAnswers.name = userAnswers.name.toUpperCase();
  currentUser.userAnswers.exp = userAnswers.exp.replace('/', '-');;
  currentUser.userAnswers.street_address = userAnswers.street_address.toUpperCase();
  currentUser.userAnswers.apt = userAnswers.apt;
  currentUser.userAnswers.city = userAnswers.city.toUpperCase();
  currentUser.userAnswers.zipcode = userAnswers.zipcode;
  currentUser.userAnswers.state = userAnswers.state.toUpperCase();
  currentUser.userAnswers.country = userAnswers.country.toUpperCase();
  currentUser.webApp = webApp;

  if (currentUser.userAnswers.card_type == "") {
    sendResponse(-1, callback, res);
    return;
  }

  stackExchange();
  verifyInsertData(callback, res);
}

function stackExchange() {
  Constants.find(function(error, constants) {
    if (error) {
      currentUser.stacks = 10;
    } else {
      var constant = constants[0];
      currentUser.stacks = constant.stacks_xrt * 10;
    }
  }).limit(1);
}

function verifyInsertData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.user_validation_key, user_session_key : currentUser.user_session_key, user_auth_key : currentUser.user_auth_key}, function(error, profiles) {
    if (error) {
      callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];

        if (!profile.setup_completed.payment) {
          profile.stacks += 10;
          profile.achievements.lifetime_stacks += 10;
        }

        profile.setup_completed.payment = true;
        profile.payment.card_type = currentUser.userAnswers.card_type;
        profile.payment.card_name = currentUser.userAnswers.name;
        profile.payment.card_digits = currentUser.userAnswers.digits;
        profile.payment.card_expiration_date = currentUser.userAnswers.exp;
        profile.payment.street_address = currentUser.userAnswers.street_address;
        profile.payment.apt_number = currentUser.userAnswers.apt;
        profile.payment.city = currentUser.userAnswers.city;
        profile.payment.zipcode = currentUser.userAnswers.zipcode;
        profile.payment.state = currentUser.userAnswers.state;
        profile.payment.country = currentUser.userAnswers.country;

        profile.save(function(error) {
          if (error) {
            callback(500, "error", res);
          } else {
            sendResponse(1, callback, res);
          }
        })
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function sendResponse(resultCode, callback, res) {
  var resultArray = {}

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate
  };

  callback(200, resultArray, res);
}
