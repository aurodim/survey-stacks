"use strict";

var Profiles = modelRequire("profiles.js");
var Polls = modelRequire("polls.js");
var Surveys = modelRequire("surveys.js");
var Constants = modelRequire("constants.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.webApp = false;
  this.token = "";
  this.type = "";
  this.payment_saved = false;
  this.ppa = "";
  this.edit = {
    delete : false,
    name : "",
    description : "",
    image_url : "",
    asm : 0,
    keys : 0
  };
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, type, token, edit, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.token = token;
  currentUser.type = type;
  currentUser.edit.delete = edit.delete;
  currentUser.edit.name = edit.name;
  currentUser.edit.description = edit.description;
  currentUser.edit.image_url = edit.image_url;
  currentUser.edit.asm = edit.asm;
  currentTeam.edit.keys = edit.keys;
  currentUser.webApp = webApp;

  getConstant();
  verifyGatherData(callback, res);
}

function getConstant() {
  Constants.find(function(error, constants) {
    if (error) {
      currentUser.ppa = 1.00;
    } else {
      var constant = constants[0];
      currentUser.ppa = constant.ppa;
    }
  }).limit(1);
}

function verifyGatherData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];

        if (currentUser.edit.delete) {
          if (currentUser.type == "surveys") {
            let i = profile.surveys_created.indexOf(currentUser.token);
            profile.surveys_created.splice(i, 1);
          } else if (currentUser.type == "polls") {
            let i = profile.polls_created.indexOf(currentUser.token);
            profile.polls_created.splice(i, 1);
          }

          profile.updated_at = new Date();

          currentUser.payment_saved = profile.setup_completed.payment;

          profile.save(function(error) {
            if (error) return callback(500, "error", res);

            if (currentUser.type == "surveys") {
              surveyEditG(callback, res);
            } else if (currentUser.type == "polls") {
              pollEditG(callback, res);
            }
          });
        } else {
          uploadImage(callback, res);
        }
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function uploadImage(callback, res) {
  var folder;
  var end;
  if (currentUser.type == "surveys") {
    folder = process.env.CLOUDINARY_SURVEYS_FOLDER;
    end = "/survey_image";
  } else if (currentUser.type == "polls") {
    folder = process.env.CLOUDINARY_POLLS_FOLDER;
    end = "/poll_image";
  }
  var image_id = folder + "/" + currentUser.token + end;
  cloudinary.v2.uploader.upload(
    currentUser.edit.image_url,
    {
      public_id: image_id,
      width: 800,
      height: 800,
      folder : folder,
      tags: ['user_'+currentUser.userKey, currentUser.token, currentUser.type]
    },
    function(error, result) {

      if (typeof error != "undefined" && error != "undefined") return sendResponse(-1, callback, res);

      currentUser.edit.image_url = result.url;
      if (currentUser.type == "surveys") {
        surveyEditG(callback, res);
      } else if (currentUser.type == "polls") {
        pollEditG(callback, res);
      }
    }
  );
}

function surveyEditG(callback, res) {
  Surveys.find({deleted : false, creator_key : currentUser.userKey, survey_token : currentUser.token}, function(error, surveys) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (surveys.length == "1") {
        var survey = surveys[0];

        if (currentUser.edit.delete) {
          survey.deleted = true;
          survey.updated_at = new Date();

          survey.save(function(error) {
            if (error) return callback(500, "error", res);

            sendResponse(1, callback, res);
          });
        } else {
          survey.survey_details.name = currentUser.edit.name;
          survey.survey_details.description = currentUser.edit.description;
          survey.survey_details.image_url = currentUser.edit.image_url;

          if (!survey.payment_details.free && currentUser.edit.asm > 0 && currentUser.payment_saved) {
            survey.stacks += (currentUser.edit.asm * 10);
            survey.payment_details.survey_charge += (currentUser.ppa * currentUser.edit.asm);
            survey.payment_details.order_total += (currentUser.ppa * currentUser.edit.asm);
            // charge
          }

          if (survey.survey_details.lab_created && currentTeam.edit.keys > 0) {
            for (var i = 0; i < currentTeam.edit.keys; i++) {
              survey.labs.keys.push(Functions.createQAnswerKey());
            }
          }

          survey.updated_at = new Date();

          survey.save(function(error) {
            if (error) return callback(500, "error", res);

            sendResponse(1, callback, res);
          });
        }
      } else {
        return callback(500, "error", res);
      }
    }
  }).limit(1);
}

function pollEditG(callback, res) {
  Polls.find({deleted : false, creator_key : currentUser.userKey, poll_token : currentUser.token}, function(error, polls) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (polls.length == "1") {
        var poll = polls[0];

        if (currentUser.edit.delete) {
          poll.deleted = true;
          poll.updated_at = new Date();

          poll.save(function(error) {
            if (error) return callback(500, "error", res);

            sendResponse(1, callback, res);
          });
        } else {
          poll.poll_details.name = currentUser.edit.name;
          poll.poll_details.description = currentUser.edit.description;
          poll.poll_details.image_url = currentUser.edit.image_url;

          poll.updated_at = new Date();

          poll.save(function(error) {
            if (error) return callback(500, "error", res);

            sendResponse(1, callback, res);
          });
        }
      } else {
        return callback(500, "error", res);
      }
    }
  }).limit(1);
}

function sendResponse(resultCode, callback, res) {
  var resultArray = {}

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate
  };

  callback(200, resultArray, res);
}
