"use strict";

var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");
var Constants = modelRequire("constants.js");

function User() {
  this.ip = "";
  this.user_validation_key = "";
  this.user_auth_key = "";
  this.user_session_key = "";
  this.preferences_completed = false;
  this.stacks = 0;
  this.userPreferences = {
    lang : "",
    monosex_filter : "",
    translated_filter : "",
    categories : [],
    companies : []
  };
  this.webApp = false;
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, user_validation_key, user_auth_key, user_session_key, preferences_completed, userAnswers, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  user_validation_key = user_validation_key.trim();
  user_auth_key = user_auth_key.trim();
  user_session_key = user_session_key.trim();

  currentUser.user_validation_key = user_validation_key;
  currentUser.user_auth_key =  user_auth_key;
  currentUser.user_session_key = user_session_key;
  currentUser.preferences_completed = preferences_completed;
  currentUser.userPreferences.lang = userAnswers.lang;
  currentUser.userPreferences.monosex_filter = userAnswers.monosex_filter;
  currentUser.userPreferences.translated_filter = userAnswers.translated_filter;
  currentUser.userPreferences.categories = userAnswers.categories;
  currentUser.userPreferences.companies = userAnswers.companies;

  currentUser.webApp = webApp;

  stackExchange();
  verifyInsertData(callback, res);
}

function stackExchange() {
  Constants.find(function(error, constants) {
    if (error) {
      currentUser.stacks = 10;
    } else {
      var constant = constants[0];
      currentUser.stacks = constant.stacks_xrt * 10;
    }
  }).limit(1);
}

function verifyInsertData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.user_validation_key, user_session_key : currentUser.user_session_key, user_auth_key : currentUser.user_auth_key}, function(error, profiles) {
    if (error) {
      callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];

        if (!profile.setup_completed.preferences) {
          profile.stacks += 10;
          profile.achievements.lifetime_stacks += 10;
        }

        profile.setup_completed.preferences = true;
        profile.preferences.lang = currentUser.userPreferences.lang;
        profile.preferences.monosex = currentUser.userPreferences.monosex_filter;
        profile.preferences.translated = currentUser.userPreferences.translated_filter;
        profile.preferences.categories_tokens = currentUser.userPreferences.categories;
        profile.preferences.companies_tokens = currentUser.userPreferences.companies;

        profile.save(function(error) {
          if (error) {
            callback(500, "error", res);
          } else {
            if (profile.setup_completed.preferences && profile.setup_completed.about && currentUser.webApp) {
              res.cookie("sd", true);
            }
            if (currentUser.webApp) {
              res.cookie("lang", currentUser.userPreferences.lang);
            }
            sendResponse(callback, res);
          }
        })
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {}

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate
  };

  callback(200, resultArray, res);
}
