"use strict";

var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.lang = "";
  this.langAbbr = "";
  this.userSessionKey = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.fullname = "";
  this.email_phone = "";
  this.username = "";
  this.password = "";
  this.hashedPassword = "";
  this.dob = "";
  this.webApp = false;
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, lang, fullname, email_phone, username, password, dOB, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  lang = lang.trim().toLowerCase();
  fullname = fullname.trim();
  email_phone = email_phone.trim();
  username = username.trim();
  username = username.toLowerCase();
  dOB = dOB.trim();
  currentUser.langAbbr = lang;
  try {
    currentUser.lang = rootRequire("/controller/" + lang + ".js");
  } catch(e) {
    currentUser.langAbbr = "en";
    currentUser.lang = rootRequire("/controller/en.js");
  }
  currentUser.userSessionKey = Functions.createUserSessionKey(username);
  currentUser.userKey = Functions.createUserKey(username);
  currentUser.userAuthKey = Functions.createUserAuthKey(username, fullname);
  currentUser.fullname = fullname;
  if (email_phone.includes("@")) {
    currentUser.email_phone = email_phone.toLowerCase();
  } else {
    currentUser.email_phone = Functions.phoneNumberNormalize(email_phone);
  }
  currentUser.username = username;
  currentUser.password = password;
  currentUser.hashedPassword = Functions.hashedPasswordCreator(password);
  currentUser.dob = dOB.replace(/\//gi, "-");
  currentUser.webApp = webApp;

  emailPhoneExist(callback, res);
}

function emailPhoneExist(callback, res) {
  Profiles.findOne({deleted : false, $or : [{email_phone : currentUser.email_phone}, {username : currentUser.username}, {user_session_key : currentUser.userSessionKey}, {user_auth_key : currentUser.userAuthKey}]}, function(error, profile) {
    if (error) return callback(500, "error", res);
    if (profile == null || profile == "") return newUser(callback, res);
    if (profile.email_phone == currentUser.email_phone) return sendResponse(-1, callback, res);
    if (profile.user_auth_key == currentUser.userAuthKey) {
      currentUser.userAuthKey = Functions.createUserAuthKey(currentUser.username, currentUser.fullname);
      return emailPhoneExist(callback, res);
    } else if (profile.user_session_key == currentUser.userSessionKey) {
      currentUser.userSessionKey = Functions.createUserSessionKey(currentUser.username);
      return emailPhoneExist(callback, res);
    }

    sendResponse(-2, callback, res);
  });
}

function newUser(callback, res) {
  if (oldEnough()) {
    var newProfile = new Profiles({
      user_key : currentUser.userKey,
      user_session_key : currentUser.userSessionKey,
      user_auth_key : currentUser.userAuthKey,
      profile_picture_url : "",
      fullname : currentUser.fullname,
      email_phone : currentUser.email_phone,
      username : currentUser.username,
      password : currentUser.hashedPassword,
      about : {
        birth_date : currentUser.dob,
      },
      preferences : {
        lang : currentUser.langAbbr
      },
      created_at : new Date(),
      updated_at : new Date()
    });

    newProfile.save(function(error) {
      if (error) return callback(500, "error", res);

      if (currentUser.email_phone.includes("@")) {
        sendRegistrationEmail(callback, res);
      } else {
        sendRegistrationSMS(callback, res);
      }
    });
  } else {
    sendResponse(-3, callback, res);
  }
}

function sendRegistrationEmail(callback, res) {
  var mailOptions = {
    from : "'SurveyStacksDevTeam' <no-reply@surveyrush.com>",
    to : currentUser.email_phone,
    subject : currentUser.lang.registationEmailSubject,
    html : currentUser.lang.registrationEmailBody
  };

  transporter.sendMail(mailOptions);
  sendResponse(1, callback, res);
}

function sendRegistrationSMS(callback, res) {
  client.messages.create({
    to : currentUser.email_phone,
    from : process.env.TWILIO_PHONE_NUMBER,
    body : currentUser.lang.registrationSMSBody
  }, function(error) {
    console.log(error);
    sendResponse(1, callback, res);
  });
}

function oldEnough() {
  var date = new Date();
  var day = date.getDate();
  var month = date.getMonth();
  var year = date.getFullYear() - 13;

  var splitDOB = currentUser.dob.split("-");
  var uday = splitDOB[0];
  var umonth = splitDOB[1] - 1;
  var uyear = splitDOB[2];

  if (day < 10) {
    day = "0" + day;
  }

  if (month < 10) {
    month = "0" + month;
  }

  var neededDateFormat = new Date(year, month, day);

  var dobFormat = new Date(uyear, umonth, uday);

  if (dobFormat <= neededDateFormat) {
    return true;
  } else {
    return false;
  }
}

function sendResponse(resultCode, callback, res) {
  var resultCode = resultCode;
  var resultArray = {};
  var resultDate = new Date();

  if (resultCode == 1) {
    resultArray = {
      "webApp" : currentUser.webApp,
      "resultCode" : resultCode,
      "resultDate" : resultDate,
      "profile_auth_data" : {
        "un" : currentUser.username,
        "vk" : currentUser.userKey,
        "sk" : currentUser.userSessionKey,
        "lk" : currentUser.userAuthKey
      }
    };

    var cookieNameAuth = "lk";
    var cookieNameSession = "sk";
    var cookieNameValidation = "vk";
    var cookieNameUsername = "un";
    var cookieNameDate = "dt";
    var cookieNameSetupComplete = "sd";
    var cookieNameDesktopNotifications = "dnotif";
    var cookieNameLang = "lang";
    var date = new Date();

    if (currentUser.webApp) {
      res.cookie(cookieNameSession, currentUser.userSessionKey, { maxAge: (86400000 * 365.25), httpOnly: true, signed : true});
      res.cookie(cookieNameValidation, currentUser.userKey, { maxAge: (86400000 * 365.25), httpOnly: true, signed : true});
      res.cookie(cookieNameAuth, currentUser.userAuthKey, { maxAge: (86400000 * 365.25), httpOnly: true, signed : true});
      res.cookie(cookieNameUsername, currentUser.username, { maxAge: (86400000 * 365.25), httpOnly: false});
      res.cookie(cookieNameDate, date.toString(), { maxAge: (86400000 * 365.25), httpOnly: true, signed : true});
      res.cookie(cookieNameLang, currentUser.langAbbr, { maxAge: (86400000 * 365.25), httpOnly: false});
      res.cookie(cookieNameSetupComplete, false, { maxAge: (86400000 * 365.25), httpOnly: true});
      res.cookie(cookieNameDesktopNotifications, false, { maxAge: (86400000 * 365.25), httpOnly: false})
    }
  } else {
    resultArray = {
      "webApp" : currentUser.webApp,
      "resultCode" : resultCode,
      "resultDate" : resultDate,
      "profile_auth_data" : {
        "un" : "",
        "vk" : "",
        "sk" : "",
        "lk" : ""
      }
    };
  }

  callback(200, resultArray, res);
}
