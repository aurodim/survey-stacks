"use strict";

var Profiles = modelRequire("profiles.js");
var Rewards = modelRequire("rewards.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.webApp = false;
  this.token = "";
  this.redeem_key = "";
  this.price = 0;
  this.stacks = 0;
  this.phone_number = "";
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, token, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.token = token;
  currentUser.webApp = webApp;

  verifyGatherData(callback, res);
}

function verifyGatherData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];

        Rewards.findOne({reward_token : currentUser.token}, function(error, reward) {
          if (error) return callback(500, "error", res);
          if (!reward) return callback(500, "error", res);
          if (reward.quantity == 0) return callback(500, "error", res);
          currentUser.price = (reward.cost * reward.discount).toFixed(0);
          currentUser.stacks = profile.stacks - currentUser.price;
          if (currentUser.stacks < 0) return callback(500, "error", res);

          var i = Math.floor(Math.random() * reward.quantity);
          currentUser.redeem_key = reward.redeem_keys[i];
          reward.redeem_keys.splice(i, 1);
          reward.times_redeemed+=1;
          reward.updated_at = new Date();

          profile.achievements.rewards_claimed+=1;
          var rewardObj = {
            reward_token : currentUser.token,
            reward_key : currentUser.redeem_key
          };
          profile.redeemed.push(rewardObj);
          profile.stacks = currentUser.stacks;
          currentUser.phone_number = profile.phone_number;
          profile.updated_at = new Date();

          reward.save(function(error) {
            if (error) return callback(500, "error", res);

            profile.save(function(error) {
              if (error) return callback(500, "error", res);

              sendReward(reward.description, callback, res);
            })
          });
        });
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function sendReward(name, callback, res) {
  var file;
  try {
    file = rootRequire("/controller/" + currentUser.lang.toLowerCase() + ".js");
  } catch(e) {
    file = rootRequire("/controller/en.js");
  }
  client.messages.create({
    to : currentUser.phone_number,
    from : process.env.TWILIO_PHONE_NUMBER,
    body : file.claimReward(name, currentUser.redeem_key)
  });

  sendResponse(callback, res);
}

function sendResponse(callback, res) {
  var resultArray = {}
  var resultCode = 1;
  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "stacks" : currentUser.stacks
  };

  callback(200, resultArray, res);
}
