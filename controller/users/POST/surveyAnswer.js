"use strict";

var Profiles = modelRequire("profiles.js");
var Surveys = modelRequire("surveys.js");
var Functions = rootRequire("/controller/globalFunctions.js");
var Constants = modelRequire("constants.js");
var Responses = modelRequire("responses.js");

function User() {
  this.ip = "";
  this.user_validation_key = "";
  this.user_auth_key = "";
  this.user_session_key = "";
  this.user = false;
  this.stacks_multipler = 0;
  this.creator_key = "";
  this.survey_name = "";
  this.stacks = 0;
  this.lang = "";
  this.token = "";
  this.response = [];
  this.labs = {};
  this.profile = {};
  this.survey = {};
  this.collected = {};
  this.response_t = "";
  this.dataResponse = {};
  this.webApp = false;
}

var currentUser;

var service = {};

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validationKey, authKey, sessionKey, lang, token, response, labs, user, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  validationKey = validationKey.trim();
  authKey = authKey.trim();
  sessionKey = sessionKey.trim();

  currentUser.lang = lang;
  currentUser.token = token;
  currentUser.response = response;
  currentUser.labs = labs;
  currentUser.user = user;
  currentUser.webApp = webApp;
  currentUser.response_t = new mongoose.Types.ObjectId();
  stacksMultiplier();

  if (user) {
    currentUser.user_validation_key = validationKey;
    currentUser.user_auth_key =  authKey;
    currentUser.user_session_key = sessionKey;

    verifyGatherData(callback, res);
  } else {
    currentUser.user_validation_key = process.env.HOMO_PUBLICA_CLAVEM;
    verifySurvey(callback, res);
  }
}

function stacksMultiplier() {
  Constants.find(function(error, constants) {
    if (error) {
      currentUser.stacks_multipler = 1;
    } else {
      var constant = constants[0];
      currentUser.stacks_multipler = constant.stacks_xrt;
    }
  }).limit(1);
}

function verifyGatherData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.user_validation_key, user_session_key : currentUser.user_session_key, user_auth_key : currentUser.user_auth_key}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];
        if (profile.surveys_answered.includes(currentUser.token)) {
          return callback(500, "error", res);
        }
        currentUser.profile = profile;
        verifySurvey(callback, res);
      } else {
        return callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function verifySurvey(callback, res) {
  Surveys.findOne({deleted : false, survey_token : currentUser.token}, function(error, survey) {
    if (error || survey == "" || survey == null) return callback(500, "error", res);
    if (!currentUser.user && !survey.survey_details.public_survey) return callback(500, "error", res);

    currentUser.stacks = survey.survey_details.stacks;
    currentUser.survey_name = survey.survey_details.name;
    currentUser.creator_key = survey.creator_key;
    currentUser.survey = survey;

    if (currentUser.user) {
      for (var i = 0; i < survey.survey_details.collected.length; i++) {
        switch(survey.survey_details.collected[i]) {
          case "fullname":
            currentUser.collected.fullname = currentUser.profile.fullname;
            break;
          case "email_phone":
            currentUser.collected.email_phone = currentUser.profile.email_phone;
            break;
          case "employment":
            currentUser.collected.employment = currentUser.profile.about.employment_status;
            break;
          case "relationship":
            currentUser.collected.relationship = currentUser.profile.about.relationship_status;
            break;
          case "education":
            currentUser.collected.education = currentUser.profile.about.education;
            break;
          case "languages":
            currentUser.collected.languages = currentUser.profile.about.languages;
            break;
          case "ethnicity":
            currentUser.collected.ethnicity = currentUser.profile.about.ethnicity;
            break;
          case "sex":
            currentUser.collected.sex = currentUser.profile.about.sex;
            break;
          case "dob":
            currentUser.collected.dob = currentUser.profile.about.birth_date;
            break;
        }
      }
    }

    var labsData = {};

    if (survey.survey_details.lab_created) {
      if (currentUser.labs.elapsed && currentUser.labs.elapsed_end) {
        labsData.elapsed = ((currentUser.labs.elapsed_end - currentUser.labs.elapsed) / 1000).toFixed(2);
      }

      if (currentUser.labs.key) {
        if (!survey.labs.keys.includes(currentUser.labs.key)) {
          sendResponse(-1, callback, res);
          return;
        }

        labsData.key_used = currentUser.labs.key;
      }

      if (survey.labs.type === "quiz") {
        scoreQuiz(function(score) {
          labsData.score = score / survey.labs.out_of;
          labsData.score *= 100;
          if (survey.labs.show_score) {
            currentUser.dataResponse.score = score;
          }
          insertResponse(callback, res, labsData);
        })
      } else if (survey.labs.type === "game") {
        var apa = 1 / currentUser.response.length;
        var total = 1;

        for (var i = 0; i < currentUser.response.length; i++) {
          if (currentUser.response[i] === "" || currentUser.response[i].length === 0) {
            total -= apa;
          }
        }

        currentUser.stacks *= total;
        insertResponse(callback, res, labsData);
      } else {
        insertResponse(callback, res, labsData);
      }
    } else {
      insertResponse(callback, res, labsData);
    }
  });
}

function scoreQuiz(cb) {
  var score = 0;
  var questions = [];
  questions = questions.concat(currentUser.survey.question_details.free_response, currentUser.survey.question_details.checkbox, currentUser.survey.question_details.boolean, currentUser.survey.question_details.radio, currentUser.survey.question_details.menu);

  for (var i = 0; i < currentUser.survey.question_details.order.length; i++) {
    var type = currentUser.survey.question_details.order[i].split("_")[0];
    var ind = currentUser.survey.question_details.order[i].split("_")[1];
    var question = {};
    var response = currentUser.response[i];

    if (type === "fr") {
      question = currentUser.survey.question_details.free_response[ind];
    } else if (type === "cr") {
      question = currentUser.survey.question_details.checkbox[ind];
    } else if (type === "br") {
      question = currentUser.survey.question_details.boolean[ind];
    } else if (type === "rr") {
      question = currentUser.survey.question_details.radio[ind];
    } else if (type === "mr") {
      question = currentUser.survey.question_details.menu[ind];
    }

    if (type === "fr") {
      var algorithm = question.options.algorithm;

      if (algorithm === "c") {
        score += Contains(response, question.options.answer) ? question.options.worth : 0;
      } else if (algorithm === "bw") {
        score += BeginsWith(response, question.options.answer) ? question.options.worth : 0;
      } else if (algorithm === "ew") {
        score += EndsWith(response, question.options.answer) ? question.options.worth : 0;
      } else if (algorithm === "o") {
        score += IsOnly(response, question.options.answer) ? question.options.worth : 0;
      } else if (algorithm === "pm") {
        score += PercentageMatch(response, question.options.answer, question.options.worth);
      }
    } else if (type === "cr") {
      var contains = false;

      for (var response in response) {
        if (question.options.answer.includes(response[response])) {
          contains = true;
        } else {
          contains = false;
          break;
        }
      }

      score += contains ? question.options.worth : 0;
    } else {
      score += question.options.answer == response ? question.options.worth : 0;
    }
  }

  cb(score);
}

function Contains(response, answer) {
  answer = answer.toLowerCase();
  response = response.toLowerCase().split(" ");
  var contains = false;

  for (var word in response) {
    if (answer.includes(response[word])) {
      contains = true;
    } else {
      contains = false;
      break;
    }
  }

  return contains;
}

function BeginsWith(response, answer) {
  answer = answer.toLowerCase();
  response = response.toLowerCase();

  return response.startsWith(answer);
}

function EndsWith(response, answer) {
  answer = answer.toLowerCase();
  response = response.toLowerCase();

  return response.endsWith(answer);
}

function IsOnly(response, answer) {
  answer = answer.toLowerCase();
  response = response.toLowerCase();

  return response.startsWith(answer);
}

function PercentageMatch(response, answer, worth) {
  var longer = response;
  var shorter = answer;
  if (response.length < answer.length) {
    longer = answer;
    shorter = response;
  }
  var longerLength = longer.length;
  if (longerLength === 0) {
    return 1.0;
  }

  var percentage = (longerLength - editDistance(longer, shorter)) / parseFloat(longerLength);

  if (percentage * 100 < 30) return 0;

  return percentage * worth;
}

function editDistance(s1, s2) {
  s1 = s1.trim().toLowerCase().replace(/\s\s+/g, ' ');
  s2 = s2.trim().toLowerCase().replace(/\s\s+/g, ' ');

  var pieces = [];
  for (var i = 0; i <= s1.length; i++) {
    var lastValue = i;
    for (var j = 0; j <= s2.length; j++) {
      if (i === 0)
        pieces[j] = j;
      else {
        if (j > 0) {
          var newValue = pieces[j - 1];
          if (s1.charAt(i - 1) != s2.charAt(j - 1))
            newValue = Math.min(Math.min(newValue, lastValue),
              pieces[j]) + 1;
          pieces[j - 1] = lastValue;
          lastValue = newValue;
        }
      }
    }
    if (i > 0)
      pieces[s2.length] = lastValue;
  }
  return pieces[s2.length];
}

function insertResponse(callback, res, labs) {
  var Response = new Responses({
    response_token : currentUser.response_t,
    respondent_token : currentUser.user_validation_key,
    respondent_ip : currentUser.ip,
    questionnaire_type : "survey",
    questionnaire_token : currentUser.token,
    response_lang : currentUser.lang,
    currency_recieved : currentUser.user ? currentUser.stacks * currentUser.stacks_multipler : 0,
    collected : currentUser.collected,
    labs : labs,
    responses : currentUser.response,
    created_at : Date.now(),
    updated_at : Date.now()
  });

  Response.save(function(error) {
    if(error) return callback(500, "error", res);

    if (currentUser.user) {
      currentUser.profile.surveys_answered.push(currentUser.token);
      currentUser.profile.stacks += (currentUser.stacks * currentUser.stacks_multipler);
    }

    if (currentUser.user) {
      saveData(callback, res);
    } else {
      currentUser.survey.responses.push(currentUser.response_t);
      currentUser.survey.updated_at = new Date();
      currentUser.survey.save(function(error) {
        if (error) return callback(500, "error", res);

        var ft = currentUser.creator_key.split('.')[1];

        if (ft.substr(ft.length - 4, ft.length) == "AuRo") {
          sendNotif(callback, res);
        } else {
          sendNotifTeam(callback, res);
        }
        
        sendNotif(callback, res);
      });
    }
  });
}

function saveData(callback, res) {
  currentUser.profile.updated_at = new Date();
  currentUser.profile.save(function(error) {
    if (error) return callback(500, "error", res);

    currentUser.survey.responses.push(currentUser.response_t);
    currentUser.survey.updated_at = new Date();
    currentUser.survey.save(function(error) {
      if (error) return callback(500, "error", res);

      var ft = currentUser.creator_key.split('.')[1];

      if (ft.substr(ft.length - 4, ft.length) == "AuRo") {
        sendNotif(callback, res);
      } else {
        sendNotifTeam(callback, res);
      }

      sendNotif(callback, res);
    });
  });
}

function sendNotif(callback, res) {
  Profiles.findOne({user_key : currentUser.creator_key}, function(error, profile) {
    if (error) return sendResponse(1, callback, res);
    if (profile == null || profile == "") return sendResponse(1, callback, res);

    currentUser.socket = profile.user_session_key;

    var data = {
      reciever : currentUser.socket,
      type : "l",
      title : "n_r",
      content : currentUser.survey_name,
      url : "/analytics"
    };

    Functions.notifSocket(data, function(error) {
      sendResponse(1, callback, res);
    });
  });
}

function sendNotifTeam(callback, res) {
  Teams.findOne({team_key : currentUser.creator_key}, function(error, team) {
    if (error) return sendResponse(callback, res);
    if (team == null || team == "") return sendResponse(callback, res);

    currentUser.socket = team.team_session_key;

    var data = {
      reciever : currentUser.socket,
      type : "l",
      title : "n_r",
      content : currentUser.poll_name,
      url : "/analytics"
    };

    Functions.notifSocket(data, function(error) {
      sendResponse(callback, res);
    });
  });
}

function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {}

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate
  };

  if (currentUser.dataResponse.score >= 0) {
    resultArray.score = currentUser.dataResponse.score;
  }

  callback(200, resultArray, res);
}
