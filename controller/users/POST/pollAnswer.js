"use strict";

var Profiles = modelRequire("profiles.js");
var Polls = modelRequire("polls.js");
var Functions = rootRequire("/controller/globalFunctions.js");
var Constants = modelRequire("constants.js");
var Responses = modelRequire("responses.js");

function User() {
  this.ip = "";
  this.user_validation_key = "";
  this.user_auth_key = "";
  this.user_session_key = "";
  this.user = false;
  this.coins_multipler = 0;
  this.coins = 0;
  this.creator_key = "";
  this.poll_name = "";
  this.notif = "";
  this.lang = "";
  this.token = "";
  this.response = null;
  this.labs = {};
  this.profile = {};
  this.poll = {};
  this.response_t = "";
  this.analytics = [];
  this.collected = {};
  this.dataResponse = {};
  this.webApp = false;
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validationKey, authKey, sessionKey, lang, token, response, labs, user, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  validationKey = validationKey.trim();
  authKey = authKey.trim();
  sessionKey = sessionKey.trim();

  currentUser.lang = lang;
  currentUser.token = token;
  if (response == null) {
    response = "";
  }
  currentUser.response = response.toString();
  currentUser.labs = labs;
  currentUser.user = user;
  currentUser.webApp = webApp;
  currentUser.response_t = new mongoose.Types.ObjectId();
  coinsMultiplier();

  if (user) {
    currentUser.user_validation_key = validationKey;
    currentUser.user_auth_key =  authKey;
    currentUser.user_session_key = sessionKey;

    verifyGatherData(callback, res);
  } else {
    currentUser.user_validation_key = process.env.HOMO_PUBLICA_CLAVEM;
    verifyPoll(callback, res);
  }
}

function coinsMultiplier() {
  Constants.find(function(error, constants) {
    if (error) {
      currentUser.coins_multipler = 1;
    } else {
      var constant = constants[0];
      currentUser.coins_multipler = constant.coins_xrt;
    }
  }).limit(1);
}

function verifyGatherData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.user_validation_key, user_session_key : currentUser.user_session_key, user_auth_key : currentUser.user_auth_key}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];
        if (profile.polls_answered.includes(currentUser.token)) {
          return callback(500, "error", res);
        }
        currentUser.profile = profile;
        verifyPoll(callback, res);
      } else {
        return callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function verifyPoll(callback, res) {
  Polls.findOne({deleted : false, poll_token : currentUser.token}, function(error, poll) {
    if (error || poll === "" || poll == null) return callback(500, "error", res);

    currentUser.poll_name = poll.poll_details.name;
    currentUser.creator_key = poll.creator_key;
    currentUser.coins = poll.poll_details.coins;

    currentUser.poll = poll;

    var labsData = {};

    if (currentUser.user) {
      for (var i = 0; i < poll.poll_details.collected.length; i++) {
        switch(poll.poll_details.collected[i]) {
          case "fullname":
            currentUser.collected.fullname = currentUser.profile.fullname;
            break;
          case "email_phone":
            currentUser.collected.email_phone = currentUser.profile.email_phone;
            break;
          case "employment":
            currentUser.collected.employment = currentUser.profile.about.employment_status;
            break;
          case "relationship":
            currentUser.collected.relationship = currentUser.profile.about.relationship_status;
            break;
          case "education":
            currentUser.collected.education = currentUser.profile.about.education;
            break;
          case "languages":
            currentUser.collected.languages = currentUser.profile.about.languages;
            break;
          case "ethnicity":
            currentUser.collected.ethnicity = currentUser.profile.about.ethnicity;
            break;
          case "sex":
            currentUser.collected.sex = currentUser.profile.about.sex;
            break;
          case "dob":
            currentUser.collected.dob = currentUser.profile.about.birth_date;
            break;
        }
      }
    }

    if (poll.poll_details.lab_created) {
      if (currentUser.labs.elapsed && currentUser.labs.elapsed_end) {
        labsData.elapsed = ((currentUser.labs.elapsed_end - currentUser.labs.elapsed) / 1000).toFixed(2);
      }

      if (poll.labs.type === "quiz") {
          labsData.score = (poll.labs.answer == currentUser.response) ? 1 : 0;
          if (poll.labs.show_score) {
            currentUser.dataResponse.score = labsData.score;
          }
          insertResponse(callback, res, labsData);
      } else if (poll.labs.type === "game") {
        var apa = 1 / (currentUser.labs.responses.length + 1);
        var total = 1;

        for (var i = 0; i < currentUser.labs.responses.length; i++) {
          if (currentUser.labs.responses[i] === "") {
            total -= apa;
          }
        }

        if (currentUser.response == null) {
          total -= apa;
        }

        currentUser.coins *= total;
        currentUser.coins = Math.floor(currentUser.coins);
        insertResponse(callback, res, labsData);
      } else {
        insertResponse(callback, res, labsData);
      }
    } else {
      insertResponse(callback, res, labsData);
    }
  });
}

function insertResponse(callback, res, labs) {
  var Response = new Responses({
    response_token : currentUser.response_t,
    respondent_token : currentUser.user_validation_key,
    respondent_ip : currentUser.ip,
    questionnaire_type : "poll",
    questionnaire_token : currentUser.token,
    response_lang : currentUser.lang,
    collected : currentUser.collected,
    labs : labs,
    currency_recieved : currentUser.user ? currentUser.coins * currentUser.coins_multipler : 0,
    responses : [currentUser.response],
    created_at : Date.now(),
    updated_at : Date.now()
  });

  Response.save(function(error) {
    if(error) return callback(500, "error", res);

    if (currentUser.user) {
      currentUser.profile.polls_answered.push(currentUser.token);
      currentUser.profile.coins += (currentUser.coins * currentUser.coins_multipler);
      currentUser.profile.achievements.lifetime_coins += (currentUser.coins * currentUser.coins_multipler);
    }

    formulateAnalytics(callback, res);
  });
}

function formulateAnalytics(callback, res) {
  currentUser.poll.responses.push(currentUser.response_t);

  Responses.find({questionnaire_token : currentUser.token, questionnaire_type : "poll"}, function(error, responses) {
    if (error) return sendResponse(callback, res);

    currentUser.analytics = Functions.analyticPercentagesPoll(responses);

    if (currentUser.user) {
      saveData(callback, res);
    } else {
      currentUser.poll.updated_at = new Date();
      currentUser.poll.save(function(error) {
        if (error) return callback(500, "error", res);

        var ft = currentUser.creator_key.split('.')[1];

        if (ft.substr(ft.length - 4, ft.length) == "AuRo") {
          sendNotif(callback, res);
        } else {
          sendNotifTeam(callback, res);
        }
      });
    }
  });
}

function saveData(callback, res) {
  currentUser.profile.updated_at = new Date();
  currentUser.profile.save(function(error) {
    if (error) return callback(500, "error", res);

    currentUser.poll.updated_at = new Date();
    currentUser.poll.save(function(error) {
      if (error) return callback(500, "error", res);

      var ft = currentUser.creator_key.split('.')[1];

      if (ft.substr(ft.length - 4, ft.length) == "AuRo") {
        sendNotif(callback, res);
      } else {
        sendNotifTeam(callback, res);
      }
    });
  });
}

function sendNotif(callback, res) {
  Profiles.findOne({user_key : currentUser.creator_key}, function(error, profile) {
    if (error) return sendResponse(callback, res);
    if (profile == null || profile == "") return sendResponse(callback, res);

    currentUser.socket = profile.user_session_key;

    var data = {
      reciever : currentUser.socket,
      type : "l",
      title : "n_r",
      content : currentUser.poll_name,
      url : "/analytics"
    };

    Functions.notifSocket(data, function(error) {
      sendResponse(callback, res);
    });
  });
}

function sendNotifTeam(callback, res) {
  Teams.findOne({team_key : currentUser.creator_key}, function(error, team) {
    if (error) return sendResponse(callback, res);
    if (team == null || team == "") return sendResponse(callback, res);

    currentUser.socket = team.team_session_key;

    var data = {
      reciever : currentUser.socket,
      type : "l",
      title : "n_r",
      content : currentUser.poll_name,
      url : "/analytics"
    };

    Functions.notifSocket(data, function(error) {
      sendResponse(callback, res);
    });
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {}

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
     "resultDate" : resultDate,
     "analytics" : currentUser.analytics,
  };

  if (currentUser.dataResponse.score >= 0) {
    resultArray.score = currentUser.dataResponse.score;
  }

  callback(200, resultArray, res);
}
