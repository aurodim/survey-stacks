var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");
var sizeOf = require('image-size');

function User() {
  this.ip = "";
  this.userKey = "";
  this.userSessionKey = "";
  this.userAuthKey = "";
  this.imageFolder = "";
  this.imageID = "";
  this.imageURL = "";
  this.file = {};
  this.first = false;
  this.webApp = false;
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, user_validation_key, user_auth_key, user_session_key, file, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = user_validation_key;
  currentUser.userAuthKey = user_auth_key;
  currentUser.userSessionKey = user_session_key;
  currentUser.imageFolder = process.env.CLOUDINARY_USERS_FOLDER + "/" + currentUser.userKey
  currentUser.imageID = process.env.CLOUDINARY_USERS_FOLDER + "/" + currentUser.userKey + "/profile_picture";
  currentUser.file = file;
  currentUser.webApp = webApp;

  verifyData(callback, res);
}

function verifyData(callback, res) {
  Profiles.find({user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      callback(500, "error", res);
      return;
    }

    if (profiles.length == "1") {
      var profile = profiles[0];
      if (profile.profile_picture_url == "") {
        currentUser.first = true;
      }
      updateProfilePicture(profile, callback, res);
    } else {
      callback(401, "wrongMatch", res);
    }
  }).limit(1);
}

function updateProfilePicture(profile, callback, res) {
  var dimensions = sizeOf(process.cwd()+"/"+currentUser.file.path);
  var height = dimensions.height;
  var width = dimensions.width;
  if (dimensions.height > 1600 || dimensions.width > 1600) {
    height = dimensions.height / 2;
    width = dimensions.width / 2;
  }
  cloudinary.uploader.upload(
    currentUser.file.path,
    function(result) {
      fs.unlink(process.cwd()+"/"+currentUser.file.path, function(error) {

      });

      if (currentUser.first) {
        profile.profile_picture_url = result.secure_url;
        profile.updated_at = new Date();
        profile.save(function(error) {
          if (error) return callback(500, "error", res);

          currentUser.imageURL = result.secure_url;
          sendResponse(1, callback, res);
        });
      } else {
        currentUser.imageURL = result.secure_url;

        sendResponse(1, callback, res);
      }
    },
    {
      public_id: "profile_picture",
      width: width,
      height: height,
      folder : currentUser.imageFolder,
      tags: ['profile_picture', currentUser.userKey, "users"]
    }
  );
}

function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {}
  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "imageURL" : Functions.imageTransform(currentUser.imageURL, 200, 200)
  };

  callback(200, resultArray, res);
}
