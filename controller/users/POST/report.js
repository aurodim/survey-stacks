"use strict";

var Profiles = modelRequire("profiles.js");
var Polls = modelRequire("polls.js");
var Surveys = modelRequire("surveys.js");
var Reports = modelRequire("reports.js");
var Functions = rootRequire("/controller/globalFunctions.js");


function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.webApp = false;
  this.token = "";
  this.type = "";
  this.profile = {};
  this.questionnaire = {};
  this.report_token = "";
  this.report = {
    reason : [],
    additional : "",
  };
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, type, token, report, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.token = token;
  currentUser.type = type;
  currentUser.report = report;
  currentUser.webApp = webApp;
  currentUser.report_token = new mongoose.Types.ObjectId();

  verifyGatherData(callback, res);
}

function verifyGatherData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];
        currentUser.profile = profile;

        if (currentUser.type == "surveys") {
          surveyReport(callback, res);
        } else {
          pollReport(callback, res);
        }


      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function surveyReport(callback, res) {
  Surveys.find({deleted : false, survey_token : currentUser.token}, function(error, surveys) {
    if (error) return callback(500, "error", res);
    if (surveys.length != "1") return sendResponse(-1, callback, res);

    currentUser.questionnaire = surveys[0];

    createReport(callback, res);
  });
}

function pollReport(callback, res) {
  Polls.find({deleted : false, poll_token : currentUser.token}, function(error, polls) {
    if (error) return callback(500, "error", res);
    if (polls.length != "1") return sendResponse(-1, callback, res);

    currentUser.questionnaire = polls[0];

    createReport(callback, res);
  });
}

function createReport(callback, res) {
    var Report = new Reports({
      report_id : currentUser.report_token,
      reported_key : currentUser.token,
      reported_type : currentUser.type,
      reported_by : currentUser.userKey,
      reason : currentUser.report.reason,
      additional : currentUser.report.additional,
      created_at : new Date()
    });

    Report.save(function(error) {
      if (error) return callback(500, "error", res);

      currentUser.profile.reports.push(currentUser.report_token);

      currentUser.profile.updated_at = new Date();
      currentUser.profile.save(function(error) {
        if (error) return callback(500, "error", res);

        currentUser.questionnaire.updated_at = new Date();
        currentUser.questionnaire.times_reported+=1;
        currentUser.questionnaire.save(function(error) {
          if (error) return callback(500, "error", res);

          sendResponse(1, callback, res);
        });
      });
    });
}

function sendResponse(resultCode, callback, res) {
  var resultArray = {}

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate
  };

  callback(200, resultArray, res);
}
