"use strict";

var Profiles = modelRequire("profiles.js");
var Polls = modelRequire("polls.js");
var Constants = modelRequire("constants.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User()  {
  this.ip = "";
  this.user_validation_token = "";
  this.user_auth_token = "";
  this.user_session_token = "";
  this.poll_token = "";
  this.poll_coins = 0;
  this.image_id = "";
  this.lang = "";
  this.image;
  this.collected = [];
  this.webApp = false;
  this.poll = {};
};

var currentUser;

var services = {};

services.initialize = initialize;
module.exports = services;

function initialize(ip, user_validation_token, user_auth_token, user_session_token, lang, image, poll, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.user_validation_token = user_validation_token;
  currentUser.user_auth_token = user_auth_token;
  currentUser.user_session_token = user_session_token;
  currentUser.lang = lang;
  currentUser.webApp = webApp;
  currentUser.poll = poll;
  currentUser.image = image;
  currentUser.poll_token = Functions.createPollToken(user_validation_token, "1");
  var collected = [];
  for (var key in currentUser.poll.details.data_collected) {
    if (currentUser.poll.details.data_collected[key]) {
      collected.push(key);
    }
  }
  currentUser.collected = collected;
  getCoins();

  verifyProfile(callback, res);
}

function getCoins() {
  Constants.find(function(error, constants) {
    if (error) {
      currentUser.poll_coins = 5;
    } else {
      var constant = constants[0];
      currentUser.poll_coins = constant.cpp;
    }
  }).limit(1);
}

function verifyProfile(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.user_validation_token, user_session_key : currentUser.user_session_token, user_auth_key : currentUser.user_auth_token}, function(error, profiles) {
    if (error) return callback(500, "error", res);

    if (profiles.length == 0) return callback(401, "wrongMatch", res);

    var profile = profiles[0];

    if (currentUser.image !== "") {
      // image we give
      uploadImageF(callback, res);
    } else if (currentUser.poll.pollDetails.image_url != "") {
      uploadImage(callback, res);
    } else {
      uploadPoll(callback, res);
    }
  }).limit(1);
}

function uploadImageF(callback, res) {
  var dimensions = sizeOf(process.cwd()+"/"+currentUser.image.path);
  var height = dimensions.height;
  var width = dimensions.width;
  if (dimensions.height > 1600 || dimensions.width > 1600) {
    height = dimensions.height / 2;
    width = dimensions.width / 2;
  }
  currentUser.image_id = process.env.CLOUDINARY_POLLS_FOLDER + "/" + currentUser.poll_token + "/poll_image";
  cloudinary.v2.uploader.upload(
    currentUser.image.path,
    function(error, result) {
      if (typeof error != "undefined" && error != "undefined") return sendResponse(-1, callback, res);

      fs.unlink(process.cwd()+"/"+currentUser.image.path, function(error) {

      });

      currentUser.poll.pollDetails.image_url = result.secure_url;

      uploadPoll(callback, res);
    },
    {
      public_id: currentUser.image_id,
      width: width,
      height: height,
      folder :  process.env.CLOUDINARY_POLLS_FOLDER,
      tags: ['user_'+currentUser.validation_token, currentUser.survey_token, "polls"]
    }
  );
}

function uploadImage(callback, res) {
  currentUser.image_id = process.env.CLOUDINARY_POLLS_FOLDER + "/" + currentUser.poll_token + "/poll_image";
  cloudinary.v2.uploader.upload(
    currentUser.poll.pollDetails.image_url,
    {
      public_id: currentUser.image_id,
      width: 800,
      height: 800,
      folder :  process.env.CLOUDINARY_POLLS_FOLDER,
      tags: ['user_'+currentUser.user_validation_token, currentUser.poll_token, "polls"]
    },
    function(error, result) {

      if (typeof error != "undefined" && error != "undefined") return sendResponse(-2, callback, res);

      currentUser.poll.pollDetails.image_url = result.secure_url;
      uploadPoll(callback, res);
    }
  );
}

function uploadPoll(callback, res) {
  var userPoll = currentUser.poll;
  var Poll = new Polls({
    poll_token : currentUser.poll_token,
    creator_key : currentUser.user_validation_token,
    poll_details : {
      public_poll : userPoll.pollDetails.public,
      lang : currentUser.lang,
      coins : currentUser.poll_coins,
      name : userPoll.pollDetails.name,
      description : userPoll.pollDetails.description,
      category_token : userPoll.pollDetails.category_token,
      target_sex : userPoll.pollDetails.sex,
      target_ethnicity : userPoll.pollDetails.ethnicity,
      target_age_group : userPoll.pollDetails.age_group,
      image_url : currentUser.poll.pollDetails.image_url,
      answer_url : Functions.answerURL(currentUser.poll_token, 1),
      lab_created : false,
      collected : currentUser.collected
    },
    question_details : {
      question : userPoll.pollQuestion.question,
      options : userPoll.pollQuestion.options
    },
    created_at : new Date(),
    updated_at : new Date()
  });

  Poll.save(function(error) {
    if (error) return callback(500, "error", res);

    updateProfile(callback, res);
  });
}

function updateProfile(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.user_validation_token, user_session_key : currentUser.user_session_token, user_auth_key : currentUser.user_auth_token}, function(error, profiles) {
    if (error) return callback(500, "error", res);

    var profile = profiles[0];
    profile.polls_created.push(currentUser.poll_token);

    profile.save(function(error) {
      if (error) return callback(500, "error", res);

      sendResponse(1, callback, res);
    });
  }).limit(1);
}

function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {}
  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate
  };

  callback(200, resultArray, res);
}
