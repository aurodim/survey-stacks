"use strict";

var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.lang = "";
  this.username = "";
  this.email_phone = "";
  this.dob = "";
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, lang, email_phone, dob, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  try {
    currentUser.lang = rootRequire("/controller/" + lang.toLowerCase() + ".js");
  } catch(e) {
    currentUser.lang = rootRequire("/controller/en.js");
  }
  currentUser.username = "";
  if (email_phone.includes("@")) {
    currentUser.email_phone = email_phone.toLowerCase();
  } else {
    currentUser.email_phone = Functions.phoneNumberNormalize(email_phone);
  }
  currentUser.dob = dob.replace(/\//gi, "-");;

  checkSend(callback, res);
}

function checkSend(callback, res) {
  Profiles.findOne({email_phone : currentUser.email_phone, "about.birth_date" : currentUser.dob}, function(error, profile) {
    if (error) return callback(500, "error", res);
    if (profile == null || profile == "") return sendResponse(-1, callback, res);

    currentUser.username = profile.username;
    sendResponse(1, callback, res);
  });
}

function sendResponse(resultCode, callback, res) {
  var resultCode = resultCode;
  var resultArray = {}

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "username" : currentUser.username
  };

  callback(200, resultArray, res);
}
