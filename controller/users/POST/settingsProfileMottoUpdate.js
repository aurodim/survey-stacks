"use strict";

var Profiles = modelRequire("profiles.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userSessionKey = "";
  this.userAuthKey = "";
  this.motto = "";
  this.webApp = false;
}

var currentUser;
var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, user_validation_key, user_auth_key, user_session_key, motto, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = user_validation_key;
  currentUser.userAuthKey = user_auth_key;
  currentUser.userSessionKey = user_session_key;
  currentUser.motto = motto;
  currentUser.webApp = webApp;

  verifyData(callback, res);
}

function verifyData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      callback(500, "error", res);
      return;
    }

    if (profiles.length == "1") {
      var profile = profiles[0];
      profile.updated_at = new Date();
      profile.motto = currentUser.motto;
      profile.save(function(error) {
        if (error) return callback(500, "error", res);
        sendResponse(callback, res);
      });
    } else {
      callback(401, "wrongMatch", res);
    }
  }).limit(1);
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {}
  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "imageURL" : currentUser.imageURL
  };

  callback(200, resultArray, res);
}
