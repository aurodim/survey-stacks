"use strict";

var Teams = modelRequire("teams.js");
var Constants = modelRequire("constants.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.webApp = false;
  this.code = "";
  this.token = "";
  this.team = {};
}

var currentUser;

var service = {};

service.initializeJoin = initializeJoin;
service.initializeLeave = initializeLeave;

module.exports = service;

function initializeJoin(ip, validation_key, auth_key, session_key, code, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.code = code;
  currentUser.webApp = webApp;

  join(callback, res);
}

function initializeLeave(ip, validation_key, auth_key, session_key, token, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.token = token;
  currentUser.webApp = webApp;

  leave(callback, res);
}

function join(callback, res) {
  Teams.findOne({"member_options.code" : currentUser.code, "member_options.open" : true}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == "" || team == null) return sendResponse(-1, callback, res);

    if (team.member_options.public) {
      team.members.push(currentUser.userKey);
    } else {
      team.awaiting.push(currentUser.userKey);
    }

    team.updated_at = new Date();
    team.save(function(error) {
      if (error) return callback(500, "error", res);

      currentUser.team = {
        name : team.name,
        token : team.team_key,
        expand : false
      };

      sendResponse(1, callback, res);
    })
  });
}

function leave(callback, res) {
  Teams.findOne({team_key : currentUser.token}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == "" || team == null) return sendResponse(1, callback, res);

    var i = team.members.indexOf(currentUser.userKey);
    if (i < 0) return sendResponse(1, callback, res);
    team.members.splice(i, 1);
    team.updated_at = new Date();
    team.save(function(error) {
      if (error) return callback(500, "error", res);

      sendResponse(1, callback, res);
    });
  });
}

function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {}

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "team" : currentUser.team
  };

  callback(200, resultArray, res);
}
