"use strict";

var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");

var currentUser = {
  ip : "",
  userKey : "",
  userSessionKey : "",
  userAuthKey : "",
  webApp : false,
  current_password : "",
  new_password : ""
};

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, user_validation_key, auth_key, session_key, current_password, new_password, webApp, callback, res) {
  currentUser.ip = ip;
  currentUser.userKey = user_validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.webApp = webApp;
  currentUser.current_password = Functions.hashedPasswordCreator(current_password.trim());
  currentUser.new_password = Functions.hashedPasswordCreator(new_password.trim());

  updateUserData(callback, res);
}

function updateUserData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey, password : currentUser.current_password}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    }

    if (profiles.length != "1") return callback(401, "error", res);
    var profile = profiles[0];
    profile.password = currentUser.new_password;
    profile.updated_at = new Date();

    profile.save(function(error) {
      if (error) {
        callback(500, "error", res);
      } else {
        sendResponse(1, callback, res);
      }
    });
  });
}

function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {}
  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate
  };

  callback(200, resultArray, res);
}
