"use strict";

var Profiles = modelRequire("profiles.js");
var Constants = modelRequire("constants.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.webApp = false;
  this.stacks = 0;
  this.coins = 0;
  this.xrt = 0;
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.webApp = webApp;

  getConstant(callback, res);
}

function getConstant(callback, res) {
  Constants.find(function(error, constants) {
    if (error) {
      callback(500, "error", res);
    } else {
      var constant = constants[0];
      currentUser.xrt = constant.sc_xrt;
      verifyGatherData(callback, res);
    }
  }).limit(1);
}

function verifyGatherData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];

        var stacks = profile.stacks;
        var coins = profile.coins;
        var xrt = currentUser.xrt;
        var exchanged;

        if (coins < xrt) {
          exchanged = 0;
        } else {
          exchanged = (coins - (coins % xrt)) / xrt;
          coins-= exchanged * xrt;
          stacks+=exchanged;
        }

        profile.stacks = stacks;
        profile.coins = coins;
        profile.updated_at = new Date();
        profile.save(function(error) {
          if (error) return callback(500, "error", res);

          currentUser.stacks = stacks;
          currentUser.coins = coins;
          sendResponse(callback, res);
        });
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function sendResponse(callback, res) {
  var resultArray = {}
  var resultCode = 1;
  var resultDate = new Date();

  resultArray = {
   "webApp" : currentUser.webApp,
   "resultCode" : resultCode,
   "resultDate" : resultDate,
   "stacks" : currentUser.stacks,
   "coins" : currentUser.coins
 };

  callback(200, resultArray, res);
}
