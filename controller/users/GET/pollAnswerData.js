"use strict";

var Profiles = modelRequire("profiles.js");
var Constants = modelRequire("constants.js");
var Responses = modelRequire("responses.js");
var Polls = modelRequire("polls.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.lang = "";
  this.webApp = false;
  this.about = {
    sex : "",
    ethnicity : "",
    age_group : ""
  };
  this.answered_polls = [];
  this.category_token = "";
  this.pollDetails = {
    token : "",
    available : true,
    public : true,
    targeted : true,
    answered : false,
    title : "",
    lang : "",
    description : "",
    collectables : [],
    coins : 0,
    image_url : "",
    question : "",
    options : [],
    analytics : {},
    next_poll : "",
    labs_created : false,
    labs_option : null,
    labs : {}
  };
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, token, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.lang = lang;
  currentUser.pollDetails.token = token;
  currentUser.webApp = webApp;

  coinsMultiplier();
  verifyGatherData(callback, res);
}

function coinsMultiplier() {
  Constants.find(function(error, constants) {
    if (error) {
      currentUser.pollDetails.coins = 1;
    } else {
      var constant = constants[0];
      currentUser.pollDetails.coins = constant.coins_xrt;
    }
  }).limit(1);
}

function verifyGatherData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];

        currentUser.answered_polls = profile.polls_answered;
        currentUser.about.sex = profile.about.sex;
        currentUser.about.ethnicity = profile.about.ethnicity;
        currentUser.about.age_group = Functions.getAgeGroup(profile.about.birth_date);

        if (currentUser.answered_polls.includes(currentUser.pollDetails.token)) {
          currentUser.pollDetails.answered = true;
          currentUser.pollDetails.coins = 0;
        }

        getPoll(callback, res);

      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function getPoll(callback, res) {
  Polls.findOne({deleted : false, poll_token : currentUser.pollDetails.token, creator_key : {$ne : currentUser.userKey}}, function(error, poll) {
    if (error) return callback(500, "error", res);
    if (poll === "" || poll == null) {
      currentUser.pollDetails.coins = 0;
      currentUser.pollDetails.available = false;
      sendResponse(callback, res);
      return;
    }

    currentUser.pollDetails.available = true;
    if (poll.poll_details.target_sex != currentUser.about.sex &&  poll.poll_details.target_sex != "both") {
      currentUser.pollDetails.targeted = false;
      return sendResponse(callback, res);
    }
    if (poll.poll_details.target_ethnicity != currentUser.about.ethnicity &&  poll.poll_details.target_ethnicity != "a") {
      currentUser.pollDetails.targeted = false;
      return sendResponse(callback, res);
    }
    if (poll.poll_details.target_age_group != currentUser.about.age_group &&  poll.poll_details.target_age_group != "a") {
      currentUser.pollDetails.targeted = false;
      return sendResponse(callback, res);
    }
    currentUser.pollDetails.targeted = true;
    currentUser.pollDetails.labs_created = poll.poll_details.lab_created;
    currentUser.pollDetails.title = poll.poll_details.name;
    currentUser.pollDetails.lang = poll.poll_details.lang;
    currentUser.pollDetails.public = poll.poll_details.public_poll;
    currentUser.pollDetails.description = poll.poll_details.description;
    currentUser.pollDetails.collectables = poll.poll_details.collected;
    for (var i = 0; i < currentUser.pollDetails.collectables.length; i++) {
      if (currentUser.pollDetails.collectables[i] === "employment") {
        currentUser.pollDetails.collectables[i] = "employment_status";
        return;
      }

      if (currentUser.pollDetails.collectables[i] === "relationship") {
        currentUser.pollDetails.collectables[i] = "relationship_status";
        return;
      }

      if (currentUser.pollDetails.collectables[i] === "education") {
        currentUser.pollDetails.collectables[i] = "education_level";
        return;
      }
    }
    currentUser.pollDetails.coins*=poll.poll_details.coins;
    currentUser.pollDetails.image_url = poll.poll_details.image_url;
    currentUser.pollDetails.question = poll.question_details.question;
    currentUser.pollDetails.options = poll.question_details.options;
    currentUser.category_token = poll.poll_details.category_token;

    if (currentUser.pollDetails.labs_created) {
      if (poll.labs.type == "quiz") {
        currentUser.pollDetails.labs_option = 0;
        currentUser.pollDetails.labs = {};
      } else if (poll.labs.type == "emoji") {
        currentUser.pollDetails.labs_option = 2;
        currentUser.pollDetails.labs = {};
      } else if (poll.labs.type == "game") {
        currentUser.pollDetails.labs_option = 3;
        currentUser.pollDetails.labs = {
          timed : poll.labs.timed,
          time : poll.labs.max_time,
          questions : poll.labs.gameQuestions
        };
      }
    }

    getNextPoll(callback, res);
  });
}

function getNextPoll(callback, res) {
  Polls.find({deleted : false, poll_token : {$nin : currentUser.answered_polls, $ne : currentUser.pollDetails.token}, "poll_details.category_token" : currentUser.category_token, creator_key : {$ne : currentUser.userKey},
  $or : [{"poll_details.target_sex" : currentUser.about.sex, "poll_details.target_ethnicity" : currentUser.about.ethnicity, "poll_details.target_age_group" : currentUser.about.age_group}, {"poll_details.target_sex" : "both", "poll_details.target_ethnicity" : "a", "poll_details.target_age_group" : "a"}]}, function(error, polls) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (polls.length == "1") {
        var poll = polls[0];

        currentUser.pollDetails.next_poll = poll.poll_details.answer_url;

        if (currentUser.pollDetails.answered) {
          formulateAnalytics(callback, res);
        } else {
          currentUser.pollDetails.options.forEach(function(option) {
            var analytic = {
              "percentage" : 0
            }
            currentUser.pollDetails.analytics[option] = analytic;
          });
          sendResponse(callback, res);
        }
      } else {

        if (currentUser.pollDetails.answered) {
          formulateAnalytics(callback, res);
        } else {
          currentUser.pollDetails.options.forEach(function(option) {
            var analytic = {
              "percentage" : 0
            }
            currentUser.pollDetails.analytics[option] = analytic;
          });
          sendResponse(callback, res);
        }
      }
    }
  }).limit(1);
}

function formulateAnalytics(callback, res) {
  Responses.find({questionnaire_token : currentUser.pollDetails.token, questionnaire_type : "poll"}, function(error, responses) {
    if (error) return sendResponse(callback, res);

    currentUser.pollDetails.analytics = Functions.analyticPercentagesPoll(responses);

    sendResponse(callback, res);
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "poll_details" : currentUser.pollDetails,
  };

  callback(200, resultArray, res);
}
