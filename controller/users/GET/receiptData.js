"use strict";

var Profiles = modelRequire("profiles.js");
var Constants = modelRequire("constants.js");
var Polls = modelRequire("polls.js");
var Surveys = modelRequire("surveys.js");
var Categories = modelRequire("categories.js");
var Functions = rootRequire("/controller/globalFunctions.js");

var currentUser = {
  ip : "",
  userKey : "",
  userAuthKey : "",
  userSessionKey : "",
  lang : "",
  webApp : false,
  viewable : true,
  token : "",
  type : "",
  details : {
    lang : "",
    name : "",
    description : "",
    category : "",
    image : "",
    questions : 0,
    sc_amount : 0,
    edit_url : "",
    answer_url : "",
    answer_code_url : ""
  },
  payment : {
    receipt : "",
    price : 0.00,
    extra : 0.00,
    total : 0.00
  }
};

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, type, token, webApp, callback, res) {
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.lang = lang;
  currentUser.token = token;
  currentUser.type = type;
  currentUser.webApp = webApp;

  if (currentUser.type == "surveys") {
    surveyRG(callback, res);
  } else if (currentUser.type == "polls") {
    pollRG(callback, res);
  }
}

function surveyRG(callback, res) {
  Surveys.find({deleted : false, creator_key : currentUser.userKey, survey_token : currentUser.token}, function(error, surveys) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (surveys.length == "1") {
        var survey = surveys[0];

        currentUser.viewable = true;
        currentUser.details.lang = survey.survey_details.lang;
        currentUser.details.name = survey.survey_details.name;
        currentUser.details.description = survey.survey_details.description;
        currentUser.details.category = survey.survey_details.category_token;
        currentUser.details.image = survey.survey_details.image_url;
        currentUser.details.questions = survey.question_details.amount;
        currentUser.details.sc_amount = survey.survey_details.stacks;
        currentUser.details.edit_url = Functions.editURL(survey.survey_token, 0);
        currentUser.details.answer_url = Functions.answerURL(survey.survey_token, 0);
        currentUser.details.answer_code_url = Functions.answerURL(survey.survey_token, 0);

        currentUser.payment.receipt = survey.payment_details.receipt_token;
        currentUser.payment.price = survey.payment_details.survey_charge;
        currentUser.payment.extra = survey.payment_details.extra_stacks;
        currentUser.payment.total = survey.payment_details.order_total;

        getCategory(callback, res);
      } else {
        currentUser.viewable = false;
        sendResponse(callback, res);
      }
    }
  }).limit(1);
}

function pollRG(callback, res) {
  Polls.find({deleted : false, creator_key : currentUser.userKey, poll_token : currentUser.token}, function(error, polls) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (polls.length == "1") {
        var poll = polls[0];

        currentUser.viewable = true;
        currentUser.details.lang = poll.poll_details.lang;
        currentUser.details.name = poll.poll_details.name;
        currentUser.details.description = poll.poll_details.description;
        currentUser.details.category = poll.poll_details.category_token;
        currentUser.details.image = poll.poll_details.image_url;
        currentUser.details.sc_amount = poll.poll_details.coins;
        currentUser.details.edit_url = Functions.editURL(poll.poll_token, 1);
        currentUser.details.answer_url = Functions.answerURL(poll.poll_token, 1);
        currentUser.details.answer_code_url = Functions.answerURL(poll.poll_token, 1);

        currentUser.payment.receipt = poll.payment_details.receipt_token;
        currentUser.payment.price = poll.payment_details.order_total;
        currentUser.payment.total = poll.payment_details.order_total;

        getCategory(callback, res);
      } else {
        currentUser.viewable = false;
        sendResponse(callback, res);
      }
    }
  }).limit(1);
}

function getCategory(callback, res) {
  Categories.find({category_token : currentUser.details.category}, function(error, categories) {
    if (error) {
      callback(500, "error", res);
    } else {
      currentUser.details.category = categories[0].category_name;
      sendResponse(callback, res);
    }
  }).limit(1);
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
     "resultDate" : resultDate,
     "viewable" : currentUser.viewable,
     "details" : currentUser.details,
     "payment" : currentUser.payment
  };

  callback(200, resultArray, res);
}
