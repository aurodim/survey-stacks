"use strict";

var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.webApp = false;
  this.accountInfo = {
    profile_picture_url : "",
    motto : "",
    since : 0
  };
  this.achievementsInfo = {
    stacks_earned : 0,
    coins_earned : 0,
    questions_answered : 0,
    surveys_answered : 0,
    rewards_claimed : 0
  };
  this.contactInfo = {
    email : "",
    phone_number : "",
    website : "",
    facebook : "",
    instagram : ""
  };
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.webApp = webApp;

  verifyGatherData(callback, res);
};

function verifyGatherData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];

        currentUser.accountInfo.profile_theme_url = Functions.imageTransform(profile.profile_theme_url, 900, null);
        currentUser.accountInfo.profile_picture_url = Functions.imageTransform(profile.profile_picture_url, 200, 200);
        currentUser.accountInfo.motto = profile.motto;
        currentUser.accountInfo.since = new Date(profile.created_at).getTime();
        currentUser.achievementsInfo.coins_earned = profile.achievements.lifetime_coins;
        currentUser.achievementsInfo.stacks_earned = profile.achievements.lifetime_stacks;
        currentUser.achievementsInfo.questions_answered = profile.achievements.questions_answered;
        currentUser.achievementsInfo.surveys_answered = profile.achievements.surveys_answered;
        currentUser.achievementsInfo.rewards_claimed = profile.achievements.rewards_claimed;
        currentUser.contactInfo.email = profile.contact_info.email;
        currentUser.contactInfo.phone_number = profile.contact_info.phone_number;
        currentUser.contactInfo.website = profile.contact_info.website;
        currentUser.contactInfo.facebook = profile.contact_info.facebook;
        currentUser.contactInfo.instagram = profile.contact_info.instagram;

        sendResponse(callback, res);
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {
   "webApp" : currentUser.webApp,
   "resultCode" : resultCode,
   "resultDate" : resultDate,
   "accountInfo" : currentUser.accountInfo,
   "achievementsInfo" : currentUser.achievementsInfo,
   "contactInfo" : currentUser.contactInfo
  };

  callback(200, resultArray, res);
}
