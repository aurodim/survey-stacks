"use strict";

var Profiles = modelRequire("profiles.js");
var Rewards = modelRequire("rewards.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.lang = "";
  this.webApp = false;
  this.stacks= 0;
  this.coins = 0;
  this.rewards_wishlisted = [];
  this.rewards_afford = [];
  this.rewards_other = [];
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.lang = lang;
  currentUser.webApp = webApp;

  verifyGatherData(callback, res);
}

function verifyGatherData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];

        currentUser.stacks = profile.stacks;
        currentUser.coins = profile.coins;
        currentUser.rewards_wishlisted = profile.wishlist;

        rewardsData(callback, res);
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  });
}

function rewardsData(callback, res) {
  Rewards.find({quantity : {$gt : 0}}, function(error, rewards) {
    if (error) return callback(500, "error", res);

    currentUser.rewards_afford = [];
    currentUser.rewards_other = [];

    rewards.forEach(function(reward) {
      var rewardObject = {
        token : reward.reward_token,
        image_url : reward.image_url,
        description : reward.description,
        stacks : (reward.cost * reward.discount).toFixed(0),
        stacks_needed : 0,
        stacks_remaining : 0,
        progress: 0,
        started : false,
        wishlisted : currentUser.rewards_wishlisted.includes(reward.reward_token),
      };

      if (rewardObject.stacks <= currentUser.stacks) {
        rewardObject.stacks_remaining = currentUser.stacks - rewardObject.stacks;
        rewardObject.stacks_remaining = rewardObject.stacks_remaining;
        currentUser.rewards_afford.push(rewardObject);
      } else {
        rewardObject.stacks_needed = rewardObject.stacks - currentUser.stacks;
        rewardObject.stacks_needed = rewardObject.stacks_needed;
        currentUser.rewards_other.push(rewardObject);
      }
    });

    sendResponse(callback, res);
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
     "resultDate" : resultDate,
     "rewards_afford" : currentUser.rewards_afford,
     "rewards_other" : currentUser.rewards_other
  };

  callback(200, resultArray, res);
}
