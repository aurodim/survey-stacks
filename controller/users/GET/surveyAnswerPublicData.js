"use strict";

var Profiles = modelRequire("profiles.js");
var Constants = modelRequire("constants.js");
var Surveys = modelRequire("surveys.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.lang = "";
  this.webApp = false;
  this.about = {
    sex : "",
    ethnicity : "",
    age_group : ""
  };
  this.answered_surveys = [];
  this.order = [];
  this.free_response = [];
  this.checkbox = [];
  this.boolean = [];
  this.radio = [];
  this.menu = [];
  this.answers_array = [];
  this.surveyDetails = {
    available : true,
    public : true,
    targeted : true,
    answered : false,
    token : "",
    lang : "",
    title : "",
    description : "",
    stacks : 0,
    questions : [],
    labs_created : false,
    labs_option : null,
    labs : {}
  };
};

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, lang, token, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.lang = lang;
  currentUser.surveyDetails.token = token;
  currentUser.webApp = webApp;

  stacksMultiplier();
  getSurvey(callback, res);
}

function stacksMultiplier() {
  Constants.find(function(error, constants) {
    if (error) {
      currentUser.surveyDetails.stacks = 10;
    } else {
      var constant = constants[0];
      currentUser.surveyDetails.stacks = constant.stacks_xrt;
    }
  }).limit(1);
}

function getSurvey(callback, res) {
  Surveys.findOne({deleted : false, survey_token : currentUser.surveyDetails.token, "survey_details.public_survey" : true}, function(error, survey) {
    if (error) return callback(500, "error", res);
    if (survey === "" || survey == null) {
      currentUser.surveyDetails.stacks = 0;
      currentUser.surveyDetails.available = false;
      sendResponse(callback, res);
      return;
    }

    currentUser.surveyDetails.available = true;
    currentUser.surveyDetails.targeted = true;
    currentUser.surveyDetails.labs_created = survey.survey_details.lab_created;
    currentUser.surveyDetails.title = survey.survey_details.name;
    currentUser.surveyDetails.lang = survey.survey_details.lang;
    currentUser.surveyDetails.public = survey.survey_details.public_survey;
    currentUser.surveyDetails.description = survey.survey_details.description;
    currentUser.surveyDetails.stacks*=survey.survey_details.stacks;
    currentUser.surveyDetails.image_url = survey.survey_details.image_url;
    currentUser.order = survey.question_details.order;
    currentUser.free_response = survey.question_details.free_response;
    currentUser.checkbox = survey.question_details.checkbox;
    currentUser.boolean = survey.question_details.boolean;
    currentUser.radio = survey.question_details.radio;
    currentUser.menu = survey.question_details.menu;

    if (currentUser.surveyDetails.labs_created) {
      if (survey.labs.type == "quiz") {
        currentUser.surveyDetails.labs_option = 0;
        currentUser.surveyDetails.labs = {
          out_of : survey.labs.out_of
        };
      } else if (survey.labs.type == "key_needed") {
        currentUser.surveyDetails.labs_option = 1;
        currentUser.surveyDetails.labs = {};
      } else if (survey.labs.type == "emoji") {
        currentUser.surveyDetails.labs_option = 2;
        currentUser.surveyDetails.labs = {};
      } else if (survey.labs.type == "game") {
        currentUser.surveyDetails.labs_option = 3;
        currentUser.surveyDetails.labs = {
          timed : survey.labs.timed,
          time : survey.labs.max_time
        }
      }
    }

    arrangeQuestions(callback, res);
  });
}

function arrangeQuestions(callback, res) {
  var answers = [];

  currentUser.order.forEach(function(question, i) {
    var type = question.split("_")[0];
    var index = question.split("_")[1];
    var selectedQ;
    var questionObj;

    switch (type) {
      case "fr":
        selectedQ = currentUser.free_response[index];
        questionObj = {
          type : type,
          question : selectedQ.question,
          required : selectedQ.options.required,
          pattern : Functions.patternRegex(selectedQ.options.pattern)
        };
        answers.push("");
        break;
      case "cbr":
        selectedQ = currentUser.checkbox[index];
        questionObj = {
          type : type,
          question : selectedQ.question,
          required : selectedQ.options.required,
          options : selectedQ.responses
        };
        answers.push([]);
        break;
      case "mr":
        selectedQ = currentUser.menu[index];
        questionObj = {
          type : type,
          question : selectedQ.question,
          required : selectedQ.options.required,
          options : selectedQ.responses
        };
        answers.push(null);
        break;
      case "br":
        selectedQ = currentUser.boolean[index];
        questionObj = {
          type : type,
          question : selectedQ.question,
          required : selectedQ.options.required,
        };
        answers.push(false);
        break;
      case "rr":
        selectedQ = currentUser.radio[index];
        questionObj = {
          type : type,
          question : selectedQ.question,
          required : selectedQ.options.required,
          options : selectedQ.responses
        };
        answers.push(null);
        break;
    }

    if (currentUser.surveyDetails.labs_option === 0) {
      questionObj.worth = selectedQ.options.worth;
    }

    currentUser.surveyDetails.questions.push(questionObj);

    if (i == currentUser.order.length - 1) {
      currentUser.answers_array = answers;
      sendResponse(callback, res);
    }
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {}

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
     "resultDate" : resultDate,
     "survey_details" : currentUser.surveyDetails,
     "answers_blank" : currentUser.answers_array
  };

  callback(200, resultArray, res);
}
