"use strict";

var Profiles = modelRequire("profiles.js");
var Teams = modelRequire("teams.js");
var Polls = modelRequire("polls.js");
var Categories = modelRequire("categories.js");
var Constants = modelRequire("constants");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.lang = "";
  this.webApp = false;
  this.coins = 0;
  this.coins_m = 0;
  this.polls = [];
  this.polls_answered = [];
  this.bookmarks = [];
  this.about = {
    sex : "",
    ethnicity : [],
    age_group : ""
  };
  this.preferences = {
    monosex_filter : false,
    translated_filter : false,
    categories : [],
    companies : []
  };
}

var currentUser;

var service = {};
service.initialize = initialize;
module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.lang = lang;
  currentUser.webApp = webApp;

  coinsExchange();
  verifyGatherData(callback, res);
}

function coinsExchange() {
  Constants.find(function(error, constants) {
    if (error) {
      currentUser.coins_m = 1;
    } else {
      var constant = constants[0];
      currentUser.coins_m = constant.coins_xrt;
    }
  }).limit(1);
}

function verifyGatherData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];

        currentUser.polls_answered = profile.polls_answered;
        currentUser.bookmarks = profile.bookmarks_polls;

        currentUser.about.sex = profile.about.sex;
        currentUser.about.ethnicity = profile.about.sex;
        currentUser.about.age_group = Functions.getAgeGroup(profile.about.birth_date);

        currentUser.preferences.monosex_filter = profile.preferences.monosex;
        currentUser.preferences.translated_filter = profile.preferences.translated;
        currentUser.preferences.categories = profile.preferences.categories_tokens;
        currentUser.preferences.companies = profile.preferences.companies_tokens;

        findPolls(callback, res);

      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function findPolls(callback, res) {
  Polls.find({blocked : false, deleted : false, poll_token : {$nin : currentUser.polls_answered}, creator_key : {$ne : currentUser.userKey},
     $or : [{"poll_details.target_sex" : currentUser.about.sex, "poll_details.target_ethnicity" : currentUser.about.ethnicity, "poll_details.target_age_group" : currentUser.about.age_group},
    {"poll_details.target_sex" : "both", "poll_details.target_ethnicity" : "a", "poll_details.target_age_group" : "a"}]},
      function(error, polls) {

    if(error) return callback(500, "error", res);

    if (polls.length == 0) return sendResponse(callback, res);

    var completion = [];
    currentUser.polls = [];
    polls.forEach(function(poll, i) {
      completion.push(false);

      var pollObj = {
        token : poll.poll_token,
        answer_url : Functions.answerURL(poll.poll_token, 1),
        image_url : poll.poll_details.image_url,
        title : poll.poll_details.name,
        surveyor_id : poll.creator_key,
        creator : "",
        creator_image_url : "",
        question : poll.question_details.question,
        description : poll.poll_details.description,
        coins : poll.poll_details.coins * currentUser.coins_m,
        interest : true,
        bookmarked : currentUser.bookmarks.includes(poll.poll_token)
      };

      if (currentUser.preferences.monosex_filter && poll.poll_details.target_sex == "both") {
        pollObj.interest = false;
      }

      if (currentUser.preferences.translated_filter && poll.poll_details.lang != currentUser.lang) {
        pollObj.interest = false;
      }

      if (!currentUser.preferences.categories.includes(poll.poll_details.category_token) && !currentUser.preferences.companies.includes(poll.creator_key)) {
        pollObj.interest = false;
      }

      if (!currentUser.preferences.categories.includes(poll.poll_details.category_token)) {
        pollObj.interest = false;
      }

      getUsername(poll.creator_key, poll.poll_token, function(username, pfp, error) {
        if(error == "d") return callback(500, "error", res);
        if(error == "a") return callback(401, "error", res);

        pollObj.creator = username;
        pollObj.creator_image_url = Functions.imageTransform(pfp, 40, 40)
        currentUser.polls.push(pollObj);

        completion[i] = true;

        if (!completion.includes(false)) {
          sendResponse(callback, res);
        }
      });
    });
  }).sort({"updated_at" : 1});
}

function getUsername(key, token, callback) {
  var ft = key.split('.')[1];

  if (ft.substr(ft.length - 4, ft.length) == "AuRo") {
    Profiles.findOne({deleted : false, user_key : key}, function(error, profile) {
      if (error) return callback(null, "d");
      if (profile === "" || profile == null) return callback(null, "d");

      callback(profile.username, profile.profile_image_url, null);
    });
  } else {
    Teams.findOne({team_key : key}, function(error, team) {
      if (error) return callback(null, null, "d");
      if (team == null || team === "") return callback(null, null, "u");


      callback(team.nickname, team.profile_picture_url, null);
    });
  }
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "pollsData" : currentUser.polls,
  };

  callback(200, resultArray, res);
}
