"use strict";

var Profiles = modelRequire("profiles.js");
var Constants = modelRequire("constants.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.lang = "";
  this.webApp = false;
  this.about_completed = false;
  this.stacks = 0;
  this.about = {
    sex : "",
    ethnicity : [],
    lang : [],
    education : "",
    relationship : "",
    employment : "",
    religion : ""
  };
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.lang = lang;
  currentUser.webApp = webApp;

  stackExchange();
  verifyGatherData(callback, res);
}

function stackExchange() {
  Constants.find(function(error, constants) {
    if (error) {
      currentUser.stacks = 10;
    } else {
      var constant = constants[0];
      currentUser.stacks = constant.stacks_xrt * 10;
    }
  }).limit(1);
}

function verifyGatherData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];

        currentUser.about_completed = profile.setup_completed.about;
        currentUser.about.sex = profile.about.sex;
        currentUser.about.ethnicity = profile.about.ethnicity;
        currentUser.about.lang = profile.about.languages;
        currentUser.about.education = profile.about.education;
        currentUser.about.relationship = profile.about.relationship_status;
        currentUser.about.employment = profile.about.employment_status;
        currentUser.about.religion = profile.about.religion;

        sendResponse(callback, res);
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "about_completed" : currentUser.about_completed,
    "stacks" : currentUser.stacks,
    "about" : currentUser.about
  };

  callback(200, resultArray, res);
}
