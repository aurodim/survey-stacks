"use strict";

var Profiles = modelRequire("profiles.js");
var Companies = modelRequire("companies.js");
var Categories = modelRequire("categories.js");
var Constants = modelRequire("constants.js");
var Functions = rootRequire("/controller/globalFunctions.js");
var Translations = rootRequire("/controller/translations.js");

var currentUser = {
  ip : "",
  userKey : "",
  userAuthKey : "",
  userSessionKey : "",
  webApp : false,
  accountInfo : {
    ep : "",
  },
  contactInfo : {
    phone_number : "",
    email : "",
    website : "",
    facebook : "",
    instagram : ""
  },
  notificationsInfo : {
    email_message_notifications : false,
    match_preferences_notifications : false
  },
  aboutInfo : {
    blank : false,
    sex : "",
    ethnicity : [],
    lang : [],
    education : "",
    relationship : "",
    employment : "",
    religion : ""
  },
  paymentInfo : {
    blank : false,
    labs_membership : false,
    labs_expire : "",
    labs_price : 2.00
    // card_type : "",
    // card_name : "",
    // card_ending_digits : "",
    // card_expiration_date : "",
    // street_address : "",
    // apt_number : "",
    // city : "",
    // zip_code : 0,
    // state : "",
    // country : "",
  },
  preferencesInfo : {
    blank : false,
    sex : false,
    translated : false,
    categories : [],
    companies : []
  }
};

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, webApp, callback, res) {
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.webApp = webApp;

  getDataProfile(callback, res);
}

function getDataProfile(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      var profile = profiles[0];

      currentUser.accountInfo.ep = profile.email_phone;

      currentUser.contactInfo.phone_number = profile.contact_info.phone_number;
      currentUser.contactInfo.email = profile.contact_info.email;
      currentUser.contactInfo.website = profile.contact_info.website;
      currentUser.contactInfo.facebook = profile.contact_info.facebook;
      currentUser.contactInfo.instagram = profile.contact_info.instagram;

      currentUser.notificationsInfo.email_message_notifications = profile.notification_settings.email_message;
      currentUser.notificationsInfo.match_preferences_notifications = profile.notification_settings.match_preferences;

      currentUser.aboutInfo.blank = !profile.setup_completed.about;
      currentUser.aboutInfo.sex = profile.about.sex;
      currentUser.aboutInfo.religion = profile.about.religion;
      currentUser.aboutInfo.relationship = profile.about.relationship_status;
      currentUser.aboutInfo.employment = profile.about.employment_status;
      currentUser.aboutInfo.education = profile.about.education;
      currentUser.aboutInfo.ethnicity = profile.about.ethnicity;
      currentUser.aboutInfo.lang = profile.about.languages;

      currentUser.preferencesInfo.blank = !profile.setup_completed.preferences;
      currentUser.preferencesInfo.sex = profile.preferences.monosex;
      currentUser.preferencesInfo.translated = profile.preferences.translated;
      var catkeys = profile.preferences.categories_tokens;
      var comkeys = profile.preferences.companies_tokens;

      currentUser.paymentInfo.blank = !profile.setup_completed.payment;
      currentUser.paymentInfo.labs_membership = profile.payment.labs_membership;
      currentUser.paymentInfo.labs_expire = new Date(profile.payment.labs_expire).getTime();
      // currentUser.paymentInfo.card_type = asterizeData(profile.payment.card_type);
      // currentUser.paymentInfo.card_name = asterizeAllButOne(profile.payment.card_name);
      // currentUser.paymentInfo.card_ending_digits = last4Digits(profile.payment.card_digits);
      // currentUser.paymentInfo.card_expiration_date = asterizeData(profile.payment.card_expiration_date);
      // currentUser.paymentInfo.street_address = asterizeData(profile.payment.street_address);
      // currentUser.paymentInfo.apt_number = asterizeData(profile.payment.apt_number);
      // currentUser.paymentInfo.city = asterizeAllButOne(profile.payment.city);
      // currentUser.paymentInfo.zip_code = asterizeAllButOne(profile.payment.zipcode);
      // currentUser.paymentInfo.state = asterizeData(profile.payment.state);
      // currentUser.paymentInfo.country = asterizeData(profile.payment.country);

      if (!currentUser.paymentInfo.labs_membership) {
        getLabsMem();
      }
      getCategories(catkeys, comkeys, callback, res);
    }
  }).limit(1);
}

function getLabsMem() {
  Constants.findOne({}, function(error, constants) {
    if (error) return;

    currentUser.paymentInfo.labs_price = constants.labs_mem;
  });
}

function getCategories(catkeys, comkeys, callback, res) {
  Categories.find({category_token : { $in : catkeys}}, function(error, categories) {
    if (error) {
      callback(500, "error", res);
    } else {
      var categories_a = [];
      categories.forEach(function(category) {
        categories_a.push(category.category_name);
      });
      currentUser.preferencesInfo.categories = categories_a;
      getCompanies(comkeys, callback, res);
    }
  });
}

function getCompanies(comkeys, callback, res) {
  Companies.find({company_token : { $in : comkeys}}, function(error, companies) {
    if (error) {
      callback(500, "error", res);
    } else {
      var companies_a = [];
      companies.forEach(function(company) {
        companies_a.push(company.company_name);
      });
      currentUser.preferencesInfo.companies = companies_a;
      sendResponse(callback, res);
    }
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {}

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
   "accountInfo" : currentUser.accountInfo,
   "contactInfo" : currentUser.contactInfo,
   "notificationsInfo" : currentUser.notificationsInfo,
   "aboutInfo" : currentUser.aboutInfo,
   "preferencesInfo" : currentUser.preferencesInfo,
   "paymentInfo" : currentUser.paymentInfo
  };

  callback(200, resultArray, res);
}

function asterizeData(data) {
  if (data === "0" || data === 0) {
    return "";
  }

  var dataString = data.toString().split('');
  var every = 2;
  var replaceWith = "*"
  for (var i = every-1; i < dataString.length; i+=every) {
    dataString[i] = replaceWith;
  }

  return dataString.join("");
}

function asterizeAllButOne(data) {
  if (data === "" || data === 0) {
    return "";
  }

  var dataString = data.toString();
  var randomAs = Math.floor((Math.random() * 15) + 1);
  var first = dataString[0];
  var full = first;

  for (var i = 0; i < randomAs; i++) {
    full += "*";
  }

  return full;
}

function last4Digits(digits) {
  if (digits !== 0 && digits !== "0") {
    digits = digits.toString();
    var length = digits.length;
    var lastFour = length - 4;

    return digits.substring(lastFour);
  } else {
    return ""
  }
}
