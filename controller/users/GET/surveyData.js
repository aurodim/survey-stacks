"use strict";

var Profiles = modelRequire("profiles.js");
var Teams = modelRequire("teams.js");
var Surveys = modelRequire("surveys.js");
var Categories = modelRequire("categories.js");
var Constants = modelRequire("constants");
var Functions = rootRequire("/controller/globalFunctions.js");

/*
  *
  *  SURVEY DATA ARRANGEMENT BY IMPORTANT BASED ON USER PREFERENCES
  *
  *
  *  PREFERENCES : User sex -> Translated -> Categories -> Companies
  *  ABOUT : Sex -> Ethnicity -> Age Group
  *
  * Different Queries
  *
  * Target Sex = (both||user_sex) -> lang = user_lang -> categories in user categories -> creator in companies
  * Target Sex = (both||user_sex) -> categories in user categories -> creator in companies
  *
  *
*/

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.lang = "";
  this.webApp = false;
  this.stacks = 0;
  this.stacks_m = 0;
  this.surveys = [];
  this.surveys_answered = [];
  this.bookmarks = [];
  this.about = {
    sex : "",
    ethnicity : [],
    age_group : ""
  };
  this.preferences = {
    monosex_filter : false,
    translated_filter : false,
    categories : [],
    companies : []
  };
}

var currentUser;

var service = {};
service.initialize = initialize;
module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.lang = lang;
  currentUser.webApp = webApp;

  stackExchange();
  verifyGatherData(callback, res);
}

function stackExchange() {
  Constants.find(function(error, constants) {
    if (error) {
      currentUser.stacks_m = 1;
    } else {
      var constant = constants[0];
      currentUser.stacks_m = constant.stacks_xrt;
    }
  }).limit(1);
}

function verifyGatherData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];

        currentUser.surveys_answered = profile.surveys_answered;
        currentUser.bookmarks = profile.bookmarks;

        currentUser.about.sex = profile.about.sex;
        currentUser.about.ethnicity = profile.about.sex;
        currentUser.about.age_group = Functions.getAgeGroup(profile.about.birth_date);

        currentUser.preferences.monosex_filter = profile.preferences.monosex;
        currentUser.preferences.translated_filter = profile.preferences.translated;
        currentUser.preferences.categories = profile.preferences.categories_tokens;
        currentUser.preferences.companies = profile.preferences.companies_tokens;

        findSurveys(callback, res);
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function findSurveys(callback, res) {
  Surveys.find({blocked : false, deleted : false, survey_token : {$nin : currentUser.surveys_answered}, creator_key : {$ne : currentUser.userKey},
     $or : [{"survey_details.target_sex" : currentUser.about.sex, "survey_details.target_ethnicity" : currentUser.about.ethnicity, "survey_details.target_age_group" : currentUser.about.age_group, creator_key : {$in : currentUser.preferences.companies}},
      {"survey_details.target_sex" : "both", "survey_details.target_ethnicity" : "a", "survey_details.target_age_group" : "a"}]},
      function(error, surveys) {

    if (error) return callback(500, "error", res);
    if (surveys.length == "0") {
      return sendResponse(callback, res);
    }

    currentUser.surveys = [];
    var completion = [];
    surveys.forEach(function(survey, i) {
      completion.push(false);

      var surveyObj = {
        token : survey.survey_token,
        answer_url : Functions.answerURL(survey.survey_token, 0),
        image_url : survey.survey_details.image_url,
        title : survey.survey_details.name,
        surveyor_id : survey.creator_key,
        creator : "",
        creator_image_url : "",
        questions : survey.question_details.amount,
        description : survey.survey_details.description,
        stacks : survey.survey_details.stacks * currentUser.stacks_m,
        bookmarked : currentUser.bookmarks.includes(survey.survey_token)
      };

      if (currentUser.preferences.monosex_filter && survey.survey_details.target_sex == "both") {
        if (i == surveys.length - 1) return sendResponse(callback, res);
        return true;
      }

      if (currentUser.preferences.translated_filter && survey.survey_details.lang != currentUser.lang) {
        if (i == surveys.length - 1) return sendResponse(callback, res);
        return true;
      }

      getUsername(survey.creator_key, survey.survey_token, function(username, pfp, error) {
        if (i == surveys.length - 1 && (error == "d" || error == "u")) {
          return sendResponse(callback, res);
        }

        if(error == "d") return true;
        if(error == "u") return true;

        surveyObj.creator = username;
        surveyObj.creator_image_url = Functions.imageTransform(pfp, 40, 40)

        if (currentUser.preferences.categories.includes(survey.survey_details.category_token) || currentUser.preferences.companies.includes(survey.creator_key)) {
          currentUser.surveys.unshift(surveyObj);
        } else {
          currentUser.surveys.push(surveyObj);
        }

        completion[i] = true;

        if (!completion.includes(false)) {
          sendResponse(callback, res);
        }
      });
    });
  }).sort({"updated_at" : 1});
}

function getUsername(key, token, callback) {
  var ft = key.split('.')[1];

  if (ft.substr(ft.length - 4, ft.length) == "AuRo") {
    Profiles.findOne({user_key : key}, function(error, profile) {
      if (error) return callback(null, null, "d");
      if (profile == null || profile === "") return callback(null, null, "u");

      callback(profile.username, profile.profile_picture_url, null);
    });
  } else {
    Teams.findOne({team_key : key}, function(error, team) {
      if (error) return callback(null, null, "d");
      if (team == null || team === "") return callback(null, null, "u");


      callback(team.nickname, team.profile_picture_url, null);
    });
  }
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {}

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
     "resultDate" : resultDate,
     "surveysData" : currentUser.surveys,
  };

  callback(200, resultArray, res);
}
