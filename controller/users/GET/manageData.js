"use strict";

var Profiles = modelRequire("profiles.js");
var Surveys = modelRequire("surveys.js");
var Polls = modelRequire("polls.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.webApp = false;
  this.surveyData = [];
  this.pollData = [];
};

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.webApp = webApp;

  verify(callback, res);
}

function verify(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {

        gather(callback, res);
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function gather(callback, res) {
  async.parallel({
      surveys : function(cb) {
        gatherSurveys(cb);
      },
      polls : function(cb) {
        gatherPolls(cb);
      }
  }, function(error){
    if (error) return callback(500, "error", res);

    sendResponse(callback, res);
  });
}

function gatherSurveys(cb) {
  Surveys.find({deleted : false, creator_key : currentUser.userKey}, function(error, surveys) {
    if (error) {
      return cb(true);
    }

    var data = [];

    surveys.forEach(function(survey) {
      var editO = {
        "edit_url" : Functions.editURL(survey.survey_token, 0),
        "image_url" : Functions.imageTransform(survey.survey_details.image_url,null,200)
      };
      data.push(editO);
    });

    currentUser.surveyData = data;

    cb(null);
  });
}

function gatherPolls(cb) {
  Polls.find({deleted : false, creator_key : currentUser.userKey}, function(error, polls) {
    if (error) {
      return cb(true);
    }

    var data = [];

    polls.forEach(function(poll) {
      var editO = {
        "edit_url" : Functions.editURL(poll.poll_token, 1),
        "image_url" : Functions.imageTransform(poll.poll_details.image_url,null,200)
      };
      data.push(editO);
    });

    currentUser.pollData = data;

    cb(null);
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "surveys" : currentUser.surveyData,
    "polls" : currentUser.pollData
  };

  callback(200, resultArray, res);
}
