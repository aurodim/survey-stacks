"use strict";

var Profiles = modelRequire("profiles.js");
var Surveys = modelRequire("surveys.js");
var Polls = modelRequire("polls.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.webApp = false;
  this.surveyData = [];
  this.pollData = [];
  this.polls_b = [];
  this.surveys_b = [];
}

var currentUser;
var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.webApp = webApp;

  verify(callback, res);
}

function verify(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        currentUser.polls_b = profiles[0].bookmarks_polls;
        currentUser.surveys_b = profiles[0].bookmarks;

        gather(callback, res);
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function gather(callback, res) {
  async.parallel({
      surveys : function(cb) {
        gatherSurveys(cb);
      },
      polls : function(cb) {
        gatherPolls(cb);
      }
  }, function(error){
    if (error) return callback(500, "error", res);

    sendResponse(callback, res);
  });
}

function gatherSurveys(cb) {
  Surveys.find({deleted : false, survey_token : {$in : currentUser.surveys_b}}, function(error, surveys) {
    if (error) {
      return cb(true);
    }

    var data = [];

    surveys.forEach(function(survey) {
      var editO = {
        "token" : survey.survey_token,
        "answer_url" : survey.survey_details.answer_url,
        "image_url" : survey.survey_details.image_url,
        bookmarked : true
      };
      data.push(editO);
    });

    currentUser.surveyData = data;

    cb(null);
  });
}

function gatherPolls(cb) {
  Polls.find({deleted : false, poll_token : {$in : currentUser.polls_b}}, function(error, polls) {
    if (error) {
      return cb(true);
    }

    var data = [];

    polls.forEach(function(poll) {
      var editO = {
        "token" : poll.poll_token,
        "answer_url" : poll.poll_details.answer_url,
        "image_url" : poll.poll_details.image_url,
        bookmarked : true
      };
      data.push(editO);
    });

    currentUser.pollData = data;

    cb(null);
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "surveys_bookmarks" : currentUser.surveyData,
    "polls_bookmarks" : currentUser.pollData
  };

  callback(200, resultArray, res);
}
