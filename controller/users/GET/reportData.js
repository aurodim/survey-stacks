"use strict";

var Profiles = modelRequire("profiles.js");
var Constants = modelRequire("constants.js");
var Polls = modelRequire("polls.js");
var Surveys = modelRequire("surveys.js");
var Reports = modelRequire("reports.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.lang = "";
  this.webApp = false;
  this.available = false;
  this.type = "";
  this.token = "";
  this.title = "";
  this.reported = false;
}

var currentUser;
var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, type, token, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.lang = lang;
  currentUser.webApp = webApp;
  currentUser.type = type;
  currentUser.token = token;

  verifyGatherData(callback, res);
}

function verifyGatherData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];

        Reports.findOne({reported_key : currentUser.token, reported_type : currentUser.type, reported_by : currentUser.userKey}, function(error, report) {
          if (report) currentUser.reported = true;

          if (currentUser.type == "surveys") {
            surveyTitle(callback, res);
          } else {
            pollTitle(callback, res);
          }
        });
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function surveyTitle(callback, res) {
  Surveys.find({deleted : false, survey_token : currentUser.token}, function(error, surveys) {
    if (error) return callback(500, "error", res);
    if (surveys.length != "1") return sendResponse(callback, res);

    currentUser.available = true;
    currentUser.title = surveys[0].survey_details.name;
    sendResponse(callback, res);
  }).limit(1);
}

function pollTitle(callback, res) {
  Polls.find({deleted : false, poll_token : currentUser.token}, function(error, polls) {
    if (error) return callback(500, "error", res);
    if (polls.length != "1") return sendResponse(callback, res);

    currentUser.available = true;
    currentUser.title = polls[0].poll_details.name;
    sendResponse(callback, res);
  }).limit(1);
}

function sendResponse(callback, res) {
  var resultArray = {};
  var resultCode = 1;
  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
     "resultDate" : resultDate,
     "available" : currentUser.available,
     "title" : currentUser.title,
     "reported" : currentUser.reported,
  };

  callback(200, resultArray, res);
}
