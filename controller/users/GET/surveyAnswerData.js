"use strict";

var Profiles = modelRequire("profiles.js");
var Constants = modelRequire("constants.js");
var Surveys = modelRequire("surveys.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.lang = "";
  this.webApp = false;
  this.about = {
    sex : "",
    ethnicity : "",
    age_group : ""
  };
  this.answered_surveys = [];
  this.order = [];
  this.free_response = [];
  this.checkbox = [];
  this.boolean = [];
  this.radio = [];
  this.menu = [];
  this.answers_array = [];
  this.surveyDetails = {
    available : true,
    public : true,
    targeted : true,
    answered : false,
    token : "",
    lang : "",
    title : "",
    description : "",
    collectables : [],
    stacks : 0,
    questions : [],
    labs_created : false,
    labs_option : null,
    labs : {}
  };
};

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, token, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.lang = lang;
  currentUser.surveyDetails.token = token;
  currentUser.webApp = webApp;

  stacksMultiplier();
  verifyGatherData(callback, res);
}

function stacksMultiplier() {
  Constants.find(function(error, constants) {
    if (error) {
      currentUser.surveyDetails.stacks = 10;
    } else {
      var constant = constants[0];
      currentUser.surveyDetails.stacks = constant.stacks_xrt;
    }
  }).limit(1);
}

function verifyGatherData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];

        currentUser.answered_surveys = profile.surveys_answered;
        currentUser.about.sex = profile.about.sex;
        currentUser.about.ethnicity = profile.about.ethnicity;
        currentUser.about.age_group = Functions.getAgeGroup(profile.about.birth_date);

        if (currentUser.answered_surveys.includes(currentUser.surveyDetails.token)) {
          currentUser.surveyDetails.answered = true;
          currentUser.surveyDetails.stacks = 0;
        }

        getSurvey(callback, res);
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function getSurvey(callback, res) {
  Surveys.findOne({deleted : false, survey_token : currentUser.surveyDetails.token, creator_key : {$ne : currentUser.userKey}}, function(error, survey) {
    if (error) return callback(500, "error", res);
    if (survey === "" || survey == null) {
      currentUser.surveyDetails.stacks = 0;
      currentUser.surveyDetails.available = false;
      sendResponse(callback, res);
      return;
    }

    currentUser.surveyDetails.available = true;
    if (survey.survey_details.target_sex != currentUser.about.sex &&  survey.survey_details.target_sex != "both") {
      currentUser.surveyDetails.targeted = false;
      return sendResponse(callback, res);
    }
    if (survey.survey_details.target_ethnicity != currentUser.about.ethnicity &&  survey.survey_details.target_ethnicity != "a") {
      currentUser.surveyDetails.targeted = false;
      return sendResponse(callback, res);
    }
    if (survey.survey_details.target_age_group != currentUser.about.age_group &&  survey.survey_details.target_age_group != "a") {
      currentUser.surveyDetails.targeted = false;
      return sendResponse(callback, res);
    }
    currentUser.surveyDetails.targeted = true;
    currentUser.surveyDetails.labs_created = survey.survey_details.lab_created;
    currentUser.surveyDetails.collectables = survey.survey_details.collected;
    for (var i = 0; i < currentUser.surveyDetails.collectables.length; i++) {
      if (currentUser.surveyDetails.collectables[i] === "employment") {
        currentUser.surveyDetails.collectables[i] = "employment_status";
        return;
      }

      if (currentUser.surveyDetails.collectables[i] === "relationship") {
        currentUser.surveyDetails.collectables[i] = "relationship_status";
        return;
      }

      if (currentUser.surveyDetails.collectables[i] === "education") {
        currentUser.surveyDetails.collectables[i] = "education_level";
        return;
      }
    }
    currentUser.surveyDetails.title = survey.survey_details.name;
    currentUser.surveyDetails.lang = survey.survey_details.lang;
    currentUser.surveyDetails.public = survey.survey_details.public_survey;
    currentUser.surveyDetails.description = survey.survey_details.description;
    currentUser.surveyDetails.stacks*=survey.survey_details.stacks;
    currentUser.surveyDetails.image_url = survey.survey_details.image_url;
    currentUser.order = survey.question_details.order;
    currentUser.free_response = survey.question_details.free_response;
    currentUser.checkbox = survey.question_details.checkbox;
    currentUser.boolean = survey.question_details.boolean;
    currentUser.radio = survey.question_details.radio;
    currentUser.menu = survey.question_details.menu;

    if (currentUser.surveyDetails.labs_created) {
      if (survey.labs.type == "quiz") {
        currentUser.surveyDetails.labs_option = 0;
        currentUser.surveyDetails.labs = {
          out_of : survey.labs.out_of
        };
      } else if (survey.labs.type == "key_needed") {
        currentUser.surveyDetails.labs_option = 1;
        currentUser.surveyDetails.labs = {};
      } else if (survey.labs.type == "emoji") {
        currentUser.surveyDetails.labs_option = 2;
        currentUser.surveyDetails.labs = {};
      } else if (survey.labs.type == "game") {
        currentUser.surveyDetails.labs_option = 3;
        currentUser.surveyDetails.labs = {
          timed : survey.labs.timed,
          time : survey.labs.max_time
        }
      }
    }

    arrangeQuestions(callback, res);
  });
}

function arrangeQuestions(callback, res) {
  var answers = [];

  currentUser.order.forEach(function(question, i) {
    var type = question.split("_")[0];
    var index = question.split("_")[1];
    var selectedQ;
    var questionObj;

    switch (type) {
      case "fr":
        selectedQ = currentUser.free_response[index];
        questionObj = {
          type : type,
          question : selectedQ.question,
          required : selectedQ.options.required,
          pattern : Functions.patternRegex(selectedQ.options.pattern)
        };
        answers.push("");
        break;
      case "cbr":
        selectedQ = currentUser.checkbox[index];
        questionObj = {
          type : type,
          question : selectedQ.question,
          required : selectedQ.options.required,
          options : selectedQ.responses
        };
        answers.push([]);
        break;
      case "mr":
        selectedQ = currentUser.menu[index];
        questionObj = {
          type : type,
          question : selectedQ.question,
          required : selectedQ.options.required,
          options : selectedQ.responses
        };
        answers.push(null);
        break;
      case "br":
        selectedQ = currentUser.boolean[index];
        questionObj = {
          type : type,
          question : selectedQ.question,
          required : selectedQ.options.required,
        };
        answers.push(false);
        break;
      case "rr":
        selectedQ = currentUser.radio[index];
        questionObj = {
          type : type,
          question : selectedQ.question,
          required : selectedQ.options.required,
          options : selectedQ.responses
        };
        answers.push(null);
        break;
    }

    if (currentUser.surveyDetails.labs_option === 0) {
      questionObj.worth = selectedQ.options.worth;
    }

    currentUser.surveyDetails.questions.push(questionObj);

    if (i == currentUser.order.length - 1) {
      currentUser.answers_array = answers;
      sendResponse(callback, res);
    }
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {}

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
     "resultDate" : resultDate,
     "survey_details" : currentUser.surveyDetails,
     "answers_blank" : currentUser.answers_array
  };

  callback(200, resultArray, res);
}
