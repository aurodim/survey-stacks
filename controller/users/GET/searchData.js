"use strict";

var Profiles = modelRequire("profiles.js");
var Surveys = modelRequire("surveys.js");
var Rewards = modelRequire("rewards.js");
var Categories = modelRequire("categories.js");
var Teams = modelRequire("teams.js");
var Functions = rootRequire("/controller/globalFunctions.js");

var service = {};
service.initialize = initialize;

module.exports = service;

var currentUser = {
  ip : "",
  lang : "",
  validation_key : "",
  session_key : "",
  auth_key : "",
  webApp : "",
  searchQuery : "",
  searchConstraint : "",
  bookmarks : [],
  wishlist : [],
  stacks : 0,
  surveys_answered : []
};

var searchData = {
  users : [],
  companies : [],
  surveys : [],
  rewards : []
};

var surveysData = [];
var rewards = [];

function initialize(ip, lang, validation_key, auth_key, session_key, webApp, searchQuery, searchConstraint, callback, res) {
  currentUser.ip = ip;
  currentUser.lang = lang;
  currentUser.validation_key = validation_key;
  currentUser.session_key = session_key;
  currentUser.auth_key = auth_key;
  currentUser.webApp = webApp;
  currentUser.searchConstraint = searchConstraint;
  if (searchConstraint != "") {
    currentUser.searchQuery = searchQuery.split(searchConstraint)[1];
  } else {
    currentUser.searchQuery = searchQuery;
  }

  verifyGather(callback, res);
}

function verifyGather(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.validation_key, user_session_key : currentUser.session_key, user_auth_key : currentUser.auth_key}, function(error, profiles) {
    if (error) return callback(500, "error", res);

    if (profiles.length != "1") return callback(401, "wrongMatch", res);

    var profile = profiles[0];
    currentUser.bookmarks = profile.bookmarks;
    currentUser.wishlist = profile.wishlist;
    currentUser.surveys_answered = profile.surveys_answered;
    currentUser.stacks = profile.current_stacks;

    searchData.users = [];
    searchData.companies = [];
    searchData.surveys = [];
    searchData.rewards = [];

    if (currentUser.searchConstraint == "") {
      asynchSearch(callback, res);
    } else if (currentUser.searchConstraint == "@") {
      usersSearch(callback, res);
    } else if (currentUser.searchConstraint == "$") {
      surveysSearch(callback, res);
    } else if (currentUser.searchConstraint == "*") {
      rewardsSearch(callback, res);
    } else {
      sendResponse(callback, res);
    }
  }).limit(1);
}

function asynchSearch(callback, res) {
  async.parallel({
      usersFind : function(cb) {
        findUsers(cb);
      },
      companiesFind : function(cb) {
        findCompanies(cb);
      },
      surveysFind : function(cb) {
        findSurveys(cb);
      },
      rewardsFind : function(cb) {
        findRewards(cb);
      }
  }, function(error, result){
    if (error) return callback(500, "error", res);

    sendResponse(callback, res);
  });
}

function usersSearch(callback, res) {
  async.parallel({
      usersFind : function(cb) {
        findUsers(cb);
      },
      companiesFind : function(cb) {
        findCompanies(cb);
      }
  }, function(error, result){
    if (error) return callback(500, "error", res);

    sendResponse(callback, res);
  });
}

function findUsers(cb) {
  Profiles.find({deleted : false, username : {$regex : currentUser.searchQuery, $options : "$i"}}, function(error, profiles) {
    if (error) return cb(true, null);

    var users_data = [];
    profiles.forEach(function(profile) {
      var userJSON = {
        token : profile.user_key,
        user_profile_url : generateProfileURL(profile.user_key, profile.username, 1),
        profile_picture_url : generateProfilePictureURL(profile.user_key, 1),
        username : profile.username
      };

      users_data.push(userJSON);
    });

    searchData.users = users_data;
    cb(false, "ok");
  }).limit(10);
}

function findCompanies(cb) {
  Companies.find({company_name : {$regex : currentUser.searchQuery, $options : "$i"}}, function(error, companies) {
    if (error) return cb(true, null);

    var companies_data = [];
    companies.forEach(function(company) {
      var companyJSON = {
        token : company.company_token,
        user_profile_url : generateProfileURL(company.company_token, company.company_value, 9),
        profile_picture_url : company.company_picture_url,
        username : company.company_name
      };

      companies_data.push(companyJSON);
    });

    searchData.companies = companies_data;
    cb(false, "ok");
  }).limit(10);
}

function surveysSearch(callback, res) {
  Surveys.find({deleted : false, "creator_key" : {$ne : currentUser.validation_key}, "survey_token" : {$nin : currentUser.surveys_answered}, "survey_details.name" : {$regex : currentUser.searchQuery, $options : "$i"}, "blocked" : false, "deleted" : false}, null, {limit : 10}, function(error, surveys) {
    if (error) return callback(500, "error", res);
    if (surveys.length == 0) return sendResponse(callback, res);

    surveysData = [];
    surveys.forEach(function(survey, i) {
      addSurvey(survey, function(error) {
        if (error) return callback(500, "error", res);

        if (i == surveys.length - 1) {
          searchData.surveys = surveysData;
          sendResponse(callback, res);
        }
      });
    });
  });
}

function findSurveys(cb) {
  Surveys.find({deleted : false, "creator_key" : {$ne : currentUser.validation_key}, "survey_token" : {$nin : currentUser.surveys_answered}, "survey_details.name" : {$regex : currentUser.searchQuery, $options : "$i"}, "blocked" : false, "deleted" : false}, null, {limit : 10}, function(error, surveys) {
    if (error) return cb(true, null);
    if (surveys.length == 0) return cb(false, "ok");

    surveysData = [];
    surveys.forEach(function(survey, i) {
      addSurvey(survey, function(error) {
        if (error) return cb(true, null);

        if (i == surveys.length - 1) {
          searchData.surveys = surveysData;
          cb(false, "ok")
        }
      });
    });
  });
}

function addSurvey(survey, next) {
  var bookmarked = false;
  var username = "";
  var category = "";

  if (currentUser.bookmarks.includes(survey.survey_token)) {
    bookmarked = true;
  } else {
    bookmarked = false;
  }

  // translate needed

  async.parallel({
      usernameFind : function(cb) {
        getUsername(survey.creator_key, cb);
      },
      categoryFind : function(cb) {
        getCategory(survey.survey_details.category_token, survey.survey_details.category, cb);
      }
  }, function(error, result){
    if (error) return next(true);

    username = result.usernameFind;
    category = result.categoryFind;

    var ft = survey.creator_key.split('.')[0];
    var type;
    if (ft.substr(ft.length - 4, ft.length) == "AuRo") {
      type = 1;
    } else {
      type = 9;
    }

    var surveyJSON = {
      "token" : survey.survey_token,
      "answer_url" : generateURL(survey.survey_token),
      "image_url" : survey.survey_details.image_url,
      "title" : survey.survey_details.name,
      "surveyor_id" : survey.creator_key,
      "surveyor_username" : username,
      "surveyor_profile_url" : generateProfileURL(survey.creator_key, username, type),
      "category_id" : survey.survey_details.category_token,
      "category_name" : category,
      "questions" : survey.question_details.amount,
      "description" : survey.survey_details.description,
      "stacks" : survey.survey_details.stacks,
      "bookmarked" : bookmarked
    };

    surveysData.push(surveyJSON);

    next(false);
  });
}

function rewardsSearch(callback, res) {
  Rewards.find({$or : [{title : {$regex : currentUser.searchQuery, $options : "$i"}}, {description : {$regex : currentUser.searchQuery, $options : "$i"}}], quantity : {$gte : 0}}, null, {limit : 10}, function(error, rewards) {
    if (error) return callback(500, "error", res);
    if (rewards.length == 0) return sendResponse(callback, res);

    rewardsData = [];
    rewards.forEach(function(reward, i) {
      addReward(reward, function(error) {
        if (error) return callback(500, "error", res);

        if (i == rewards.length - 1) {
          searchData.rewards = rewardsData;
          sendResponse(callback, res);
        }
      });
    });
  });
}

function addReward(reward, next) {
  var wishlisted = false;
  var company_profile_picture_url = generateProfilePictureURL(reward.company_token, 9);
  var company_profile_url = generateProfileURL(reward.company_token, 9);
  var stacks_needed = reward.stacks_cost - currentUser.stacks;

  if (stacks_needed < 0) {
    stacks_needed = 0;
  }

  if (currentUser.wishlist.includes(reward.reward_token)) {
    wishlisted = true;
  } else {
    wishlisted = false;
  }

  // translate needed

  var surveyJSON = {
    "token" : reward.reward_token,
    "redeem_url" : reward.reedem_url,
    "image_url" : reward.image_url,
    "title" : reward.title,
    "description" : reward.description,
    "company_profile_picture_url" : company_profile_picture_url,
    "company_profile_url" : company_profile_url,
    "quantity" : reward.quantity,
    "stacks" : reward.stacks_cost,
    "stacks_needed" : stacks_needed,
    "wishlisted" : wishlisted
  };

  surveysData.push(surveyJSON);

  next(false);
}

function findRewards(cb) {
  Rewards.find({$or : [{title : {$regex : currentUser.searchQuery, $options : "$i"}}, {description : {$regex : currentUser.searchQuery, $options : "$i"}}], quantity : {$gte : 0}}, null, {limit : 10}, function(error, rewards) {
    if (error) return cb(true, null);
    if (rewards.length == 0) return cb(false, "ok")

    rewardsData = [];
    rewards.forEach(function(reward, i) {
      addReward(reward, function(error) {
        if (error) return callback(500, "error", res);

        if (i == rewards.length - 1) {
          searchData.rewards = rewardsData;
          sendResponse(callback, res);
        }
      });
    });
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "searchData" : {
    "users" : searchData.users,
    "companies" : searchData.companies,
    "surveys" : searchData.surveys,
    "rewards" : searchData.rewards
  }};

  callback(200, resultArray, res);
}

function generateProfileURL(key, username, type) {
  if (type == 1) {
    return process.env.DOMAIN_HTTPS + "/users/" + key + "/" + username + "/profile";
  } else {
    return process.env.DOMAIN_HTTPS + "/companies/" + key + "/" + username + "/profile";
  }
}

function generateProfilePictureURL(key, type) {
  if (type == 1) {
    var now = new Date()
    var time_int = new Date(now).getTime();
    return process.env.CLOUDINARY_HTTP + "h_110,w_110,q_auto:best" + process.env.CLOUDINARY_USERS_FOLDER + key + "/profile_picture.png?" + time_int;
  } else if (type == 9){
    var now = new Date()
    var time_int = new Date(now).getTime();
    return process.env.CLOUDINARY_HTTP + "h_110,w_110,q_auto:best" + process.env.CLOUDINARY_COMPANIES_FOLDER + key + "/profile_picture.png?" + time_int;
  }
}

function generateURL(token) {
  return process.env.DOMAIN_HTTPS + "/surveys/" + token + "/answer";
}

function getUsername(key, cb) {
  var ft = key.split('.')[0];

  if (ft.substr(ft.length - 4, ft.length) == "AuRo") {
    Profiles.find({deleted : false, user_key : key}, function(error, profiles) {
      if (error) return cb(true, null);

      return cb(false, profiles[0].username);
    }).limit(1);
  } else {
    Companies.find({company_token : key}, function(error, companies) {
      if (error) return cb(true, null);

      return cb(false, companies[0].company_name);
    }).limit(1);
  }
}

function getCategory(key, value, cb) {
  Categories.find({category_token : key, category_value : value}, function(error, categories) {
    if (error) return cb(true, null);

    return cb(false, categories[0].category_name);
  }).limit(1);
}
