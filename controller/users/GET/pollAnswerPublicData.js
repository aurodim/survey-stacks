"use strict";

var Constants = modelRequire("constants.js");
var Responses = modelRequire("responses.js");
var Polls = modelRequire("polls.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.lang = "";
  this.webApp = false;
  this.about = {
    sex : "",
    ethnicity : "",
    age_group : ""
  };
  this.answered_polls = [];
  this.category_token = "";
  this.pollDetails = {
    token : "",
    available : true,
    public : true,
    targeted : true,
    answered : false,
    title : "",
    lang : "",
    description : "",
    coins : 0,
    image_url : "",
    question : "",
    options : [],
    analytics : {},
    next_poll : "",
    labs_created : false,
    labs_option : null,
    labs : {}
  };
};

var currentUser;
var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, lang, token, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.lang = lang;
  currentUser.pollDetails.token = token;
  currentUser.webApp = webApp;

  getPoll(callback, res);
  coinsMultiplier();
}

function coinsMultiplier() {
  Constants.find(function(error, constants) {
    if (error) {
      currentUser.pollDetails.coins = 1;
    } else {
      var constant = constants[0];
      currentUser.pollDetails.coins = constant.coins_xrt;
    }
  }).limit(1);
}

function getPoll(callback, res) {
  Polls.findOne({deleted : false, poll_token : currentUser.pollDetails.token, "poll_details.public_poll" : true}, function(error, poll) {
    if (error) return callback(500, "error", res);
    if (poll === "" || poll == null) {
      currentUser.pollDetails.coins = 0;
      currentUser.pollDetails.available = false;
      sendResponse(callback, res);
      return;
    }

    currentUser.pollDetails.available = true;
    currentUser.pollDetails.targeted = true;
    currentUser.pollDetails.labs_created = poll.poll_details.lab_created;
    currentUser.pollDetails.title = poll.poll_details.name;
    currentUser.pollDetails.lang = poll.poll_details.lang;
    currentUser.pollDetails.public = poll.poll_details.public_poll;
    currentUser.pollDetails.description = poll.poll_details.description;
    currentUser.pollDetails.coins*=poll.poll_details.coins;
    currentUser.pollDetails.image_url = poll.poll_details.image_url;
    currentUser.pollDetails.question = poll.question_details.question;
    currentUser.pollDetails.options = poll.question_details.options;
    currentUser.category_token = poll.poll_details.category_token;
    currentUser.pollDetails.options.forEach(function(option, i) {
      var analytic = {
        "percentage" : 0
      };
      currentUser.pollDetails.analytics[i] = analytic;
    });

    if (currentUser.pollDetails.labs_created) {
      if (poll.labs.type == "quiz") {
        currentUser.pollDetails.labs_option = 0;
        currentUser.pollDetails.labs = {};
      } else if (poll.labs.type == "emoji") {
        currentUser.pollDetails.labs_option = 2;
        currentUser.pollDetails.labs = {};
      } else if (poll.labs.type == "game") {
        currentUser.pollDetails.labs_option = 3;
        currentUser.pollDetails.labs = {
          timed : poll.labs.timed,
          time : poll.labs.max_time,
          questions : poll.labs.gameQuestions
        };
      }
    }

    getNextPoll(callback, res);
  });
}

function getNextPoll(callback, res) {
  Polls.find({deleted : false, poll_token : {$ne : currentUser.pollDetails.token}, "poll_details.category_token" : currentUser.category_token,
  "poll_details.public_poll" : true}, function(error, polls) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (polls.length == "1") {
        var poll = polls[0];

        currentUser.pollDetails.next_poll = poll.poll_details.answer_url;

        sendResponse(callback, res);
      } else {
        sendResponse(callback, res);
      }
    }
  }).limit(1);
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "poll_details" : currentUser.pollDetails,
  };

  callback(200, resultArray, res);
}
