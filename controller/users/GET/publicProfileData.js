"use strict";

var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.username = "";
  this.webApp = false;
  this.profileInfo = {
    profile_theme_url : "",
    profile_picture_url : "",
    motto : "",
    since : ""
  };
  this.achievementsInfo = {
    stacks_earned : 0,
    coins_earned : 0,
    questions_answered : 0,
    surveys_answered : 0,
    rewards_claimed : 0
  };
  this.contactInfo = {
    email : "",
    phone_number : "",
    website : "",
    facebook : "",
    instagram : ""
  };
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, username, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.username = username;
  currentUser.webApp = webApp;

  gatherData(callback, res);
}

function gatherData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, username : currentUser.username}, function(error, profiles) {
    if (error) {
      callback(500, "error", res);
    } else {
      if (profiles.length == "0") return sendResponse(-1, callback, res);

      var profile = profiles[0];
      currentUser.profileInfo.profile_theme_url = Functions.imageTransform(profile.profile_theme_url, 900, null);
      currentUser.profileInfo.profile_picture_url = Functions.imageTransform(profile.profile_picture_url, 200, 200);
      currentUser.profileInfo.motto = profile.motto;
      currentUser.profileInfo.since = new Date(profile.created_at).getTime();
      currentUser.achievementsInfo.stacks_earned = profile.achievements.lifetime_stacks;
      currentUser.achievementsInfo.coins_earned = profile.achievements.lifetime_coins;
      currentUser.achievementsInfo.questions_answered = profile.achievements.questions_answered;
      currentUser.achievementsInfo.surveys_answered = profile.achievements.surveys_answered;
      currentUser.achievementsInfo.rewards_claimed = profile.achievements.rewards_claimed;
      currentUser.contactInfo.email = profile.contact_info.email;
      currentUser.contactInfo.phone_number = profile.contact_info.phone_number;
      currentUser.contactInfo.website = profile.contact_info.website;
      currentUser.contactInfo.facebook = profile.contact_info.facebook;
      currentUser.contactInfo.instagram = profile.contact_info.instagram;

      sendResponse(1, callback, res);
    }
  }).limit(1);
}

function sendResponse(resultCode, callback, res) {
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {
   "webApp" : currentUser.webApp,
   "resultCode" : resultCode,
   "resultDate" : resultDate,
   "profileInfo" : currentUser.profileInfo,
   "achievementsInfo" : currentUser.achievementsInfo,
   "contactInfo" : currentUser.contactInfo
  };

  callback(200, resultArray, res);
}
