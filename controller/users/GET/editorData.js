"use strict";

var Profiles = modelRequire("profiles.js");
var Constants = modelRequire("constants.js");
var Polls = modelRequire("polls.js");
var Surveys = modelRequire("surveys.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.lang = "";
  this.webApp = false;
  this.editable = true;
  this.ppa = 0.00;
  this.token = "";
  this.type = "";
  this.details = {
    name : "",
    description : "",
    image_url : "",
    answer_url : "",
    free : false,
    sc_amount : 0,
    labs_created : false,
    labs_option : null,
    unique_keys : false,
    keys : []
  };
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, type, token, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.lang = lang;
  currentUser.token = token;
  currentUser.type = type;
  currentUser.webApp = webApp;

  if (type == "surveys") {
    ppaValue();
  }
  verifyGatherData(callback, res);
}

function ppaValue() {
  Constants.find(function(error, constants) {
    if (error) {
      currentUser.ppa = 1.00;
    } else {
      var constant = constants[0];
      currentUser.ppa = (constant.ppa).toFixed(2);
    }
  }).limit(1);
}

function verifyGatherData(callback, res) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];

        if (currentUser.type == "surveys") {
          surveyEditG(callback, res);
        } else if (currentUser.type == "polls") {
          pollEditG(callback, res);
        }
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function surveyEditG(callback, res) {
  Surveys.find({deleted : false, creator_key : currentUser.userKey, survey_token : currentUser.token}, function(error, surveys) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (surveys.length == "1") {
        var survey = surveys[0];

        currentUser.editable = true;
        currentUser.details.description = survey.survey_details.description;
        currentUser.details.image_url = survey.survey_details.image_url;
        currentUser.details.answer_url = survey.survey_details.answer_url;
        currentUser.details.free = survey.payment_details.free;
        currentUser.details.name = survey.survey_details.name;
        currentUser.details.sc_amount = survey.survey_details.stacks;

        if (currentUser.details.labs_created) {
          if (survey.labs.type === "quiz") {
            currentUser.details.labs_option = 0;
          } else if (survey.labs.type === "key_needed") {
            currentUser.details.labs_option = 1;
            currentUser.details.unique_keys = survey.labs.unique_keys;
            currentUser.details.keys = survey.labs.keys;
          } else if (survey.labs.type === "emoji") {
            currentUser.details.labs_option = 2;
          } else if (survey.labs.type === "game") {
            currentUser.details.labs_option = 3;
          }
        }

        sendResponse(callback, res);
      } else {
        currentUser.editable = false;
        currentUser.ppa = 0;
        sendResponse(callback, res);
      }
    }
  }).limit(1);
}

function pollEditG(callback, res) {
  Polls.find({deleted : false, creator_key : currentUser.userKey, poll_token : currentUser.token}, function(error, polls) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (polls.length == "1") {
        var poll = polls[0];

        currentUser.editable = true;
        currentUser.details.description = poll.poll_details.description;
        currentUser.details.image_url = poll.poll_details.image_url;
        currentUser.details.answer_url = poll.poll_details.answer_url;
        currentUser.details.name = poll.poll_details.name;
        currentUser.details.sc_amount = poll.poll_details.coins;
        currentUser.details.labs_created = poll.poll_details.lab_created;

        sendResponse(callback, res);
      } else {
        currentUser.editable = false;
        currentUser.ppa = 0;
        sendResponse(callback, res);
      }
    }
  }).limit(1);
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "editable" : currentUser.editable,
    "ppa" : currentUser.ppa,
    "details" : {
       "name" : currentUser.details.name,
       "description" : currentUser.details.description,
       "image_url" : currentUser.details.image_url,
       "free" : currentUser.details.free,
       "sc_amount" : currentUser.details.sc_amount,
       "labs_created" : currentUser.details.labs_created,
       "labs_type" : currentUser.details.labs_option
     },
     "labs" : {

     }
  };

  if (currentUser.details.labs_created && currentUser.details.labs_option == 1) {
    resultArray.labs.unique_keys = currentUser.details.unique_keys;
    resultArray.labs.keys = currentUser.details.keys;
  }

  callback(200, resultArray, res);
}
