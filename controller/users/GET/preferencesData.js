var Profiles = modelRequire("profiles.js");
var Categories = modelRequire("categories.js");
var Companies = modelRequire("companies.js");
var Constants = modelRequire("constants.js");
var Globals = rootRequire("/controller/globals.js");

var currentUser = {
	ip : "",
	lang : "",
	userKey : "",
	userAuthKey : "",
	userSessionKey : "",
	webApp : false,
	preferences_completed : "",
	categories : {},
  stacks : 0,
	companies : {},
	preferences : {
		monosex_filter : false,
    translated_filter : false,
    categories : [],
    companies : []
	}
};

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, webApp, callback, res) {
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.lang = lang;
  currentUser.webApp = webApp;

  stackExchange();
  AsyncData(callback, res);
}

function stackExchange() {
  Constants.find(function(error, constants) {
    if (error) {
      currentUser.stacks = 10;
    } else {
      var constant = constants[0];
      currentUser.stacks = constant.stacks_xrt * 10;
    }
  }).limit(1);
}

function AsyncData(callback, res) {
  async.parallel({
      categories : function(cb) {
        Globals.categories(currentUser.lang, cb);
      }, verify : function(cb) {
      	verifyGatherData(cb);
      }, companies : function(cb) {
      	Globals.companies(currentUser.lang, cb);
      }
  }, function(err, results) {
      if (err == "d") return callbackSendResponse(500, "callback", res);
      if (err == "a") return callbackSendResponse(500, "callback", res);

      currentUser.categories = results.categories;

      currentUser.companies = results.companies;

      sendResponse(callback, res);
  });
}

function verifyGatherData(callback) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {
    if (error) {
      	return callback("d", null);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];

        currentUser.preferences_completed = profile.setup_completed.preferences;
        currentUser.preferences.monosex_filter = profile.preferences.monosex;
        currentUser.preferences.translated_filter = profile.preferences.translated;
        currentUser.preferences.categories = profile.preferences.categories_tokens;
        currentUser.preferences.companies = profile.preferences.companies_tokens;

        callback(null, "");
      } else {
        callback("a", null);
      }
    }
  }).limit(1);
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {
		"webApp" : currentUser.webApp,
    "resultCode" : resultCode,
     "resultDate" : resultDate,
     "stacks" : currentUser.stacks,
     "preferences_completed" : currentUser.preferences_completed,
     "categories" : currentUser.categories,
     "companies" : currentUser.companies,
     "preferences" : currentUser.preferences
  };

  callback(200, resultArray, res);
}
