"use strict";

var Profiles = modelRequire("profiles.js");
var Rewards = modelRequire("rewards.js");
var Notifications = modelRequire("notifications.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.lang = "";
  this.webApp = false;
  this.accountInfo = {
    profile_theme_url : "",
    profile_picture_url : "",
    stacks_amount : 0,
    coins_amount : 0,
    sat : "",
    payment_saved : false
  };
  this.wishlistInfo = [];
  this.notificationsInfo = {
    unread : 0,
    list : []
  };
}

var currentUser;

var csrfToken = "";

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, webApp, csrfTkn, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.lang = lang;
  currentUser.webApp = webApp;
  csrfToken = csrfTkn;

  AsyncData(callback, res);
}

function AsyncData(callback, res) {
  async.parallel({
      profileD : function(callback) {
        verifyGatherData(callback);
      },
      notificationD : function(callback) {
        notificationData(callback);
      }
  }, function(err) {
      if (err == "d") {
        callback(500, "callback", res);
        return;
      }

      if (err == "a" && currentUser.webApp) {
        res.clearCookie("sk");
        res.clearCookie("lk");
        res.clearCookie("vk");
        res.clearCookie("un");
        res.clearCookie("dt");
        res.clearCookie("sd");
        res.clearCookie("dnotif");
        res.clearCookie("lang");
        res.clearCookie("_csrf");
        res.clearCookie("io");
        callback(401, "no auth", res);
        return;
      }

      if (err == "a") {
        callback(401, "no auth", res);
        return;
      }

      sendResponse(callback, res);
  });
}

function verifyGatherData(callback) {
  Profiles.find({deleted : false, user_key : currentUser.userKey, user_session_key : currentUser.userSessionKey, user_auth_key : currentUser.userAuthKey}, function(error, profiles) {

    if (error) return callback("d");
    if (profiles.length == "0") return callback("a");

    var profile = profiles[0];
    if (profile.profile_theme_url != "") {
      currentUser.accountInfo.profile_theme_url = profile.profile_theme_url +"?"+ Date.now();
    } else {
      currentUser.accountInfo.profile_theme_url = "https://res.cloudinary.com/survey-stacks/image/upload/v1515239424/constants/theme.png";
    }
    if (profile.profile_picture_url != "") {
      currentUser.accountInfo.profile_picture_url = Functions.imageTransform(profile.profile_picture_url, 48, 48) +"?"+ Date.now();
    } else {
      currentUser.accountInfo.profile_picture_url = "";
    }
    currentUser.accountInfo.stacks_amount = profile.stacks;
    currentUser.accountInfo.coins_amount = profile.coins;
    currentUser.accountInfo.sat = profile.user_session_key;
    currentUser.accountInfo.payment_saved = profile.setup_completed.payment;

    Rewards.find({reward_token : {$in : profile.wishlist}}, function(error, rewards) {
      if (error) return callback("d");

      rewards.forEach(function(reward) {
        var wishlistObj = {
          token : reward.reward_token,
          title : reward.description,
          progress : ((profile.stacks / (reward.cost * reward.discount)) * 100).toFixed(0),
          wishlisted : true
        };

        if (wishlistObj.progress > 100) {
          wishlistObj.progress = 100;
        }

        currentUser.wishlistInfo.push(wishlistObj);
      });

      return callback(null);
    });
  }).limit(1);
}

function notificationData(callback) {
  Notifications.find({reciever : currentUser.userSessionKey}, function(error, notifications) {
    if (error) return callback("d");

    if (notifications.length == 0) return callback(null);

    for(var index in notifications) {
      var notification = notifications[index];
      if (!notification.read) {
        currentUser.notificationsInfo.unread += 1;
      }

      currentUser.notificationsInfo.list.push({
        "type" : notification.type,
        "token" : notification.notification_token,
        "title" : notification.title,
        "content" : notification.content,
        "url" : notification.url
      });
    }

    callback(null);
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray;

  var resultDate = new Date();

  resultArray = {
   "webApp" : currentUser.webApp,
   "resultCode" : resultCode,
   "resultDate" : resultDate,
   "accountInfo" : currentUser.accountInfo,
   "wishlistInfo" : currentUser.wishlistInfo,
   "notificationsInfo" : currentUser.notificationsInfo,
   csrf : csrfToken
 };

  callback(200, resultArray, res);
}
