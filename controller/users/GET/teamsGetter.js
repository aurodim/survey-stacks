"use strict";

var Teams = modelRequire("teams.js");
var Constants = modelRequire("constants.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function User() {
  this.ip = "";
  this.userKey = "";
  this.userAuthKey = "";
  this.userSessionKey = "";
  this.lang = "";
  this.webApp = false;
  this.teams = [];
}

var currentUser;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, webApp, callback, res) {
  currentUser = new User();
  currentUser.ip = ip;
  currentUser.userKey = validation_key;
  currentUser.userAuthKey = auth_key;
  currentUser.userSessionKey = session_key;
  currentUser.lang = lang;
  currentUser.webApp = webApp;

  gatherTeams(callback, res);
}

function gatherTeams(callback, res) {
  Teams.find({deleted : false, members : currentUser.userKey}, function(error, teams) {
    if (error) return callback(500, "error", res);

    for (var i = 0; i < teams.length; i++) {
      var team = teams[i];

      currentUser.teams.push({
        name : team.name,
        token : team.team_key,
        expand : false
      });
    }

    sendResponse(callback, res);
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {}

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentUser.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "teams" : currentUser.teams,
  };

  callback(200, resultArray, res);
}
