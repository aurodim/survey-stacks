"use strict";

var Profiles = modelRequire("profiles.js");
var Surveys = modelRequire("surveys.js");
var Polls = modelRequire("polls.js");
var Responses = modelRequire("responses.js");
var Functions = rootRequire("/controller/globalFunctions.js");
var Analyzer = rootRequire("/controller/analyzer.js");
var jsonexport = require('jsonexport');
var EasyXml = require("easyxml");

function Request() {
  this.ip = "";
  this.userKey = "";
  this.lang = "";
  this.type = "";
  this.format = "";
  this.token = "";
  this.analytic = {};
  this.surveys_created = [];
  this.polls_created = [];
}

var currentRequest;
var serializer = new EasyXml({
    singularize: true,
    rootElement: 'response',
    dateFormat: 'ISO',
    manifest: true
});

var service = {};

service.init = init;

module.exports = service;

function init(ip, validation_key, ps, token, type, format, callback) {
  currentRequest = new Request();
  currentRequest.userKey = validation_key;
  currentRequest.token = token;
  currentRequest.type = type;
  currentRequest.format = format;
  if (ps == "survey") {
    surveyGather(callback);
  } else {
    pollGather(callback);
  }
}

function surveyGather(callback) {
  Surveys.findOne({deleted : false, survey_token : currentRequest.token, creator_key : currentRequest.userKey}, function(error, survey) {
    if (error || survey === null) return callback(true, null);

    switch(currentRequest.type) {
      case "4":
        RD(survey, callback);
        break;
      case "5":
        R(survey, callback);
        break;
      default:
        defaultD(survey, callback);
    }
  });
}

function pollGather(callback) {
  Polls.findOne({deleted : false, poll_token : currentRequest.token, creator_key : currentRequest.userKey}, function(error, poll) {
    if (error || poll === null) return callback(true, null);

    switch(currentRequest.type) {
      case "3":
        RDP(poll, callback);
        break;
      default:
        defaultDP(poll, callback);
    }
  });
}

function RD(survey, callback) {
  var surveyObj = {
    analytics : {
      questions : [],
      users : []
    }
  };

  var order = survey.question_details.order,
      questions = {
        fr : survey.question_details.free_response,
        cbr : survey.question_details.checkbox,
        rr : survey.question_details.radio,
        mr : survey.question_details.menu,
        br : survey.question_details.boolean
      };

  surveyObj.analytics.questions = Analyzer.QuestionsAssort(survey.question_details.order, questions);

  findResponses(survey.survey_token, function(error, responses) {
    if (error) return true;
    var d = 0;
    var loopR = function(responses) {
      if (d == responses.length) {

        currentRequest.analytic = surveyObj;

        currentRequest.analytic = JSON.stringify(currentRequest.analytic, null, 2);

        if (currentRequest.format == "csv") {
          currentRequest.analytic = JSON.parse(currentRequest.analytic);
          jsonexport(currentRequest.analytic, function(err, csv){
              if(err) throw error;
              currentRequest.analytic = csv;
          });
        } else if (currentRequest.format == "xml") {
          currentRequest.analytic = JSON.parse(currentRequest.analytic);
          currentRequest.analytic = serializer.render(currentRequest.analytic)
        }

        return callback(null, currentRequest.analytic);
      }

      var userObj = {
        about : {
          sex : "",
          ethnicity : [],
          languages : [],
          education : "",
          relationship_status : "",
          employment_status : "",
          religion : "",
          age_group : "",
        },
        responses : {}
      };

      var response = responses[d];
      userObj.responses = Analyzer.ResponsesAssort(survey.question_details.order, questions, response.responses);
      if (response.respondent_token == process.env.HOMO_PUBLICA_CLAVEM) {

        userObj.about.sex = "n/a";
        userObj.about.ethnicity = "n/a";
        userObj.about.languages.push(response.response_lang);
        userObj.about.education = "n/a";
        userObj.about.relationship_status = "n/a";
        userObj.about.employment_status = "n/a";
        userObj.about.religion = "n/a";
        userObj.about.age_group = "n/a";

        surveyObj.analytics.users.push(userObj);
        d++;
        loopR(responses);
      } else {
        Profiles.findOne({user_key : response.respondent_token}, function(error, profile) {
          if (profile == null || error) return true;

          userObj.about.sex = profile.about.sex;
          userObj.about.ethnicity = profile.about.ethnicity;
          userObj.about.languages = profile.about.languages;
          userObj.about.education = profile.about.education;
          userObj.about.relationship_status = profile.about.relationship_status;
          userObj.about.employment_status = profile.about.employment_status;
          userObj.about.religion = profile.about.religion;
          userObj.about.age_group = Functions.getAgeGroup(profile.about.birth_date);

          surveyObj.analytics.users.push(userObj);
          d++;
          loopR(responses);
        });
      };
    };
  loopR(responses);
  });
}

function R(survey, callback) {
  var surveyObj = {
    analytics : {
      questions : [],
      responses : {}
    }
  };

  var order = survey.question_details.order,
      questions = {
        fr : survey.question_details.free_response,
        cbr : survey.question_details.checkbox,
        rr : survey.question_details.radio,
        mr : survey.question_details.menu,
        br : survey.question_details.boolean
      };

  surveyObj.analytics.questions = Analyzer.QuestionsAssort(survey.question_details.order, questions);
  surveyObj.analytics.responses = {};

  findResponses(survey.survey_token, function(error, responses) {
    if (error) return true;
    var d = 0;
    var loopR = function(responses) {
      if (d == responses.length) {

        currentRequest.analytic = surveyObj;

        currentRequest.analytic = JSON.stringify(currentRequest.analytic, null, 2);

        if (currentRequest.format == "csv") {
          currentRequest.analytic = JSON.parse(currentRequest.analytic);
          jsonexport(currentRequest.analytic, function(err, csv){
              if(err) throw error;
              currentRequest.analytic = csv;
          });
        } else if (currentRequest.format == "xml") {
          currentRequest.analytic = JSON.parse(currentRequest.analytic);
          currentRequest.analytic = serializer.render(currentRequest.analytic)
        }

        return callback(null, currentRequest.analytic);
      }
      var response = responses[d];
      surveyObj.analytics.responses[d] = Analyzer.ResponsesAssort(survey.question_details.order, questions, response.responses);
      d++;
      loopR(responses);
    };
  loopR(responses);
  });
}

function defaultD(survey, callback) {
  var surveyObj = {
    view : {
      token : survey.survey_token,
      image_url : survey.survey_details.image_url
    },
    analytics : {
      questionnaire : {
        name : survey.survey_details.name,
        first_response : "",
        total_responses : survey.responses.length,
        last_response : "",
        avr_resp : 0,
        avr_resp_d : Analyzer.AverageDailyResponses(survey.created_at, survey.responses.length),
        avr_stacks : 0,
        daily : [],
        monthly : [],
        yearly : {
          years : [],
          values : []
        }
      },
      respondents : [],
      graphic_questions : [],
      questions : [],
      responses : []
    }
  };

  var responses_array = [],
      average_stacks_array = [],
      dates = [],
      order = survey.question_details.order,
      questions = {
        fr : survey.question_details.free_response,
        cbr : survey.question_details.checkbox,
        rr : survey.question_details.radio,
        mr : survey.question_details.menu,
        br : survey.question_details.boolean
      },
      about = {
        sex : [],
        ethnicity : [],
        languages : [],
        education : [],
        relationship_status : [],
        employment_status : [],
        religion : [],
        age_group : [],
      };

  findResponses(survey.survey_token, function(error, responses) {
    if (error) return true;
    var d = 0;
    var loopR = function(responses) {
      if (d == responses.length) {
        surveyObj.analytics.questionnaire.avr_resp = Analyzer.AverageResponses(responses_array);
        surveyObj.analytics.questionnaire.avr_stacks = Analyzer.AverageCurrency(average_stacks_array, responses.length);
        surveyObj.analytics.questionnaire.daily = Analyzer.DayResponses(dates);
        surveyObj.analytics.questionnaire.monthly = Analyzer.MonthResponses(dates);
        var years = Analyzer.YearResponses(dates);
        surveyObj.analytics.questionnaire.yearly.years = years[0];
        surveyObj.analytics.questionnaire.yearly.values = years[1];
        surveyObj.analytics.respondents = Analyzer.RespondentAnalyzer(about);
        surveyObj.analytics.graphic_questions = Analyzer.GraphicQuestions(order, questions, responses_array);
        var q_r = Analyzer.ResponseOrganizer(order, questions, responses_array);
        surveyObj.analytics.questions = q_r[0];
        surveyObj.analytics.responses = q_r[1];

        if (currentRequest.type == "0") {
          currentRequest.analytic = surveyObj;
        } else if (currentRequest.type == "1") {
          currentRequest.analytic = {
            questionnaire : surveyObj.analytics.questionnaire
          };
        } else if (currentRequest.type == "2") {
          currentRequest.analytic = {
            graphic_questions : surveyObj.analytics.graphic_questions
          };
        } else if (currentRequest.type == "3") {
          currentRequest.analytic = {
            respondents : surveyObj.analytics.respondents
          };
        } else if (currentRequest.type == "6") {
          currentRequest.analytic = {
            questions : surveyObj.analytics.questions,
            responses : surveyObj.analytics.responses
          };
        }

        currentRequest.analytic = JSON.stringify(currentRequest.analytic, null, 2);

        if (currentRequest.format == "csv") {
          currentRequest.analytic = JSON.parse(currentRequest.analytic);
          jsonexport(currentRequest.analytic, function(err, csv){
              if(err) throw error;
              currentRequest.analytic = csv;
          });
        } else if (currentRequest.format == "xml") {
          currentRequest.analytic = JSON.parse(currentRequest.analytic);
          currentRequest.analytic = serializer.render(currentRequest.analytic)
        }

        return callback(null, currentRequest.analytic);
      }
      var response = responses[d];

      if (d == 0) surveyObj.analytics.questionnaire.first_response = response.created_at;
      if (d == responses.length - 1) surveyObj.analytics.questionnaire.last_response = response.created_at;

      if (response.respondent_token == process.env.HOMO_PUBLICA_CLAVEM) {

        about.sex.push("n/a");
        about.ethnicity.push("n/a");
        about.languages.push(response.response_lang);
        about.education.push("n/a");
        about.relationship_status.push("n/a");
        about.employment_status.push("n/a");
        about.religion.push("n/a");
        about.age_group.push("n/a");

        responses_array.push(response.responses);
        average_stacks_array.push(response.currency_recieved);
        dates.push(response.created_at);

        d++;
        loopR(responses);
      } else {
        Profiles.findOne({user_key : response.respondent_token}, function(error, profile) {
          if (profile == null || error) return true;

          responses_array.push(response.responses);
          average_stacks_array.push(response.currency_recieved);
          dates.push(response.created_at);

          about.sex.push(profile.about.sex);
          about.ethnicity.push(profile.about.ethnicity);
          about.languages.push(profile.about.languages);
          about.education.push(profile.about.education);
          about.relationship_status.push(profile.about.relationship_status);
          about.employment_status.push(profile.about.employment_status);
          about.religion.push(profile.about.religion);
          about.age_group.push(Functions.getAgeGroup(profile.about.birth_date));

          d++;
          loopR(responses);
        });
      }
    };
  loopR(responses);
  });
}

function RDP(poll, callback) {
  var pollObj = {
    analytics : {
      question : poll.question_details.question,
      options : poll.question_details.options,
      users : []
    }
  };

  findResponses(poll.poll_token, function(error, responses) {
    if (error) {
      return true;
    }

    var d = 0;
    var loopR = function(responses) {
      if (d == responses.length) {
        currentRequest.analytic = pollObj;

        currentRequest.analytic = JSON.stringify(currentRequest.analytic, null, 2);

        if (currentRequest.format == "csv") {
          currentRequest.analytic = JSON.parse(currentRequest.analytic);
          jsonexport(currentRequest.analytic, function(err, csv){
              if(err) throw error;
              currentRequest.analytic = csv;
          });
        } else if (currentRequest.format == "xml") {
          currentRequest.analytic = JSON.parse(currentRequest.analytic);
          currentRequest.analytic = serializer.render(currentRequest.analytic)
        }

        return callback(null, currentRequest.analytic);
      }
      var response = responses[d];

      if (response.respondent_token == process.env.HOMO_PUBLICA_CLAVEM) {

        var userObjA = {
          about : {
            sex : "n/a",
            ethnicity : "n/a",
            languages : response.response_lang,
            education : "n/a",
            relationship_status : "n/a",
            employment_status : "n/a",
            religion : "n/a",
            age_group : "n/a",
          },
          response : poll.question_details.options[response.responses[0]]
        };

        pollObj.analytics.users.push(userObjA);

        d++;
        loopR(responses);
      } else {
        Profiles.findOne({user_key : response.respondent_token}, function(error, profile) {
          if (profile == null || error) {
            return true;
          }

          var userObjA = {
            about : {
              sex : profile.about.sex,
              ethnicity : profile.about.ethnicity,
              languages : profile.about.languages,
              education : profile.about.education,
              relationship_status : profile.about.relationship_status,
              employment_status : profile.about.employment_status,
              religion : profile.about.religion,
              age_group : Functions.getAgeGroup(profile.about.birth_date)
            },
            response : poll.question_details.options[response.responses[0]]
          };

          pollObj.analytics.users.push(userObjA);

          d++;
          loopR(responses);
        });
      }
    };
    loopR(responses);
  });
}

function defaultDP(poll, callback) {
  var pollObj = {
    view : {
      token : poll.poll_token,
      image_url : poll.poll_details.image_url
    },
    analytics : {
      questionnaire : {
        name : poll.poll_details.name,
        question : poll.question_details.question,
        options : poll.question_details.options,
        percentages : [],
        amounts : [],
        first_response : "",
        total_responses : 0,
        last_response : "",
        avr_resp : 0,
        avr_resp_d : Analyzer.AverageDailyResponses(poll.created_at, poll.responses.length),
        avr_coins : 0,
        daily : [],
        monthly : [],
        yearly : {
          years : [],
          values : []
        }
      },
      respondents : [],
    }
  };

  var responses_array = [],
      average_coins_array = [],
      dates = [],
      about = {
        sex : [],
        ethnicity : [],
        languages : [],
        education : [],
        relationship_status : [],
        employment_status : [],
        religion : [],
        age_group : [],
      };

  findResponses(poll.poll_token, function(error, responses) {
    if (error) {
      return true;
    }

    var d = 0;
    var loopR = function(responses) {
      if (d == responses.length) {
        pollObj.analytics.questionnaire.avr_resp = Analyzer.AverageResponses(responses_array);
        pollObj.analytics.questionnaire.avr_coins = Analyzer.AverageCurrency(average_coins_array, responses.length);
        pollObj.analytics.questionnaire.daily = Analyzer.DayResponses(dates);
        pollObj.analytics.questionnaire.monthly = Analyzer.MonthResponses(dates);
        var years = Analyzer.YearResponses(dates);
        pollObj.analytics.questionnaire.yearly.years = years[0];
        pollObj.analytics.questionnaire.yearly.values = years[1];
        pollObj.analytics.respondents = Analyzer.RespondentAnalyzer(about);
        var percentages = Analyzer.PercentagesPoll(pollObj.analytics.questionnaire.options.length, responses_array);
        pollObj.analytics.questionnaire.amounts = percentages[0];
        pollObj.analytics.questionnaire.percentages = percentages[1];

        if (currentRequest.type == "0") {
          currentRequest.analytic = pollObj;
        } else if (currentRequest.type == "1") {
          currentRequest.analytic = {
            questionnaire : pollObj.analytics.questionnaire
          };
        } else if (currentRequest.type == "2") {
          currentRequest.analytic = {
            respondents : pollObj.analytics.respondents
          };
        }

        currentRequest.analytic = JSON.stringify(currentRequest.analytic, null, 2);

        if (currentRequest.format == "csv") {
          currentRequest.analytic = JSON.parse(currentRequest.analytic);
          jsonexport(currentRequest.analytic, function(err, csv){
              if(err) throw error;
              currentRequest.analytic = csv;
          });
        } else if (currentRequest.format == "xml") {
          currentRequest.analytic = JSON.parse(currentRequest.analytic);
          currentRequest.analytic = serializer.render(currentRequest.analytic)
        }

        return callback(null, currentRequest.analytic);
      }
      var response = responses[d];
      if (d == 0) pollObj.analytics.questionnaire.first_response = response.created_at;
      if (d == responses.length - 1) pollObj.analytics.questionnaire.last_response = response.created_at;

      pollObj.analytics.questionnaire.total_responses+=1;
      responses_array.push(response.responses);
      average_coins_array.push(response.currency_recieved);
      dates.push(response.created_at);

      if (response.respondent_token == process.env.HOMO_PUBLICA_CLAVEM) {

        about.sex.push("n/a");
        about.ethnicity.push("n/a");
        about.languages.push(response.response_lang);
        about.education.push("n/a");
        about.relationship_status.push("n/a");
        about.employment_status.push("n/a");
        about.religion.push("n/a");
        about.age_group.push("n/a");

        d++;
        loopR(responses);
      } else {
        Profiles.findOne({user_key : response.respondent_token}, function(error, profile) {
          if (profile == null || error) {
            return true;
          }

          about.sex.push(profile.about.sex);
          about.ethnicity.push(profile.about.ethnicity);
          about.languages.push(profile.about.languages);
          about.education.push(profile.about.education);
          about.relationship_status.push(profile.about.relationship_status);
          about.employment_status.push(profile.about.employment_status);
          about.religion.push(profile.about.religion);
          about.age_group.push(Functions.getAgeGroup(profile.about.birth_date));

          d++;
          loopR(responses);
        });
      }
    };
    loopR(responses);
  });
}

function findResponses(token, cb) {
  Responses.find({questionnaire_token : token}, function(error, responses) {
    if (error) {
      return cb("d", null);
    }

    cb(null, responses);

  }).sort({"created_at": 1});
}
