var functions = {};
var Notifications = modelRequire("notifications.js");

functions.hashedPasswordCreator = hashedPasswordCreator;
functions.createUserKey = createUserKey;
functions.createUserSessionKey = createUserSessionKey;
functions.createUserAuthKey = createUserAuthKey;
functions.phoneNumberNormalize = phoneNumberNormalize;
functions.generateCredentialKey = generateCredentialKey;
functions.createCategoryToken = createCategoryToken;
functions.createTeamKey = createTeamKey;
functions.createTeamSessionKey = createTeamSessionKey;
functions.createTeamAuthKey = createTeamAuthKey;
functions.hashedPasswordCreatorTeams = hashedPasswordCreatorTeams;
functions.createAdminAuthKey = createAdminAuthKey;
functions.hashedPasswordAdminCreator = hashedPasswordAdminCreator;
functions.createSurveyToken = createSurveyToken;
functions.createPollToken = createPollToken;
functions.createQAnswerKey = createQAnswerKey;
functions.determineStacks = determineStacks;
functions.cardType = cardType;
functions.answerURL = answerURL;
functions.editURL = editURL;
functions.receiptURL = receiptURL;
functions.imageTransform = imageTransform;
functions.receiptToken = receiptToken;
functions.getAgeGroup = getAgeGroup;
functions.patternRegex = patternRegex;
functions.analyticPercentagesPoll = analyticPercentagesPoll;
functions.notifSocket = NotifSocket;
functions.regexEscape = regexEscape;
functions.encryptResponse = encryptResponse;
module.exports = functions;

var phoneDigits = ["+", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];

function createUserSessionKey(username) {

  var multiplier = Math.floor(1 + Math.random() * 9);

  var tint = Date.now();

  var four_digits = (tint * multiplier).toString();

  var finalizedfd = four_digits.substr(four_digits.length - 4, 4);

  var currentTime = new Date();
  var currentTimeString = currentTime.toString();
  var timeUsername = currentTimeString + username;
  var md5Hashed = crypto.createHash('md5').update(timeUsername).digest("hex");
  var hashedUserSessionKey = crypto.createHash('sha1').update(md5Hashed).digest("hex");

  return finalizedfd + "." + process.env.USER_SESSION_CONSTANT + "." + hashedUserSessionKey;
}

function createUserKey(username) {

  if (typeof username == "undefined" || username == "undefined" || username == null) username = "";

  var usernameReplaced = "";

  var arrayReplacement = {"a":"x3d", "b":"sh3", "c":"5s9", "d":"63e", "e":"n7l", "f":"a2n", "g":"2t9", "h":"1lo", "i":"pe3", "j":"c1S", "k":"x0l", "l":"34q", "m":"lmd", "n":"gnm", "o":"z32", "p":"3vu", "q":"15z", "r":"3mq", "s":"og2", "t":"6vk", "u":"qf8", "v":"nv1", "w":"nke", "x":"hoq", "y":"vbv", "z":"5pr"};

  for (var char in username.toLowerCase()) {
    for (var key in arrayReplacement) {
      if (username[char] == key) {
        usernameReplaced += arrayReplacement[key];
      } else {
        usernameReplaced += username[char];
      }
    }
  }

  var sha1 = crypto.createHash('sha1').update(username).digest('hex');
  hash = new global.adler();
  hash.update(usernameReplaced);
  var md5Hashed = crypto.createHash('md5').update(usernameReplaced + process.env.USER_KEY_ENCRYPTION).digest("hex");
  var hashedUserKey = crypto.createHash('sha1').update(md5Hashed + process.env.USER_KEY_ENCRYPTION).digest("hex");

  return sha1.substr(sha1.length - 7, 7) + "." + hash.digest('hex') + "AuRo" + "." + hashedUserKey;
}

function createUserAuthKey(username, lastname) {
  var usernameReplaced = "";
  var lastnameReplaced = "";

  var arrayReplacement = {
    "a" : "hLz",
    "b" : "uKt",
    "c" : "hBC",
    "d" : "FmP",
    "e" : "29N",
    "f" : "8IJ",
    "g" : "Bsr",
    "h" : "ycZ",
    "i" : "Cc2",
    "j" : "0tD",
    "k" : "Ze3",
    "l" : "C9Q",
    "m" : "kTo",
    "n" : "i4m",
    "o" : "gII",
    "p" : "5tF",
    "q" : "EAI",
    "r" : "9hF",
    "s" : "FSi",
    "t" : "qNL",
    "u" : "37W",
    "v" : "asu",
    "w" : "wuI",
    "x" : "QcJ",
    "y" : "UOK",
    "z" : "H6o",
    "0" : "Nio",
    "1" : "ozl",
    "2" : "jHi",
    "3" : "uId",
    "4" : "xmk",
    "5" : "za0",
    "6" : "uf7",
    "9" : "iIl",
    "!" : "qkw",
    "?" : "Tta",
    "$" : "Lmy",
    "/" : "qqD",
    "%" : "lNF",
    "-" : "sh9",
    "_" : "d55",
    "." : "zxU"
  };

  for (let char in username.toLowerCase()) {
    for (let key in arrayReplacement) {
      if (username[char] == key) {
        usernameReplaced += arrayReplacement[key];
      } else {
        usernameReplaced += username[char];
      }
    }
  }

  for (let char in lastname) {
    for (let key in arrayReplacement) {
      if (lastname[char] == key) {
        lastnameReplaced += arrayReplacement[key];
      } else {
        lastnameReplaced += lastname[char];
      }
    }
  }

  var md5Hashed = crypto.createHash('md5').update(usernameReplaced + lastnameReplaced).digest("hex");
  var hashedUserAuthKey = crypto.createHash('sha1').update(md5Hashed).digest("hex");

  var currentTime = new Date();
  var tint = Date.now().toString();
  hashTINT = new global.adler();
  hashTINT.update(tint);
  var currentTimeString = currentTime.toString();
  var timeHashed = currentTimeString + hashedUserAuthKey;
  var md5Hashedd = crypto.createHash('md5').update(timeHashed + process.env.USER_AUTH_ENCRYPTION).digest("hex");
  var protectedUserAuthKey = crypto.createHash('sha1').update(md5Hashedd + process.env.USER_AUTH_ENCRYPTION).digest("hex");

  return process.env.USER_AUTH_KEY_CONSTANT + "." + hashTINT.digest('hex') + "." + protectedUserAuthKey;
}

function createTeamSessionKey(nickname) {

  var multiplier = Math.floor(1 + Math.random() * 9);
  var tint = Date.now();
  var four_digits = (tint * multiplier).toString();
  var finalizedfd = four_digits.substr(four_digits.length - 4, 4);
  var currentTime = new Date();
  var currentTimeString = currentTime.toString();
  var timeNickname = currentTimeString + nickname;
  var md5Hashed = crypto.createHash('md5').update(timeNickname).digest("hex");
  var hashedTeamSessionKey = crypto.createHash('sha1').update(md5Hashed).digest("hex");

  return finalizedfd + "." + process.env.TEAM_SESSION_CONSTANT + "." + hashedTeamSessionKey;
}

function createTeamKey(nickname) {
  if (typeof nickname == "undefined") nickname = "";

  var nicknameReplaced = "";

  var arrayReplacement = {"a":"x3d", "b":"sh3", "c":"5s9", "d":"63e", "e":"n7l", "f":"a2n", "g":"2t9", "h":"1lo", "i":"pe3", "j":"c1S", "k":"x0l", "l":"34q", "m":"lmd", "n":"gnm", "o":"z32", "p":"3vu", "q":"15z", "r":"3mq", "s":"og2", "t":"6vk", "u":"qf8", "v":"nv1", "w":"nke", "x":"hoq", "y":"vbv", "z":"5pr"};

  for (var char in nickname.toLowerCase()) {
    for (var key in arrayReplacement) {
      if (nickname[char] == key) {
        nicknameReplaced += arrayReplacement[key];
      } else {
        nicknameReplaced += nickname[char];
      }
    }
  }

  var sha1 = crypto.createHash('sha1').update(nickname).digest('hex');
  hash = new global.adler();
  hash.update(nicknameReplaced);
  var md5Hashed = crypto.createHash('md5').update(nicknameReplaced + process.env.TEAM_KEY_ENCRYPTION).digest("hex");
  var hashedTeamKey = crypto.createHash('sha1').update(md5Hashed + process.env.TEAM_KEY_ENCRYPTION).digest("hex");

  return sha1.substr(sha1.length - 7, 7) + "." + hash.digest('hex') + "dIm" + "." + hashedTeamKey;
}

function createTeamAuthKey(fullname, name) {
  var nameReplaced = "";
  var fullnameReplaced = "";

  var arrayReplacement = {
    "a" : "hLz",
    "b" : "uKt",
    "c" : "hBC",
    "d" : "FmP",
    "e" : "29N",
    "f" : "8IJ",
    "g" : "Bsr",
    "h" : "ycZ",
    "i" : "Cc2",
    "j" : "0tD",
    "k" : "Ze3",
    "l" : "C9Q",
    "m" : "kTo",
    "n" : "i4m",
    "o" : "gII",
    "p" : "5tF",
    "q" : "EAI",
    "r" : "9hF",
    "s" : "FSi",
    "t" : "qNL",
    "u" : "37W",
    "v" : "asu",
    "w" : "wuI",
    "x" : "QcJ",
    "y" : "UOK",
    "z" : "H6o",
    "0" : "Nio",
    "1" : "ozl",
    "2" : "jHi",
    "3" : "uId",
    "4" : "xmk",
    "5" : "za0",
    "6" : "uf7",
    "9" : "iIl",
    "!" : "qkw",
    "?" : "Tta",
    "$" : "Lmy",
    "/" : "qqD",
    "%" : "lNF",
    "-" : "sh9",
    "_" : "d55",
    "." : "zxU"
  };

  for (let char in fullname.toLowerCase()) {
    for (let key in arrayReplacement) {
      if (fullname[char] == key) {
        fullnameReplaced += arrayReplacement[key];
      } else {
        fullnameReplaced += fullname[char];
      }
    }
  }

  for (let char in name.toLowerCase()) {
    for (let key in arrayReplacement) {
      if (name[char] == key) {
        nameReplaced += arrayReplacement[key];
      } else {
        nameReplaced += name[char];
      }
    }
  }

  var md5Hashed = crypto.createHash('md5').update(fullnameReplaced + nameReplaced).digest("hex");
  var hashedTeamAuthKey = crypto.createHash('sha1').update(md5Hashed).digest("hex");

  var currentTime = new Date();
  var tint = Date.now().toString();
  hashTINT = new global.adler();
  hashTINT.update(tint);
  var currentTimeString = currentTime.toString();
  var timeHashed = currentTimeString + hashedTeamAuthKey;
  var md5Hashedd = crypto.createHash('md5').update(timeHashed + process.env.TEAM_AUTH_ENCRYPTION).digest("hex");
  var protectedTeamAuthKey = crypto.createHash('sha1').update(md5Hashedd + process.env.TEAM_AUTH_ENCRYPTION).digest("hex");

  return process.env.TEAM_AUTH_KEY_CONSTANT + "." + hashTINT.digest('hex') + "." + protectedTeamAuthKey;
}

function hashedPasswordCreator(password) {
  var passwordReplaced = "";

  var arrayReplacement = {"a":"2ed", "b":"8h3", "c":"2s4", "d":"23s", "e":"k2y", "f":"m7n", "g":"2v4", "h":"9m2", "i":"f3x", "j":"2cS", "k":"1s2", "l":"x7w", "m":"7q2", "n":"g3x", "o":"2z3", "p":"u3v", "q":"5z1", "r":"q3m", "s":"58g", "t":"6v1",
                           "u":"23c", "v":"nv3", "w":"2k2", "x":"h4s", "y":"1g3", "z":"3zw", "A":"l2n", "B":"y3z", "C":"u6d", "D":"jez", "E":"19x", "F":"loz", "G":"lp1", "H":"ir3", "I":"de2","J":"n42", "K":"j2a", "L":"nrz", "M":"nh2", "N":"l5z",
                           "O":"ht2", "P":"re1", "Q":"eq1", "R":"mzk", "S":"nha", "T":"y42", "U":"4za", "V":"mz2", "W":"jsu", "X":"o82", "Y":"o2k", "Z":"01m"};

  for (var char in password) {
    for (var key in arrayReplacement) {
      if (password[char] == key) {
        passwordReplaced += arrayReplacement[key];
      } else {
        passwordReplaced += password[char];
      }
    }
  }

  var md5Hashed = crypto.createHash('md5').update(passwordReplaced + process.env.PASSWORD_ENCRYPTION).digest("hex");
  var hashedPassword = crypto.createHash('sha1').update(md5Hashed + process.env.PASSWORD_ENCRYPTION).digest("hex");

  return  hashedPassword;
}

function hashedPasswordCreatorTeams(password) {
  var passwordReplaced = "";

  var arrayReplacement = {"a":"2ed", "b":"8h3", "c":"2s4", "d":"23s", "e":"k2y", "f":"m7n", "g":"2v4", "h":"9m2", "i":"f3x", "j":"2cS", "k":"1s2", "l":"x7w", "m":"7q2", "n":"g3x", "o":"2z3", "p":"u3v", "q":"5z1", "r":"q3m", "s":"58g", "t":"6v1",
                           "u":"23c", "v":"nv3", "w":"2k2", "x":"h4s", "y":"1g3", "z":"3zw", "A":"l2n", "B":"y3z", "C":"u6d", "D":"jez", "E":"19x", "F":"loz", "G":"lp1", "H":"ir3", "I":"de2","J":"n42", "K":"j2a", "L":"nrz", "M":"nh2", "N":"l5z",
                           "O":"ht2", "P":"re1", "Q":"eq1", "R":"mzk", "S":"nha", "T":"y42", "U":"4za", "V":"mz2", "W":"jsu", "X":"o82", "Y":"o2k", "Z":"01m"};

  for (var char in password) {
    for (var key in arrayReplacement) {
      if (password[char] == key) {
        passwordReplaced += arrayReplacement[key];
      } else {
        passwordReplaced += password[char];
      }
    }
  }

  var md5Hashed = crypto.createHash('md5').update(passwordReplaced + process.env.PASSWORD_CO_ENCRYPTION).digest("hex");
  var sha1Hashed = crypto.createHash('sha1').update(md5Hashed + passwordReplaced + process.env.PASSWORD_CO_ENCRYPTION).digest("hex");
  var hashedPassword = crypto.createHash('sha1').update(sha1Hashed + md5Hashed + passwordReplaced).digest("hex");

  return  hashedPassword;
}

function generateCredentialKey() {
  var CredentialKeySix = "";
  var randomInt = Math.floor(Math.random() * 9000000) + 1000;
  var currentTime = new Date();
  var md5Hashed = crypto.createHash('md5').update(currentTime + process.env.FORGOT_CREDENTIAL_ENCRYPTION+ randomInt).digest("hex");
  var CredentialKey = crypto.createHash('sha1').update(md5Hashed + process.env.FORGOT_CREDENTIAL_ENCRYPTION + randomInt).digest("hex");

  for (var char in CredentialKey) {
    var i = Math.floor(Math.random() * 900) + 1;
    var v = Math.floor(Math.random() * 15) + 25;
    var num = Math.floor(Math.random() * 13) + 12;
    var c = Math.floor(Math.random() * 11) + 1;

    if (CredentialKeySix.length != 6) {
      if (i >= 600) {
        CredentialKeySix += CredentialKey[v];
      } else if (i >= 300 && i < 600) {
        CredentialKeySix += CredentialKey[num];
      } else if (i < 300) {
        CredentialKeySix += CredentialKey[c];
      }
    } else {
      break;
    }
  }

  return  CredentialKeySix;
}

function phoneNumberNormalize(phoneNumber) {
  var normalizedNumber = "";
  for (var digit in phoneNumber) {
    for (var currentDigit in phoneDigits) {
      if (phoneNumber[digit] == phoneDigits[currentDigit]) {
        normalizedNumber += phoneNumber[digit];
      }
    }
  }

  return normalizedNumber;
}

function createCategoryToken(name) {
  var currentTime = new Date();
  var currentTimeString = currentTime.toString();
  var timeCategory = currentTimeString + name;
  var md5Hashed = crypto.createHash('md5').update(timeCategory + process.env.CATEGORY_HASH).digest("hex");
  var hashedCategoryKey = crypto.createHash('sha1').update(md5Hashed + process.env.CATEGORY_HASH).digest("hex");

  return  hashedCategoryKey;
}

function createAdminAuthKey(name, password) {

  var a = name.split("");
  var n = a.length;

  for(var i = n - 1; i > 0; i--) {
     var j = Math.floor(Math.random() * (i + 1));
     var tmp = a[i];
     a[i] = a[j];
     a[j] = tmp;
  }
  var m = a.join("");
  var d = new Date();
  d = d.getTime();
  var p = [password[1], password[0], password[password.length-1], password[password.length-2]];

  var md5Hashed = crypto.createHash('md5').update(m).digest("hex");
  var hashedAuthKey = crypto.createHash('sha1').update(md5Hashed + d).digest("hex");

  return hashedAuthKey.substr(0, 12) + p.join("");
}

function hashedPasswordAdminCreator(password) {
  var md5Hashed = crypto.createHash('md5').update(password + process.env.ADMIN_PASSWORD_HASH).digest("hex");
  var hashedAdminPassword = crypto.createHash('sha256').update(md5Hashed + process.env.ADMIN_PASSWORD_HASH).digest("hex");
  var rr = hashedAdminPassword.slice(0, hashedAdminPassword.length / 2);
  var rl = hashedAdminPassword.slice(hashedAdminPassword.length / 2, hashedAdminPassword.length)

  return rl + rr;
}

function createSurveyToken(key, type) {
  var currentTime = new Date();
  var currentTimeString = currentTime.toString();
  var timeKey = currentTimeString + key;
  var md5Hashed = crypto.createHash('md5').update(timeKey + process.env.SURVEY_HASH).digest("hex");
  var hashedCategoryKey = crypto.createHash('sha1').update(md5Hashed + key + process.env.SURVEY_HASH).digest("hex");

  return  type+hashedCategoryKey;
}

function createPollToken(key, type) {
  var currentTime = new Date();
  var currentTimeString = currentTime.toString();
  var timeKey = currentTimeString + key;
  var md5Hashed = crypto.createHash('md5').update(timeKey + process.env.POLL_HASH).digest("hex");
  var hashedCategoryKey = crypto.createHash('sha1').update(md5Hashed + key + process.env.POLL_HASH).digest("hex");

  return  type+hashedCategoryKey;
}

function determineStacks(fr, cb, br, rr, mr) {
  var total_stacks = 0;
  var stacks_value = {
    fr_stacks : 7,
    cb_stacks : 5,
    br_stacks : 2,
    rr_stacks : 3,
    mr_stacks : 4
  };

  var fr_length = fr.length;
      cb_length = cb.length;
      br_length = br.length;
      rr_length = rr.length;
      mr_length = mr.length;

  var questions_array = [fr_length, cb_length, br_length, rr_length, mr_length];

  /*
    *
    * ORDER OF SURVEY SIP MINIMALITY
    *
    * THE MORE QUESTIONS THE LESS STACKS
    *
    * BOOLEAN RESPONSE
    * RADIO RESPONSE
    * MENU ITEM
    * CHECKBOX
    * FREE RESPONSE
    *
  */

  var i = 0;

  for (var stacks in stacks_value) {
    total_stacks += stackMathmatician(stacks_value[stacks], questions_array[i]);
    i++;
  }

  return total_stacks.toFixed(0);
}

function stackMathmatician(stacks, questions) {
  if (questions => 60) {
    stacks /= 2;
  }

  return stacks * questions;
}

function cardType(number) {
    // visa
    var re = new RegExp("^4");
    if (number.match(re) != null)
        return "Visa";

    // Mastercard
    // Updated for Mastercard 2017 BINs expansion
     if (/^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/.test(number))
        return "Mastercard";

    // AMEX
    re = new RegExp("^3[47]");
    if (number.match(re) != null)
        return "AMEX";

    // Discover
    re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
    if (number.match(re) != null)
        return "Discover";

    // Diners
    re = new RegExp("^36");
    if (number.match(re) != null)
        return "Diners";

    // Diners - Carte Blanche
    re = new RegExp("^30[0-5]");
    if (number.match(re) != null)
        return "Diners - Carte Blanche";

    // JCB
    re = new RegExp("^35(2[89]|[3-8][0-9])");
    if (number.match(re) != null)
        return "JCB";

    // Visa Electron
    re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
    if (number.match(re) != null)
        return "Visa Electron";

    return "";
}

function answerURL(token, t) {
  if (t == 0) {
    t = "/surveys/";
  } else if (t == 1) {
    t = "/polls/";
  }
  return process.env.DOMAIN_HTTPS + t + token + "/answer";
}

function editURL(token, t) {
  if (t == 0) {
    t = "/surveys/";
  } else if (t == 1) {
    t = "/polls/";
  }
  return process.env.DOMAIN_HTTPS + t + token + "/edit";
}

function receiptURL(token, t) {
  if (t == 0) {
    t = "/surveys/";
  } else if (t == 1) {
    t = "/polls/";
  }
  return process.env.DOMAIN_HTTPS + t + token + "/receipt";
}

function imageTransform(link, w, h) {
  if (link === null || link == "" || typeof link == "undefined") return "";
  var s = link.split("upload/");
  var ns;
  if (w == null) {
    ns = "upload/c_scale,h_"+h+"/";
  } else if (h == null) {
    ns = "upload/c_fill,w_"+w+"/";
  } else {
    ns = "upload/c_fill,w_"+w+",h_"+h+"/";
  }
  var d = new Date();
  return s[0]+ns+s[1]+"?"+d.getTime();
}

function receiptToken() {
  var token = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 6; i++)
    token += possible.charAt(Math.floor(Math.random() * possible.length));

  var d = Date.now();
  d = d.toString();
  token+=d.substr(d.length-8, d.length);

  return token;
}

function getAgeGroup(birth) {
  var birthdate = new Date(birth);
  var today = new Date();
  var diff = today-birthdate;
  var age = Math.floor(diff/31536000000);
  var age_group;

  switch(true) {
    case (age < 18):
      age_group = "t";
      break;
    case (age < 25):
      age_group = "lt";
      break;
    case (age < 35):
      age_group = "ea";
      break;
    case (age < 45):
      age_group = "ad";
      break;
    case (age < 55):
      age_group = "la";
      break;
    case (age < 65):
      age_group = "es";
      break;
    case (age < 75):
      age_group = "s";
      break;
    case (age >= 75):
      age_group = "ls";
      break;
  }

  return age_group;
}

function analyticPercentagesPoll(responses) {
  var responsesObj = {};
  var analyticsObj = {};
  var actualResponses = 0;

  responses.forEach(function(response) {
    if (response.responses[0] === "") return;
    if (typeof responsesObj[response.responses[0]] == "undefined") {
      var object = {
        "times" : 1
      };
      var analytic = {
        "percentage" : 0
      };

      responsesObj[response.responses[0]] = object;
      analyticsObj[response.responses[0]] = analytic;
    } else {
      responsesObj[response.responses[0]].times+=1;
    }
    actualResponses += 1;
  });

  for (var response in responsesObj) {
    analyticsObj[response].percentage = ((responsesObj[response].times / actualResponses) * 100).toFixed(2);
  }

  return analyticsObj;
}

function patternRegex(pattern) {
  var regex = [];
  switch (pattern) {
    case "a":
      regex[0] = ".+$";
      regex[1] = "mi";
      break;
    case "t":
      regex[0] = "^([A-Z !?'\"&$]+)$";
      regex[1] = "mi";
      break;
    case "n":
      regex[0] = "\d+$";
      regex[1] = "m";
      break;
    case "tn":
      regex[0] = "[A-Z \d]+$";
      regex[1] = "mi";
      break;
    case "d":
      regex[0] = "([0][1-9]|[1][0-2])[-|\/]([0][1-9]|[1-2][0-9]|[3][0-2])[-|\/]([\d]{4})( A\.D| B\.C)?$";
      regex[1] = "m";
      break;
    case "ts":
      regex[0] = "([0-1][0-9]|2[0-3])(:[0-5][0-9])(:[0-5][0-9])?$";
      regex[1] = "m";
      break;
    case "h":
      regex[0] = "^(http:\/\/|https:\/\/)(.+\..+\..+)$";
      regex[1] = "mi";
      break;
  }

  return regex;
}

function NotificationToken() {
  var intt = new Date().getTime();
  var rand = Math.floor(Math.random() * 1000);
  var token = rand.toString() + intt.toString().substr(5,intt.length);

  return token;
}

function NotifSocket(data, callback) {
  var Notification = new Notifications({
    reciever : data.reciever,
    notification_token : NotificationToken(),
    read : false,
    type : data.type,
    title : data.title,
    content : data.content,
    url : data.url,
    created_at : new Date(),
    updated_at : new Date()
  });

  Notification.save(function(error) {
    if(error) return callback(true);

    global.io.emit(data.reciever, "cn");
    callback(null);
  });
}

function regexEscape(regex) {
  return regex.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

var alphabet=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",",","!","?",".","#","@","$","%","^","&","*","(",")","-","_","+","=","{","}","[","]","\\","/","|","`","~","<",">",":",";","'",'"',"1","2","3","4","5","6","7","8","9","0"];function shift(r){for(var a=[],t=r;t<alphabet.length;t++)a.push(alphabet[t]);for(t=0;t<r;t++)a.push(alphabet[t]);return a}function encrypt(r,a){if("number"==typeof a||"boolean"==typeof a||""==a||"undefined"==typeof a)return a;a=a.toString();var t=shift(r),e="";for(var n in a){var o=a[n],f=t.indexOf(o);e+=-1==f?" ":alphabet[f]}return e}function loop(r,a){var t={};Array.isArray(a)&&(t=[]);for(var e in a){var n;if(Array.isArray(a)){n=e}else{n=encrypt(r,e)};if("object"!=typeof a[e]){var o=encrypt(r,a[e]);t[n]=o}else t[n]=loop(r,a[e])}return t}

function encryptResponse(response) {
  var s = Math.floor(Math.random() * (alphabet.length - 2)) + 1
  var encrypted = {
    __s__ : s
  };
  for (var key in response) {
    if (key == "resultCode" || key == "resultDate") {
      encrypted[key] = response[key];
      continue;
    }
    ke = encrypt(s, key);
    if (typeof response[key] == "object") {
      encrypted[ke] = loop(s, response[key]);
      continue;
    }
    ve = encrypt(s, response[key]);

    encrypted[ke] = ve;
  }

  return JSON.parse(JSON.stringify(encrypted));
}

function createQAnswerKey() {
  var GlobalAnswerKey = "";
  var randomInt = Math.floor(Math.random() * 9000000) + 1000;
  var currentTime = new Date();
  var md5Hashed = crypto.createHash('md5').update(currentTime + randomInt).digest("hex");
  var CredentialKey = crypto.createHash('sha1').update(md5Hashed + randomInt).digest("hex");

  for (var char in CredentialKey) {
    var i = Math.floor(Math.random() * 900) + 1;
    var v = Math.floor(Math.random() * 15) + 25;
    var num = Math.floor(Math.random() * 13) + 12;
    var c = Math.floor(Math.random() * 11) + 1;

    if (GlobalAnswerKey.length != 8) {
      if (i >= 600) {
        GlobalAnswerKey += CredentialKey[v];
      } else if (i >= 300 && i < 600) {
        GlobalAnswerKey += CredentialKey[num];
      } else if (i < 300) {
        GlobalAnswerKey += CredentialKey[c];
      }
    } else {
      break;
    }
  }

  return  GlobalAnswerKey;
}
