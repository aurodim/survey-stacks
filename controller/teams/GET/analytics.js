"use strict";

var Profiles = modelRequire("profiles.js");
var Teams = modelRequire("teams.js");
var Surveys = modelRequire("surveys.js");
var Polls = modelRequire("polls.js");
var Responses = modelRequire("responses.js");
var Functions = rootRequire("/controller/globalFunctions.js");
var Analyzer = rootRequire("/controller/analyzer.js");

function Team() {
  this.ip = "";
  this.validation_key = "";
  this.auth_key = "";
  this.session_key = "";
  this.lang = "";
  this.webApp = false;
  this.analytics = {
    surveys : [],
    polls : []
  };
  this.surveys_created = [];
  this.polls_created = [];
}

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.validation_key = validation_key;
  currentTeam.auth_key = auth_key;
  currentTeam.session_key = session_key;
  currentTeam.lang = lang;
  currentTeam.webApp = webApp;

  verifyGatherData(callback, res);
}

function verifyGatherData(callback, res) {
  Teams.find({deleted : false, team_key : currentTeam.validation_key, team_session_key : currentTeam.session_key, team_auth_key : currentTeam.auth_key}, function(error, profiles) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (profiles.length == "1") {
        var profile = profiles[0];

        currentTeam.surveys_created = profile.surveys_created;
        currentTeam.polls_created = profile.polls_created;

        asyncData(callback, res);
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function asyncData(callback, res) {
  async.parallel({
      surveys : function(callback) {
        surveyAnalytics(callback);
      },
      polls : function(callback) {
        pollAnalytics(callback);
      }
  }, function(err) {

      if (err == "d") {
        callback(500, "callback", res);
        return;
      }

      sendResponse(callback, res);
  });
}

function surveyAnalytics(callback) {
  Surveys.find({deleted : false, survey_token : {$in : currentTeam.surveys_created}, creator_key : currentTeam.validation_key}, function(error, surveys) {
    if (error) return callback("d");
    if (surveys.length == 0) return callback(null);

    async.each(surveys, function(survey, cb) {
      var surveyObj = {
        view : {
          token : survey.survey_token,
          image_url : Functions.imageTransform(survey.survey_details.image_url, null, 200)
        },
        analytics : {
          questionnaire : {
            name : survey.survey_details.name,
            first_response : "",
            total_responses : survey.responses.length,
            last_response : "",
            avr_resp : 0,
            avr_resp_d : 0,
            avr_stacks : 0,
            daily : [],
            monthly : [],
            yearly : {
              years : [],
              values : []
            }
          },
          respondents : [],
          graphic_questions : [],
          questions : [],
          responses : []
        },
        cards : []
      };

      var responses_array = [],
          average_stacks_array = [],
          dates = [],
          order = survey.question_details.order,
          questions = {
            fr : survey.question_details.free_response,
            cbr : survey.question_details.checkbox,
            rr : survey.question_details.radio,
            mr : survey.question_details.menu,
            br : survey.question_details.boolean
          },
          about = {
            sex : [],
            ethnicity : [],
            languages : [],
            education : [],
            relationship_status : [],
            employment_status : [],
            religion : [],
            age_group : [],
          };

      findResponses(survey.survey_token, function(error, responses) {
        if (error) return cb("d");
        if (responses.length == 0) return cb(null);

        var d = 0;
        var loopR = function(responses) {
          if (d == responses.length) {
            surveyObj.analytics.questionnaire.avr_resp_d = Analyzer.AverageDailyResponses(dates);
            surveyObj.analytics.questionnaire.avr_resp = Analyzer.AverageResponses(responses_array);
            surveyObj.analytics.questionnaire.avr_stacks = Analyzer.AverageCurrency(average_stacks_array, responses.length);
            surveyObj.analytics.questionnaire.daily = Analyzer.DayResponses(dates);
            surveyObj.analytics.questionnaire.monthly = Analyzer.MonthResponses(dates);
            var years = Analyzer.YearResponses(dates);
            surveyObj.analytics.questionnaire.yearly.years = years[0];
            surveyObj.analytics.questionnaire.yearly.values = years[1];
            surveyObj.analytics.respondents = Analyzer.RespondentAnalyzer(about);
            surveyObj.analytics.graphic_questions = Analyzer.GraphicQuestions(order, questions, responses_array);
            var q_r = Analyzer.ResponseOrganizer(order, questions, responses_array);
            surveyObj.analytics.questions = q_r[0];
            surveyObj.analytics.responses = q_r[1];
            currentTeam.analytics.surveys.push(surveyObj);

            return cb(null);
          }
          var response = responses[d];

          if (d == 0) surveyObj.analytics.questionnaire.first_response = response.created_at;
          if (d == responses.length - 1) surveyObj.analytics.questionnaire.last_response = response.created_at;

          var card = {
            username : "",
            profile_image_url : "",
            labs : {},
            collected : {},
            responses : [],
            date : "",
            expand : false
          };

          if (response.respondent_token == process.env.HOMO_PUBLICA_CLAVEM) {

            about.sex.push("n/a");
            about.ethnicity.push("n/a");
            about.languages.push(response.response_lang);
            about.education.push("n/a");
            about.relationship_status.push("n/a");
            about.employment_status.push("n/a");
            about.religion.push("n/a");
            about.age_group.push("n/a");

            responses_array.push(response.responses);
            average_stacks_array.push(response.currency_recieved);
            dates.push(response.created_at);

            card.profile_image_url = "RANDOM IMAGE";
            card.response = response.responses;
            card.date = new Date(response.created_at).getTime();
            card.labs = response.labs;
            surveyObj.cards.push(card);

            d++;
            loopR(responses);
          } else {
            Profiles.findOne({user_key : response.respondent_token}, function(error, profile) {
              if (profile == null || error) return true;

              responses_array.push(response.responses);
              average_stacks_array.push(response.currency_recieved);
              dates.push(response.created_at);

              if (profile.about.sex == "") {
                about.sex.push("n/a");
              } else {
                about.sex.push(profile.about.sex);
              }

              if (profile.about.ethnicity.length == 1) {
                about.ethnicity.push(profile.about.ethnicity[0]);
              } else if (profile.about.ethnicity.length == 0) {
                about.ethnicity.push("n/a");
              }else {
                about.ethnicity.push(profile.about.ethnicity);
              }

              if (profile.about.languages.length == 1) {
                about.languages.push(profile.about.languages[0]);
              } else if (profile.about.languages.length == 0) {
                about.languages.push("n/a");
              }else {
                about.languages.push(profile.about.languages);
              }

              if (profile.about.education == "") {
                about.education.push("n/a");
              } else {
                about.education.push(profile.about.education);
              }

              if (profile.about.relationship_status == "") {
                about.relationship_status.push("n/a");
              } else {
                about.relationship_status.push(profile.about.relationship_status);
              }

              if (profile.about.employment_status == "") {
                about.employment_status.push("n/a");
              } else {
                about.employment_status.push(profile.about.employment_status);
              }

              if (profile.about.religion == "") {
                about.religion.push("n/a");
              } else {
                about.religion.push(profile.about.religion);
              }

              about.age_group.push(Functions.getAgeGroup(profile.about.birth_date));

              card.username = profile.username;
              card.profile_image_url = Functions.imageTransform(profile.profile_picture_url, null, 350);
              card.labs = response.labs;
              card.responses = response.responses;
              card.date = new Date(response.created_at).getTime();

              for (var i = 0; i < survey.survey_details.collected.length; i++) {
                switch (survey.survey_details.collected[i]) {
                  case "fullname":
                    card.collected.fullname = profile.fullname;
                    break;
                  case "email_phone":
                    card.collected.fullname = profile.email_phone;
                    break;
                  case "employment":
                    card.collected.employment_status = profile.about.employment;
                    break;
                  case "relationship":
                    card.collected.relationship_status = profile.about.relationship;
                    break;
                  case "education":
                    card.collected.education = profile.about.education;
                    break;
                  case "languages":
                    card.collected.languages = profile.about.languages;
                    break;
                  case "ethnicity":
                    card.collected.ethnicity = profile.about.ethnicity;
                    break;
                  case "sex":
                    card.collected.sex = profile.about.sex;
                    break;
                  case "religion":
                    card.collected.religion = profile.about.religion;
                    break;
                  case "birth_date":
                    card.collected.birth_date = profile.about.birth_date;
                    break;
                }
              }

              surveyObj.cards.push(card);

              d++;
              loopR(responses);
            });
          }
        };
        loopR(responses);
      });
    }, function(error) {
      if (error) return callback(error);

      callback(null);
    });
  });
}

function pollAnalytics(callback) {
  Polls.find({deleted : false, poll_token : {$in : currentTeam.polls_created}, creator_key : currentTeam.validation_key}, function(error, polls) {
    if (error) return callback("d");
    if (polls.length == 0) return callback(null);

    async.each(polls, function(poll, cb) {
      var pollObj = {
        view : {
          token : poll.poll_token,
          image_url : Functions.imageTransform(poll.poll_details.image_url, null, 200)
        },
        analytics : {
          questionnaire : {
            name : poll.poll_details.name,
            question : poll.question_details.question,
            options : poll.question_details.options,
            percentages : [],
            amounts : [],
            first_response : "",
            total_responses : 0,
            last_response : "",
            avr_resp : 0,
            avr_resp_d : 0,
            avr_coins : 0,
            daily : [],
            monthly : [],
            yearly : {
              years : [],
              values : []
            }
          },
          respondents : [],
        },
        cards : []
      };

      var responses_array = [],
          average_coins_array = [],
          dates = [],
          about = {
            sex : [],
            ethnicity : [],
            languages : [],
            education : [],
            relationship_status : [],
            employment_status : [],
            religion : [],
            age_group : [],
          };

      findResponses(poll.poll_token, function(error, responses) {
        if (error) return cb("d");
        if (responses.length == 0) return cb(null);

        var d = 0;
        var loopR = function(responses) {
          if (d == responses.length) {
            pollObj.analytics.questionnaire.avr_resp_d = Analyzer.AverageDailyResponses(dates);
            pollObj.analytics.questionnaire.avr_resp = Analyzer.AverageResponses(responses_array);
            pollObj.analytics.questionnaire.avr_coins = Analyzer.AverageCurrency(average_coins_array, responses.length);
            pollObj.analytics.questionnaire.daily = Analyzer.DayResponses(dates);
            pollObj.analytics.questionnaire.monthly = Analyzer.MonthResponses(dates);
            var years = Analyzer.YearResponses(dates);
            pollObj.analytics.questionnaire.yearly.years = years[0];
            pollObj.analytics.questionnaire.yearly.values = years[1];
            pollObj.analytics.respondents = Analyzer.RespondentAnalyzer(about);
            var percentages = Analyzer.PercentagesPoll(pollObj.analytics.questionnaire.options.length, responses_array);
            pollObj.analytics.questionnaire.amounts = percentages[0];
            pollObj.analytics.questionnaire.percentages = percentages[1];
            currentTeam.analytics.polls.push(pollObj);

            return cb(null);
          }

          var response = responses[d];
          if (d == 0) pollObj.analytics.questionnaire.first_response = response.created_at;
          if (d == responses.length - 1) pollObj.analytics.questionnaire.last_response = response.created_at;

          if (response.responses[0] == "") {
            d++;
            return loopR(responses);
          }

          pollObj.analytics.questionnaire.total_responses+=1;
          responses_array.push(response.responses);
          average_coins_array.push(response.currency_recieved);
          dates.push(response.created_at);

          var card = {
            username : "",
            profile_image_url : "",
            labs : {},
            collected : {
              fullname : "",
              sex : "",
              ethnicity : "",
              languages : "",
              education : "",
              relationship_status : "",
              employment_status : "",
              religion : "",
              age_group : "",
            },
            responses : [],
            date : "",
            expand : false
          };

          if (response.respondent_token == process.env.HOMO_PUBLICA_CLAVEM) {

            about.sex.push("n/a");
            about.ethnicity.push("n/a");
            about.languages.push(response.response_lang);
            about.education.push("n/a");
            about.relationship_status.push("n/a");
            about.employment_status.push("n/a");
            about.religion.push("n/a");
            about.age_group.push("n/a");

            card.profile_image_url = "RANDOM IMAGE";
            card.labs = response.labs;
            card.response = response.responses;
            card.date = new Date(response.created_at).getTime();

            pollObj.cards.push(card);

            d++;
            loopR(responses);
          } else {
            findProfile(response.respondent_token, function(error, profile) {
              if ((profile == null || error) && i == polls.length - 1) {
                d++;
                return loopR(responses);
              }
              if (profile == null || error) {
                d++;
                return loopR(responses);
              }

              if (profile.about.sex == "") {
                about.sex.push("n/a");
              } else {
                about.sex.push(profile.about.sex);
              }

              if (profile.about.ethnicity.length == 1) {
                about.ethnicity.push(profile.about.ethnicity[0]);
              } else if (profile.about.ethnicity.length == 0) {
                about.ethnicity.push("n/a");
              }else {
                about.ethnicity.push(profile.about.ethnicity);
              }

              if (profile.about.languages.length == 1) {
                about.languages.push(profile.about.languages[0]);
              } else if (profile.about.languages.length == 0) {
                about.languages.push("n/a");
              }else {
                about.languages.push(profile.about.languages);
              }

              if (profile.about.education == "") {
                about.education.push("n/a");
              } else {
                about.education.push(profile.about.education);
              }

              if (profile.about.relationship_status == "") {
                about.relationship_status.push("n/a");
              } else {
                about.relationship_status.push(profile.about.relationship_status);
              }

              if (profile.about.employment_status == "") {
                about.employment_status.push("n/a");
              } else {
                about.employment_status.push(profile.about.employment_status);
              }

              if (profile.about.religion == "") {
                about.religion.push("n/a");
              } else {
                about.religion.push(profile.about.religion);
              }

              about.age_group.push(Functions.getAgeGroup(profile.about.birth_date));

              card.profile_image_url = profile.profile_picture_url;
              card.labs = response.labs;
              card.response = response.responses;
              card.date = new Date(response.created_at).getTime();

              for (var i = 0; i < poll.poll_details.collected.length; i++) {
                switch (poll.poll_details.collected[i]) {
                  case "fullname":
                    card.collected.fullname = profile.fullname;
                    break;
                  case "email_phone":
                    card.collected.fullname = profile.email_phone;
                    break;
                  case "employment":
                    card.collected.employment_status = profile.about.employment;
                    break;
                  case "relationship":
                    card.collected.relationship_status = profile.about.relationship;
                    break;
                  case "education":
                    card.collected.education = profile.about.education;
                    break;
                  case "languages":
                    card.collected.languages = profile.about.languages;
                    break;
                  case "ethnicity":
                    card.collected.ethnicity = profile.about.ethnicity;
                    break;
                  case "sex":
                    card.collected.sex = profile.about.sex;
                    break;
                  case "religion":
                    card.collected.religion = profile.about.religion;
                    break;
                  case "birth_date":
                    card.collected.birth_date = profile.about.birth_date;
                    break;
                }
              }

              pollObj.cards.push(card);

              d++;
              loopR(responses);
            });
          }
        };
        loopR(responses);
      });
    }, function(error) {
      if (error) return callback(error);

      callback(null);
    });
  });
}


function findResponses(token, cb) {
  Responses.find({questionnaire_token : token}, function(error, responses) {
    if (error) {
      return cb("d", null);
    }

    cb(null, responses);

  }).sort({"created_at": 1});
}

function findProfile(token, cb) {
  Profiles.findOne({user_key : token}, function(error, profile) {
    if (error) {
      return cb("d", null);
    }

    cb(null, profile);
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {
    "webApp" : false,
    "resultCode" : resultCode,
     "resultDate" : resultDate,
     "analytics" : currentTeam.analytics,
  };

  callback(200, resultArray, res);
}
