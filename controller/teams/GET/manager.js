"use strict";

var Teams = modelRequire("teams.js");
var Surveys = modelRequire("surveys.js");
var Polls = modelRequire("polls.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Team() {
  this.ip = "";
  this.validation_key = "";
  this.auth_key = "";
  this.session_key = "";
  this.lang = "";
  this.webApp = false;
  this.surveyData = [];
  this.pollData = [];
};

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.validation_key = validation_key;
  currentTeam.auth_key = auth_key;
  currentTeam.session_key = session_key;
  currentTeam.lang = lang;
  currentTeam.webApp = webApp;

  verify(callback, res);
}

function verify(callback, res) {
  Teams.find({deleted : false, team_key : currentTeam.validation_key, team_session_key : currentTeam.session_key, team_auth_key : currentTeam.auth_key}, function(error, teams) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (teams.length == "1") {

        gather(callback, res);
      } else {
        callback(401, "wrongMatch", res);
      }
    }
  }).limit(1);
}

function gather(callback, res) {
  async.parallel({
      surveys : function(cb) {
        gatherSurveys(cb);
      },
      polls : function(cb) {
        gatherPolls(cb);
      }
  }, function(error){
    if (error) return callback(500, "error", res);

    sendResponse(callback, res);
  });
}

function gatherSurveys(cb) {
  Surveys.find({deleted : false, creator_key : currentTeam.validation_key}, function(error, surveys) {
    if (error) {
      return cb(true);
    }

    var data = [];

    surveys.forEach(function(survey) {
      var editO = {
        "edit_url" : Functions.editURL(survey.survey_token, 0),
        "image_url" : Functions.imageTransform(survey.survey_details.image_url,null,200)
      };
      data.push(editO);
    });

    currentTeam.surveyData = data;

    cb(null);
  });
}

function gatherPolls(cb) {
  Polls.find({deleted : false, creator_key : currentTeam.validation_key}, function(error, polls) {
    if (error) {
      return cb(true);
    }

    var data = [];

    polls.forEach(function(poll) {
      var editO = {
        "edit_url" : Functions.editURL(poll.poll_token, 1),
        "image_url" : Functions.imageTransform(poll.poll_details.image_url,null,200)
      };
      data.push(editO);
    });

    currentTeam.pollData = data;

    cb(null);
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentTeam.webApp,
    "resultCode" : resultCode,
     "resultDate" : resultDate,
     "surveys" : currentTeam.surveyData,
     "polls" : currentTeam.pollData
  };

  callback(200, resultArray, res);
}
