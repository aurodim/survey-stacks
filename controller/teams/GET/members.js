"use strict";

var Teams = modelRequire("teams.js");
var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Team() {
  this.ip = "";
  this.validation_key = "";
  this.auth_key = "";
  this.session_key = "";
  this.lang = "";
  this.webApp = false;
  this.awaiting_list = [];
  this.members_list = [];
  this.awaiting = [];
  this.members = [];
};

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.validation_key = validation_key;
  currentTeam.auth_key = auth_key;
  currentTeam.session_key = session_key;
  currentTeam.lang = lang;
  currentTeam.webApp = webApp;

  verify(callback, res);
}

function verify(callback, res) {
  Teams.findOne({deleted : false, team_key : currentTeam.validation_key, team_session_key : currentTeam.session_key, team_auth_key : currentTeam.auth_key}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == "" || team == null) return callback(401, "wrongMatch", res);

    currentTeam.awaiting_list = team.tbc;
    currentTeam.members_list = team.members;

    gather(callback, res);
  });
}

function gather(callback, res) {
  async.parallel({
      awaiting : function(callback) {
        lookA(callback);
      },
      members : function(callback) {
        lookM(callback);
      }
  }, function(err) {

      if (err == "d") {
        callback(500, "callback", res);
        return;
      }

      sendResponse(callback, res);
  });
}

function profileURL(token, username) {
  return "/users/"+token+"/"+username+"/profile";
}

function lookA(cb) {
  Profiles.find({user_key : {$in : currentTeam.awaiting_list}}, function(error, profiles) {
    if (error) return cb("d");

    for (var i = 0; i < profiles.length; i++) {
      var person = profiles[i];
      currentTeam.awaiting.push({
        profile_url : profileURL(profile.user_key, profile.username),
        image_url : Functions.imageTransform(profile.profile_picture_url, null, 200),
        fullname : profile.fullname,
        username : profile.username,
        focused : false
      });
    }

    cb(null);
  })
}

function lookM(cb) {
  Profiles.find({user_key : {$in : currentTeam.members_list}}, function(error, profiles) {
    if (error) return cb("d");

    for (var i = 0; i < profiles.length; i++) {
      var person = profiles[i];
      currentTeam.members.push({
        profile_url : profileURL(profile.user_key, profile.username),
        image_url :  Functions.imageTransform(profile.profile_picture_url, null, 250),
        fullname : profile.fullname,
        username : profile.username,
        focused : false
      });
    }

    cb(null);
  })
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentTeam.webApp,
    "resultCode" : resultCode,
     "resultDate" : resultDate,
     "awaiting" : currentTeam.awaiting,
     "members" : currentTeam.members
  };

  callback(200, resultArray, res);
}
