"use strict";

var Teams = modelRequire("teams.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Team() {
  this.ip = "";
  this.lang = "";
  this.webApp = false;
  this.token = "";
  this.nickname = "";
  this.profileData = {
    name : "",
    team_theme_url : "",
    team_logo_url : "",
    description : "",
    since : "",
  };
  this.contactInfo = {
    email : "",
    phone_number : "",
    website : "",
    facebook : "",
    instagram : ""
  };
  this.additional = {
    verified : false,
    members : 0
  };
};

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, lang, token, nickname, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.lang = lang;
  currentTeam.token = token;
  currentTeam.nickname = nickname;
  currentTeam.webApp = webApp;

  verify(callback, res);
}

function verify(callback, res) {
  Teams.findOne({team_key : currentTeam.token, nickname : currentTeam.nickname}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return sendResponse(-1, callback, res);

    currentTeam.profileData.name = team.name;
    currentTeam.profileData.description = team.description;
    currentTeam.profileData.team_logo_url = Functions.imageTransform(team.team_picture_url, 200, 200);
    currentTeam.profileData.team_theme_url = team.team_theme_url;
    currentTeam.profileData.since = team.created_at.getTime();

    currentTeam.contactInfo.email = team.contact_info.email;
    currentTeam.contactInfo.phone_number = team.contact_info.phone_number;
    currentTeam.contactInfo.website = team.contact_info.website;
    currentTeam.contactInfo.facebook = team.contact_info.facebook;
    currentTeam.contactInfo.instagram = team.contact_info.instagram;

    currentTeam.additional.members = team.members.length;
    currentTeam.additional.verified = (team.company_status == 2);

    sendResponse(1, callback, res)
  });
}

function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentTeam.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "profileData" : currentTeam.profileData,
    "contactInfo" : currentTeam.contactInfo,
    "additional" : currentTeam.additional
  };

  callback(200, resultArray, res);
}
