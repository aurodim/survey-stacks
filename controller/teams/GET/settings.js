"use strict";

var Teams = modelRequire("teams.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Team() {
  this.ip = "";
  this.validation_key = "";
  this.auth_key = "";
  this.session_key = "";
  this.lang = "";
  this.webApp = false;
  this.account_data = {
    fullname : "",
    email_phone : "",
    name : "",
    nickname : "",
    description : "",
    theme_color : "",
    accent_color : ""
  };
  this.contact_info = {
    email : "",
    phone_number : "",
    website : "",
    facebook : "",
    instagram : ""
  };
  this.members_info = {
    code : "",
    public : false,
    open : false
  };
  this.membership_info = {
    plan : "",
    auto_renew : false,
    expiration_date : "",
    digits : "",
    price : ""
  };
  this.notifications_info = {
    email_phone : false,
    freq : 1
  };
  this.cs = 0;
};

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.validation_key = validation_key;
  currentTeam.auth_key = auth_key;
  currentTeam.session_key = session_key;
  currentTeam.lang = lang;
  currentTeam.webApp = webApp;

  verify(callback, res);
}

function verify(callback, res) {
  Teams.findOne({deleted : false, team_key : currentTeam.validation_key, team_auth_key : currentTeam.auth_key, team_session_key : currentTeam.session_key}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == "" || team == null) return callback(401, "error", res);

    currentTeam.account_data.fullname = team.fullname;
    currentTeam.account_data.email_phone = team.email_phone;
    currentTeam.account_data.name = team.name;
    currentTeam.account_data.nickname = team.nickname;
    currentTeam.account_data.description = team.description;
    currentTeam.account_data.theme_color = team.preferences.theme_color;
    currentTeam.account_data.accent_color = team.preferences.theme_dark;

    currentTeam.contact_info.email = team.contact_info.email;
    currentTeam.contact_info.phone_number = team.contact_info.phone_number;
    currentTeam.contact_info.website = team.contact_info.website;
    currentTeam.contact_info.facebook = team.contact_info.facebook;
    currentTeam.contact_info.instagram = team.contact_info.instagram;

    currentTeam.members_info.code = team.member_options.code;
    currentTeam.members_info.public = team.member_options.public;
    currentTeam.members_info.open = team.member_options.open;

    currentTeam.cs = team.company_status;

    if (currentTeam.cs == -1) {
      currentTeam.cs == 3;
    }

    if (team.membership.type == 0) {
      currentTeam.membership_info.plan = "free";
    } else if (team.membership.type == 1) {
      currentTeam.membership_info.plan = "premium";
    }
    currentTeam.membership_info.auto_renew = team.membership.auto_bill;
    currentTeam.membership_info.digits = team.membership.last_four;
    currentTeam.membership_info.expiration_date = team.membership.expiration_date;
    currentTeam.membership_info.price = team.membership.paid;

    currentTeam.notifications_info.email_phone = team.notification_settings.email_phone;
    currentTeam.notifications_info.freq = team.notification_settings.notification_freq;

    sendResponse(callback, res);
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentTeam.webApp,
    "resultCode" : resultCode,
     "resultDate" : resultDate,
     "account_data" : currentTeam.account_data,
     "contact_info" : currentTeam.contact_info,
     "members_info" : currentTeam.members_info,
     "membership_info" : currentTeam.membership_info,
     "notifications_info" : currentTeam.notifications_info,
     "cs" : currentTeam.cs
  };

  callback(200, resultArray, res);
}
