"use strict";

var Teams = modelRequire("teams.js");
var Notifications = modelRequire("notifications.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Team() {
  this.ip = "";
  this.validation_key = "";
  this.auth_key = "";
  this.session_key = "";
  this.lang = "";
  this.webApp = false;
  this.notifications = [];
};

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.validation_key = validation_key;
  currentTeam.auth_key = auth_key;
  currentTeam.session_key = session_key;
  currentTeam.lang = lang;
  currentTeam.webApp = webApp;

  verify(callback, res);
}

function verify(callback, res) {
  Teams.findOne({deleted : false, team_key : currentTeam.validation_key, team_auth_key : currentTeam.auth_key, team_session_key : currentTeam.session_key}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == "" || team == null) return callback(401, "error", res);

    gatherNotif(callback, res);
  });
}

function gatherNotif(callback, res) {
  Notifications.find({reciever : currentTeam.session_key}, function(error, notifications) {
    if (error) return callback(500, "error", res);
    if (notifications.length == 0) return sendResponse(callback, res);

    for (var i = 0; i < notifications.length; i++) {
      var notification = notifications[i];

      currentTeam.notifications.push({
        type : notification.type,
        title : notification.title,
        content : notification.content,
        url : notification.url,
        token : notification.notification_token
      })
    }

    markAsRead(callback, res);
  }).sort({created_at : -1});
}

function markAsRead(callback, res) {
  Notifications.find({reciever : currentTeam.session_key, read : false}, function(error, notifications) {
    if (error) return callback(500, "error", res);
    if (notifications.length == 0) return sendResponse(callback, res);

    for (var i = 0; i < notifications.length; i++) {
      var notification = notifications[i];
      notification.read = true;
      notification.updated_at = new Date();
      notification.save(function(error) {
        if (error) return callback(500, "error", res);

        if (i == notifications.length) {
          sendResponse(callback, res);
        }
      });
    }
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentTeam.webApp,
    "resultCode" : resultCode,
     "resultDate" : resultDate,
     "notifications" : currentTeam.notifications
  };

  callback(200, resultArray, res);
}
