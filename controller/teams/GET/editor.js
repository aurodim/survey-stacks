"use strict";

var Teams = modelRequire("teams.js");
var Constants = modelRequire("constants.js");
var Polls = modelRequire("polls.js");
var Surveys = modelRequire("surveys.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Team() {
  this.ip = "";
  this.validation_key = "";
  this.auth_key = "";
  this.session_key = "";
  this.lang = "";
  this.webApp = false;
  this.editable = true;
  this.ppa = 0.00;
  this.token = "";
  this.type = "";
  this.details = {
    name : "",
    description : "",
    image_url : "",
    answer_url : "",
    sc_amount : 0,
    labs_created : false,
    labs_option : null,
    unique_keys : false,
    keys : []
  };
}

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, type, token, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.validation_key = validation_key;
  currentTeam.auth_key = auth_key;
  currentTeam.session_key = session_key;
  currentTeam.lang = lang;
  currentTeam.token = token;
  currentTeam.type = type;
  currentTeam.webApp = webApp;

  if (type == "surveys") {
    ppaValue();
  }
  verifyGatherData(callback, res);
}

function ppaValue() {
  Constants.find(function(error, constants) {
    if (error) {
      currentTeam.ppa = 1.00;
    } else {
      var constant = constants[0];
      currentTeam.ppa = (constant.ppa).toFixed(2);
    }
  }).limit(1);
}

function verifyGatherData(callback, res) {
  Teams.findOne({deleted : false, team_key : currentTeam.validation_key, team_session_key : currentTeam.session_key, team_auth_key : currentTeam.auth_key}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return callback(401, "no auth", res);

    if (currentTeam.type == "surveys") {
      surveyEditG(callback, res);
    } else if (currentTeam.type == "polls") {
      pollEditG(callback, res);
    }
  });
}

function surveyEditG(callback, res) {
  Surveys.find({deleted : false, creator_key : currentTeam.validation_key, survey_token : currentTeam.token}, function(error, surveys) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (surveys.length == "1") {
        var survey = surveys[0];

        currentTeam.editable = true;
        currentTeam.details.description = survey.survey_details.description;
        currentTeam.details.image_url = survey.survey_details.image_url;
        currentTeam.details.answer_url = survey.survey_details.answer_url;
        currentTeam.details.name = survey.survey_details.name;
        currentTeam.details.sc_amount = survey.survey_details.stacks;
        currentTeam.details.labs_created = survey.survey_details.lab_created;

        if (currentTeam.details.labs_created) {
          if (survey.labs.type === "quiz") {
            currentTeam.details.labs_option = 0;
          } else if (survey.labs.type === "key_needed") {
            currentTeam.details.labs_option = 1;
            currentTeam.details.unique_keys = survey.labs.unique_keys;
            currentTeam.details.keys = survey.labs.keys;
          } else if (survey.labs.type === "emoji") {
            currentTeam.details.labs_option = 2;
          } else if (survey.labs.type === "game") {
            currentTeam.details.labs_option = 3;
          }
        }

        sendResponse(callback, res);
      } else {
        currentTeam.editable = false;
        currentTeam.ppa = 0;
        sendResponse(callback, res);
      }
    }
  }).limit(1);
}

function pollEditG(callback, res) {
  Polls.find({deleted : false, creator_key : currentTeam.validation_key, poll_token : currentTeam.token}, function(error, polls) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (polls.length == "1") {
        var poll = polls[0];

        currentTeam.editable = true;
        currentTeam.details.description = poll.poll_details.description;
        currentTeam.details.image_url = poll.poll_details.image_url;
        currentTeam.details.answer_url = poll.poll_details.answer_url;
        currentTeam.details.name = poll.poll_details.name;
        currentTeam.details.sc_amount = poll.poll_details.coins;
        currentTeam.details.labs_created = poll.poll_details.lab_created;

        sendResponse(callback, res);
      } else {
        currentTeam.editable = false;
        currentTeam.ppa = 0;
        sendResponse(callback, res);
      }
    }
  }).limit(1);
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentTeam.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "editable" : currentTeam.editable,
    "ppa" : currentTeam.ppa,
    "details" : {
       "name" : currentTeam.details.name,
       "description" : currentTeam.details.description,
       "image_url" : currentTeam.details.image_url,
       "answer_url" : currentTeam.details.answer_url,
       "sc_amount" : currentTeam.details.sc_amount,
       "labs_created" : currentTeam.details.labs_created,
       "labs_type" : currentTeam.details.labs_option
     },
     "labs" : {

     }
  };

  if (currentTeam.details.labs_created && currentTeam.details.labs_option == 1) {
    resultArray.labs.unique_keys = currentTeam.details.unique_keys;
    resultArray.labs.keys = currentTeam.details.keys;
  }

  callback(200, resultArray, res);
}
