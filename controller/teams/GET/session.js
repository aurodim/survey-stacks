"use strict";

var Teams = modelRequire("teams.js");
var Notifications = modelRequire("notifications.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Team() {
  this.ip = "";
  this.validation_key = "";
  this.auth_key = "";
  this.session_key = "";
  this.lang = "";
  this.webApp = false;
  this.name = "";
  this.colors = {
    theme : "",
    dark : ""
  };
  this.team_logo_url = "";
  this.payment_completed = false;
  this.unread = false;
}

var currentTeam;

var csrfToken = "";

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, webApp, csrfTkn, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.validation_key = validation_key;
  currentTeam.auth_key = auth_key;
  currentTeam.session_key = session_key;
  currentTeam.lang = lang;
  currentTeam.webApp = webApp;
  csrfToken = csrfTkn;

  AsyncData(callback, res);
}

function AsyncData(callback, res) {
  async.parallel({
      profileD : function(callback) {
        verifyGatherData(callback);
      },
      notificationD : function(callback) {
        notificationData(callback);
      }
  }, function(err) {
      if (err == "d") {
        callback(500, "callback", res);
        return;
      }

      if (err == "a") {
        res.clearCookie("sk-team");
        res.clearCookie("lk-team");
        res.clearCookie("vk-team");
        res.clearCookie("nn-team");
        res.clearCookie("dt-team");
        res.clearCookie("sd-team");
        res.clearCookie("dnotif");
        res.clearCookie("lang-team");
        res.clearCookie("_csrf");
        res.clearCookie("io");
        callback(401, "no auth", res);
        return;
      }

      sendResponse(callback, res);
  });
}

function verifyGatherData(callback) {
  Teams.findOne({deleted : false, team_key : currentTeam.validation_key, team_auth_key : currentTeam.auth_key, team_session_key : currentTeam.session_key}, function(error, team) {
    if (error) return callback("d");
    if (team == null || team == "") return callback("a");

    if (team.team_picture_url != "") {
      currentTeam.team_logo_url = Functions.imageTransform(team.team_picture_url, 48, 48) +"?"+ Date.now();
    } else {
      currentTeam.team_logo_url = "";
    }
    currentTeam.name = team.name;
    currentTeam.colors.theme = team.preferences.theme_color;
    currentTeam.colors.dark = team.preferences.theme_dark;
    currentTeam.active_membership = (team.membership.type != 0);

    callback(null)
  });
}

function notificationData(callback) {
  Notifications.count({reciever : currentTeam.session_key, read : false}, function(error, amount) {
    if (error) return callback("d");
    if (amount != 0) {
      currentTeam.unread = true;
    }
    callback(null);
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray;

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentTeam.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "name" : currentTeam.name,
    "csrf" : csrfToken,
    "membership_active" : currentTeam.active_membership,
    "team_logo_url" : currentTeam.team_logo_url,
    "colors" : currentTeam.colors,
    "unread" : currentTeam.unread,
    "csrf" : csrfToken
 };

  callback(200, resultArray, res);
}
