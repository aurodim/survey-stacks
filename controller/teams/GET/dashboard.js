"use strict";

var Teams = modelRequire("teams.js");
var Responses = modelRequire("responses.js");
var Functions = rootRequire("/controller/globalFunctions.js");


function Team() {
  this.ip = "";
  this.validation_key = "";
  this.auth_key = "";
  this.session_key = "";
  this.lang = "";
  this.webApp = false;
  this.tips = {
    "tm" : 0,
    "me" : "",
    "qc" : 0
  };
  this.graphics = {
    "responses" : [[0,0,0,0,0,0,0],[0,0,0,0,0,0,0]],
    "max" : 0,
    "created" : [0,0] // surveys : polls
  };
  this.surveys = [];
  this.polls = [];
}

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, lang, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.validation_key = validation_key;
  currentTeam.auth_key = auth_key;
  currentTeam.session_key = session_key;
  currentTeam.lang = lang;
  currentTeam.webApp = webApp;

  verifyGatherData(callback, res);
}

function verifyGatherData(callback, res) {
  Teams.findOne({deleted : false, team_key : currentTeam.validation_key, team_session_key : currentTeam.session_key, team_auth_key : currentTeam.auth_key}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return callback(401, "no auth", res);

    currentTeam.tips.tm = team.members.length;
    currentTeam.tips.me = team.membership.expiration_date;
    currentTeam.tips.qc = team.surveys_created.length + team.polls_created.length;

    currentTeam.graphics.created[0] = team.surveys_created.length;
    currentTeam.graphics.created[1] = team.polls_created.length;

    currentTeam.surveys = team.surveys_created;
    currentTeam.polls = team.polls_created;

    responsesData(callback, res);
  });
}

function responsesData(callback, res) {
  Responses.find({$or : [{questionnaire_type : 'survey', questionnaire_token : {$in : currentTeam.surveys}}, {questionnaire_type : 'poll', questionnaire_token : {$in : currentTeam.polls}}]}, function(error, responses) {
    if (error) return callback(500, "error", res);

    for (var i = 0; i < responses.length; i++) {
      var response = responses[i];
      var dow = response.created_at.getDay();

      if (response.questionnaire_type == 'survey') {
        currentTeam.graphics.responses[0][dow] += 1;
      } else {
        currentTeam.graphics.responses[1][dow] += 1;
      }
    }

    var maxS = Math.max.apply(null, currentTeam.graphics.responses[0]);
    var maxP = Math.max.apply(null, currentTeam.graphics.responses[1]);

    currentTeam.graphics.max = (maxS >= maxP) ? maxS : maxP;

    sendResponse(callback, res);
  });
}

function sendResponse(callback, res) {
  var resultCode = 1;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentTeam.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate,
    "tips" : currentTeam.tips,
    "graphics" : currentTeam.graphics
  };

  callback(200, resultArray, res);
}
