"use strict";

var Teams = modelRequire("teams.js");
var Surveys = modelRequire("surveys.js");
var Polls = modelRequire("polls.js");
var sizeOf = require('image-size');
var Functions = rootRequire("/controller/globalFunctions.js");


function Team() {
  this.ip = "";
  this.validation_token = "";
  this.auth_token = "";
  this.session_token = "";
  this.q_token = "";
  this.q_stacks = 0;
  this.q_type = "";
  this.image_id = "";
  this.lang = "";
  this.webApp = false;
  this.image;
  this.survey = {};
  this.poll = {};
  this.questionnaire = {};
  this.labs = {};
  this.collected = [];
  this.poll_coins = 5;
}

var currentTeam;

var services = {};

services.initialize = initialize;
module.exports = services;

function initialize(ip, validation_token, auth_token, session_token, lang, image, q, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.validation_token = validation_token;
  currentTeam.auth_token = auth_token;
  currentTeam.session_token = session_token;
  currentTeam.lang = lang;
  currentTeam.webApp = webApp;
  currentTeam.image = image;
  currentTeam.questionnaire = JSON.parse(q);
  currentTeam.q_type = currentTeam.questionnaire.type;
  currentTeam.survey = currentTeam.questionnaire.survey;
  currentTeam.poll = currentTeam.questionnaire.poll;
  if (currentTeam.q_type === "survey") {
    currentTeam.q_token = Functions.createSurveyToken(validation_token, "9");
    currentTeam.q_stacks = Functions.determineStacks(currentTeam.survey.freeResponseQuestions, currentTeam.survey.checkboxResponseQuestions, currentTeam.survey.booleanResponseQuestions, currentTeam.survey.radioResponseQuestions, currentTeam.survey.menuResponseQuestions);
  } else {
    currentTeam.q_token = Functions.createPollToken(validation_token, "9");
  }
  var collected = [];
  for (var key in currentTeam.questionnaire.details.data_collected) {
    if (currentTeam.questionnaire.details.data_collected[key]) {
      collected.push(key);
    }
  }
  currentTeam.collected = collected;
  if (currentTeam.questionnaire.lab_option == 0) {
    if (currentTeam.q_type === "survey") {
      currentTeam.labs = {
        type : "quiz",
        show_score : currentTeam.questionnaire.method_options.show_score,
        out_of : currentTeam.questionnaire.method_options.out_of
      };
    } else {
      var ind = currentTeam.poll.options.indexOf(currentTeam.questionnaire.method_options.answer);
      if (ind == -1) return callback(500, "error", res);
      currentTeam.labs = {
        type : "quiz",
        show_score : currentTeam.questionnaire.method_options.show_score,
        answer : ind
      };
    }
  } else if (currentTeam.questionnaire.lab_option == 1) {
    currentTeam.labs = {
      type : "key_needed",
      unique_keys : currentTeam.questionnaire.method_options.unique,
      keys : [Functions.createQAnswerKey()],
    };
  } else if (currentTeam.questionnaire.lab_option == 2) {
    currentTeam.labs = {
      type : "emoji"
    };
  } else if (currentTeam.questionnaire.lab_option == 3) {
    if (currentTeam.q_type === "survey") {
      currentTeam.labs = {
        type : "game",
        timed : currentTeam.questionnaire.method_options.timed,
        max_time : currentTeam.questionnaire.method_options.max_time
      };
    } else {
      if (currentTeam.questionnaire.method_options.pollQuestionsExtra.length > 0) {
        currentTeam.poll_coins = currentTeam.poll_coins * currentTeam.questionnaire.method_options.pollQuestionsExtra.length;
      }
      currentTeam.labs = {
        type : "game",
        timed : currentTeam.questionnaire.method_options.timed,
        max_time : currentTeam.questionnaire.method_options.max_time,
        gameQuestions : currentTeam.questionnaire.method_options.pollQuestionsExtra
      };
    }
  }

  verifyProfile(callback, res);
}

function getCoins() {
  Constants.find(function(error, constants) {
    if (error) {
      currentTeam.poll_coins = 5;
    } else {
      var constant = constants[0];
      currentTeam.poll_coins = constant.cpp;
    }
  }).limit(1);
}

function verifyProfile(callback, res) {
  Teams.findOne({deleted : false, team_key : currentTeam.validation_token, team_session_key : currentTeam.session_token, team_auth_key : currentTeam.auth_token, "membership.type" : 1}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return callback(401, "wrongMatch", res);

    if (currentTeam.image !== "") {
      // image we give
      uploadImageF(callback, res);
    } else if (currentTeam.questionnaire.details.image_url != "") {
      uploadImage(callback, res);
    } else {
      if (currentTeam.q_type === "survey") {
        uploadSurvey(callback, res);
      } else {
        uploadPoll(callback, res);
      }
    }
  });
}

function uploadImageF(callback, res) {
  var dimensions = sizeOf(process.cwd()+"/"+currentTeam.image.path);
  var height = dimensions.height;
  var width = dimensions.width;
  var folder;
  var tags;
  if (dimensions.height > 1600 || dimensions.width > 1600) {
    height = dimensions.height / 2;
    width = dimensions.width / 2;
  }
  if (currentTeam.q_type === "survey") {
    currentTeam.image_id = process.env.CLOUDINARY_SURVEYS_FOLDER + "/" + currentTeam.q_token + "/survey_image";
    tags = ['team_'+currentTeam.validation_token, currentTeam.q_token, "surveys", "labs"];
    folder = process.env.CLOUDINARY_SURVEYS_FOLDER;
  } else {
    currentTeam.image_id = process.env.CLOUDINARY_POLLS_FOLDER + "/" + currentTeam.q_token + "/poll_image";
    tags = ['team_'+currentTeam.validation_token, currentTeam.q_token, "polls", "labs"];
    folder = process.env.CLOUDINARY_POLLS_FOLDER;
  }
  cloudinary.v2.uploader.upload(
    currentTeam.image.path,
    function(error, result) {
      if (typeof error != "undefined" && error != "undefined") return sendResponse(-1, callback, res);

      fs.unlink(process.cwd()+"/"+currentTeam.image.path, function(error) {

      });

      currentTeam.questionnaire.details.image_url = result.secure_url;

      if (currentTeam.q_type === "survey") {
        uploadSurvey(callback, res);
      } else {
        uploadPoll(callback, res);
      }
  },
    {
      public_id: currentTeam.image_id,
      width: width,
      height: height,
      folder :  folder,
      tags: tags
    }
  );
}

function uploadImage(callback, res) {
  var folder;
  var tags;
  if (currentTeam.q_type === "survey") {
    currentTeam.image_id = process.env.CLOUDINARY_SURVEYS_FOLDER + "/" + currentTeam.q_token + "/survey_image";
    tags = ['team_'+currentTeam.validation_token, currentTeam.q_token, "surveys", "labs"];
    folder = process.env.CLOUDINARY_SURVEYS_FOLDER;
  } else {
    currentTeam.image_id = process.env.CLOUDINARY_POLLS_FOLDER + "/" + currentTeam.q_token + "/poll_image";
    tags = ['team_'+currentTeam.validation_token, currentTeam.q_token, "polls", "labs"];
    folder = process.env.CLOUDINARY_POLLS_FOLDER;
  }
  cloudinary.v2.uploader.upload(
    currentTeam.questionnaire.details.image_url,
    {
      public_id: currentTeam.image_id,
      width: 800,
      height: 800,
      folder :  folder,
      tags: tags
    },
    function(error, result) {

      if (typeof error != "undefined" && error != "undefined") return sendResponse(-1, callback, res);

      currentTeam.questionnaire.details.image_url = result.secure_url;

      if (currentTeam.q_type === "survey") {
        uploadSurvey(callback, res);
      } else {
        uploadPoll(callback, res);
      }
    }
  );
}

function uploadSurvey(callback, res) {
  var teamSurvey = currentTeam.survey;
  var Survey = new Surveys({
    survey_token : currentTeam.q_token,
    creator_key : currentTeam.validation_token,
    survey_details : {
      public_survey : currentTeam.questionnaire.details.public,
      lang : currentTeam.lang,
      stacks : currentTeam.q_stacks,
      name : currentTeam.questionnaire.details.name,
      description : currentTeam.questionnaire.details.description,
      category_token : currentTeam.questionnaire.details.category_token,
      target_sex : currentTeam.questionnaire.details.sex,
      target_ethnicity : currentTeam.questionnaire.details.ethnicity,
      target_age_group : currentTeam.questionnaire.details.age_group,
      image_url : currentTeam.questionnaire.details.image_url,
      answer_url : Functions.answerURL(currentTeam.q_token, 0),
      collected : currentTeam.collected,
      lab_created : true
    },
    labs : currentTeam.labs,
    question_details : {
      order : teamSurvey.order,
      amount : teamSurvey.order.length,
      free_response : teamSurvey.freeResponseQuestions,
      checkbox : teamSurvey.checkboxResponseQuestions,
      boolean : teamSurvey.booleanResponseQuestions,
      radio : teamSurvey.radioResponseQuestions,
      menu : teamSurvey.menuResponseQuestions
    },
    payment_details : {
      free : false,
      survey_charge : "0.00",
      order_total : "0.00",
      receipt_token : Functions.receiptToken()
    },
    created_at : new Date(),
    updated_at : new Date()
  });

  Survey.save(function(error) {
    if (error) return callback(500, "error", res);

    updateProfile(callback, res);
  });
}

function uploadPoll(callback, res) {
  var userPoll = currentTeam.poll;
  var Poll = new Polls({
    poll_token : currentTeam.q_token,
    creator_key : currentTeam.validation_token,
    poll_details : {
      public_poll : currentTeam.questionnaire.details.public,
      lang : currentTeam.lang,
      coins : currentTeam.poll_coins,
      name : currentTeam.questionnaire.details.name,
      description : currentTeam.questionnaire.details.description,
      category_token : currentTeam.questionnaire.details.category_token,
      target_sex : currentTeam.questionnaire.details.sex,
      target_ethnicity : currentTeam.questionnaire.details.ethnicity,
      target_age_group : currentTeam.questionnaire.details.age_group,
      image_url : currentTeam.questionnaire.details.image_url,
      answer_url : Functions.answerURL(currentTeam.q_token, 1),
      collected : currentTeam.collected,
      lab_created : true
    },
    labs : currentTeam.labs,
    question_details : {
      question : userPoll.question,
      options : userPoll.options
    },
    created_at : new Date(),
    updated_at : new Date()
  });

  Poll.save(function(error) {
    if (error) return callback(500, "error", res);

    updateProfile(callback, res);
  });
}

function updateProfile(callback, res) {
  Teams.findOne({deleted : false, team_key : currentTeam.validation_token, team_session_key : currentTeam.session_token, team_auth_key : currentTeam.auth_token}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return callback(401, "no auth", res);

    if (currentTeam.q_type === "survey") {
      team.surveys_created.push(currentTeam.q_token);
    } else {
      team.polls_created.push(currentTeam.q_token);
    }
    team.updated_at = new Date();

    team.save(function(error) {
      if (error) return callback(500, "error", res);

      sendResponse(1, callback, res);
    });
  });
}

function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {"resultCode" : resultCode, "resultDate" : resultDate};

  callback(200, resultArray, res);
}
