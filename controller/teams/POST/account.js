"use strict";

var Teams = modelRequire("teams.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Team() {
  this.ip = "";
  this.validation_key = "";
  this.session_key = "";
  this.auth_key = "";
  this.accountData = {};
  this.webApp = false;
}

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, account, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.validation_key = validation_key;
  currentTeam.auth_key = auth_key;
  currentTeam.session_key = session_key;
  currentTeam.accountData = account;
  currentTeam.webApp = webApp;

  noDoubles(callback, res);
}

function noDoubles(callback, res) {
  Teams.findOne({team_key : {$ne : currentTeam.validation_key}, $or : [{email_phone : currentTeam.accountData.email_phone}, {name : currentTeam.accountData.name}, {nickname : currentTeam.accountData.nickname}]}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return verifyData(callback, res);

    if (team.email_phone == currentTeam.accountData.email_phone) return sendResponse(-1, callback, res);
    if (team.name == currentTeam.accountData.name) return sendResponse(-2, callback, res);
    if (team.nickname == currentTeam.accountData.nickname) return sendResponse(-3, callback, res);
  });
}

function verifyData(callback, res) {
  Teams.findOne({team_key : currentTeam.validation_key, team_session_key : currentTeam.session_key, team_auth_key : currentTeam.auth_key}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return callback(401, "wrongMatch", res);

    team.fullname = currentTeam.accountData.fullname;
    team.email_phone = currentTeam.accountData.email_phone;
    team.name = currentTeam.accountData.name;
    team.description = currentTeam.accountData.description;
    team.nickname = currentTeam.accountData.nickname;
    team.preferences.theme_color = currentTeam.accountData.theme_color;
    team.preferences.theme_dark = currentTeam.accountData.accent_color;
    team.updated_at = new Date();

    team.save(function(error) {
      if (error) return callback(500, "error", res);

      sendResponse(1, callback, res);
    });
  });
}

function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {"resultCode" : resultCode, "resultDate" : resultDate};

  callback(200, resultArray, res);
}
