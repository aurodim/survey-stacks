"use strict";

var Teams = modelRequire("teams.js");
var Surveys = modelRequire("surveys.js");
var sizeOf = require('image-size');
var Functions = rootRequire("/controller/globalFunctions.js");


function Team() {
  this.ip = "";
  this.validation_token = "";
  this.auth_token = "";
  this.session_token = "";
  this.survey_token = "";
  this.survey_stacks = 0;
  this.image_id = "";
  this.lang = "";
  this.webApp = false;
  this.image;
  this.collected = [];
  this.survey = {};
}

var currentTeam;

var services = {};

services.initialize = initialize;
module.exports = services;

function initialize(ip, validation_token, auth_token, session_token, lang, image, survey, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.validation_token = validation_token;
  currentTeam.auth_token = auth_token;
  currentTeam.session_token = session_token;
  currentTeam.lang = lang;
  currentTeam.webApp = webApp;
  currentTeam.image = image;
  currentTeam.survey = JSON.parse(survey);
  currentTeam.survey_token = Functions.createSurveyToken(validation_token, "9");

  currentTeam.survey_stacks = Functions.determineStacks(currentTeam.survey.freeResponseQuestions, currentTeam.survey.checkboxResponseQuestions, currentTeam.survey.booleanResponseQuestions, currentTeam.survey.radioResponseQuestions, currentTeam.survey.menuResponseQuestions);
  var collected = [];
  for (var key in currentTeam.survey.details.data_collected) {
    if (currentTeam.survey.details.data_collected[key]) {
      collected.push(key);
    }
  }
  currentTeam.collected = collected;
  verifyProfile(callback, res);
}

function verifyProfile(callback, res) {
  Teams.findOne({deleted : false, team_key : currentTeam.validation_token, team_session_key : currentTeam.session_token, team_auth_key : currentTeam.auth_token, "membership.type" : 1}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return callback(401, "wrongMatch", res);

    if (currentTeam.image !== "") {
      // image we give
      uploadImageF(callback, res);
    } else if (currentTeam.survey.surveyDetails.image_url != "") {
      uploadImage(callback, res);
    } else {
      uploadSurvey(callback, res);
    }
  });
}

function uploadImageF(callback, res) {
  var dimensions = sizeOf(process.cwd()+"/"+currentTeam.image.path);
  var height = dimensions.height;
  var width = dimensions.width;
  if (dimensions.height > 1600 || dimensions.width > 1600) {
    height = dimensions.height / 2;
    width = dimensions.width / 2;
  }
  currentTeam.image_id = process.env.CLOUDINARY_SURVEYS_FOLDER + "/" + currentTeam.survey_token + "/survey_image";
  cloudinary.v2.uploader.upload(
    currentTeam.image.path,
    function(error, result) {
      if (typeof error != "undefined" && error != "undefined") return sendResponse(-1, callback, res);

      fs.unlink(process.cwd()+"/"+currentTeam.image.path, function(error) {

      });

      currentTeam.survey.surveyDetails.image_url = result.secure_url;

      uploadSurvey(callback, res);
    },
    {
      public_id: currentTeam.image_id,
      width: width,
      height: height,
      folder :  process.env.CLOUDINARY_SURVEYS_FOLDER,
      tags: ['team_'+currentTeam.validation_token, currentTeam.survey_token, "surveys"]
    }
  );
}

function uploadImage(callback, res) {
  currentTeam.image_id = process.env.CLOUDINARY_SURVEYS_FOLDER + "/" + currentTeam.survey_token + "/survey_image";
  cloudinary.v2.uploader.upload(
    currentTeam.survey.surveyDetails.image_url,
    {
      public_id: currentTeam.image_id,
      width: 800,
      height: 800,
      folder :  process.env.CLOUDINARY_SURVEYS_FOLDER,
      tags: ['team_'+currentTeam.validation_token, currentTeam.survey_token, "surveys"]
    },
    function(error, result) {

      if (typeof error != "undefined" && error != "undefined") return sendResponse(-1, callback, res);

      currentTeam.survey.surveyDetails.image_url = result.secure_url;
      uploadSurvey(callback, res);
    }
  );
}

function uploadSurvey(callback, res) {
  var teamSurvey = currentTeam.survey;
  var Survey = new Surveys({
    survey_token : currentTeam.survey_token,
    creator_key : currentTeam.validation_token,
    survey_details : {
      public_survey : teamSurvey.surveyDetails.public,
      lang : currentTeam.lang,
      stacks : currentTeam.survey_stacks,
      name : teamSurvey.surveyDetails.name,
      description : teamSurvey.surveyDetails.description,
      category_token : teamSurvey.surveyDetails.category_token,
      target_sex : teamSurvey.surveyDetails.sex,
      target_ethnicity : teamSurvey.surveyDetails.ethnicity,
      target_age_group : teamSurvey.surveyDetails.age_group,
      image_url : currentTeam.survey.surveyDetails.image_url,
      answer_url : Functions.answerURL(currentTeam.survey_token, 0),
      lab_created : false,
      collected : currentTeam.collected
    },
    question_details : {
      order : teamSurvey.order,
      amount : teamSurvey.order.length,
      free_response : teamSurvey.freeResponseQuestions,
      checkbox : teamSurvey.checkboxResponseQuestions,
      boolean : teamSurvey.booleanResponseQuestions,
      radio : teamSurvey.radioResponseQuestions,
      menu : teamSurvey.menuResponseQuestions
    },
    payment_details : {
      free : false,
      survey_charge : "0.00",
      order_total : "0.00",
      receipt_token : Functions.receiptToken()
    },
    created_at : new Date(),
    updated_at : new Date()
  });

  Survey.save(function(error) {
    if (error) return callback(500, "error", res);

    updateProfile(callback, res);
  });
}

function updateProfile(callback, res) {
  Teams.findOne({deleted : false, team_key : currentTeam.validation_token, team_session_key : currentTeam.session_token, team_auth_key : currentTeam.auth_token}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return callback(401, "no auth", res);

    team.surveys_created.push(currentTeam.survey_token);
    team.updated_at = new Date();

    team.save(function(error) {
      if (error) return callback(500, "error", res);

      sendResponse(1, callback, res);
    });
  });
}

function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {"resultCode" : resultCode, "resultDate" : resultDate};

  callback(200, resultArray, res);
}
