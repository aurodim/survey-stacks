"use strict";

var Teams = modelRequire("teams.js");
var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Team() {
  this.ip = "";
  this.validation_key = "";
  this.session_key = "";
  this.auth_key = "";
  this.token = "";
  this.member = {};
  this.profile = {};
  this.webApp = false;
}

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, token, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.validation_key = validation_key;
  currentTeam.auth_key = auth_key;
  currentTeam.session_key = session_key;
  currentTeam.token = token;
  currentTeam.webApp = webApp;

  profile(callback, res);
}

function profile(callback,res) {
  Profiles.findOne({}, function(error, profile) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return sendResponse(-1, callback, res);

    currentTeam.profile = profile;
    verifyData(callback, res);
  });
}

function verifyData(callback, res) {
  Teams.findOne({team_key : currentTeam.validation_key, team_session_key : currentTeam.session_key, team_auth_key : currentTeam.auth_key}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return callback(401, "wrongMatch", res);

    if (team.awaiting.indexOf(currentTeam.token) > -1) {
      var i = team.awaiting.indexOf(currentTeam.token);
      team.awaiting.splice(i, 1);
      team.members.push(currentTeam.token);
    } else {
      return sendResponse(-1, callback, res);
    }
    team.updated_at = new Date();

    team.save(function(error) {
      if (error) return callback(500, "error", res);

      var profile = currentTeam.profile;
      
      currentTeam.member = {
        profile_url : profileURL(profile.user_key, profile.username),
        image_url :  Functions.imageTransform(profile.profile_picture_url, null, 250),
        fullname : profile.fullname,
        username : profile.username,
        focused : false
      };

      sendResponse(1, callback, res);
    });
  });
}

function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {"resultCode" : resultCode, "resultDate" : resultDate, "member" : currentTeam.member};

  callback(200, resultArray, res);
}
