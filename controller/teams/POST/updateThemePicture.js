"use strict";

var Teams = modelRequire("teams.js");
var Functions = rootRequire("/controller/globalFunctions.js");
var sizeOf = require('image-size');

function Team() {
  this.ip = "";
  this.validation_key = "";
  this.session_key = "";
  this.auth_key = "";
  this.imageFolder = "";
  this.imageID = "";
  this.file = {};
  this.first = false;
  this.webApp = false;
}

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, file, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.validation_key = validation_key;
  currentTeam.auth_key = auth_key;
  currentTeam.session_key = session_key;
  currentTeam.imageFolder = process.env.CLOUDINARY_TEAMS_FOLDER + "/" + currentTeam.validation_key
  currentTeam.imageID = process.env.CLOUDINARY_TEAMS_FOLDER + "/" + currentTeam.validation_key + "/theme_image";
  currentTeam.file = file;
  currentTeam.webApp = webApp;

  verifyData(callback, res);
}

function verifyData(callback, res) {
  Teams.findOne({team_key : currentTeam.validation_key, team_session_key : currentTeam.session_key, team_auth_key : currentTeam.auth_key}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return callback(401, "wrongMatch", res);

    if (team.team_picture_url == "") {
      currentTeam.first = true;
    }
    updateThemePicture(team, callback, res);

  });
}

function updateThemePicture(team, callback, res) {
  var dimensions = sizeOf(process.cwd()+"/"+currentTeam.file.path);
  var height = dimensions.height;
  var width = dimensions.width;
  if (dimensions.height > 1600 || dimensions.width > 1600) {
    height = dimensions.height / 2;
    width = dimensions.width / 2;
  }
  cloudinary.uploader.upload(
    currentTeam.file.path,
    function(result) {
      fs.unlink(process.cwd()+"/"+currentTeam.file.path, function(error) {

      });

      if (currentTeam.first) {
        team.team_theme_url = result.secure_url;
        team.updated_at = new Date();
        team.save(function(error) {
          if (error) return callback(500, "error", res);

          sendResponse(1, callback, res);
        });
      } else {

        sendResponse(1, callback, res);
      }
    },
    {
      public_id: "team_theme_picture",
      width: width,
      height: height,
      folder : currentTeam.imageFolder,
      tags: ['team_picture', currentTeam.validation_key, "teams", "theme images"]
    }
  );
}

function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {"resultCode" : resultCode, "resultDate" : resultDate};

  callback(200, resultArray, res);
}
