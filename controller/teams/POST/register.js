"use strict";

var Teams = modelRequire("teams.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Team() {
  this.ip = "";
  this.lang = "";
  this.langAbbr = "";
  this.teamSessionKey = "";
  this.teamKey = "";
  this.teamAuthKey = "";
  this.fullname = "";
  this.email_phone = "";
  this.name = "";
  this.nickname = "";
  this.password = "";
  this.hashedPassword = "";
  this.webApp = false;
}

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, lang, fullname, email_phone, name, nickname, password, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  lang = lang.trim().toLowerCase();
  fullname = fullname.trim();
  email_phone = email_phone.trim();
  nickname = nickname.trim();
  nickname = nickname.toLowerCase();
  password = password;
  currentTeam.langAbbr = lang;
  try {
    currentTeam.lang = rootRequire("/controller/" + lang + ".js");
  } catch(e) {
    currentTeam.langAbbr = "en";
    currentTeam.lang = rootRequire("/controller/en.js");
  }
  currentTeam.teamSessionKey = Functions.createTeamSessionKey(nickname);
  currentTeam.teamKey = Functions.createTeamKey(nickname);
  currentTeam.teamAuthKey = Functions.createTeamAuthKey(fullname, name);
  currentTeam.fullname = fullname;
  if (email_phone.includes("@")) {
    currentTeam.email_phone = email_phone.toLowerCase();
  } else {
    currentTeam.email_phone = Functions.phoneNumberNormalize(email_phone);
  }
  currentTeam.nickname = nickname;
  currentTeam.password = password;
  currentTeam.hashedPassword = Functions.hashedPasswordCreator(password);
  currentTeam.name = name;
  currentTeam.webApp = webApp;

  emailPhoneExist(callback, res);
}

function emailPhoneExist(callback, res) {
  Teams.findOne({deleted : false, $or : [{email_phone : currentTeam.email_phone}, {nickname : currentTeam.nickname}, {team_session_key : currentTeam.teamSessionKey}, {team_auth_key : currentTeam.teamAuthKey}]}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return newTeam(callback, res);
    if (team.email_phone == currentTeam.email_phone) return sendResponse(-1, callback, res);
    if (team.team_auth_key == currentTeam.teamAuthKey) {
      currentTeam.teamAuthKey = Functions.createTeamAuthKey(currentTeam.nickname, currentTeam.fullname);
      return emailPhoneExist(callback, res);
    } else if (team.team_session_key == currentTeam.teamSessionKey) {
      currentTeam.teamSessionKey = Functions.createTeamSessionKey(currentTeam.nickname);
      return emailPhoneExist(callback, res);
    }

    sendResponse(-2, callback, res);
  });
}

function newTeam(callback, res) {
  var newTeam = new Teams({
    team_key : currentTeam.teamKey,
    team_session_key : currentTeam.teamSessionKey,
    team_auth_key : currentTeam.teamAuthKey,
    team_picture_url : "",
    fullname : currentTeam.fullname,
    name : currentTeam.name,
    email_phone : currentTeam.email_phone,
    nickname : currentTeam.nickname,
    password : currentTeam.hashedPassword,
    preferences : {
      lang : currentTeam.langAbbr,
    },
    created_at : new Date(),
    updated_at : new Date()
  });

  newTeam.save(function(error, result) {
    if (error) return callback(500, "error", res);

    if (currentTeam.email_phone.includes("@")) {
      sendRegistrationEmail(callback, res);
    } else {
      sendRegistrationSMS(callback, res);
    }
  });
}

function sendRegistrationEmail(callback, res) {
  var mailOptions = {
    from : "'SurveyStacksDevTeam' <no-reply@surveyrush.com>",
    to : currentTeam.email_phone,
    subject : currentTeam.lang.registationEmailSubject,
    html : currentTeam.lang.registrationEmailBody
  };

  transporter.sendMail(mailOptions);
  sendResponse(1, callback, res);
}

function sendRegistrationSMS(callback, res) {
  client.messages.create({
    to : currentTeam.email_phone,
    from : process.env.TWILIO_PHONE_NUMBER,
    body : currentTeam.lang.registrationSMSBody
  });
  sendResponse(1, callback, res);
}

function sendResponse(resultCode, callback, res) {
  var resultCode = resultCode;
  var resultArray = {};
  var resultDate = new Date();

  if (resultCode == 1) {
    resultArray = {"resultCode" : resultCode, "resultDate" : resultDate, "team_auth_data" : {
      "nn" : currentTeam.nickname,
      "vk" : currentTeam.teamKey,
      "sk" : currentTeam.teamSessionKey,
      "lk" : currentTeam.teamAuthKey
    }};

    var cookieNameAuth = "lk-team";
    var cookieNameSession = "sk-team";
    var cookieNameValidation = "vk-team";
    var cookieNameTeamNickname = "nn-team";
    var cookieNameDate = "dt-team";
    var cookieNameSetupComplete = "sd-team";
    var cookieNameDesktopNotifications = "dnotif";
    var cookieNameLang = "lang-team";
    var date = new Date();

    if (currentTeam.webApp) {
      res.cookie(cookieNameSession, currentTeam.teamSessionKey, { maxAge: (86400000 * 365.25), httpOnly: true, signed : true});
      res.cookie(cookieNameValidation, currentTeam.teamKey, { maxAge: (86400000 * 365.25), httpOnly: true, signed : true});
      res.cookie(cookieNameAuth, currentTeam.teamAuthKey, { maxAge: (86400000 * 365.25), httpOnly: true, signed : true});
      res.cookie(cookieNameTeamNickname, currentTeam.nickname, { maxAge: (86400000 * 365.25), httpOnly: false});
      res.cookie(cookieNameDate, date.toString(), { maxAge: (86400000 * 365.25), httpOnly: true, signed : true});
      res.cookie(cookieNameLang, currentTeam.langAbbr, { maxAge: (86400000 * 365.25), httpOnly: false});
      res.cookie(cookieNameSetupComplete, false, { maxAge: (86400000 * 365.25), httpOnly: true});
      res.cookie(cookieNameDesktopNotifications, false, { maxAge: (86400000 * 365.25), httpOnly: false})
    }
  } else {
    resultArray = {"resultCode" : resultCode, "resultDate" : resultDate, "auth_data" : {
      "nn" : "",
      "vk" : "",
      "sk" : "",
      "lk" : ""
    }};
  }

  callback(200, resultArray, res);
}
