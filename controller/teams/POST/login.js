"use strict";

var Teams = modelRequire("teams.js");
var FailedLogins = modelRequire("failedLogins.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Team() {
  this.ip = "";
  this.lang = "";
  this.langAbbr = "";
  this.teamSessionKey = "";
  this.teamKey = "";
  this.teamAuthKey = "";
  this.nickname = "";
  this.password = "";
  this.hashedPassword = "";
  this.setup_completed = false;
  this.webApp = false;
}

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, lang, nickname, password, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  lang = lang.trim();
  nickname = nickname.trim();
  password = password.trim();
  try {
    currentTeam.lang = rootRequire("/controller/" + lang.toLowerCase() + ".js");
  } catch(e) {
    currentTeam.lang = rootRequire("/controller/en.js");
  }
  currentTeam.langAbbr = lang;
  currentTeam.teamKey = Functions.createTeamKey(nickname);
  currentTeam.nickname = nickname.toLowerCase();
  currentTeam.password = password;
  currentTeam.hashedPassword = Functions.hashedPasswordCreator(password);
  currentTeam.webApp = webApp;

  verifyAccount(callback, res);
}

function verifyAccount(callback, res) {
  Teams.findOne({deleted : false, team_key : currentTeam.teamKey, nickname : currentTeam.nickname}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return sendResponse(-1, callback, res);

    if (currentTeam.hashedPassword != team.password) return insertFailedLogin(team, callback, res);

    if (team.login_attempts > 4) return sendResponse(-3, callback, res);

    team.login_attempts = 0;
    team.updated_at = new Date();
    currentTeam.teamKey = team.team_key;
    currentTeam.teamAuthKey = team.team_auth_key;
    currentTeam.teamSessionKey = team.team_session_key;
    currentTeam.setup_completed = (team.membership.type != 0);

    team.save(function(error) {
      if (error) return callback(500, "error", res);

      sendResponse(1, callback, res);
    });
  });
}

function insertFailedLogin(team, callback, res) {
  var newFailedLogin = new FailedLogins({
    key : currentTeam.teamKey,
    username : currentTeam.nickname,
    ip_address : currentTeam.ip,
    created_at : new Date(),
  });

  newFailedLogin.save(function(error) {
    if (error) return callback(500, "error", res);

    team.login_attempts += 1;
    team.save(function(error) {
      if (error) return callback(500, "error", res);

      sendResponse(-2, callback, res);
    });
  });
}


function sendResponse(resultCode, callback, res) {
  var resultCode = resultCode;
  var resultArray = {};
  var resultDate = new Date();

  if (resultCode == 1) {

      resultArray = {
        "webApp" : currentTeam.webApp,
        "resultCode" : resultCode,
        "resultDate" : resultDate,
        "team_auth_data" : {
        "nn" : currentTeam.nickname,
        "vk" : currentTeam.teamKey,
        "sk" : currentTeam.teamSessionKey,
        "lk" : currentTeam.teamAuthKey,
        "sd" : currentTeam.setup_completed
      }};

      var cookieNameAuth = "lk-team";
      var cookieNameSession = "sk-team";
      var cookieNameValidation = "vk-team";
      var cookieNameTeamNickname = "nn-team";
      var cookieNameDate = "dt-team";
      var cookieNameSetupComplete = "sd-team";
      var cookieNameDesktopNotifications = "dnotif";
      var cookieNameLang = "lang-team";
      var date = new Date();

      if (currentTeam.webApp) {
        res.cookie(cookieNameSession, currentTeam.teamSessionKey, { maxAge: (86400000 * 365.25), httpOnly: true, signed : true});  // secure : true, domain : 'http://www.surveystacks.com'
        res.cookie(cookieNameAuth, currentTeam.teamAuthKey, { maxAge: (86400000 * 365.25), httpOnly: true, signed : true});
        res.cookie(cookieNameValidation, currentTeam.teamKey, { maxAge: (86400000 * 365.25), httpOnly: true, signed : true});
        res.cookie(cookieNameTeamNickname, currentTeam.nickname, { maxAge: (86400000 * 365.25), httpOnly: false});
        res.cookie(cookieNameLang, currentTeam.langAbbr, { maxAge: (86400000 * 365.25), httpOnly: false});
        res.cookie(cookieNameDate, date.toString(), { maxAge: (86400000 * 365.25), httpOnly: true, signed : true});
        res.cookie(cookieNameSetupComplete, currentTeam.setup_completed, { maxAge: (86400000 * 365.25), httpOnly: false});
        res.cookie(cookieNameDesktopNotifications, false, { maxAge: (86400000 * 365.25), httpOnly: false});
      }

      callback(200, resultArray, res);
  } else {
    resultArray = {
      "webApp" : currentTeam.webApp,
      "resultCode" : resultCode, "resultDate" : resultDate, "team_auth_data" : {
      "nn" : "",
      "vk" : "",
      "sk" : "",
      "lk" : "",
      "sd" : ""
    }};

    callback(200, resultArray, res);
  }
}
