"use strict";

var Profiles = modelRequire("profiles.js");
var Functions = rootRequire("/controller/globalFunctions.js");
var ForgotCredentialKeys = modelRequire("forgotCredential.js");

function Team() {
  this.ip = "";
  this.lang = "";
  this.team_key = "";
  this.fullname = "";
  this.nickname = "";
  this.email_phone = "";
  this.second_left = "0";
}

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, lang, fullname, email_phone, nickname, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  try {
    currentTeam.lang = rootRequire("/controller/" + lang.toLowerCase() + ".js");
  } catch(e) {
    currentTeam.lang = rootRequire("/controller/en.js");
  }
  currentTeam.fullname = fullname;
  currentTeam.team_key = Functions.createTeamKey(nickname);
  currentTeam.nickname = nickname;
  if (email_phone.includes("@")) {
    currentTeam.email_phone = email_phone.toLowerCase();
  } else {
    currentTeam.email_phone = Functions.phoneNumberNormalize(email_phone);
  }

  verifyData(callback, res);
}

function verifyData(callback, res) {
  Profiles.findOne({deleted : false, team_key : currentTeam.team_key, fullname : currentTeam.fullname, nickname : currentTeam.nickname, email_phone : currentTeam.email_phone}, function(error, profile) {
    if (error) return callback(500, "error", res);
    if (profile == null || profile != "") return sendResponse(-1, callback, res);

    checkForSpam(callback, res);
  });
}

function checkForSpam(callback, res) {
  ForgotCredentialKeys.findOne({key : currentTeam.team_key, nickname : currentTeam.nickname, used : false, expired : false}, function(error, credentialKey) {
    if (error) return callback(500, "error", res);
    if (credentialKey == null || credentialKey == "") return insertCredentialKey(callback, res);

    var current_time = new Date();
    var created_time = new Date(credentialKey.created_at);
    var difference = Math.round((current_time - created_time) / 1000);

    if (difference >= 60) {
      credentialKey.expired = true;
      credentialKey.save(function(error, data) {
        if (error) {
        } else {
          currentTeam.second_left = "0";
          insertCredentialKey(callback, res);
        }
      });
    } else if (difference < 60) {
      currentTeam.second_left = 60 - difference;
      sendResponse(-2, callback, res);
    }
  }).sort({created_at : -1});
}

function insertCredentialKey(callback, res) {
  var CredentialKey = Functions.generateCredentialKey();

  var newForgotCredentialKey = new ForgotCredentialKeys({
    validation_key : CredentialKey,
    key : currentTeam.team_key,
    nickname : currentTeam.nickname,
    used : false,
    expired : false,
    created_at : new Date(),
    updated_at : new Date()
  });

  newForgotCredentialKey.save(function(error, result) {
    if (error) {
      callback("error", res);
    } else {
      if (currentTeam.email_phone.includes("@")) {
        sendEmail(CredentialKey, callback, res);
      } else {
        sendSMS(CredentialKey, callback, res);
      }
    }
  });
}

function sendEmail(CredentialKey, callback, res) {
  var mailOptions = {
    from : "'SurveyStacksDevTeam' <no-reply@surveyrush.com>",
    to : currentTeam.email_phone,
    subject : currentTeam.lang.forgotPasswordEmailSubject,
    html : currentTeam.lang.forgotPasswordEmailBody(currentTeam.nickname, CredentialKey)
  };

  transporter.sendMail(mailOptions);
  sendResponse(1, callback, res);
}

function sendSMS(CredentialKey, callback, res) {
  client.messages.create({
    to : currentTeam.email_phone,
    from : process.env.TWILIO_PHONE_NUMBER,
    body : currentTeam.lang.forgotPasswordSMSBody(currentTeam.nickname, CredentialKey)
  }, function(error) {
    if (error) return callback(500, "error", res);

    sendResponse(1, callback, res);
  });
}

function sendResponse(resultCode, callback, res) {
  var resultCode = resultCode;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {"resultCode" : resultCode, "resultDate" : resultDate, time_to_wait : currentTeam.second_left};

  callback(200, resultArray, res);
}
