"use strict";

var Teams = modelRequire("teams.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Team() {
  this.ip = "";
  this.validation_key = "";
  this.session_key = "";
  this.auth_key = "";
  this.membersInfo = {};
  this.webApp = false;
}

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, members, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.validation_key = validation_key;
  currentTeam.auth_key = auth_key;
  currentTeam.session_key = session_key;
  currentTeam.membersInfo = members;
  currentTeam.webApp = webApp;

  noDoubles(callback, res);
}

function noDoubles(callback, res) {
  Teams.findOne({team_key : {$ne : currentTeam.validation_key}, "member_options.code" : currentTeam.membersInfo.code}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return verifyData(callback, res);

    if (team.member_options.code == currentTeam.membersInfo.code) return sendResponse(-1, callback, res);
  });
}

function verifyData(callback, res) {
  Teams.findOne({team_key : currentTeam.validation_key, team_session_key : currentTeam.session_key, team_auth_key : currentTeam.auth_key}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return callback(401, "wrongMatch", res);

    team.member_options.code = currentTeam.membersInfo.code;
    team.member_options.open = currentTeam.membersInfo.open;
    team.member_options.public = currentTeam.membersInfo.public;

    if (team.member_options.public) {
      for (var i = 0; i < team.tbc.length; i++) {
        team.members.push(team.tbc[i]);
      }
      team.tbc = [];
    }
    team.updated_at = new Date();

    team.save(function(error) {
      if (error) return callback(500, "error", res);

      sendResponse(1, callback, res);
    });
  });
}

function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {"resultCode" : resultCode, "resultDate" : resultDate};

  callback(200, resultArray, res);
}
