"use strict";

var Teams = modelRequire("teams.js");
var Polls = modelRequire("polls.js");
var Constants = modelRequire("constants.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Team()  {
  this.ip = "";
  this.validation_token = "";
  this.auth_token = "";
  this.session_token = "";
  this.poll_token = "";
  this.poll_coins = 0;
  this.image_id = "";
  this.lang = "";
  this.image;
  this.collected = [];
  this.webApp = false;
  this.poll = {};
};

var currentTeam;

var services = {};

services.initialize = initialize;
module.exports = services;

function initialize(ip, validation_token, auth_token, session_token, lang, image, poll, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.validation_token = validation_token;
  currentTeam.auth_token = auth_token;
  currentTeam.session_token = session_token;
  currentTeam.lang = lang;
  currentTeam.webApp = webApp;
  currentTeam.image = image;
  currentTeam.poll = JSON.parse(poll);
  currentTeam.poll_token = Functions.createPollToken(validation_token, "1");
  var collected = [];
  for (var key in currentTeam.poll.details.data_collected) {
    if (currentTeam.poll.details.data_collected[key]) {
      collected.push(key);
    }
  }
  currentTeam.collected = collected;
  getCoins();

  verifyProfile(callback, res);
}

function getCoins() {
  Constants.find(function(error, constants) {
    if (error) {
      currentTeam.poll_coins = 5;
    } else {
      var constant = constants[0];
      currentTeam.poll_coins = constant.cpp;
    }
  }).limit(1);
}

function verifyProfile(callback, res) {
  Teams.findOne({deleted : false, team_key : currentTeam.validation_token, team_session_key : currentTeam.session_token, team_auth_key : currentTeam.auth_token, "membership.type" : 1}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return callback(401, "wrongMatch", res);

    if (currentTeam.image !== "") {
      // image we give
      uploadImageF(callback, res);
    } else if (currentTeam.poll.pollDetails.image_url != "") {
      uploadImage(callback, res);
    } else {
      uploadPoll(callback, res);
    }
  });
}

function uploadImageF(callback, res) {
  var dimensions = sizeOf(process.cwd()+"/"+currentTeam.image.path);
  var height = dimensions.height;
  var width = dimensions.width;
  if (dimensions.height > 1600 || dimensions.width > 1600) {
    height = dimensions.height / 2;
    width = dimensions.width / 2;
  }
  currentTeam.image_id = process.env.CLOUDINARY_POLLS_FOLDER + "/" + currentTeam.poll_token + "/poll_image";
  cloudinary.v2.uploader.upload(
    currentTeam.image.path,
    function(error, result) {
      if (typeof error != "undefined" && error != "undefined") return sendResponse(-1, callback, res);

      fs.unlink(process.cwd()+"/"+currentTeam.image.path, function(error) {

      });

      currentTeam.poll.pollDetails.image_url = result.secure_url;

      uploadPoll(callback, res);
    },
    {
      public_id: currentTeam.image_id,
      width: width,
      height: height,
      folder :  process.env.CLOUDINARY_POLLS_FOLDER,
      tags: ['team_'+currentTeam.validation_token, currentTeam.survey_token, "polls"]
    }
  );
}

function uploadImage(callback, res) {
  currentTeam.image_id = process.env.CLOUDINARY_POLLS_FOLDER + "/" + currentTeam.poll_token + "/poll_image";
  cloudinary.v2.uploader.upload(
    currentTeam.poll.pollDetails.image_url,
    {
      public_id: currentTeam.image_id,
      width: 800,
      height: 800,
      folder :  process.env.CLOUDINARY_POLLS_FOLDER,
      tags: ['team_'+currentTeam.validation_token, currentTeam.poll_token, "polls"]
    },
    function(error, result) {

      if (typeof error != "undefined" && error != "undefined") return sendResponse(-2, callback, res);

      currentTeam.poll.pollDetails.image_url = result.secure_url;
      uploadPoll(callback, res);
    }
  );
}

function uploadPoll(callback, res) {
  var userPoll = currentTeam.poll;
  var Poll = new Polls({
    poll_token : currentTeam.poll_token,
    creator_key : currentTeam.validation_token,
    poll_details : {
      public_poll : userPoll.pollDetails.public,
      lang : currentTeam.lang,
      coins : currentTeam.poll_coins,
      name : userPoll.pollDetails.name,
      description : userPoll.pollDetails.description,
      category_token : userPoll.pollDetails.category_token,
      target_sex : userPoll.pollDetails.sex,
      target_ethnicity : userPoll.pollDetails.ethnicity,
      target_age_group : userPoll.pollDetails.age_group,
      image_url : currentTeam.poll.pollDetails.image_url,
      answer_url : Functions.answerURL(currentTeam.poll_token, 1),
      lab_created : false,
      collected : currentTeam.collected
    },
    question_details : {
      question : userPoll.pollQuestion.question,
      options : userPoll.pollQuestion.options
    },
    created_at : new Date(),
    updated_at : new Date()
  });

  Poll.save(function(error) {
    if (error) return callback(500, "error", res);

    updateProfile(callback, res);
  });
}

function updateProfile(callback, res) {
  Teams.findOne({deleted : false, team_key : currentTeam.validation_token, team_session_key : currentTeam.session_token, team_auth_key : currentTeam.auth_token}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return callback(401, "no auth", res);

    team.polls_created.push(currentTeam.poll_token);
    team.updated_at = new Date();

    team.save(function(error) {
      if (error) return callback(500, "error", res);

      sendResponse(1, callback, res);
    });
  });
}

function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {}
  var resultDate = new Date();

  resultArray = {
    "webApp" : currentTeam.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate
  };

  callback(200, resultArray, res);
}
