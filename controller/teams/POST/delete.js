"use strict";

var Teams = modelRequire("teams.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Team() {
  this.ip = "";
  this.validation_key = "";
  this.session_key = "";
  this.auth_key = "";
  this.webApp = false;
}

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.validation_key = validation_key;
  currentTeam.auth_key = auth_key;
  currentTeam.session_key = session_key;
  currentTeam.webApp = webApp;

  verifyData(callback, res);
}

function verifyData(callback, res) {
  Teams.findOne({deleted : false, team_key : currentTeam.validation_key, team_session_key : currentTeam.session_key, team_auth_key : currentTeam.auth_key}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return callback(401, "wrongMatch", res);

    team.deleted = true;
    team.updated_at = new Date();

    team.save(function(error) {
      if (error) return callback(500, "error", res);

      res.clearCookie("sk-team");
      res.clearCookie("lk-team");
      res.clearCookie("vk-team");
      res.clearCookie("nn-team");
      res.clearCookie("dt-team");
      res.clearCookie("sd-team");
      res.clearCookie("dnotif");
      res.clearCookie("lang-team");
      res.clearCookie("_csrf");
      res.clearCookie("io");

      sendResponse(1, callback, res);
    });
  });
}

function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {"resultCode" : resultCode, "resultDate" : resultDate, "webApp" : currentTeam.webApp};

  callback(200, resultArray, res);

  global.io.emit(currentTeam.session_key, "es");
}
