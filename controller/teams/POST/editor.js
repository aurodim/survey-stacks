"use strict";

var Teams = modelRequire("teams.js");
var Polls = modelRequire("polls.js");
var Surveys = modelRequire("surveys.js");
var Constants = modelRequire("constants.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Team() {
  this.ip = "";
  this.validation_key = "";
  this.auth_key = "";
  this.session_key = "";
  this.webApp = false;
  this.token = "";
  this.type = "";
  this.ppa = "";
  this.edit = {
    delete : false,
    name : "",
    description : "",
    image_url : "",
    asm : 0,
    keys : 0
  };
}

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, type, token, edit, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.validation_key = validation_key;
  currentTeam.auth_key = auth_key;
  currentTeam.session_key = session_key;
  currentTeam.token = token;
  currentTeam.type = type;
  currentTeam.edit.delete = edit.delete;
  currentTeam.edit.name = edit.name;
  currentTeam.edit.description = edit.description;
  currentTeam.edit.image_url = edit.image_url;
  currentTeam.edit.asm = edit.asm;
  currentTeam.edit.keys = edit.keys;
  currentTeam.webApp = webApp;

  getConstant();
  verifyGatherData(callback, res);
}

function getConstant() {
  Constants.find(function(error, constants) {
    if (error) {
      currentTeam.ppa = 1.00;
    } else {
      var constant = constants[0];
      currentTeam.ppa = constant.ppa;
    }
  }).limit(1);
}

function verifyGatherData(callback, res) {
  Teams.findOne({deleted : false, "membership.type" : 1, team_key : currentTeam.validation_key, team_session_key : currentTeam.session_key, team_auth_key : currentTeam.auth_key}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return (401, "no auth", res);

    if (currentTeam.edit.delete) {
      if (currentTeam.type == "surveys") {
        let i = team.surveys_created.indexOf(currentTeam.token);
        team.surveys_created.splice(i, 1);
      } else if (currentTeam.type == "polls") {
        let i = team.polls_created.indexOf(currentTeam.token);
        team.polls_created.splice(i, 1);
      }

      team.updated_at = new Date();

      team.save(function(error) {
        if (error) return callback(500, "error", res);

        if (currentTeam.type == "surveys") {
          surveyEditG(callback, res);
        } else if (currentTeam.type == "polls") {
          pollEditG(callback, res);
        }
      });
    } else {
      if (currentTeam.edit.image_url == "") {
        if (currentTeam.type == "surveys") {
          surveyEditG(callback, res);
        } else if (currentTeam.type == "polls") {
          pollEditG(callback, res);
        }
      } else {
        uploadImage(callback, res);
      }
    }
  })
}

function uploadImage(callback, res) {
  var folder;
  var end;
  if (currentTeam.type == "surveys") {
    folder = process.env.CLOUDINARY_SURVEYS_FOLDER;
    end = "/survey_image";
  } else if (currentTeam.type == "polls") {
    folder = process.env.CLOUDINARY_POLLS_FOLDER;
    end = "/poll_image";
  }
  var image_id = folder + "/" + currentTeam.token + end;
  cloudinary.v2.uploader.upload(
    currentTeam.edit.image_url,
    {
      public_id: image_id,
      width: 800,
      height: 800,
      folder : folder,
      tags: ['team_'+currentTeam.validation_key, currentTeam.token, currentTeam.type]
    },
    function(error, result) {

      if (typeof error != "undefined" && error != "undefined") return sendResponse(-1, callback, res);

      currentTeam.edit.image_url = result.url;
      if (currentTeam.type == "surveys") {
        surveyEditG(callback, res);
      } else if (currentTeam.type == "polls") {
        pollEditG(callback, res);
      }
    }
  );
}

function surveyEditG(callback, res) {
  Surveys.find({deleted : false, creator_key : currentTeam.validation_key, survey_token : currentTeam.token}, function(error, surveys) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (surveys.length == "1") {
        var survey = surveys[0];

        if (currentTeam.edit.delete) {
          survey.deleted = true;
          survey.updated_at = new Date();

          survey.save(function(error) {
            if (error) return callback(500, "error", res);

            sendResponse(1, callback, res);
          });
        } else {
          survey.survey_details.name = currentTeam.edit.name;
          survey.survey_details.description = currentTeam.edit.description;
          survey.survey_details.image_url = currentTeam.edit.image_url;

          if (!survey.payment_details.free && currentTeam.edit.asm > 0 && currentTeam.payment_saved) {
            survey.stacks += (currentTeam.edit.asm * 10);
            survey.payment_details.survey_charge += (currentTeam.ppa * currentTeam.edit.asm);
            survey.payment_details.order_total += (currentTeam.ppa * currentTeam.edit.asm);
            // charge
          }

          if (survey.survey_details.lab_created && currentTeam.edit.keys > 0) {
            for (var i = 0; i < currentTeam.edit.keys; i++) {
              survey.labs.keys.push(Functions.createQAnswerKey());
            }
          }

          survey.updated_at = new Date();

          survey.save(function(error) {
            if (error) return callback(500, "error", res);

            sendResponse(1, callback, res);
          });
        }
      } else {
        return callback(500, "error", res);
      }
    }
  }).limit(1);
}

function pollEditG(callback, res) {
  Polls.find({deleted : false, creator_key : currentTeam.validation_key, poll_token : currentTeam.token}, function(error, polls) {
    if (error) {
      return callback(500, "error", res);
    } else {
      if (polls.length == "1") {
        var poll = polls[0];

        if (currentTeam.edit.delete) {
          poll.deleted = true;
          poll.updated_at = new Date();

          poll.save(function(error) {
            if (error) return callback(500, "error", res);

            sendResponse(1, callback, res);
          });
        } else {
          poll.poll_details.name = currentTeam.edit.name;
          poll.poll_details.description = currentTeam.edit.description;
          poll.poll_details.image_url = currentTeam.edit.image_url;

          poll.updated_at = new Date();

          poll.save(function(error) {
            if (error) return callback(500, "error", res);

            sendResponse(1, callback, res);
          });
        }
      } else {
        return callback(500, "error", res);
      }
    }
  }).limit(1);
}

function sendResponse(resultCode, callback, res) {
  var resultArray = {}

  var resultDate = new Date();

  resultArray = {
    "webApp" : currentTeam.webApp,
    "resultCode" : resultCode,
    "resultDate" : resultDate
  };

  callback(200, resultArray, res);
}
