"use strict";

var Teams = modelRequire("teams.js");
var Functions = rootRequire("/controller/globalFunctions.js");
var ForgotCredentialKeys = modelRequire("forgotCredential.js");

function Team() {
  this.ip = "";
  this.lang = "";
  this.team_key = "";
  this.nickname = "";
  this.code = "";
  this.new_password = "";
}

var currentTeam;
var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, lang, nickname, code, new_password, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  try {
    currentTeam.lang = rootRequire("/controller/" + lang.toLowerCase() + ".js");
  } catch(e) {
    currentTeam.lang = rootRequire("/controller/en.js");
  }
  currentTeam.team_key = Functions.createTeamKey(nickname);
  currentTeam.nickname = nickname;
  currentTeam.code = code;
  currentTeam.new_password = Functions.hashedPasswordCreator(new_password);

  verifyData(callback, res);
}

function verifyData(callback, res) {
  Teams.findOne({deleted : false, team_key : currentTeam.team_key, nickname : currentTeam.nickname}, function(error, profile) {
    if (error) return callback(500, "error", res);
    if (profile == null || profile == "") return sendResponse(-1, callback, res);

    ForgotCredentialKeys.findOne({validation_key : currentTeam.code, key : currentTeam.team_key, nickname : currentTeam.nickname, used : false, expired : false}, function(error, key) {
      if (error) return callback(500, "error", res);
      if (key == null || key == "") return sendResponse(-2, callback, res);

      profile.password = currentTeam.new_password;
      profile.updated_at = new Date();

      key.used = true;
      key.expired = true;
      key.updated_at = new Date();

      key.save(function(error) {
        if (error) return callback(500, "error", res);

        profile.save(function(error) {
          if (error) return callback(500, "error", res);

          sendResponse(1, callback, res);
        });
      });

    }).sort({created_at : -1});
  });
}

function sendResponse(resultCode, callback, res) {
  var resultCode = resultCode;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {"resultCode" : resultCode, "resultDate" : resultDate};

  callback(200, resultArray, res);
}
