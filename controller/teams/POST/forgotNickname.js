"use strict";

var Teams = modelRequire("teams.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Team() {
  this.ip = "";
  this.lang = "";
  this.nickname = "";
  this.email_phone = "";
  this.name = "";
}

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, lang, email_phone, name, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  try {
    currentTeam.lang = rootRequire("/controller/" + lang.toLowerCase() + ".js");
  } catch(e) {
    currentTeam.lang = rootRequire("/controller/en.js");
  }
  currentTeam.nickname = "";
  if (email_phone.includes("@")) {
    currentTeam.email_phone = email_phone.toLowerCase();
  } else {
    currentTeam.email_phone = Functions.phoneNumberNormalize(email_phone);
  }
  currentTeam.name = name;

  checkSend(callback, res);
}

function checkSend(callback, res) {
  Teams.findOne({email_phone : currentTeam.email_phone, name : currentTeam.name}, function(error, profile) {
    if (error) return callback(500, "error", res);
    if (profile == null || profile == "") return sendResponse(-1, callback, res);

    currentTeam.nickname = profile.nickname;
    sendResponse(1, callback, res);
  });
}

function sendResponse(resultCode, callback, res) {
  var resultCode = resultCode;
  var resultArray = {};

  var resultDate = new Date();

  resultArray = {"resultCode" : resultCode, "resultDate" : resultDate, "nickname" : currentTeam.nickname};

  callback(200, resultArray, res);
}
