"use strict";

var Teams = modelRequire("teams.js");
var Functions = rootRequire("/controller/globalFunctions.js");

function Team() {
  this.ip = "";
  this.validation_key = "";
  this.session_key = "";
  this.auth_key = "";
  this.token = "";
  this.webApp = false;
}

var currentTeam;

var service = {};

service.initialize = initialize;

module.exports = service;

function initialize(ip, validation_key, auth_key, session_key, token, webApp, callback, res) {
  currentTeam = new Team();
  currentTeam.ip = ip;
  currentTeam.validation_key = validation_key;
  currentTeam.auth_key = auth_key;
  currentTeam.session_key = session_key;
  currentTeam.token = token;
  currentTeam.webApp = webApp;

  verifyData(callback, res);
}

function verifyData(callback, res) {
  Teams.findOne({team_key : currentTeam.validation_key, team_session_key : currentTeam.session_key, team_auth_key : currentTeam.auth_key}, function(error, team) {
    if (error) return callback(500, "error", res);
    if (team == null || team == "") return callback(401, "wrongMatch", res);

    if (team.awaiting.indexOf(currentTeam.token) > -1) {
      var i = team.awaiting.indexOf(currentTeam.token);
      team.awaiting.splice(i, 1);
    } else {
      return sendResponse(1, callback, res);
    }
    team.updated_at = new Date();

    team.save(function(error) {
      if (error) return callback(500, "error", res);

      sendResponse(1, callback, res);
    });
  });
}

function sendResponse(code, callback, res) {
  var resultCode = code;
  var resultArray = {};
  var resultDate = new Date();

  resultArray = {"resultCode" : resultCode, "resultDate" : resultDate};

  callback(200, resultArray, res);
}
