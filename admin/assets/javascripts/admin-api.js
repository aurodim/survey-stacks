app.filter('capitalizeFirst', function() {
    return function(text) {
      return (!!text) ? text.charAt(0).toUpperCase() + text.substr(1).toLowerCase() : '';
    }
});

app.service("adminAPI", function($http, $window) {
	var self = this;
	self.name = "";
	self.restriction = "";
	self.expiration = 0;
	self.csrf = "";
	self.gather = function() {
		$http({
			method : "GET",
			url : "/api/5757c384c37f705ef28f68ed0e5932830c0a3393/0b3c02e5",
			headers : {'Content-Type': "application/json"},
			params : {
				reference_code : "FridayLateNight",
				key : "996652"
			}
		}).then(function success(res) {
			var response = res.data;
			self.name = response.data.name;
			self.restriction = response.data.restriction;
			self.csrf = response.data.csrf;
			self.expiration = response.data.expiration;
		}, function(err) {
			$window.location.reload();
		});
	};
	self.gather();
});

app.service('socketAPI', function ($cookies, $timeout, $http) {
  var self = this;
  self.socket = io();
  self.traffic = 0;
  self.socket.on("056101ea.2a56cbf3d54a751094f419540928136459546babbf1c07c43e6eca66b21ddd6dd0a370edc93eeebaec73dc7064ac5845", function(todo) {
    if (todo.type == "at") {
      self.traffic = todo.value;
      charts[0].config.data.datasets[0].data.push(self.traffic);
      charts[0].config.data.labels.push(new Date().toLocaleTimeString());
      charts[0].update();
    } else if (todo.type == "rt") {
      self.traffic = todo.value;
      charts[0].config.data.datasets[0].data.push(self.traffic);
      charts[0].config.data.labels.push(new Date().toLocaleTimeString());
      charts[0].update();
    }
  });
});

app.service("homeAPI", function($http) {
  var self = this;
  self.updatesNews = [{
		heading : "Update for SS 1.45.55",
		body : "<span>This was the third update for SurveyStacks. These are some of the new features</span><br /><ul><li>Fixed the bug where prices were not adjusted</li></ul>",
    date : new Date().getTime(),
    expanded : false
	}, {
		heading : "Admins page update 11.2.3",
		body : "Admins page update 11.2.3",
    date : new Date().getTime(),
    expanded : false
	}, {
		heading : "Added new admins +34",
		body : "Added new admins +34",
    date : new Date().getTime(),
    expanded : false
	}];
});

app.service("adminsAPI", function($http) {
	var self = this;
	self.admins = [];
	self.new = {
		fullname : "",
		email : "",
		phone_number : "",
		password : ""
	};
	self.addAdmin = function() {
		$http({
			method : "POST",
			url : "/api/b793d93c5acf46a759583d7db31b4affe3073c3f/admins/create",
			headers : {'Content-Type': "application/json"},
			data : {
				AUTH_TOKEN : "beade2400869fabe8986aa701d23d96cea74c62470a71a",
				name : self.new.fullname,
				email : self.new.email,
				phone_number : self.new.phone_number,
				password : self.new.password
			}
		}).then(function success(res) {
			var response = res.data;
			self.admins.unshift(response.new_admin);
			self.new = {
				fullname : "",
				email : "",
				phone_number : "",
				password : ""
			};
		}, function(err) {
			self.new = {
				fullname : "",
				email : "",
				phone_number : "",
				password : ""
			};
			alert("ERROR");
		});
	};
	self.gather = function() {
		$http({
			method : "GET",
			url : "/api/ce26827e838bfa07ba61a7d5480d9b3c005d7d000885362d139608eeabedf8a1/admins/gather",
			headers : {'Content-Type': "application/json"},
			params : {
				reference_code : "SaturdayMidMidnight",
				key : "997623"
			}
		}).then(function success(res) {
			var response = res.data;
			self.admins = response.admins;
		}, function(err) {
			alert("ERROR");
		});
	};
	self.promote = function($index) {
		$http({
			method : "POST",
			url : "/api/ce26827e838bfa07ba61a7d5480d9b3c005d7d000885362d139608eeabedf8a1/admins/promote",
			headers : {'Content-Type': "application/json"},
			data : {
				AUTH_TOKEN : "ea0c5ee7255ad0e9d6c90ecef57cdc46815c7eed",
				name : self.admins[$index].name,
				token : self.admins[$index].key
			}
		}).then(function success(res) {
			var response = res.data;
			self.admins[$index].status = "senior";
		}, function(err) {
			alert("ERROR");
		});
	};
	self.delete = function($index) {
		$http({
			method : "POST",
			url : "/api/ce26827e838bfa07ba61a7d5480d9b3c005d7d000885362d139608eeabedf8a1/admins/delete",
			headers : {'Content-Type': "application/json"},
			data : {
				AUTH_TOKEN : "fe322a94e43e7f0d6bed69bb9ef652a9a4068a32",
				name : self.admins[$index].name,
				token : self.admins[$index].key
			}
		}).then(function success(res) {
			var response = res.data;
			self.admins.splice($index, 1);
		}, function(err) {
			alert("ERROR");
		});
	};
});

app.service("categoriesAPI", function($http) {
	var self = this;
	self.valneed = [];
	self.categories = [];
	self.toAdd = "";
	self.gather = function() {
		$http({
			method : "GET",
			url : "/api/fc8a073f6d80f71db6bbfcc1030728f66c14270a9a76a467d7589f7db5c496bf/categories/gather",
			headers : {'Content-Type': "application/json"},
			params : {
				reference_code : "SaturdayEarlyMidnight",
				key : "998787"
			}
		}).then(function success(res) {
			var response = res.data;
			self.valneed = response.toval;
			self.categories = response.categories;
		}, function(err) {
			alert("ERROR");
		});
	};
	self.addCategory = function($index) {
		$http({
			method : "POST",
			url : "/api/fc8a073f6d80f71db6bbfcc1030728f66c14270a9a76a467d7589f7db5c496bf/categories/create",
			headers : {'Content-Type': "application/json"},
			data : {
				AUTH_TOKEN : "38ad924ff50a62c30a6e5c8815a61375775419f4",
				names : self.toAdd
			}
		}).then(function success(res) {
			var response = res.data;
			if (response.resultCode == "1") {
				for (var i = 0; i < response.catd.length; i++) {
					self.valneed.unshift(response.catd[i]);
				}
				self.toAdd = "";
			} else {
				alert("ERROR CODE: "+response.error_code+"\nMESSAGE: "+response.error_message);
			}
		}, function(err) {
			alert("ERROR");
		});
	};
	self.approveCategory = function($index) {
		$http({
			method : "POST",
			url : "/api/fc8a073f6d80f71db6bbfcc1030728f66c14270a9a76a467d7589f7db5c496bf/categories/approve",
			headers : {'Content-Type': "application/json"},
			data : {
				AUTH_TOKEN : "6e66da3ad5ba1ebeb81bc36b80c020a873a12ffb",
				catkey : self.valneed[$index].token
			}
		}).then(function success(res) {
			var response = res.data;
			self.valneed.splice($index, 1);
			self.categories.unshift(response.catd);
		}, function(err) {
			alert("ERROR");
		});
	};
	self.renameCategory = function($index, $scope) {
		if (self.categories[$index].text == "") return;
		$http({
			method : "POST",
			url : "/api/fc8a073f6d80f71db6bbfcc1030728f66c14270a9a76a467d7589f7db5c496bf/categories/rename",
			headers : {'Content-Type': "application/json"},
			data : {
				AUTH_TOKEN : "a883d369309b661dc69153b2e5877d6e447235a8",
				catkey : self.categories[$index].token,
				rename : self.categories[$index].text
			}
		}).then(function success(res) {
			var response = res.data;
			$scope.catrenameForm.$setPristine();
		}, function(err) {
			alert("ERROR");
		});
	};
	self.disapproveCategory = function($index) {
		$http({
			method : "POST",
			url : "/api/fc8a073f6d80f71db6bbfcc1030728f66c14270a9a76a467d7589f7db5c496bf/categories/delete",
			headers : {'Content-Type': "application/json"},
			data : {
				AUTH_TOKEN : "1cc90394f99c3579cab2d4ce35f6de6d53039942",
				catkey : self.valneed[$index].token
			}
		}).then(function success(res) {
			var response = res.data;
			self.valneed.splice($index, 1);
		}, function(err) {
			alert("ERROR");
		});
	};
});

app.service("usersAPI", function($http) {
  var self = this;
  self.users = [];
  self.teams = [];
  self.gatherUsers = function(keyword) {
    $http({
      method : "GET",
      url : "/api/49c266baaaa70981ea188fa714d5c40cf13830d786a861c9943ae0d26a7f3fe9/gather/users",
      headers : {'Content-Type': "application/json"},
      params : {
        reference_code : "SaturdayFourPastMorning",
        key : "997763",
        keyword : keyword
      }
    }).then(function success(res) {
      var response = res.data;
      self.users = response.users;
    }, function(err) {
      alert("ERROR");
    });
  };
  self.gatherTeams = function(keyword) {
    $http({
      method : "GET",
      url : "/api/49c266baaaa70981ea188fa714d5c40cf13830d786a861c9943ae0d26a7f3fe9/gather/teams",
      headers : {'Content-Type': "application/json"},
      params : {
        reference_code : "SaturdayFourMorning",
        key : "998712",
        keyword : keyword
      }
    }).then(function success(res) {
      var response = res.data;
      self.teams = response.teams;
    }, function(err) {
      alert("ERROR");
    });
  };
});
