var app = angular.module('survey-stacks-admin', ['ngResource', 'ngRoute', 'ngCookies', 'ngAnimate', 'ngSanitize']);

app.config(function($routeProvider, $locationProvider) {
	$locationProvider.hashPrefix('');
	$routeProvider
		.when('/', {
			templateUrl : '/admin/views/home.html',
			controller  : 'HomeController'
		})

		.when('/home', {
			templateUrl : '/admin/views/home.html',
			controller  : 'HomeController'
		})

		.when('/admins', {
			templateUrl : '/admin/views/admins.html',
			controller  : 'AdminsController'
		})

		.when('/user-traffic', {
			templateUrl : '/admin/views/user_traffic.html',
			controller  : 'UserTrafficController'
		})

		.when('/reports', {
			templateUrl : '/admin/views/reports.html',
			controller  : 'ReportsController'
		})

		.when('/income', {
			templateUrl : '/admin/views/income.html',
			controller  : 'IncomeController'
		})

		.when('/categories', {
			templateUrl : '/admin/views/categories.html',
			controller  : 'CategoriesController'
		})

		.when('/users', {
			templateUrl : '/admin/views/users.html',
			controller  : 'UsersController'
		})

		.when('/companies', {
			templateUrl : '/admin/views/companies.html',
			controller  : 'CompaniesController'
		})

		.when('/settings', {
			templateUrl : '/admin/views/settings.html',
			controller  : 'SettingsController'
		})

		.when('/logout', {
			templateUrl : '/admin/views/settings.html',
			controller  : 'SettingsController'
		})

		.when('/logout', {
			 controller: '',
			 templateUrl: '',
			 resolve: {
				 init: function() {
					 window.location = "/4a8c9c790110e4/admins/logout";
				 }
			 }
		});

		$locationProvider.html5Mode({
			enabled: true,
			requiredBase: true,
			rewriteLinks:true
		});
});

app.controller('AdminController', function($scope, $http, $cookies, $interval, $window, adminAPI) {
	$scope.admin = adminAPI;
	$scope.tl = [11, 59, 59];
	$interval(function() {
		var s = $scope.admin.expiration % 60;
		var m = (($scope.admin.expiration - s) / 60) % 60;
		var h = Math.floor(((($scope.admin.expiration - s) - (m * 60)) / 60) / 60);
		if (h == 0 && m == 0 && s == 0) {
			$window.location.reload();
			return;
		}
		var a = [h, m, s];
		$scope.tl = a;
		$scope.admin.expiration -= 1;
	}, 1000);
});

app.controller('ToolbarController', function($scope, $http, adminAPI) {
	$scope.admin = adminAPI;
	$scope.route = "";
	$scope.$on('$routeChangeStart', function($event, next, current) {
    $scope.route = next["$$route"].originalPath.split("/")[next["$$route"].originalPath.split("/").length - 1];
    if ($scope.route == "") {
    	$scope.route = "home";
    }
		DestroyCharts();
	});
});

app.controller('HomeController', function($scope, $http, adminAPI, homeAPI) {
	$scope.admin = adminAPI;
	$scope.api = homeAPI;
});

app.controller('AdminsController', function($scope, $http, $interval, adminAPI, adminsAPI) {
	$scope.admin = adminAPI;
	$scope.api = adminsAPI;
	$scope.api.gather();
	$scope.addNew = false;
	var pressurehold;
	$scope.promote = function(index) {
		if ($scope.api.admins[index].status == "senior") return;
		pressurehold = $interval(function() {
			if ($scope.api.admins[index].heldapp == 100) {
				$scope.api.promote(index);
				$scope.stopD(index);
				return;
			};
			$scope.api.admins[index].heldapp += 25;
		}, 500);
	};
	$scope.delete = function(index) {
		if ($scope.api.admins[index].status == "senior") return;

		pressurehold = $interval(function() {
			if ($scope.api.admins[index].held == 100) {
				$scope.api.delete(index);
				$scope.stopD(index);
				return;
			};
			$scope.api.admins[index].held += 25;
		}, 500);
	};
	$scope.stopD = function(index) {
		$scope.api.admins[index].held = 0;
		$scope.api.admins[index].heldapp = 0;
		$interval.cancel(pressurehold);
		pressurehold = null;
	};
});

app.controller('UserTrafficController', function($scope, $http, socketAPI) {
	$scope.now = [new Date().toLocaleTimeString(), new Date().toLocaleTimeString(), new Date().toLocaleTimeString()];
	$scope.update = function(w) {
		$scope.now[w] = new Date().toLocaleTimeString();
	}
	LineChart("lineGraphTLI", "Estimated Traffic", [], []);

	LineChart("lineGraphQC", "Questionnaires Created", ["1:16", "1:20", "1:25", "1:26"], [20, 10, 4, 5]);
	LineChart("lineGraphNC", "Notifications", ["1:16", "1:20", "1:25", "1:26"], [20, 10, 4, 5]);
	LineChart("lineGraphQA", "Questionnaires Answered", ["1:16", "1:20", "1:25", "1:26"], [20, 10, 4, 5]);

	LineChart("lineGraphPC", "Profiles Created", ["1:16", "1:20", "1:25", "1:26"], [20, 10, 4, 5]);
	LineChart("lineGraphRC", "Rewards Claimed", ["1:16", "1:20", "1:25", "1:26"], [20, 10, 4, 5]);
	LineChart("lineGraphPU", "Profile Updates", ["1:16", "1:20", "1:25", "1:26"], [20, 10, 4, 5]);

	LineChart("lineGraphTC", "Teams Created", ["1:16", "1:20", "1:25", "1:26"], [20, 10, 4, 5]);
	LineChart("lineGraphMA", "Members Added", ["1:16", "1:20", "1:25", "1:26"], [20, 10, 4, 5]);
	LineChart("lineGraphTU", "Teams Updates", ["1:16", "1:20", "1:25", "1:26"], [20, 10, 4, 5]);
});

app.controller('ReportsController', function($scope, $http) {
	$scope.recent = [
		{
			reporter : "isaac.meedinaa",
			reported : "abiel_xox",
			reason : "Drug Sale",
			more : "He posted his email and said 'I Sell Drugs'",
			expand : false
		}
	];
	$scope.important = [
		{
			reporter : "isaac.meedinaa",
			reported : "abiel_xox",
			reason : "Hateful Comment",
			more : "His survey said 'Niggers Deserve to Die'",
			expand : false
		}
	];
});

app.controller('IncomeController', function($scope, $http) {
	$scope.EST_REV = "40,000";
	$scope.EST_LOSS = "10,000";

	BarGraph("income", "Surveys Cost", ["2", "4", "6", "10"], [30, 400, 200, 100]);
	BarGraph("mem", "Membership Cost", ["50", "75", "100", "30"], [30, 400, 200, 100]);
	BarGraph("reward", "Reward Claims", ["10", "20", "40", "50"], [30, 30, 10, 5]);
});

app.controller('CategoriesController', function($scope, $http, $interval, adminAPI, categoriesAPI) {
	$scope.admin = adminAPI;
	$scope.api = categoriesAPI;
	$scope.api.gather();
	$scope.approve = function(index) {
		$scope.api.approveCategory(index);
	};
	var pressurehold;
	$scope.delete = function(index) {
		pressurehold = $interval(function() {
			if ($scope.api.valneed[index].held == 100) {
				$scope.stopD(index);
				$scope.api.disapproveCategory(index);
				return;
			};
			$scope.api.valneed[index].held += 25;
		}, 500);
	};
	$scope.stopD = function(index) {
		$scope.api.valneed[index].held = 0;
		$interval.cancel(pressurehold);
		pressurehold = null;
	};
	$scope.addNew = false;
	$scope.addToCats = function($index) {
		if ($scope.api.toAdd == "") return;
		$scope.api.addCategory($index);
	};
});

app.controller('UsersController', function($scope, $http, adminAPI, usersAPI) {
	$scope.admin = adminAPI;
	$scope.api = usersAPI;
	$scope.keywordU = "";
	$scope.keywordT = "";
	$scope.searchUser = function() {
		if ($scope.keywordU == "") {
			$scope.api.users = [];
			return;
		}
		$scope.api.gatherUsers($scope.keywordU);
	}
	$scope.searchTeam = function() {
		if ($scope.keywordT == "") {
			$scope.api.teams = [];
			return;
		}
		$scope.api.gatherTeams($scope.keywordT);
	}
});

app.controller('CompaniesController', function($scope, $http, $interval, adminAPI) {

	$http({
		method : "GET",
		url : "/admin/companies",
		headers : {'Content-Type': "application/json"},
		data : {
			AUTH_TOKEN : "99014c5222f570f2a541fe4f9fa9abbbeaa5b39e",
			webApp : true

		}
	}).then(function success(res) {
		var response = res.data;
		if (response.resultCode == "1") {

		}
	}, function(err) {

	});

$scope.admin = adminAPI;
	$scope.valneed = [
		{
		 	value : "Tech",
		 	text : "Tech",
		 	validated : false,
			held : 0,
			clicks : 0
	 	}
	];
	$scope.companiesData = [{
		value : "Music",
		text : "Music",
		validated : true
	}, {
		value : "Sports",
		text : "Sports",
		validated : true
	}, {
		value : "Tech",
		text : "Tech",
		validated : false
	}];
	$scope.approve = function(index) {
		$scope.valneed[index].clicks += 1;
		if ($scope.valneed[index].clicks == 1) {
			alert("APPROVED");
		}
	};
	var pressurehold;
	$scope.delete = function(index) {
		pressurehold = $interval(function() {
			if ($scope.valneed[index].held == 100) {
				alert("DELETED");
				$scope.stopD(index);
				return;
			};
			$scope.valneed[index].held += 25;
		}, 500);
	};
	$scope.stopD = function(index) {
		$scope.valneed[index].held = 0;
		$interval.cancel(pressurehold);
		pressurehold = null;
	};
	$scope.toAdd = [];
	$scope.addNew = function() {
		$scope.toAdd.push("");	};
	$scope.addToCats = function($index) {
		if ($scope.toAdd[$index] == "") {
			$scope.toAdd.splice($index, 1);
			return;
		};
		$scope.companiesData.unshift({
			value : $scope.toAdd[$index].toLowerCase().replace(/ /g, "_"),
			text : $scope.toAdd[$index],
			validated : false
		});
		$scope.toAdd.splice($index, 1);
	};
});

app.controller('SettingsController', function($scope, $http) {

});
